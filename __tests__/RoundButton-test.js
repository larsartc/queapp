import React from 'react'
import renderer from 'react-test-renderer'
import RoundButton from '../components/RoundButton'
import 'jest-styled-components'

test('render correctly', ()=>{
  const obj = renderer.create(<RoundButton />).toJSON()
  expect(obj).toMatchSnapshot()
})