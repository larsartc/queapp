import React from 'react'
import renderer from 'react-test-renderer'
import TimeRoundPicker from '../components/TimeRoundPicker'
import 'jest-styled-components'

test('render correctly', ()=>{
  const MOCK = {
    data: {time:"11:00"}
  }
  const obj = renderer.create(<TimeRoundPicker {...MOCK} />).toJSON()
  expect(obj).toMatchSnapshot()
})