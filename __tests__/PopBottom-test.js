import React from 'react'
import renderer from 'react-test-renderer'
import PopBottom from '../components/PopBottom'
import 'jest-styled-components'

describe('PopBottom tests', () => {

  // beforeEach(() => {
  //   jest.setTimeout(10000)
  // });

  test('render correctly', () =>{
    const obj = renderer.create(<PopBottom />).toJSON()
    expect(obj).toMatchSnapshot()
  })

});