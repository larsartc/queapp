import React from 'react'
import renderer from 'react-test-renderer'
import DefaultConfigAptt from '../components/Shop/components/DefaultConfigAptt'
import 'jest-styled-components'

test('render correctly', ()=>{
  const obj = renderer.create(<DefaultConfigAptt />).toJSON()
  expect(obj).toMatchSnapshot()
})