import React from 'react'
import renderer from 'react-test-renderer'
import PinCounters from '../components/PinCounters'
import 'jest-styled-components'

test('render correctly', ()=>{
  const obj = renderer.create(<PinCounters />).toJSON()
  expect(obj).toMatchSnapshot()
})