import React from 'react'
import renderer from 'react-test-renderer'
import DetailsPopAppt from '../components/Shop/components/DetailsPopAppt'
import 'jest-styled-components'

test('render correctly', ()=>{
  const obj = renderer.create(<DetailsPopAppt />).toJSON()
  expect(obj).toMatchSnapshot()
})