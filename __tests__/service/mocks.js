const generateRandomNumber = n => {
    return Math.floor(Math.random() * (9 * Math.pow(10, n - 1))) + Math.pow(10, n - 1);
}


export const DEFAULT_FEEDBACK = Object.freeze({
    text: "just a text"
})

export const USER_MOCK = Object.freeze({
    email: "barber@quedb.com",
    password: "12345678"
})

export const RATE_MOCK = Object.freeze({
    entity_id: "5d97ce4989cb8446d69c86c8",
	entity_type: "barber",
	value: Math.floor(Math.random() * 6) + 1,
	category: "Fade"
})

// This is the minimum information to a user become a barber
export const BARBER_MOCK = Object.freeze({
    photos: ["https://picsum.photos/1000", "https://picsum.photos/1000", "https://picsum.photos/1000"],
    facebook: "https://www.facebook.com/peexell/",
    instagram: "https://www.instagram.com/wearepeexell/",
    pinterest: "https://www.instagram.com/wearepeexell/",
    whatsapp: "11998903371",
    twitter: "https://www.instagram.com/wearepeexell/",
    mid_rate: 0
})


// ALL NEXT REQUEST MUST BE DONE WITH A BARBER LOGGED IN 
// Barber making an appointment for a customer
export const BARBER_APPOINTMENT = Object.freeze({
    shop: "5db20394abe0c82d535b9b6f",
    user_name: "jeff",
    total_price: 4,
    date: "1570560693",
    services: [{
        name: "haircut",
        category: "AGE 18",
        price: 2
    }, {
        name: "haircut",
        category: "AGE 18",
        price: 2
    }]
})

export const SHOP_MOCK = Object.freeze({
    name: "Test from JEST " + generateRandomNumber(10).toString(),
    photos: ["https://picsum.photos/1000", "https://picsum.photos/1000", "https://picsum.photos/1000", "https://picsum.photos/1000", "https://picsum.photos/1000"],
    services: [{
        name: "Haircut",
        category: "AGE",
        price: 10
    }, {
        name: "Shaving",
        category: "AGE 18",
        price: 20
    }],
    open_time: [
        {
            week_day: 1,
            start_time: "11:30am",
            end_time: "18:00pm"
        },
        {
            week_day: 2,
            start_time: "15:30pm",
            end_time: "18:00pm"
        },
        {
            week_day: 3,
            start_time: "14:30pm",
            end_time: "18:00pm"
        }
    ],
    latitude: "-22.976247",
    longitude: "-46.545294",
    opened_from: "12/07/2016"
});