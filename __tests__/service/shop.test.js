import * as userService from "../../services/userService"
import * as shopService from "../../services/shopService"
import * as barberService from "../../services/barberService"
import * as mock from "./mocks"
import * as response from "./responses"

describe('Shop action', () => {
    let shops = [];
    let barbers = [];
    beforeAll(async () => {
        await userService.login(mock.USER_MOCK);

        shops = await shopService.listByUser();
        barbers = await barberService.list();
    })

    it('Should create a new shop', async () => {
        const shop = await shopService.create(mock.SHOP_MOCK);
        expect(shop.data).toMatchObject(response.shop_created);
    });

    it('Should return all shops that logged user is OWNER', async () => {
        const user_shops = await shopService.listByUser();
        expect(user_shops.data).toEqual(expect.any(Array));
    });

    it('Should add a barber into shop', async () => {
        const shop = await shopService.includeWorker({ shop: shops.data[getPosition(shops.data)]._id, barber: barbers.data[getPosition(barbers.data)]._id });
        expect(shop.data).toMatchObject(response.shop_add_barber)
    });
});

export const getPosition = (array) => {
    return Math.floor(array.length * Math.random())
}