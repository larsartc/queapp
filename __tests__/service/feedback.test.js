
import * as feedbackService from "../../services/feedbackService"
import * as userService from "../../services/userService"

import * as mock from "./mocks"
import * as response from "./responses"

describe('feedback action', () => {

    beforeAll(async() => {
        await userService.login(mock.USER_MOCK);
    });

    it('Should do the systemfeedback', async () => {
        const feedback = await feedbackService.systemFeedback(mock.DEFAULT_FEEDBACK);
        expect(feedback.data).toMatchObject({...response.systemfeedback });
    });
});