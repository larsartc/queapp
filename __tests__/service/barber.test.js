
import * as barberService from "../../services/barberService"
import * as shopService from "../../services/shopService"
import * as userService from "../../services/userService"
import * as cutService from "../../services/cutService"
import * as mock from "./mocks"
import * as response from "./responses"

describe('Barber action', () => {
    let hair_style = "";
    let beard_style = "";
    let shop = "";
    
    beforeAll(async() => {
        await userService.login(mock.USER_MOCK);
        const listCuts = await cutService.list();
        const shops = await shopService.list({ latitude: -22.975972, longitude: -46.545655 })

        
        // Just pick some data to work with real example
        hair_style = listCuts.data.find(cut => cut.type === 'haircut')._id;
        beard_style = listCuts.data.find(cut => cut.type === 'shaving')._id;
        shop = shops.data[0]._id;
    });


    it('Should turn the logged user into a barber', async () => {
        const user_barber = await barberService.create(mock.BARBER_MOCK);
        expect(user_barber.data).toMatchObject({ barber: response.barber });
        response.barber["_id"] = user_barber.data._id;
    });

    it('Should ask to work in a Shop.', async () => {
        const work_request = await barberService.workRequest({ shop });
        expect(work_request.data).toMatchObject({ ...response.work_request_response });
    });

    it('Should create a appointment for a customer', async () => {
        appointment = { ...mock.BARBER_APPOINTMENT, hair_style, beard_style }
        const new_appointment = await barberService.createAppointment(appointment);
        expect(new_appointment.data).toMatchObject({ ...appointment});
    });
});