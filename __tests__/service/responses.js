import * as mock from './mocks'

export const systemfeedback = {
    user: expect.any(String),
    text: expect.any(String),
    _id: expect.any(String),
    createdAt: expect.any(String),
    updatedAt: expect.any(String),
    __v: expect.any(Number),
}


export const user = {
    player_id: expect.any(String),
    type: expect.any(Number),
    photo: expect.any(String),
    _id: expect.any(String),
    createdAt: expect.any(String),
    email: expect.any(String),
    name: expect.any(String)
}

export const rating = {
    entity_id: expect.any(String),
    entity_type: expect.any(String),
    value: expect.any(Number),
    category: expect.any(String)
}

// This is the response for become a BARBER
export const barber = {
    ...mock.BARBER_MOCK,
    num_raters: expect.any(Number),
    total_rating: expect.any(Number),
    carrer: [],
    work_time: [],
    past_cuts: [],
}

// Work Request pending
export const work_request_response = {
    ...mock.BARBER_WORK_REQUEST,
    status: "pending",
}

export const shop_created = {
    ...mock.SHOP_MOCK,
    opened_from: expect.any(String),
    ratings: []
}

export const shop_add_barber = {
    status: "accepted",
    requested_at: expect.any(Number),
    _id: expect.any(String),
    shop: expect.any(String),
    barber: expect.any(String),
    createdAt: expect.any(String),
    updatedAt: expect.any(String),
    __v: expect.any(Number)
}