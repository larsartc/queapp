import * as userService from "../../services/userService"
import * as shopService from "../../services/shopService"
import * as barberService from "../../services/barberService"
import * as mock from "./mocks"
import * as response from "./responses"

import { getPosition } from "./shop.test"

describe('User action', () => {
    let shops = [];
    let barbers = [];
    beforeAll(async () => {
        await userService.login(mock.USER_MOCK);

        shops = await shopService.list({ latitude: -22.975972, longitude: -46.545655, order: "distance" });
        barbers = await barberService.list();
    })

    it('Should get the details of the logged user', async () => {
        const user = await userService.getProfile();
        expect(user.data).toMatchObject(response.user)
    })

    it('Should make a rate into a BARBER', async () => {
        const rating = {
            ...mock.RATE_MOCK,
            entity_type: "barber",
            entity_id: barbers.data[getPosition(barbers.data)]._id
        }

        const user = await userService.rating(rating);
        expect(user.data).toMatchObject(response.user)
    })

    it('Should make a rate into a SHOP', async () => {
        const rating = {
            ...mock.RATE_MOCK,
            entity_type: "shop",
            entity_id: shops.data[getPosition(shops.data)]._id
        }

        const user = await userService.rating(rating);
        expect(user.data).toMatchObject(response.user)
    })
});