import React, {useContext, useState, useEffect} from 'react'
import THEMES from './themes.json'
import AsyncStorage from '@react-native-community/async-storage'
import {TAG_THEME_KEY} from "./storeNames"

export const themes = THEMES
export const theme = THEMES[1]

const ThemeContext = React.createContext()

export const ThemeContextProvider = ({children}) => {
  const [themeID, setThemeID] = useState()

  useEffect(() => {
    (async () => {
      const storedThemeID = await AsyncStorage.getItem(TAG_THEME_KEY);
      if (storedThemeID) setThemeID(storedThemeID);
      else setThemeID(THEMES[0].key);
    })()
  }, [])


  return (
    <ThemeContext.Provider value={{themeID, setThemeID}}>
      {!!themeID ? children : null}
    </ThemeContext.Provider>
  )
}

export const withTheme = Component => {
  return props => {
    const {themeID, setThemeID} = useContext(ThemeContext)

    const getTheme = themeID => THEMES.find(theme=>theme.key === themeID)
    const setTheme = themeID => {
      AsyncStorage.setItem(TAG_THEME_KEY, themeID);
      setThemeID(themeID);
    }

    return <Component 
              {...props} 
              themes={THEMES} 
              theme={getTheme(themeID)} 
              setTheme={setTheme}
              />
  }
}