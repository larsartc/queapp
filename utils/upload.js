export const uploadImage = (photo) => {
    const data = new FormData();
    data.append("photo", {
        name: photo.fileName,
        type: photo.type,
        uri: Platform.OS === "android" ? photo.uri : photo.uri.replace("file://", ""),
        data: photo.data
    });

    return fetch("http://34.201.231.101:3000/upload/image", {
        method: "POST",
        headers: {
            Accept: "application/json",
            "Content-Type": "multipart/form-data"
        },
        body: data
    });
}