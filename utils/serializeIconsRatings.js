export default function (number) {

    if (number == null) {
        return {
            number: null,
            partial: false,
        }
    }

    if (number > 5) {
        number = 5;
    }

    if (number <= 0) {
        return {
            number: 0,
            partial: false,
        }
    }

    if (number % 1 == 0) {
        return {
            number: number,
            partial: false,
        }
    } else {
        return {
            number: parseInt(number),
            partial: true
        }
    }
}