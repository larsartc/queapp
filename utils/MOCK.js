const MOCK_BARBERS = [
  {
      name: "Frank",
      starts: 3,
      image: require('que/assets/images/barbers/barber1.jpg'),
      address: {
          barberName: "Shear Space Brilliants Barber Shop",
          name: "1550 Kingston Rd, Pickering, ON, L1V 1C3 Canada",
          distance: "2km"
      },
      hoursAvaliable: ['Monday: 05:00am - 10:00pm', 'Monday: 05:00am - 10:00pm', 'Monday: 05:00am - 10:00pm', 'Monday: 05:00am - 10:00pm', 'Monday: 05:00am - 10:00pm', 'Monday: 05:00am - 10:00pm'],
  },
  {
      name: "Math",
      starts: 2,
      image: require('que/assets/images/barbers/barber2.jpg'),
      address: {
          barberName: "Shear Space Brilliants Barber Shop",
          name: "1550 Kingston Rd, Pickering, ON, L1V 1C3 Canada",
          distance: "2km"
      },
      hoursAvaliable: ['Monday: 05:00am - 10:00pm', 'Monday: 05:00am - 10:00pm', 'Monday: 05:00am - 10:00pm', 'Monday: 05:00am - 10:00pm', 'Monday: 05:00am - 10:00pm', 'Monday: 05:00am - 10:00pm'],
  },
  {
      name: "Alfred",
      starts: 5,
      image: require('que/assets/images/barbers/barber3.jpg'),
      address: {
          barberName: "Shear Space Brilliants Barber Shop",
          name: "1550 Kingston Rd, Pickering, ON, L1V 1C3 Canada",
          distance: "2km"
      },
      hoursAvaliable: ['Monday: 05:00am - 10:00pm', 'Monday: 05:00am - 10:00pm', 'Monday: 05:00am - 10:00pm', 'Monday: 05:00am - 10:00pm', 'Monday: 05:00am - 10:00pm', 'Monday: 05:00am - 10:00pm'],
  },
  {
      name: "Jake",
      starts: 3,
      image: require('que/assets/images/barbers/barber4.jpg'),
      address: {
          barberName: "Shear Space Brilliants Barber Shop",
          name: "1550 Kingston Rd, Pickering, ON, L1V 1C3 Canada",
          distance: "2km"
      },
      hoursAvaliable: ['Monday: 05:00am - 10:00pm', 'Monday: 05:00am - 10:00pm', 'Monday: 05:00am - 10:00pm', 'Monday: 05:00am - 10:00pm', 'Monday: 05:00am - 10:00pm', 'Monday: 05:00am - 10:00pm'],
  },
  {
      name: "Johanson",
      starts: 1,
      image: require('que/assets/images/barbers/barber5.jpg'),
      address: {
          barberName: "Shear Space Brilliants Barber Shop",
          name: "1550 Kingston Rd, Pickering, ON, L1V 1C3 Canada",
          distance: "2km"
      },
      hoursAvaliable: ['Monday: 05:00am - 10:00pm', 'Monday: 05:00am - 10:00pm', 'Monday: 05:00am - 10:00pm', 'Monday: 05:00am - 10:00pm', 'Monday: 05:00am - 10:00pm', 'Monday: 05:00am - 10:00pm'],
  },
  {
      name: "FrankFields",
      starts: 3,
      image: require('que/assets/images/barbers/barber6.jpg'),
      address: {
          barberName: "Shear Space Brilliants Barber Shop",
          name: "1550 Kingston Rd, Pickering, ON, L1V 1C3 Canada",
          distance: "2km"
      },
      hoursAvaliable: ['Monday: 05:00am - 10:00pm', 'Monday: 05:00am - 10:00pm', 'Monday: 05:00am - 10:00pm', 'Monday: 05:00am - 10:00pm', 'Monday: 05:00am - 10:00pm', 'Monday: 05:00am - 10:00pm'],
  },
  {
      name: "FrankFields",
      starts: 5,
      image: require('que/assets/images/barbers/barber7.jpg'),
      address: {
          barberName: "Shear Space Brilliants Barber Shop",
          name: "1550 Kingston Rd, Pickering, ON, L1V 1C3 Canada",
          distance: "2km"
      },
      hoursAvaliable: ['Monday: 05:00am - 10:00pm', 'Monday: 05:00am - 10:00pm', 'Monday: 05:00am - 10:00pm', 'Monday: 05:00am - 10:00pm', 'Monday: 05:00am - 10:00pm', 'Monday: 05:00am - 10:00pm'],
  },
  {
      name: "FrankFields",
      starts: 2,
      image: require('que/assets/images/barbers/barber8.jpg'),
      address: {
          barberName: "Shear Space Brilliants Barber Shop",
          name: "1550 Kingston Rd, Pickering, ON, L1V 1C3 Canada",
          distance: "2km"
      },
      hoursAvaliable: ['Monday: 05:00am - 10:00pm', 'Monday: 05:00am - 10:00pm', 'Monday: 05:00am - 10:00pm', 'Monday: 05:00am - 10:00pm', 'Monday: 05:00am - 10:00pm', 'Monday: 05:00am - 10:00pm'],
  }
]

const MOCK_USERS = [
    {
        name: "Scott",
        image: require('que/assets/images/profile_sign_in_img.jpg')
    },
    {
        name: "Koda",
        image: require('que/assets/images/profile_kids_img.jpg')
    },
]

const MOCK_HAIRS = [
    {name: "Frank", image: require('que/assets/images/cuts/cut1.jpg')},
    {name: "Alfred", image: require('que/assets/images/cuts/cut4.jpg')},
    {name: "Frank", image: require('que/assets/images/cuts/cut2.jpg')},
    {name: "Alfred", image: require('que/assets/images/cuts/cut3.jpg')},
    {name: "Frank", image: require('que/assets/images/cuts/cut1.jpg')},
    {name: "Alfred", image: require('que/assets/images/cuts/cut5.jpg')},
    {name: "Frank", image: require('que/assets/images/cuts/cut6.jpg')},
    {name: "Alfred", image: require('que/assets/images/cuts/cut3.jpg')},
    {name: "Frank", image: require('que/assets/images/cuts/cut1.jpg')},
    {name: "Alfred", image: require('que/assets/images/cuts/cut6.jpg')},
    {name: "Frank", image: require('que/assets/images/cuts/cut2.jpg')},
    {name: "Alfred", image: require('que/assets/images/cuts/cut3.jpg')},
    {name: "Frank", image: require('que/assets/images/cuts/cut6.jpg')},
    {name: "Alfred", image: require('que/assets/images/cuts/cut5.jpg')},
    {name: "Frank", image: require('que/assets/images/cuts/cut2.jpg')},
    {name: "Alfred", image: require('que/assets/images/cuts/cut3.jpg')},
    {name: "Frank", image: require('que/assets/images/cuts/cut1.jpg')},
    {name: "Alfred", image: require('que/assets/images/cuts/cut5.jpg')},
    {name: "Frank", image: require('que/assets/images/cuts/cut6.jpg')},
    {name: "Frank", image: require('que/assets/images/cuts/cut1.jpg')},
]

const MOCK_DAYS = [
    {day: "Aug 24", day2:"FRI"},{day: "Aug 25", day2:"FRI"},{day: "Aug 27", day2:"FRI", day: "Aug 28", day2:"FRI"},{day: "Aug 29", day2:"FRI"},{day: "Aug 30", day2:"FRI"},
    {day: "Aug 24", day2:"FRI"},{day: "Aug 25", day2:"FRI"},{day: "Aug 27", day2:"FRI", day: "Aug 28", day2:"FRI"},{day: "Aug 29", day2:"FRI"},{day: "Aug 30", day2:"FRI"}
]

const MOCK_HOURS = [
    {time: "9:00"},{time: "10:00"},{time: "11:00"},{time: "12:00"},{time: "13:00"},{time: "14:00"},{time: "15:00"},{time: "16:00"},{time: "17:00"}
]

const MOCK_SERVICES = {
    price:[
      {
          title: 'ADULTS (AGE 18+)', 
          options: [
              {name: "HAIRCUT", value: "$ 30.00"},
              {name: "HAIRCUT AND BEARD", value: "$ 40.00"},
              {name: "FULL GROOMING", value: "$ 50.00"}
          ]
      },
      {
          title: 'JUNIOR (AGE 5-17)', 
          options: [
              {name: "CUT", value: "$ 25.00"},
              {name: "CUT AND BEARD", value: "$ 30.00"},
              {name: "HOT TOWEL BEARD/SHAVE", value: "$ 30.00"}
          ]
      },
      {
          title: 'ADULTS (AGE 18+)', 
          options: [
              {name: "HAIRCUT", value: "$ 30.00"},
              {name: "HAIRCUT AND BEARD", value: "$ 40.00"},
              {name: "FULL GROOMING", value: "$ 50.00"}
          ]
      },
      {
          title: 'JUNIOR (AGE 5-17)', 
          options: [
              {name: "CUT", value: "$ 25.00"},
              {name: "CUT AND BEARD", value: "$ 30.00"},
              {name: "HOT TOWEL BEARD/SHAVE", value: "$ 30.00"}
          ]
      },
      {
          title: 'ADULTS (AGE 18+)', 
          options: [
              {name: "HAIRCUT", value: "$ 30.00"},
              {name: "HAIRCUT AND BEARD", value: "$ 40.00"},
              {name: "FULL GROOMING", value: "$ 50.00"}
          ]
      },
      {
          title: 'JUNIOR (AGE 5-17)', 
          options: [
              {name: "CUT", value: "$ 25.00"},
              {name: "CUT AND BEARD", value: "$ 30.00"},
              {name: "HOT TOWEL BEARD/SHAVE", value: "$ 30.00"}
          ]
      }
    ]
}

export {
    MOCK_BARBERS,
    MOCK_HAIRS, 
    MOCK_DAYS,
    MOCK_HOURS,
    MOCK_USERS, 
    MOCK_SERVICES,
}