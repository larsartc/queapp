import moment from "moment";
import _ from "lodash"

export const getHoursByDay = () => {
    let currentDate = moment().set("minute", 0).format('MMMM DD hh:mm A')
    let endDate = moment().set("hour", 23).set("minute", 59).format('MMMM DD hh:mm A')
    const diff = moment(endDate, "MMMM DD hh:mm A").diff(moment(currentDate, "MMMM DD hh:mm A"), 'hours')
    let arrayHours = []
    for (let i = diff; i > 0; i--) {
        arrayHours.push(moment(currentDate, "MMMM DD hh:mm A").add(i, "h").format("MMMM DD hh:mm A"));
    }
    arrayHours.push(currentDate);
    return arrayHours.reverse();
}

export const getHoursForThisDay = (hours) => {
    const start_type = hours.start_time.includes('am') ? 'am' : 'pm';
    const end_type = hours.end_time.includes('am') ? 'am' : 'pm';

    let hoursBetween = moment.duration(moment(hours.end_time, 'HH:mm' + start_type).diff(moment(hours.start_time, 'HH:mm' + end_type))).asHours();
    let hourStart = moment(hours.start_time, 'HH:mm' + start_type).format('HH:mm');

    let arrayHours = [];

    while (hoursBetween >= 0) {
        const hour = moment(hourStart, "HH:mm").add(hoursBetween, 'hours').format('HH:mm');
        arrayHours.push({ time: hour });
        hoursBetween = hoursBetween - 1;
    }

    return _.sortBy(arrayHours, "time")
}