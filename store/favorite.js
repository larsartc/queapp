import * as favoriteService from "../services/favoriteService";

const TAG = "FAVORITE_REDUX"

export const type = {
    FAVORITE_CUT_LIST_LOADING: "FAVORITE_CUT_LIST_LOADING",
    FAVORITE_CUT_LIST_SUCCESS: "FAVORITE_CUT_LIST_SUCCESS",
    FAVORITE_CUT_LIST_ERROR: "FAVORITE_CUT_LIST_ERROR",
    FAVORITE_CUT_UNFAVORITE_SUCCESS: "FAVORITE_CUT_UNFAVORITE_SUCCESS",
    FAVORITE_CUT_UNFAVORITE_ERROR: "FAVORITE_CUT_UNFAVORITE_ERROR",
    FAVORITE_BARBERS_LIST_SUCCESS: "FAVORITE_BARBERS_LIST_SUCCESS",
    FAVORITE_BARBERS_LIST_ERROR: "FAVORITE_BARBERS_LIST_ERROR",
    FAVORITE_BARBER_LIST_LOADING: "FAVORITE_BARBER_LIST_LOADING",
    FAVORITE_BARBER_UNFAVORITE_SUCCESS: "FAVORITE_BARBER_UNFAVORITE_SUCCESS",
    FAVORITE_BARBER_UNFAVORITE_ERROR: "FAVORITE_BARBER_UNFAVORITE_ERROR",
    FAVORITE_SHOP_LIST_SUCCESS: "FAVORITE_SHOP_LIST_SUCCESS",
    FAVORITE_SHOP_LIST_ERROR: "FAVORITE_SHOP_LIST_ERROR",
    FAVORITE_SHOP_UNFAVORITE_SUCCESS: "FAVORITE_SHOP_UNFAVORITE_SUCCESS",
    FAVORITE_SHOP_UNFAVORITE_ERROR: "FAVORITE_SHOP_UNFAVORITE_ERROR",
};

const initialState = {
    loading: false,
    error: null,
    listCuts: [],
    listBarbers: [],
    listShops: []
};

export default favoriteReducer = (state = initialState, action) => {
    switch (action.type) {
        case type.FAVORITE_CUT_LIST_LOADING:
            return {
                ...state,
                loading: true
            };
        case type.FAVORITE_CUT_LIST_SUCCESS:
            return {
                ...state,
                loading: false,
                listCuts: action.payload,
                error: null
            };
        case type.FAVORITE_CUT_LIST_ERROR:
            return {
                ...state,
                loading: false,
                error: action.payload
            };
        case type.FAVORITE_CUT_UNFAVORITE_SUCCESS:
            return {
                ...state,
                listCuts: action.payload,
                loading: false,
                error: null
            };
        case type.FAVORITE_BARBERS_LIST_SUCCESS:
            return {
                ...state,
                listBarbers: action.payload,
                loading: false,
                error: null
            };
        case type.FAVORITE_BARBER_UNFAVORITE_SUCCESS:
            return {
                ...state,
                listBarbers: action.payload,
                loading: false,
                error: null
            };


        case type.FAVORITE_SHOP_LIST_SUCCESS:
            return {
                ...state,
                listShops: action.payload,
                loading: false,
                error: null
            };
        case type.FAVORITE_SHOP_UNFAVORITE_SUCCESS:
            return {
                ...state,
                listShops: action.payload,
                loading: false,
                error: null
            };

        default:
            return state;
    }
};

export const listFavoriteCut = payload => async dispatch => {
    dispatch({ type: type.FAVORITE_CUT_LIST_LOADING });
    favoriteService
        .cuts(payload)
        .then(res => {
            console.log(TAG, res)
            dispatch({ type: type.FAVORITE_CUT_LIST_SUCCESS, payload: res.data });
        })
        .catch(e => {
            console.log(TAG, e)
            dispatch({ type: type.FAVORITE_CUT_LIST_ERROR });
        });
};
export const unfavoriteCut = payload => async (dispatch, getState) => {
    dispatch({ type: type.FAVORITE_CUT_LIST_LOADING });
    favoriteService
        .unfavorite(payload)
        .then(res => {
            console.log(TAG, res)
            let list = getState().favoriteReducer.listCuts.filter(i => i._id !== payload.entity_id);
            dispatch({ type: type.FAVORITE_CUT_UNFAVORITE_SUCCESS, payload: list });
        })
        .catch(e => {
            console.log(TAG, e)
            dispatch({ type: type.FAVORITE_CUT_UNFAVORITE_ERROR });
        });
};


export const listFavoriteBarber = payload => async dispatch => {
    dispatch({ type: type.FAVORITE_CUT_LIST_LOADING });
    favoriteService
        .barbers(payload)
        .then(res => {
            console.log(TAG, res)
            dispatch({ type: type.FAVORITE_BARBERS_LIST_SUCCESS, payload: res.data });
        })
        .catch(e => {
            console.log(TAG, e)
            dispatch({ type: type.FAVORITE_BARBERS_LIST_ERROR });
        });
};
export const unfavoriteBarber = payload => async (dispatch, getState) => {
    dispatch({ type: type.FAVORITE_BARBER_LIST_LOADING });
    favoriteService
        .unfavorite(payload)
        .then(res => {
            console.log(TAG, res)
            let list = getState().favoriteReducer.listBarbers.filter(i => i._id !== payload.entity_id);
            dispatch({ type: type.FAVORITE_BARBER_UNFAVORITE_SUCCESS, payload: list });
        })
        .catch(e => {
            console.log(TAG, e)
            dispatch({ type: type.FAVORITE_BARBER_UNFAVORITE_ERROR });
        });
};


export const listFavoriteShop = payload => async dispatch => {
    dispatch({ type: type.FAVORITE_CUT_LIST_LOADING });
    favoriteService
        .shops(payload)
        .then(res => {
            console.log(TAG, res)
            dispatch({ type: type.FAVORITE_SHOP_LIST_SUCCESS, payload: res.data });
        })
        .catch(e => {
            console.log(TAG, e)
            dispatch({ type: type.FAVORITE_SHOP_LIST_ERROR });
        });
};
export const unfavoriteShops = payload => async (dispatch, getState) => {
    dispatch({ type: type.FAVORITE_BARBER_LIST_LOADING });
    favoriteService
        .unfavorite(payload)
        .then(res => {
            console.log(TAG, res)
            let list = getState().favoriteReducer.listShops.filter(i => i._id !== payload.entity_id);
            dispatch({ type: type.FAVORITE_SHOP_UNFAVORITE_SUCCESS, payload: list });
        })
        .catch(e => {
            console.log(TAG, e)
            dispatch({ type: type.FAVORITE_SHOP_UNFAVORITE_ERROR });
        });
};

// TODO ..... refactore it
// ..
// export const favoriteShops = payload => async (dispatch, getState) => {
//   dispatch({ type: type.FAVORITE_BARBER_LIST_LOADING });
//   favoriteService
//     .favorite(payload)
//     .then(res => {
//       console.log(TAG, res)
//       let list = getState().favoriteReducer.listShops.filter(i => i._id !== payload.entity_id);
//       dispatch({ type: type.FAVORITE_SHOP_UNFAVORITE_SUCCESS, payload: list });
//     })
//     .catch(e => {
//       console.log(TAG, e)
//       dispatch({ type: type.FAVORITE_SHOP_UNFAVORITE_ERROR });
//     });
// };
