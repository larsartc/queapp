import * as appointmentService from "../services/appointmentService"
import moment from "moment"
import _ from "lodash"
const TAG = "APPOINTMENT"

export const type = {
  APPOINTMENTS_LOADING: "APPOINTMENTS_LOADING",
  APPOINTMENTS_LIST_SUCCESS: "APPOINTMENTS_LIST_SUCCESS",
  APPOINTMENTS_LIST_ERROR: "APPOINTMENTS_LIST_ERROR",
  APPOINTMENTS_LIST_DAYS: 'APPOINTMENTS_LIST_DAYS',
  APPOINTMENTS_CLEAR: "APPOINTMENTS_CLEAR",
  NOTIFICATION_RECEIVED: "NOTIFICATION_RECEIVED",
  NOTIFICATION_CLEAR: "NOTIFICATION_CLEAR"
}

const initialState = {
  loading: false,
  error: null,
  list: [],
  listDays: [],
  notification: {}
}

export default appointmentReducer = (state = initialState, action) => {
  switch (action.type) {
    case type.APPOINTMENTS_LOADING:
      return {
        ...state,
        loading: true,
      }
    case type.APPOINTMENTS_LIST_SUCCESS:
      return {
        ...state,
        loading: false,
        list: action.payload,
        error: null,
      }
    case type.APPOINTMENTS_LIST_ERROR:
      return {
        ...state,
        loading: false,
        error: action.payload
      }

    case type.APPOINTMENTS_LIST_DAYS:
      return {
        ...state,
        listDays: action.payload,
      }

    case type.APPOINTMENTS_CLEAR:
      return {
        ...state,
        listDays: [],
        list: []
      }

    case type.NOTIFICATION_RECEIVED:
      return {
        ...state,
        notification: action.payload
      }

    case type.NOTIFICATION_CLEAR:
      return {
        ...state,
        notification: {}
      }

    default:
      return state
  }
}

export const appointmentsClear = () => ({ type: type.APPOINTMENTS_CLEAR })

export const getApptByDay = (month, day) => async dispatch => {

  let { data } = await appointmentService.listDay({ month, day })

  dispatch({ type: type.APPOINTMENTS_LIST_SUCCESS, payload: data })
}

export const listAppoitments = payload => async dispatch => {

  try {
    dispatch({ type: type.APPOINTMENTS_LOADING });


    const days = await appointmentService.list(payload)

    // fill the days of the month with appointments
    dispatch({ type: type.APPOINTMENTS_LIST_DAYS, payload: days.data.daysWithAppointments })

  } catch (e) {
    console.log(e)
  }
}
 
export const closeReview = () => ({ type: type.NOTIFICATION_CLEAR, payload: {} })

export const makeAppointment = payload => async dispatch => {
  console.log(TAG, "start makeAppointment")
  console.log(TAG, payload)

  appointmentService.make(payload).then(res => {
    console.log(TAG, res)
  }).catch(e => {
    console.log(TAG, e)
  })

}

export const updateAppointment = (payload, oldDate) => async (dispatch, getState) => {

  console.log(TAG, "update appt")
  console.log(TAG, payload)

  try {
    await appointmentService.update(payload)

    let appointments = {}

    const newMonth = moment(payload.day, "YYYY-MM-DD").format("MM");

    let days = await appointmentService.list(newMonth);

    // fill the days of the month with appointments
    dispatch({ type: type.APPOINTMENTS_LIST_DAYS, payload: days.data.daysWithAppointments })

    const listApptDays = await Promise.all(
      days.data.daysWithAppointments.map(i => appointmentService.listDay({ month: newMonth, day: i }))
    )

    listApptDays.forEach((apptDay) => {
      apptDay.data.forEach((value, key) => {
        const dateAppointment = moment.unix(value.date).format('YYYY-MM-DD');
        appointments[dateAppointment] = !appointments[dateAppointment] ? [value] : [...appointments[dateAppointment], value];
      })
    });

    dispatch({ type: type.APPOINTMENTS_LIST_SUCCESS, payload: appointments })

  } catch (e) {
    console.log(TAG, e)
  }
}
export const removeAppointment = (_id, date) => async (dispatch, getState) => {

  try {
    await appointmentService.remove(_id);

    const days = await appointmentService.list(moment(date, "YYYY-MM-DD").format("MM"))

    dispatch({ type: type.APPOINTMENTS_LIST_DAYS, payload: days.data.daysWithAppointments })

    let apptList = getState().appointmentReducer.list;

    apptList = apptList.filter((appt) => appt._id != _id);

    dispatch({ type: type.APPOINTMENTS_LIST_SUCCESS, payload: apptList })

  } catch (e) {
    console.log(e);
  }
}