import {type as usertype, UI_TYPE} from './user'
import * as barberApi from "../services/barberService"

const TAG = "BARBER_REDUX"

export const type = {
  BARBER_WORK_REQUEST_SUCCESS: "BARBER_WORK_REQUEST_SUCCESS",
  BARBER_WORK_REQUEST_ERROR: "BARBER_WORK_REQUEST_ERROR",

  BARBER_CREATE_SUCCESS: "BARBER_CREATE_SUCCESS",
  BARBER_CREATE_ERROR: "BARBER_CREATE_ERROR",

  SAVE_TEMP_CARRER_SELECTED: "SAVE_TEMP_CARRER_SELECTED", 

  FEEDBACK_BARBER_SUCCESS: "FEEDBACK_BARBER_SUCCESS",

  LIST_CARRER_LOADING: "LIST_CARRER_LOADING",
}

const initialState = {
  loading: false,
  error: null, 
  listCarrer: [], 
  barber: {}, 
  barberRatings: {}
}

export default barberReducer = (state=initialState, action) => {
  switch(action.type){
    case type.LIST_CARRER_LOADING:
      return {
        ...state, 
        loading: true 
      }

    case type.SAVE_TEMP_CARRER_SELECTED:
      return {
        ...state, 
        listCarrer: action.payload
      }

    case type.BARBER_CREATE_SUCCESS:
      return {
        ...state, 
        barber: action.payload
      }

    case type.FEEDBACK_BARBER_SUCCESS:
      return {
        ...state, 
        barberRatings: action.payload
      }

    default: 
      return state
  }
}

export const changeEnvToBarber = (data) => async dispatch => {
  //dispatch({ type: usertype.CHANGE_ENV, payload: UI_TYPE.BARBER })

  barberApi.workRequest({shop: data}).then(res => {

    //TODO TRATAR AQUI
    // res.data 
    /*let payload = {
      status: "pending",
      requested_at: 0,
      _id: "5e288f03df900c1ccd1882c7",
      shop: "5db891c11145156550c8b8a2",
      barber: "5dc2fea0c33ec921933406a7",
      createdAt: "2020-01-22T18:05:55.036Z",
      updatedAt: "2020-01-22T18:05:55.036Z",
    }*/

   // dispatch({ type: usertype.CHANGE_ENV, payload: UI_TYPE.BARBER })
    dispatch({ type: type.BARBER_WORK_REQUEST_SUCCESS });
    console.log(TAG, res)
  }).catch(e => {
    dispatch({ type: type.BARBER_WORK_REQUEST_ERROR, payload: "Try again" });
    console.log(TAG, e)
  })

}

export const createProfileBarber = payload => async (dispatch, getState) => {

  barberApi.create(payload).then(res=>{
    console.log(TAG, res)

    let idBarber = res.data._id // TODO same id user

    // if success , get the carrers itens and put to backend too
    let carrers = getState().barberReducer.listCarrer

    carrers.forEach(val=>{
      let payloadCarrer = {
        shop: val.shop._id,
        started_at: val.started_at,
        ended_at: val.ended_at,
      }

      barberApi.createCarrer(payloadCarrer, idBarber)
    })

    dispatch({type: usertype.UPDATE_USER_BARBER_PROFILE, payload: payload})
    
  }).catch(e=>{
    dispatch({type: type.BARBER_CREATE_ERROR})
    console.log(TAG, e)
  })

}

export const createCarrer = payload => async dispatch => {
  //
}

export const saveTempCarrerSelected = (payload, action) => async dispatch => {
  dispatch({type: type.SAVE_TEMP_CARRER_SELECTED, payload:payload})
  action()
}

export const getRatingsBarberProfile = () => async (dispatch, getState) => {

  let idBarber = getState().barberReducer.barber._id
  let idShop = getState().barberReducer.barber.shop._id

  barberApi.listFeedbackBarber({idBarber, idShop}).then(res=> {
    console.log(res)

    let ratings = res.data

    dispatch({type: type.FEEDBACK_BARBER_SUCCESS, payload: ratings})
  }).catch(e=>{
    console.log(e)

  })
}

export const makeAppointment = payload => async dispatch => {

  barberApi.createAppointment(payload).then(res=>{
    console.log(TAG, res)    

  }).catch(e=>{
    console.log(TAG, e)

  })

}