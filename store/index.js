import { createStore, combineReducers, applyMiddleware } from "redux"
import AsyncStorage from '@react-native-community/async-storage';

import thunk from "redux-thunk"
// reducers 
import userReducer from "./user"
import shopReducer from "./shop"
import cutsReducer from "./cut"
import chatReducer from "./chat"
import helpReducer from "./help"
import appointmentReducer from "./appointment"
import categoryReducer from "./category"
import feedbackReducer from "./feedback"
import favoriteReducer from "./favorite"
import barberReducer from "./barber"

import { persistStore, persistReducer } from 'redux-persist';

// Persistor Configuration to whitelist and blacklist any reducer
const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  whitelist: ['userReducer']
};

const combinedReducer = combineReducers({
  userReducer,
  shopReducer,
  cutsReducer,
  chatReducer,
  helpReducer,
  appointmentReducer,
  categoryReducer,
  feedbackReducer,
  favoriteReducer,
  barberReducer,
})

const persistedReducer = persistReducer(persistConfig, combinedReducer)

export const store = createStore(persistedReducer, applyMiddleware(thunk));
window.store = store;
export const persistor = persistStore(store);
