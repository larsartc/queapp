import * as cutService from "../services/cutService"

const TAG = "CUTS_REDUX"

export const type = {
  CUT_LIST_LOADING: "CUT_LIST_LOADING",
  CUT_LIST_SUCCESS: "CUT_LIST_SUCCESS",
  CUT_LIST_ERROR: "CUT_LIST_ERROR",
  CUT_FAVORITE_LOADING: "CUT_FAVORITE_LOADING",
  CUT_FAVORITE_SUCCESS: "CUT_FAVORITE_SUCCESS",
  CUT_FAVORITE_ERROR: "CUT_FAVORITE_ERROR",
  CUT_UNFAVORITE_LOADING: "CUT_UNFAVORITE_LOADING",
  CUT_UNFAVORITE_SUCCESS: "CUT_UNFAVORITE_SUCCESS",
  CUT_UNFAVORITE_ERROR: "CUT_UNFAVORITE_ERROR",
  CUT_SEARCH: "CUT_SEARCH"
}

const initialState = {
  loading: false,
  error: null, 
  list: [],
  search: ""
}

export default cutsReducer = (state=initialState, action) => {
  switch(action.type){
    case type.CUT_LIST_LOADING:
      return {
        ...state, 
        loading: true 
      }
    case type.CUT_LIST_SUCCESS:
      return {
        ...state, 
        loading: false, 
        list: action.payload, 
        error: null,
      }
    case type.CUT_LIST_ERROR:
      return {
        ...state, 
        loading: false, 
        error: action.payload
      }
    case type.CUT_FAVORITE_SUCCESS:
      return {
        ...state, 
        list: action.payload,
        loading: false, 
        error: null,
      }
    case type.CUT_UNFAVORITE_SUCCESS:
      return {
        ...state, 
        list: action.payload,
        loading: false, 
        error: null,
      }

    case type.CUT_SEARCH:
      return {
        ...state,
        search: action.payload
      }

    default:
      return state
  }
}

export const listCut = payload => async dispatch => {
  dispatch({ type: type.CUT_LIST_LOADING });
  cutService.list(payload)
  .then(res=>{
    console.log(TAG, res)
    dispatch({type:type.CUT_LIST_SUCCESS, payload: res.data})
  }).catch(e=>{
    console.log(TAG, e)
    dispatch({type:type.CUT_LIST_ERROR, payload: res.data})
  })
}

export const setSearch = payload => ({ type: type.CUT_SEARCH, payload })

export const setFilter = payload => ({ type: type.SET_FILTER, payload })

export const cutSearch = (store) => {
  let { search , list } = store.cutsReducer
  if (search === '') return list
  return list.filter(cut => cut.name.toLowerCase().includes(search.toLowerCase()))
}

export const favoriteCut = payload => async(dispatch, getState) => {
  dispatch({ type: type.CUT_LIST_LOADING });
  cutService.favorite(payload)
  .then(res=>{
    let list = getState().cutsReducer.list.map(i => {
      if(i._id === payload.entity_id){
        i.favorite = true;
        i.num_favorites++;
      }
      return i;
    }) 
    dispatch({type:type.CUT_FAVORITE_SUCCESS, payload: list})
  }).catch(e=>{
    //console.log(e)
    dispatch({type:type.CUT_FAVORITE_ERROR, payload: res.data})
  })
}

export const unfavoriteCut = payload => async(dispatch, getState) => {
  dispatch({ type: type.CUT_LIST_LOADING });
  cutService.unfavorite(payload)
  .then(res=>{
    let list = getState().cutsReducer.list.map(i => {
      if(i._id === payload.entity_id){
        i.favorite = false;
        i.num_favorites--;
      }
      return i;
    })
    dispatch({type:type.CUT_UNFAVORITE_SUCCESS, payload: list})
  }).catch(e=>{
    //console.log(e)
    dispatch({type:type.CUT_UNFAVORITE_ERROR, payload: res.data})
  })
}