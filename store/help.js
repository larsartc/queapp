import * as helpApi from "../services/helpService"

export const type = {
    HELP_LIST_LOADING: "HELP_LIST_LOADING",
    HELP_LIST_SUCCESS: "HELP_LIST_SUCCESS",
    HELP_LIST_ERROR: "HELP_LIST_ERROR",
}

const initialState = {
  loading: false,
  error: null, 
  list: []
}

export default helpReducer = (state=initialState, action) => {
    switch(action.type){
        case type.HELP_LIST_LOADING:
            return {
                ...state, 
                loading: true, 
            }
        case type.HELP_LIST_SUCCESS:
            return {
                ...state, 
                loading: false, 
                list: action.payload, 
                error: null,
            }
        case type.HELP_LIST_ERROR:
            return {
                ...state, 
                loading: false, 
                error: action.payload
            }
        default: 
            return state
    }
}

// actions here
export const listHelps = payload => async dispatch => {
    dispatch({ type: type.HELP_LIST_LOADING });
    helpApi.list(payload)
    .then(res=>{
        console.log(res)
        dispatch({type:type.HELP_LIST_SUCCESS, payload: res.data})
    }).catch(e=>{
        console.log(e)
        dispatch({type:type.HELP_LIST_ERROR})
    })
}