import * as shopService from "../services/shopService"
import * as TokenService from "../services/tokenService"
import * as favoriteService from "../services/favoriteService";
import { getHoursByDay } from "../utils/getHoursByDay"

import { MOCK_BARBERS } from "../utils/MOCK"

const TAG = "SHOP_REDUX"

export const type = {
  SHOP_LOADING: "SHOP_LOADING",
  SHOP_LIST_SUCCESS: "SHOP_CREATE_SUCCESS",
  SHOP_LIST_ERROR: "SHOP_CREATE_ERROR",
  SHOP_SELECTED: 'SHOP_SELECTED',
  SHOP_SET_FILTERS: "SHOP_SET_FILTERS",
  BARBER_SELECTED: 'BARBER_SELECTED',
  BARBER_LOADING: 'BARBER_LOADING',
  SHOP_LIST_SERVICES: 'SHOP_LIST_SERVICES',
  SHOP_SEARCH: "SHOP_SEARCH"
}

const initialState = {
  loading: false,
  error: null,
  list_shop: [],
  filters: {
    availableNow: false,
    availableOn: {
      check: false,
      options: getHoursByDay(),
      value: null,
    },
    distance: [50],
    coast: {
      check: false,
      count: null,
    },
    rating: {
      check: false,
      expanded: false,
      count: null,
      types: []
    },
    orderBy: "distance"
  },
  shop: {
    barbers: [],
  },
  barber_loading: false,
  barber: {},
  search: "",
  services: []
}

export default shopReducer = (state = initialState, action) => {
  switch (action.type) {
    case type.SHOP_LOADING:
      return {
        ...state,
        loading: true,
        //shop: initialState.shop
      }
    case type.BARBER_LOADING:
      return {
        ...state,
        barber_loading: true,
        barber: initialState.barber
      }
    case type.SHOP_LIST_SUCCESS:
      return {
        ...state,
        loading: false,
        list_shop: action.payload,
        error: null,
      }
    case type.SHOP_LIST_ERROR:
      return {
        ...state,
        loading: false,
        error: action.payload
      }
    case type.SHOP_SELECTED:
      return {
        ...state,
        loading: false,
        shop: action.payload
      }
    case type.BARBER_SELECTED:
      return {
        ...state,
        loading: false,
        barber: action.payload
      }
    case type.SHOP_LIST_SERVICES:
      return {
        ...state,
        services: action.payload
      }
    case type.SHOP_SET_FILTERS:
      return {
        ...state,
        filters: action.filters
      }

    case type.SHOP_SEARCH:
      return {
        ...state,
        search: action.payload
      }

    default:
      return state
  }
}

export const setFilters = filters => ({
  type: type.SHOP_SET_FILTERS, filters
})

export const listShops = payload => async dispatch => {
  //dispatch({type:type.SHOP_LIST_SUCCESS, payload: MOCK_BARBERS})
  dispatch({ type: type.SHOP_LOADING });

  // await TokenService.verify().then(val => console.log(TAG + " - success", val)).catch(e => console.log(TAG + " - error", e))

  shopService.list(payload)
    .then(res => {
      console.log(TAG + "---", res)
      dispatch({ type: type.SHOP_LIST_SUCCESS, payload: res.data })
    }).catch(e => {
      console.log(TAG, e)
      dispatch({ type: type.SHOP_LIST_ERROR, payload: res.data })
    })
}

export const setSearch = payload => ({ type: type.SHOP_SEARCH, payload })

export const shopsFilter = (store) => {
  const { search, list_shop } = store.shopReducer
  if (search === '') return list_shop

  return list_shop.filter(shop => shop.name.toLowerCase().includes(search.toLowerCase()) || shop.address.toLowerCase().includes(search.toLowerCase()))
}

export const findShop = payload => async dispatch => {
  dispatch({ type: type.SHOP_LOADING });
  shopService.find(payload)
    .then(res => {

      let services = {}
      let category = null

      res.data.services.forEach(val => {
        if (category == null || category != val.category) {
          category = val.category
        }

        if (!(services[category] instanceof Array)) {
          services[category] = []
        }

        services[category].push({ ...val })

      })

      dispatch({ type: type.SHOP_SELECTED, payload: res.data })
      dispatch({ type: type.SHOP_LIST_SERVICES, payload: services })
    }).catch(e => {
      console.log(TAG, e)
      dispatch({ type: type.SHOP_LIST_ERROR, payload: res.data })
    })
}

export const findBarber = payload => async (dispatch, getState) => {
  dispatch({ type: type.BARBER_LOADING });
  shopService.findBarber(payload)
    .then(res => {
      console.log(TAG, res)
      let data = {
        ...res.data,
        address: getState().shopReducer.shop.address,
        shop_name: getState().shopReducer.shop.name,
        open_time: res.data.barber ? res.data.barber.work_time : []
      }
      dispatch({ type: type.BARBER_SELECTED, payload: { ...data } })
    }).catch(e => {
      console.log(TAG, e)
      dispatch({ type: type.SHOP_LIST_ERROR, payload: res.data })
    })
}

export const favAction = (entity_type, entity_id = null) => (dispatch, getState) => {
  let newObj = entity_type == 3 ? { ...getState().shopReducer.shop } : { ...getState().shopReducer.barber };
  if (entity_type == 3 && !!newObj) {
    const selected_shop_id = getState().shopReducer.shop._id;
    let values = getState().shopReducer.list_shop.map((shop) => {
      if (shop._id == selected_shop_id) {
        shop.favorite = !shop.favorite;
      }
      return shop;
    });
    dispatch({ type: type.SHOP_LIST_SUCCESS, payload: values })

    dispatch({ type: type.SHOP_SELECTED, payload: { ...newObj, favorite: !newObj.favorite } })
  }
  return dispatch({ type: type.BARBER_SELECTED, payload: { ...newObj, favorite: !newObj.favorite } })
}

export const updateListShop = (shops) => (dispatch, getState) => {
  dispatch({ type: type.SHOP_LIST_SUCCESS, payload: shops })
}

export const unFavorite = payload => async (dispatch, getState) => {
  favoriteService
    .unfavorite(payload)
    .then(res => res)
    .catch(e => {
      console.log(e)
    })
};

export const favorite = payload => async (dispatch, getState) => {
  favoriteService
    .favorite(payload)
    .then(res => res)
    .catch(e => {
      console.log(e)
    });
};

export const setShopOpened = payload => async (dispatch) => {
  dispatch({ type: type.SHOP_SELECTED, payload: payload })
}