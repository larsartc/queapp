import * as userApi from "../services/userService"
import AsyncStorage from "@react-native-community/async-storage"
import * as ASYNCTAG from "../utils/storeNames"
import axios from "../services"

const TAG = "USER_REDUX"

export const UI_TYPE = {
  CUSTOMER: "CUSTOMER",
  BARBER: "BARBER",
  SHOP: "SHOP",
}

export const type = {
  LOGIN_LOADING: "LOGIN_LOADING",
  LOGIN_SUCCESS: "LOGIN_SUCCESS",
  LOGIN_ERROR: "LOGIN_ERROR",
  CREATE_SUCCESS: "CREATE_SUCCESS",
  CREATE_ERROR: "CREATE_ERROR",
  CHANGE_INTERFACE: "CHANGE_INTERFACE",
  CREATE_FAMILY_LOADING: "CREATE_FAMILY_LOADING",
  CREATE_FAMILY_SUCCESS: "CREATE_FAMILY_SUCCESS",
  CREATE_FAMILY_ERROR: "CREATE_FAMILY_ERROR",
  UPDATE_USER_BARBER_PROFILE: "UPDATE_USER_BARBER_PROFILE",
  CHANGE_ENV: "CHANGE_ENV",
  FILL_FAMILY: "FILL_FAMILY",
  LOGOUT: "LOGOUT",
  SET_USER: "SET_USER",
  REFRESH_TOKEN: "REFRESH_TOKEN"
}

const initialState = {
  loading: false,
  create_success: false,
  error: null,
  user: null,
  interface: UI_TYPE.CUSTOMER, //UI_TYPE.BARBER, //
  //
  createFamilyLoading: false,
}

export default userReducer = (state = initialState, action) => {
  switch (action.type) {
    case type.LOGIN_LOADING:
      return {
        ...state,
        loading: true,
        error: null,
      }

    case type.LOGIN_SUCCESS:
      return {
        ...state,
        loading: false,
        user: action.payload,
        error: null,
      }

    case type.LOGIN_ERROR:
      return {
        ...state,
        loading: false,
        error: action.payload,
        user: null,
      }

    case type.CHANGE_INTERFACE:
      return {
        ...state,
        interface: action.payload,
      }

    case type.CREATE_SUCCESS:
      return {
        ...state,
        loading: false,
        create_success: true
      }

    case type.CREATE_ERROR:
      return {
        ...state,
        loading: false,
        create_success: false,
        error: action.payload,
      }

    case type.UPDATE_USER_BARBER_PROFILE:
      return {
        ...state,
        user: {
          ...state.user,
          barber: {
            ...state.user.barber,
            ...action.payload,
          }
        }
      }

    // case type.GET_USER_FAMILY:
    //   return {
    //     ...state,
    //     family: [...action.payload.parents, action.payload.user]
    //   }


    case type.CREATE_FAMILY_LOADING:
      return {
        ...state,
      }

    case type.CREATE_FAMILY_SUCCESS:
      return {
        ...state,
      }

    case type.CREATE_FAMILY_ERROR:
      return {
        ...state,
      }

    case type.FILL_FAMILY:
      return {
        ...state,
        user: {
          ...state.user,
          family: action.payload
        }
      }

    case type.SET_USER:
      return {
        ...state,
        user: { ...action.payload }
      }

    case type.CHANGE_ENV:
      return {
        ...state,
        interface: action.payload,
      }

    case type.LOGOUT:
      return {
        ...initialState
      }

    case type.REFRESH_TOKEN:
      return {
        ...state,
        user: { ...state.user, ...action.payload }
      }

    default:
      return state
  }
}

export const forgotPassword = email => async dispatch => {
  try {
    return await userApi.forgotPassword(email)
  } catch (e) {
    return await Promise.reject()
  }
}

export const login = payload => async dispatch => {

  dispatch({ type: type.LOGIN_LOADING })

  try {
    const res = await userApi.login(payload)

    if (res.data.user.shopsWorking && res.data.user.shopsWorking[0] && res.data.user.shopsWorking[0].status == "accepted") {
      dispatch({ type: type.CHANGE_ENV, payload: UI_TYPE.BARBER })
    };

    axios.defaults.headers.common['Authorization'] = res.data.token.token

    let { data } = await userApi.userFamily()

    const user = {
      ...res.data.user,
      position: { latitude: 0, longitude: 0 },
      token: res.data.token.token,
      refresh_token: res.data.token.refresh_token,
      family: [data.user, ...data.parents]
    }
    dispatch({ type: type.LOGIN_SUCCESS, payload: { ...user } })
  }
  catch (e) {
    console.log(TAG, "error: " + e)
    dispatch({ type: type.LOGIN_ERROR, payload: "Try again" })
  }
}

export const loginSocial = payload => async dispatch => {

  dispatch({ type: type.LOGIN_LOADING })

  try {
    const res = await userApi.loginSocial(payload);

    axios.defaults.headers.common['Authorization'] = res.data.token.token

    const { data } = await userApi.userFamily();

    const user = {
      ...res.data.user,
      position: { latitude: 0, longitude: 0 },
      token: res.data.token.token,
      refresh_token: res.data.token.refresh_token,
      family: [data.user, ...data.parents]
    }
    dispatch({ type: type.LOGIN_SUCCESS, payload: { ...user } })
  }
  catch (e) {
    console.log(TAG, "error: " + e)
    dispatch({ type: type.LOGIN_ERROR, payload: "Try again" })
  }
}

export const changeInterface = type => dispatch => {
  dispatch({ type: type.CHANGE_INTERFACE, payload: type })
}

export const regenerateToken = () => async dispatch => {
  userApi.getToken().then(res => {

  }).catch(e => {

  })
}

export const makeRating = (payload) => async dispatch => {
  result = await userApi.rating({ ratings: payload });
}

export const makeFeedback = (payload) => async dispatch => {
  result = await userApi.feedback(payload);
  dispatch({ type: "NOTIFICATION_CLEAR", payload: {} })
}

export const create = payload => async dispatch => {
  dispatch({ type: type.LOGIN_LOADING });
  console.log(TAG, payload)
  userApi.create(payload)
    .then(res => {
      dispatch({ type: type.CREATE_SUCCESS });
      console.log(TAG, res)
    }).catch(e => {
      dispatch({ type: type.CREATE_ERROR, payload: "Try again" });
      console.log(TAG, e)
    })
}

export const createFamily = payload => async dispatch => {
  dispatch({ type: type.CREATE_FAMILY_LOADING })

  userApi.createFamily(payload).then(res => {
    console.log(TAG, res)

    userApi.userFamily().then(({ data }) => {
      dispatch({ type: type.FILL_FAMILY, payload: data.parents })
    })

    dispatch({ type: type.CREATE_FAMILY_SUCCESS })
  }).catch(e => {
    console.log(TAG, e)
    dispatch({ type: type.CREATE_FAMILY_ERROR })
  })

}

export const changeEnvToBarber = () => async dispatch => {
  dispatch({ type: type.CHANGE_ENV, payload: UI_TYPE.BARBER })
}

export const setUser = (user) => dispatch => {
  dispatch({ type: type.SET_USER, payload: user })
}

export const updateUser = user => async dispatch => {
  try {
    const us = await userApi.updateUser(user);
    return us.data
  } catch (e) {
    console.log("error update user", e);
    return null;
  }
}

export const removeUser = payload => async dispatch => {
  // dispatch({type: type.CREATE_FAMILY_LOADING})
  console.log(TAG, payload)

  userApi.removeFamily(payload).then(res => {
    console.log(TAG, res)

    userApi.userFamily().then(({ data }) => {
      dispatch({ type: type.FILL_FAMILY, payload: data.parents })
    })

    //dispatch({type: type.CREATE_FAMILY_SUCCESS})
  }).catch(e => {
    console.log(TAG, e)
    // dispatch({type:type.CREATE_FAMILY_ERROR})
  })

}

export const logout = () => dispatch => {
  dispatch({ type: type.LOGOUT })
}

export const refreshToken = (refreshToken) => async (dispatch, getState) => {
  try {
    const data = await userApi.refresh_token(refreshToken)
    const { token, refresh_token } = data;
    if (!token || refresh_token) return false;
    console.log("token refreshed", { token, refresh_token })
    dispatch({ type: type.REFRESH_TOKEN, payload: { token, refresh_token } })
  } catch (e) {
    await Promise.reject(e)
  }
}

export const getProfile = () => async dispatch => {
  userApi.getProfile().then(res => {
    console.log("get_profile", res)
  }).catch(e => {
    console.log("get_profile", e)
  })
}