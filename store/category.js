import * as categoryService from "../services/categoryService"

export const type = {
  CATEGORY_LOADING: "CATEGORY_LOADING",
  CATEGORY_LIST_SUCCESS: "CATEGORY_LIST_SUCCESS",
  CATEGORY_LIST_ERROR: "CATEGORY_LIST_ERROR",
}

const initialState = {
  loading: false,
  error: null, 
  list: []
}

export default categoryReducer = (state=initialState, action) => {
  switch(action.type){
    case type.CATEGORY_LOADING:
      return {
        ...state, 
        loading: true, 
      }
    case type.CATEGORY_LIST_SUCCESS:
      return {
        ...state, 
        loading: false, 
        list: action.payload, 
        error: null,
      }
    case type.CATEGORY_LIST_ERROR:
      return {
        ...state, 
        loading: false, 
        error: action.payload
      }

    default: 
      return state
  }
}

export const listCategory = payload => async dispatch => {
  dispatch({ type: type.CATEGORY_LOADING });
  categoryService.list(payload)
  .then(res=>{
    //console.log(res)
    dispatch({type:type.CATEGORY_LIST_SUCCESS, payload: res.data})
  }).catch(e=>{
    //console.log(e)
    dispatch({type:type.CATEGORY_LIST_ERROR, payload: res.data})
  })
}