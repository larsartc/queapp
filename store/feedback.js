import * as feedbackService from "../services/feedbackService"

export const type = {
  FEEDBACK_LOADING: "FEEDBACK_LOADING",
  FEEDBACK_CREATE_SUCCESS: "FEEDBACK_CREATE_SUCCESS",
  FEEDBACK_CREATE_ERROR: "FEEDBACK_CREATE_ERROR",
}

const initialState = {
  loading: false,
  error: null,
  list: []
}

export default feedbackReducer = (state = initialState, action) => {
  switch (action.type) {
    case type.FEEDBACK_LOADING:
      return {
        ...state,
        loading: true,
      }
    case type.FEEDBACK_CREATE_SUCCESS:
      return {
        ...state,
        loading: false,
        list: action.payload,
        error: null,
      }
    case type.FEEDBACK_CREATE_ERROR:
      return {
        ...state,
        loading: false,
        error: action.payload
      }

    default:
      return state
  }
}

export const createFeedback = payload => async dispatch => {
  dispatch({ type: type.FEEDBACK_LOADING });
  feedbackService.create(payload)
    .then(res => {
      //console.log(res)
      dispatch({ type: type.FEEDBACK_CREATE_SUCCESS, payload: res.data })
    }).catch(e => {
      //console.log(e)
      dispatch({ type: type.FEEDBACK_CREATE_ERROR, payload: res.data })
    })
}

export const systemFeedBack = payload => async dispatch => {
  try {
    const result = await feedbackService.systemFeedback(payload)
    console.log("feedback", result)
    return result.data;
  } catch (e) {
    throw e;
  }
}