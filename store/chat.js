import axios from "../services"
import * as chatService from "../services/chatService";

export const type = {
  LOADING: "LOADING",
  GET_MESSAGES: "GET_MESSAGES"
}

const initialState = {
  chats: [],
  loading: false,
  error: null,
}

export default chatReducer = (state = initialState, action) => {
  switch (action.type) {
    case type.LOADING:
      return {
        ...state,
        loading: true,
      }
    case type.GET_MESSAGES: {
      return {
        ...state,
        chats: action.payload
      }
    }
    default:
      return state
  }
}

export const getMessages = () => async (dispatch) => {
  try {
    const chats = await chatService.getMessages()
    console.log("chats", chats)
    dispatch({ type: type.GET_MESSAGES, payload: chats.data })
  } catch (e) {
    console.log("chats", e)
  }
};

export const createMessage = (to = null, message) => async (dispatch) => {
  try {
    await chatService.createMessage(to, message)
    const chats = await chatService.getMessages()
    dispatch({ type: type.GET_MESSAGES, payload: chats.data })
  } catch (e) {
    console.log("chats", e)
  }
};

// actions here