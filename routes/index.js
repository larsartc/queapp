
import { createStackNavigator, createAppContainer } from "react-navigation";

//IMPORT ALL PAGES OF APPLICATION
import SignInFlowScreen from '../pages/SignIn/routes';
import ShopFlowScreen from '../pages/Shops/routes';
import SignUpFlowScreen from '../pages/SignUp/routes';
import CutsScreen from '../pages/Cuts/routes';
import AppointmentsScreen from '../pages/Appointment/routes';
import HelpScreen from '../pages/Help/routes';
import FavoritesScreen from '../pages/Favorites/routes';
import ProfileScreen from '../pages/Profile/routes';
import ChatScreen from '../pages/Chat/routes';
import FamilyMember from '../pages/FamilyMember/routes';
import BarberFlow from '../pages/Barber/routes'
import ChangeToBarber from '../pages/ChangeToBarber/routes'
import ProfileBarber from '../pages/ProfileBarber/routes'
import BarberRatings from '../pages/BarberRatings/routes'
import ImageSliderView from '../pages/ImageSliderView/routes'
import OwnersFlow from '../pages/Owner/routes'
import SplashScreen from '../pages/Splash/routes'

const DefaultStack = createStackNavigator(
    {
        ...SignInFlowScreen,
        ...SignUpFlowScreen,
        ...ShopFlowScreen,
        ...CutsScreen,
        ...AppointmentsScreen,
        ...HelpScreen,
        ...FavoritesScreen,
        ...ProfileScreen,
        ...ChatScreen, 
        ...FamilyMember,
        ...BarberFlow,
        ...ChangeToBarber,
        ...ProfileBarber,
        ...BarberRatings,
        ...ImageSliderView,
        ...OwnersFlow,
        ...SplashScreen,
    }, { initialRouteName: 'SplashScreen', headerMode: 'none' });

export default createAppContainer(DefaultStack);

//ShopsScreen
