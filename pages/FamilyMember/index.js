import React from "react";
import CustomInput from "../../components/CustomInput";
import { TouchableOpacity, Image, Text, Alert } from "react-native"
import { View, Content, Thumbnail } from "native-base";

//Redux
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { createFamily } from '../../store/user'

import CustomImageCircle from 'que/components/CustomImageCircle'
import ImagePicker from 'react-native-image-picker'
import CustomButton from 'que/components/CustomButton'
import { ButtonSignInDefaultStyle } from 'que/assets/defaultStyle';
import HeaderBar from "que/components/HeaderBar"
import { withTheme } from '../../utils/themeProvider'
import SelectSimple from './components/selectSimple'
import { uploadImage } from '../../utils/upload'

// style 
import { ShopContainerProfile, InformationsAccount } from "../Profile/styled";

const configImagePicker = {
  title: 'Choose a photo',
  storageOptions: {
    skipBackup: true,
    path: 'images'
  }
}

class FamilyMember extends React.Component {

  state = {
    name: null,
    member: null,
    temp_photo: null,
    listMember: [
      'son',
    ]
  }

  componentDidMount = () => {

  }

  handleField = (field, val) => {
    this.setState({
      ...this.state,
      [field]: val
    })
  }

  saveMember = () => {
    if (!this.state.name) {
      return Alert.alert("Ops", "Fill the name!")
    }

    if (!this.state.member) {
      return Alert.alert("Ops", "Fill the family member!")
    }

    if (!!this.state.temp_photo) {
      uploadImage(this.state.response)
        .then(resp => resp.json())
        .then(resp => {
          this.props.createFamily({
            name: this.state.name,
            photo: resp.data,
            member: this.state.type,
          })
          this.props.navigation.pop()
        })
    } else {
      this.props.createFamily({
        name: this.state.name,
        photo: "",
        member: this.state.type,
      })
      this.props.navigation.pop()
    }
  }

  pickImage = () => {
    ImagePicker.showImagePicker(configImagePicker, response => {
      if (response.error) {
        alert('Error. ', response.error)
      } else {
        const source = { uri: response.uri }
        if (response.uri != null) {
          this.setState({
            temp_photo: source,
            temp_photo_path: response.path,
            response: response
          })
        }
      }
    })
  }

  render() {
    const { name, member, temp_photo } = this.state
    const { theme } = this.props

    return (
      <View
        style={{
          backgroundColor: theme.primary,
          flex: 1,
        }}
      >

        <View style={{ paddingHorizontal: 16 }}>
          <HeaderBar
            hideTitle={true}
            rightIcon={require('que/assets/images/close_button_chat_icn.png')}
            onPressRightIcon={() => this.props.navigation.pop()}
            lightMode
          />
          <Text style={{ color: "white", textAlign: 'center' }}>FAMILY MEMBER</Text>
        </View>

        <InformationsAccount>

          <TouchableOpacity onPress={() => this.pickImage()}>
            <CustomImageCircle width={90} height={90}>
              <Thumbnail large source={temp_photo} />
              {
                (temp_photo == null || temp_photo == undefined) &&
                <Image
                  resizeMode={"contain"}
                  source={require('que/assets/images/perfil_placeholder_sign_in_icn.png')}
                  style={{ height: 16, width: 16, marginTop: -80, marginLeft: "auto", marginRight: "auto" }}
                />
              }
            </CustomImageCircle>
          </TouchableOpacity>

          <Content padder>
            <CustomInput
              image={require("que/assets/images/name_sign_in_icn.png")}
              placeholder={"Name…"}
              onChange={val => this.handleField("name", val)}
              value={name}
            />

            <SelectSimple
              leftImage={require("que/assets/images/add_member_icn.png")}
              list={this.state.listMember}
              onSelect={val => this.handleField("member", val)}
              label={"Select a member family"}
            />

          </Content>

          <View style={{ flex: 1, justifyContent: "center", alignContent: "center", alignItems: "center" }}>
            <ButtonSignInDefaultStyle>
              <CustomButton
                opacity={1}
                onPress={() => this.saveMember()}
                text={'SAVE'} />
            </ButtonSignInDefaultStyle>
          </View>
        </InformationsAccount>
      </View>
    );
  }
}

const mapStateToProps = store => ({
  user: store.userReducer.user,
})

const mapDispatchToProps = dispatch =>
  bindActionCreators({
    createFamily,
  }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(withTheme(FamilyMember))