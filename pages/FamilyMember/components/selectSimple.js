import React from 'react'
import { View, Text, TouchableOpacity, StyleSheet, Image } from 'react-native'

export default SelectSimple = ({
  list = [],
  onSelect = () => { },
  label = "Select",
  leftImage
}) => {
  const [active, setActive] = React.useState(false)
  const [activeLabel, setActiveLabel] = React.useState(label)

  const [item, setItem] = React.useState("")

  const selectItem = (val) => {
    onSelect(val)
    setActive(false)
    setActiveLabel(val)
    setItem(val)
  }

  return (
    <View style={style.base}>
      <TouchableOpacity style={style.baseTouch} onPress={() => setActive(!active)}>
        {leftImage && (
          <Image
            source={leftImage}
            resizeMode={"contain"}
            style={{
              height: 25,
              width: 25,
              marginRight: 10
            }}
          />
        )}
        <Text style={!item.length ? [style.label, style.labelInactive] : style.label}>{activeLabel}</Text>
          <Image
            source={require("que/assets/images/arrow_dropdown_blank_icn.png")}
            resizeMode={"contain"}
            style={{
              height: 25,
              width: 25,
            }}
          />
      </TouchableOpacity>

      {active && (
        <View style={style.popup}>
          {list.map(val => {
            return (
              <TouchableOpacity onPress={() => selectItem(val)} style={style.popupItem}>
                <Text style={[style.label, style.marginLeft]}>{val}</Text>
              </TouchableOpacity>
            )
          })}
        </View>
      )}
    </View>
  )

}

const style = StyleSheet.create({
  marginLeft: {
    marginLeft: 30,
  },
  base: {
    flex: 1,
    marginTop: 16,
  },
  baseTouch: {
    flex: 1,
    flexDirection: 'row',
    alignItems: "center",
    paddingVertical: 5,
    paddingHorizontal: 5,
    borderBottomColor: 'white',
    borderBottomWidth: .5,
  },
  label: {
    color: 'white',
    fontSize: 13,
    flex: 1,
  },
  labelInactive: {
    color: 'rgba(255,255,255,0.6)'
  },
  popup: {
    flex: 1,
    marginTop: 36,
    width: '100%',
    position: 'absolute',
    padding: 10,
  },
  popupItem: {
    paddingVertical: 10
  }
})