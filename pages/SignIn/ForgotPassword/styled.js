import styled from 'styled-components';
import { Dimensions } from 'react-native'
import {ButtonSignInDefaultStyle} from 'que/assets/defaultStyle';

export const ViewBG = styled.View`
    width: 100%;
    height: 100%;
    background-color: rgba(0, 0, 0, 0.5)
`

export const ViewContainer = styled.View`
    ${'' /* height: ${Dimensions.get('window').height};
    width: 100%; */}
    background-color: white;
    align-items: center;
    justify-content: center;
    margin-top: 200;
    margin-left:30;
    margin-right:30;
    padding-bottom:40;
    padding-top: 5;
`

export const ViewPage = styled.View`
    ${'' /* height: 60%; */}
    width: 90%;
`

export const ViewClose = styled.View`
    height: 40;
    width:100%;
    padding-right: 20;
    padding-top: 10;
    justify-content: center;
`

export const TouchableOpacity = styled.TouchableOpacity`
    display: flex;
    flex-direction: row;
    justify-content: flex-end;
`

export const CloseImage = styled.Image`
    height: 15;
    width: 15;
`

export const Text = styled.Text`
    text-align: center;
    color: #3D2E52;
    margin-bottom: 30;
`

export const ViewInput = styled.View`
    width: 100%;
    margin-bottom: 10%;
`
export const ButtonSignInContainer = styled(ButtonSignInDefaultStyle)`
    align-self: center;
`