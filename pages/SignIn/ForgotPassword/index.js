import React, { useState } from 'react';
import {
  ViewContainer, ViewPage, Text, ViewClose,
  TouchableOpacity,
  CloseImage, ViewInput, ButtonSignInContainer, ViewBG
} from './styled';
import { Modal} from 'react-native'
import CustomButton from 'que/components/CustomButton'
import CustomInput from 'que/components/CustomInput';
import { colors } from 'que/components/base';

const ForgotPassword = (props) => {

  const [email, setEmail] = useState('')

  resendPass = () => {
    props.resendPass(email)
  }

  return (
    <ViewBG>
      <Modal
        animationType="slide"
        transparent={true}
        visible={props.open}
        onRequestClose={props.close}
      >
        <ViewContainer>
          <ViewClose>
            <TouchableOpacity onPress={props.close}>
              <CloseImage source={require('que/assets/images/close_forgot_password_sign_in_icn.png')} />
            </TouchableOpacity>
          </ViewClose>
          <ViewPage>

            <Text style={{ marginTop: 20 }}>FORGOT YOUR PASSWORD?</Text>
            <ViewInput>
              <CustomInput
                imageStyle={{ height: 17, width: 16 }}
                image={require('que/assets/images/email_forgot_password_sign_in_icn.png')}
                placeholder={'Type your email...'}
                lineColor={colors.darkGray}
                textColor={colors.darkGray}
                placeholderColor={colors.darkGray}
                onChange={(e) => setEmail(e)}
              />
            </ViewInput>
            <Text style={{ color: colors.darkGray, fontSize: 12 }}>Type your email, click on reset and you will receive an email with instructions to reset your password</Text>

            <ButtonSignInContainer>
              <CustomButton onPress={() => resendPass()} text={'RESET'} opacity={1} borderColor={colors.darkGray} color={colors.darkGray} />
            </ButtonSignInContainer>

          </ViewPage>
        </ViewContainer>
      </Modal>
    </ViewBG>
  )
}

export default ForgotPassword;

