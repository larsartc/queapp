import styled from 'styled-components'
import { ButtonSignInDefaultStyle } from 'que/assets/defaultStyle';

export const ContainerSignin = styled.View`
    width: 100%;
    height: 100%;
    
    display: flex;
    flex-direction: column;
    align-items: center;
    justifyContent: center;
    padding-bottom:40;
`
export const MarginDefault = styled.View`
    margin-top: 10%;
`

export const ButtonSignInContainer = styled(ButtonSignInDefaultStyle)``

export const CircleContainer = styled.View`
    margin-top: 10%;
    display: flex;
    flex-direction: row;
    justify-content: center;
`

export const SkipContainer = styled.TouchableOpacity`
    width: 50;
    height: 20;
    align-self: center;
    margin-bottom: 30;
    margin-top: -60;
`

export const Skip = styled.Text`
    margin: 0 auto;
    color: white;
`