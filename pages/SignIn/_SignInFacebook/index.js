import React, { useState } from 'react';
import CustomBackgroundImage from 'que/components/CustomBackgroundImage'
import CustomImageCircle from 'que/components/CustomImageCircle'
import CustomInput from 'que/components/CustomInput'
import CustomButton from 'que/components/CustomButton'
import Logo from 'que/components/Logo';
import { LogoContainer, PageTitle, ImageContainer, Icon, PageMargin, ButtonSignInContainer, MarginDefault } from './styled'
import CheckTerms from "que/components/checkTerms"

const SignInFacebook = (props) => {

    const [terms, setTerms] = useState(false);

    return (
        <CustomBackgroundImage>
            <LogoContainer>
                <Logo />
            </LogoContainer>
            <PageTitle>
                SIGN IN WITH FACEBOOK
            </PageTitle>
            <ImageContainer>
                <CustomImageCircle>
                    <Icon source={require('que/assets/images/email_placeholder_icn.png')} style={{ height: 25, width: 25 }} />
                </CustomImageCircle>
            </ImageContainer>
            <PageMargin>
                <CustomInput
                    image={require('que/assets/images/name_sign_in_icn.png')}
                    onChange={()=>{}}
                />

                <CheckTerms checkAction={()=> setTerms(!terms)} checked={terms} />
            </PageMargin>
            <MarginDefault>
                <ButtonSignInContainer>
                    <CustomButton text={'SIGN IN'} opacity={'1.0'} />
                </ButtonSignInContainer>
            </MarginDefault>
        </CustomBackgroundImage>
    )
}

export default SignInFacebook;