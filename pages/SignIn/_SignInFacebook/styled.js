import styled from 'styled-components';
import {ScreenTitle, MarginDefaultUnAuthenticatedFlow, ButtonSignInDefaultStyle} from 'que/assets/defaultStyle';
import { ifIphoneX } from 'react-native-iphone-x-helper'

export const LogoContainer = styled.View`
    ${ifIphoneX({marginTop: 60},{marginTop: 20})}
    align-self: center;
    margin-bottom: 15;
`
export const ImageContainer = styled.View`
   margin-top: 15;
   margin-bottom: 25;
`

export const Icon = styled.Image`
    align-self: center;
`

export const PageMargin = styled(MarginDefaultUnAuthenticatedFlow)``


export const PageTitle = styled(ScreenTitle)`text-align: center;`

export const ButtonSignInContainer = styled(ButtonSignInDefaultStyle)`
    /* height: 100%; */
`

export const MarginDefault = styled.View`
    margin-top: 10%;
    align-self: center;
`