
import SignInScreen from './index.js';
import SignInEmailScreen from './SignInEmail';
// import SignInFacebookScreen from './SignInFacebook';

export default {
    SignIn: SignInScreen,
    SignInEmail: SignInEmailScreen,
    // SignInFacebook: SignInFacebookScreen
};