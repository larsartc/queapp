import React, { useState, Component } from 'react';
import { Keyboard, ActivityIndicator, Alert, StatusBar, KeyboardAvoidingView, ScrollView, View } from 'react-native';
import CustomBackgroundImage from 'que/components/CustomBackgroundImage'
import CustomImageCircle from 'que/components/CustomImageCircle';
import Logo from 'que/components/Logo'
import CustomInput from 'que/components/CustomInput';
import CustomButton from 'que/components/CustomButton';
import ModalForgot from 'que/pages/SignIn/ForgotPassword';
import LoadingDefault from 'que/components/LoadingDefault';
import LottieView from 'lottie-react-native'
import AsyncStorage from "@react-native-community/async-storage"
import * as ASYNCTAG from "../../../utils/storeNames"
import { SignInEmailContainer, LogoContainer, Title, EmailIcon, EmailIconContainer, PageMargin, MarginInput, ButtonSignInContainer, MarginDefault } from './styled';

import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { login, forgotPassword } from '../../../store/user'
import { colors } from 'que/components/base'

class SignInEmail extends Component {
    state = {
        email: '',
        password: '',
        modalOpen: false,
    }

    makeLogin = () => {
        //this.animation.play()

        const { email, password } = this.state;
        if (email != '' && password != '') {
            AsyncStorage.getItem(ASYNCTAG.TAG_PUSH_ID).then(val => {
                if (val !== null) { player_id = JSON.parse(val).userId }
                this.props.login({ email, password, player_id })
                Keyboard.dismiss()
            })
        } else {
            alert('Oops, some fields are wrong!')
        }
    }

    resendPass = email => {
        this.props.forgotPassword(email)
            .then((e) => {

            })
            .catch((e) => {
                alert("Error!")
            });
    }

    componentWillReceiveProps = nextProps => {
        if (nextProps.user != null) {
            if (nextProps.user._id != null) {
                this.props.navigation.navigate("ShopsScreen")
                return
            }
        }

        if (nextProps.error != null) {
            Alert.alert("Error", nextProps.error)
        }
    }

    toggleModal = () => {
        this.setState({ modalOpen: !this.state.modalOpen })
    }

    handleField = (field, value) => {
        this.setState({ ...this.state, [field]: value })
    }

    render() {
        const { email, password, modalOpen } = this.state;
        const { loading } = this.props

        let activeButton = email != '' && password != '' && !modalOpen

        return (
            <CustomBackgroundImage>
                {modalOpen &&
                    <ModalForgot
                        open={modalOpen}
                        close={() => this.toggleModal()}
                        resendPass={(email) => this.resendPass(email)} />
                }

                {/* <StatusBar translucent backgroundColor={'rgba(0,0,0,.1)'} barStyle="light-content" /> */}

                <KeyboardAvoidingView>
                    <ScrollView>
                        <SignInEmailContainer>
                            <LogoContainer>
                                <Logo />
                            </LogoContainer>
                            <Title>SIGN IN WITH E-MAIL</Title>
                            <EmailIconContainer>
                                <CustomImageCircle>
                                    {/* <LottieView source={require('que/assets/animations/heart.json')} ref={animation=>{this.animation = animation}} loop={false} /> */}
                                    <EmailIcon source={require('que/assets/images/email_placeholder_icn.png')} style={{ height: 25, width: 25 }} />
                                </CustomImageCircle>
                            </EmailIconContainer>
                            <PageMargin>
                                <CustomInput
                                    autoCapitalize={"none"}
                                    onChange={(value) => this.handleField("email", value)}
                                    image={require('que/assets/images/email_placeholder_icn.png')}
                                    placeholder={'Type your email…'}
                                    value={email}
                                />
                                <MarginInput />
                                <CustomInput
                                    autoCapitalize={"none"}
                                    onChange={(value) => this.handleField("password", value)}
                                    image={require('que/assets/images/password_sign_in_icn.png')}
                                    rightImage={require('que/assets/images/forgot_password_sign_in_icn.png')}
                                    eyeImage={true}
                                    password={true}
                                    placeholder={'Type your password…'}
                                    value={password}
                                    rightImageOnPress={() => this.toggleModal()}
                                />
                                <MarginInput />
                            </PageMargin>
                            <MarginDefault>
                                <ButtonSignInContainer>
                                    {loading ? (
                                        <LoadingDefault color={colors.white} />
                                    ) :
                                        <CustomButton
                                            opacity={activeButton ? 1 : .6}
                                            onPress={() => this.makeLogin()}
                                            text={'SIGN IN'} />
                                    }
                                </ButtonSignInContainer>
                            </MarginDefault>
                        </SignInEmailContainer>
                    </ScrollView>
                </KeyboardAvoidingView>
            </CustomBackgroundImage>
        )
    }
}

const mapStateToProps = store => ({
    user: store.userReducer.user,
    loading: store.userReducer.loading,
    error: store.userReducer.error,
})

const mapDispatchToProps = dispatch =>
    bindActionCreators({
        login,
        forgotPassword,
    }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(SignInEmail)