import styled from 'styled-components';
import { ScreenTitle, MarginDefaultUnAuthenticatedFlow, MarginInputsUnAuthenticatedFlow, ButtonSignInDefaultStyle } from 'que/assets/defaultStyle';
import { ifIphoneX } from 'react-native-iphone-x-helper'

export const SignInEmailContainer = styled.View`
    height: 100%;
    width: 100%;
    align-items: center;
`

export const BGContainer = styled.View`
    width: 100%;
    height: 100%;
`


export const LogoContainer = styled.View`
    ${ifIphoneX({ marginTop: 60 }, { marginTop: 20 })}
`

export const Title = styled(ScreenTitle)`
    text-align: center;
    margin-top: 10%;
`
export const EmailIconContainer = styled.View`
   margin-top: 15;
   margin-bottom: 25;
`

export const EmailIcon = styled.Image`
    align-self: center;
`

export const PageMargin = styled(MarginDefaultUnAuthenticatedFlow)``

export const MarginInput = styled(MarginInputsUnAuthenticatedFlow)``

export const ButtonSignInContainer = styled(ButtonSignInDefaultStyle)`
    height: 100
`

export const MarginDefault = styled.View`
    margin-top: 10%;
`