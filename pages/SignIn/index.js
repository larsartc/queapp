import React from "react";

import AsyncStorage from "@react-native-community/async-storage"
import * as ASYNCTAG from "../../utils/storeNames"

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { login, loginSocial, logout} from '../../store/user'

import { AuthFacebook } from "../../services/facebookLogin"
import { GoogleSignin, statusCodes } from '@react-native-community/google-signin'
import configEnv from '../../config.env.json'

import { withTheme } from '../../utils/themeProvider'

// components
import { Platform, Alert } from 'react-native'
import { Drawer } from "native-base";
import CustomBackgroundImage from "que/components/CustomBackgroundImage";
import CustomImageCircle from "que/components/CustomImageCircle";
import Logo from "que/components/Logo";
import CustomButton from "que/components/CustomButton";
import SideBar from "que/components/Sidebar";
import LottieView from 'lottie-react-native'
import LinkRoundIcon from "que/components/LinkRoundIcon"
import { StatusBar, TouchableOpacity, Text, View } from "react-native"
import LoadingDefault from 'que/components/LoadingDefault'
import { StackActions, NavigationActions } from 'react-navigation';

// styles
import {
  ContainerSignin,
  ButtonSignInContainer,
  CircleContainer,
  Skip,
  MarginDefault,
  SkipContainer
} from "./styled";


class SignIn extends React.Component {

  componentDidMount = () => {
    this.props.setTheme("CUSTOMER")
    this.props.logout();
  }

  componentWillReceiveProps = nextProps => {
    if (nextProps.user != null) {
      if (nextProps.user._id != null) {

        if (!!nextProps.user.shopsWorking[0]) {
          this.props.setTheme("BARBER")
        }


        const resetAction = StackActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({ routeName: 'ShopsScreen' })],
        });
        return this.props.navigation.dispatch(resetAction);
      }
    }
  }

  singinFacebook = async () => {

    const response = await AuthFacebook();

    if (response.error) {
      console.log(response)
      //this.setState({ error: response.error, loading: false });
      Alert.alert("Auth Facebook", response.error)
      return false
    }

    let player_id = ""
    AsyncStorage.getItem(ASYNCTAG.TAG_PUSH_ID).then(val => {
      if (val !== null) { player_id = JSON.parse(val).userId }
      const data = {
        //id: response.user.id,
        email: response.user.email,
        name: response.user.name,
        image: response.user.picture.data.url,
        social_media: "facebook",
        accessToken: response.accessToken,
        player_id: player_id
      }

      // TODO continue ...
      console.log(data)
      this.props.loginSocial(data)
    })
  }

  signinGoogle = async () => {
    try {
      await GoogleSignin.hasPlayServices()

      await GoogleSignin.configure({
        //scopes: [],
        androidClientId: configEnv.androidClientId,
        iosClientId: configEnv.iosClientId,
        webClientId: configEnv.webClientId,
        offlineAccess: false
      })

      const userInfo = await GoogleSignin.signIn();

      if (userInfo.user.email != "" && userInfo.user.name != "") {

        let player_id = ""
        AsyncStorage.getItem(ASYNCTAG.TAG_PUSH_ID).then(val => {
          player_id = JSON.parse(val).userId
          const payloadGoogle = {
            //id: userInfo.user.id,
            email: userInfo.user.email,
            name: userInfo.user.name,
            image: userInfo.user.photo,
            social_media: "google",
            accessToken: null,
            player_id: player_id
          }

          //TODO MAKE THE FUNCTION TO GO HEAD SIGNIN
          this.props.loginSocial(payloadGoogle)
        })
      } else {
        Alert.alert("Google Auth", "Google sign in not enabled for your account.")
      }

    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        Alert.alert("Google Auth", "Google sign in was canceled.")
      } else if (error.code === statusCodes.IN_PROGRESS) {
        Alert.alert("Google Auth", "Google sign in in progress.")
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        Alert.alert("Google Auth", "Google sign in unsupported, update your smartphone.")
      } else {
        Alert.alert("Google Auth", "Google sign in have a problem. Try again.")
      }
    }
  }

  render() {
    const { navigation, loading } = this.props
    return (
      <CustomBackgroundImage style={{ backgroundColor: '#3D2E52' }}>
        <StatusBar translucent backgroundColor="transparent" barStyle="light-content" />

        <ContainerSignin>

          <Logo />

          {!loading ? (
            <>
              <MarginDefault>
                <ButtonSignInContainer>
                  <CustomButton
                    text={"SIGN UP"}
                    color={"white"}
                    opacity={"1.0"}
                    onPress={() => navigation.navigate("SignUp")}
                  />
                </ButtonSignInContainer>
              </MarginDefault>

              <CircleContainer>
                <LinkRoundIcon press={() => this.singinFacebook()} icon={require("../../assets/images/facebook_sign_in_icn.png")} />
                <LinkRoundIcon press={() => navigation.navigate("SignInEmail")} icon={require("../../assets/images/email_placeholder_icn.png")} />
                <LinkRoundIcon press={() => this.signinGoogle()} icon={require("../../assets/images/google_sign_in_icn.png")} />
              </CircleContainer>

            </>
          ) : (
              <LoadingDefault color={"#fff"} />
            )}

        </ContainerSignin>


        <SkipContainer onPress={() => navigation.navigate("ShopsScreen")}>
          <Skip>SKIP</Skip>
        </SkipContainer>

      </CustomBackgroundImage>
    );
  }
}

const mapStateToProps = store => ({
  loading: store.userReducer.loading,
  erro: store.userReducer.error,
  user: store.userReducer.user,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      login,
      loginSocial,
      logout,
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(withTheme(SignIn));
