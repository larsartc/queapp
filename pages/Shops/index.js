import React from 'react'
import { Platform, PermissionsAndroid, View, StatusBar, ToastAndroid } from 'react-native'
import { Drawer } from 'native-base'
import SideBar from '../../components/Sidebar'
import FilterSideBar from '../../components/FilterSideBar'
import SearchBar from "../../components/SearchBar"
import HeaderBar from "../../components/HeaderBar"
import Geolocation from 'react-native-geolocation-service'
import MapView from "../../components/MapView"
import ListView from "../../components/ListView"
//Redux
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import {
  listShops,
  favorite,
  unFavorite,
  updateListShop,
  setFilters,
  setShopOpened,
  shopsFilter,
  setSearch
} from '../../store/shop'
import { listCut } from '../../store/cut'
import { listHelps } from '../../store/help'
import { listCategory } from "../../store/category"
import { getProfile, setUser, makeRating, makeFeedback } from '../../store/user'
import { closeReview } from '../../store/appointment'
import moment from "moment"
import RightSideBar from "que/components/rightSideBar"
import ReviewNotification from '../Appointment/reviewNotification'

class Shops extends React.Component {
  constructor(props) {
    super(props)
    this.drawer = React.createRef()
  }

  state = {
    shop: {},
    view: 'map',
    search: '',
    position: {
      latitude: 0,
      longitude: 0,
    },
    sidebar: "menu",
    showMenu: false,
    filterBar: false,
  }

  formatFilters = (position = { ...this.props.user.position }) => {
    const { filters } = this.props;
    let filter = {}
    let obj = { ...position }

    if (filters.availableNow || filters.availableOn.check) {
      if (filters.availableNow) {
        filter.available = moment().unix()
      } else {
        filter.available = moment(filters.availableOn.value, 'MMMM DD hh:mm A').unix()
      }
    }
    if (filters.coast.check) {
      filter.cost = filters.coast.count + 1
    }
    if (filters.rating.check && !!filters.rating.types.length) {
      filter.ratings = filters.rating.types.map((type) => {
        const cat = this.props.categories.find((category) => category._id == type._id);
        return {
          category: cat.name,
          mid_rate: type.mid_rate + 1
        }
      });
    }
    filter.minDistance = 0
    filter.maxDistance = filters.distance[0]
    obj.order = filters.orderBy
    if (Object.keys(filter).length > 0) obj.filter = filter;

    console.log("shop_filter", obj)
    return obj;
  }

  openShop = (id, shop) => {
    this.props.setShopOpened(shop)
    this.props.navigation.navigate('ShopViewer', { shop_id: id })
  }

  hasLocationPermission = async () => {
    if (Platform.OS === 'ios' || (Platform.OS === 'android' && Platform.Version < 23)) {
      return true;
    }

    const hasPermission = await PermissionsAndroid.check(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
    );

    if (hasPermission) return true;

    const status = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);

    if (status === PermissionsAndroid.RESULTS.GRANTED) return true;

    if (status === PermissionsAndroid.RESULTS.DENIED) {
      ToastAndroid.show('Location permission denied by user.', ToastAndroid.LONG);
    } else if (status === PermissionsAndroid.RESULTS.NEVER_ASK_AGAIN) {
      ToastAndroid.show('Location permission revoked by user.', ToastAndroid.LONG);
    }

    return false;
  }


  getLocation = async () => {
    const hasLocationPermission = await this.hasLocationPermission();

    if (!hasLocationPermission) return;

    Geolocation.getCurrentPosition(
      (position) => {
        const { latitude, longitude } = position.coords;
        const user = { ...this.props.user, position: { latitude, longitude } };
        this.props.setUser(user);
        this.props.listShops(this.formatFilters({ latitude, longitude }))
        console.log("gps", position);
      },
      (error) => {
        console.log("errroo!", error)
      },
      { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000, distanceFilter: 50, forceRequestLocation: true }
    );
  }



  componentDidMount = async () => {
    await this.getLocation();
  }

  changeView = () => {
    this.setState({ view: this.state.view == 'map' ? 'cards' : 'map' })
  }

  openFilter = () => {
    this.setState({
      ...this.state,
      filterBar: !this.state.filterBar
    })
    this.props.listShops(this.formatFilters())
  }

  filterSideBar = (field, value) => {
    let filters = { ...this.props.filters };
    switch (field) {
      case "availableNow": {
        if (value && filters.availableOn.check) {
          filters.availableOn.check = false
          filters.availableOn.value = false
        }
        filters[field] = value;
        break;
      }
      case "availableOnCheck": {
        if (value && filters.availableNow) {
          filters.availableNow = false;
        }
        filters.availableOn.check = value;
        break;
      }
      case "availableOnOption": {
        filters.availableOn.value = value;
        break;
      }
      case "coastCheckBox": {
        filters.coast.count = value ? 5 : null;
        filters.coast.check = value;
        break;
      }
      case "coastValue": {
        filters.coast.count = value
        break;
      }
      case "ratingCheck": {
        filters.rating.check = value;
        filters.rating.expanded = !!value;
        break;
      }
      case "ratingValue": {
        if (filters.rating.check) {
          filters.rating.count = value;
          filters.rating.types = filters.rating.types.map((item) => {
            return { ...item, mid_rate: value }
          })
        }
        break;
      }
      case "ratingType": {
        if (filters.rating.types.find((item) => item._id == value._id)) {
          filters.rating.types = filters.rating.types.filter((i) => i._id != value._id)
          break;
        }
        filters.rating.types = [...filters.rating.types, { ...value, mid_rate: 4 }]
        break;
      }
      case "ratingStyle": {
        filters.rating.types = filters.rating.types.map((item) => {
          if (item._id == value.category._id) {
            filters.rating.count = null;
            return { ...item, mid_rate: value.key }
          }
          return item;
        })
        break;
      }
      case "ratingExpanded": {
        if (!filters.rating.check) {
          break;
        }
        filters.rating.expanded = value
        break;
      }
      case "order": {
        filters.orderBy = value;
        break;
      }
      case "slider": {
        filters.distance = value;
        break;
      }
      default:
        return filters;
    }
    this.props.setFilters(filters)
  }

  render() {
    const { position, filterBar, showMenu } = this.state
    const { shops, setSearch, search, closeReview, makeRating, makeFeedback } = this.props

    console.log("gps", position)
    return (
      <>

        <StatusBar translucent backgroundColor="transparent" barStyle="dark-content" />

        <SideBar
          {...this.props}
          navigation={this.props.navigation}
          close={() => this.setState({ showMenu: false })}
          showMenu={showMenu} />

        {filterBar && (
          <RightSideBar close={() => this.openFilter()}>
            <FilterSideBar
              {...this.props}
              {...this.props.filters}
              close={() => this.openFilter()}
              action={(field, value) => this.filterSideBar(field, value)}
            />
          </RightSideBar>
        )}

        <View style={{ flex: 1 }}>
          <HeaderBar
            title={'SHOPS'}
            rightIcon={this.state.view === "map" ?
              require('que/assets/images/cards_view_home_costumer_icn.png') :
              require('que/assets/images/pin_map_view_home_icn.png')}
            onPressRightIcon={() => this.changeView()}
            onPressLeftIcon={() => this.setState({ showMenu: true })}
          />

          <View style={{ flex: 1 }}>
            <SearchBar
              value={search}
              onChangeText={value => setSearch(value)}
              placeholder={'Where to...'}
              openFilter={() => this.openFilter()}
            />

            {
              this.state.view == 'map' ?
                <MapView
                  showMask={showMenu || filterBar ? false : true}
                  position={this.props.user.position}
                  shops={shops}
                  pressAction={(id, shop) => this.openShop(id, shop)}
                  favorite={(obj) => this.props.favorite(obj)}
                  unFavorite={(obj) => this.props.unFavorite(obj)}
                  updateListShop={list => this.props.updateListShop(list)}
                />
                :
                <ListView
                  showMask={showMenu || filterBar ? false : true}
                  pressAction={(id, shop) => this.openShop(id, shop)}
                  favorite={(obj) => this.props.favorite(obj)}
                  unFavorite={(obj) => this.props.unFavorite(obj)}
                  updateListShop={list => this.props.updateListShop(list)}
                  shops={shops}
                />
            }
          </View>
        </View>

        {/* NOTIFICATIONS HANDLER */}
        <ReviewNotification
          makeFeedback={makeFeedback}
          makeRate={makeRating}
          close={closeReview}
          showReviewAppt={!!Object.keys(this.props.notification).length}
          notificationPayload={this.props.notification}
        />
      </>
    )
  }
}

const mapStateToProps = store => ({
  filters: store.shopReducer.filters,
  shops: shopsFilter(store),
  notification: store.appointmentReducer.notification,
  loading: store.shopReducer.loading,
  user: store.userReducer.user,
  search: store.shopReducer.search,
  categories: store.categoryReducer.list
})

const mapDispatchToProps = dispatch =>
  bindActionCreators({
    listShops,
    favorite,
    unFavorite,
    updateListShop,
    listCut,
    listHelps,
    setFilters,
    listCategory,
    setShopOpened,
    getProfile,
    setUser,
    setSearch,
    makeRating,
    makeFeedback,
    closeReview
  }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Shops)