import React from 'react'
import { ImageBackground, TouchableOpacity, View, Image, Text, Alert, StatusBar, SafeAreaView } from 'react-native'
import style from './style'

import Buttons from './components/Buttons'
import Middle from './components/Middle'
import BottomList from './components/BottomList'
import DarkMask from '../../../components/DarkMask'
import DefaultConfigAptt from '../../../components/DefaultConfigAptt'
import ServicesConfigAptt from '../../../components/ServicesConfigAptt'
import StyleSelect from '../../../components/StyleSelect'
import PinCounters from '../../../components/PinCounters'
import LottieView from 'lottie-react-native'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { findShop, findBarber, favAction, favorite, unFavorite } from '../../../store/shop'
import { makeAppointment } from '../../../store/appointment'
import WizardAppointment from '../../Appointment/wizardAppointment'

import { withTheme } from '../../../utils/themeProvider'
import Carousel from 'react-native-snap-carousel'
import Slideshow from 'react-native-image-slider-show'

//Colors
import { colors, BasePop } from 'que/components/base'
import LinearGradient from 'react-native-linear-gradient'

class ShopViewer extends React.Component {

    state = {
        wasFavorited: false,
        type: 'shop',
        createAppointment: false,
        stepAppointment: 0,
        appointmentDay: null,
        appopintmentTime: null,
        entity_id: {
            'shop': 3,
            'barber': 2
        },
        payloadAppointment: null,
        startView: 1,
        posFavorite: 0,
    }

    componentDidMount = async () => {
        this.props.findShop(this.props.navigation.getParam('shop_id', null))
    }

    favorite = () => {
        this.setState({
            wasFavorited: !this.state.wasFavorited
        })
    }

    close = () => {
        this.props.navigation.goBack()
    }

    favActions = () => {

        const { favAction, unFavorite, shop, favorite } = this.props
        const type = this.state.entity_id.shop; // TODO refactore this ... 

        if (shop.favorite) {
            unFavorite({ type, entity_id: shop._id }).then(() => favAction(type))
        } else {
            favorite({ type, entity_id: shop._id }).then(() => favAction(type))
        }
    }

    chooseBarber = id => {
        this.props.findBarber(id)
        this.props.navigation.navigate('BarberScreen')
    }

    // CREATE APPOINTMENT ....
    newAppointment = () => {
        const { isBarber, user } = this.props
        if (user != undefined) {

            if (isBarber == "BARBER") {
                this.props.navigation.push("AppointmentsScreen")
            } else {
                this.setState({
                    ...this.state,
                    createAppointment: true
                })
            }

        } else {
            Alert.alert("SignIn", "Please, make your signin to create a new appointment.")
        }
    }

    changeStepAppointment = () => {
        this.setState({
            ...this.state,
            stepAppointment: this.state.stepAppointment + 1
        })
    }

    backStepAppointment = () => {
        this.setState({
            ...this.state,
            stepAppointment: this.state.stepAppointment - 1
        })
    }

    closeCreateAppoint = () => {
        this.setState({
            ...this.state,
            createAppointment: false,
            stepAppointment: 0
        })
    }

    playHeart = () => {
        if (this.props.shop.favorite) {
            this.heartAnim.play(0, 30)
        } else {
            this.heartAnim.play(30, 0)
        }
    }

    render() {
        const { shop, user, cuts, services, theme } = this.props
        const { type, startView, stepAppointment, createAppointment } = this.state

        let imageBg = 'que/assets/images/card_brilliants_img.jpg'
        if (shop.photos[0] != null) {
            imageBg = `${shop.photos[0]}`
        }

        const posFavorite = shop.favorite ? 0 : 1
        return (

            <View style={{ display: 'flex', flex: 1, flexDirection: 'column', backgroundColor: theme.primary }} contentContainerStyle={{ flex: 1 }}>

                <StatusBar translucent backgroundColor="transparent" barStyle="light-content" />

                <ImageBackground source={{ uri: imageBg }} style={style.imageBackground}>
                    <LinearGradient colors={['rgba(0,0,0,.8)', 'rgba(0,0,0,0)']} style={{ flex: 1, height: 100 }}>

                        <TouchableOpacity style={[style.closeBtnContainer, { paddingTop: 20 }]} onPress={() => this.close()}>
                            <Image source={require('que/assets/images/close_barber_shop_icn.png')} style={style.btnClose} />
                        </TouchableOpacity>

                    </LinearGradient>

                    <View style={{ flex: 1 }}>
                        <TouchableOpacity style={[style.favoriteContainer, { zIndex: 2 }]} activeOpacity={1} onPress={() => this.playHeart()}>
                            <LottieView
                                style={style.btnFavorite}
                                source={require('que/assets/animations/heart.json')}
                                autoPlay={false}
                                loop={false}
                                ref={anim => this.heartAnim = anim}
                                progress={posFavorite}
                                onAnimationFinish={() => this.favActions()}
                            />
                        </TouchableOpacity>
                        {/* <Buttons 
                            shop={shop}
                            startView={startView}
                            type={type}
                            changeView={(view) => this.setState({ startView: view })}
                            /> */}

                        <View style={{ zIndex: 2 }}>
                            <Text style={style.barberTypeName}>
                                BARBER SHOP
                            </Text>
                            <View style={style.barberNameContainer}>
                                <Text style={style.barberName}>
                                    {shop.name}
                                </Text>
                            </View>
                            <View style={style.sliderContainer} />
                        </View>
                    </View>
                </ImageBackground>

                {/* <Slideshow 
                arrowLeft={()=>{}}
                arrowRight={()=>{}}
                containerStyle={}
                height={300}
                dataSource={[
                    { url:'http://placeimg.com/640/480/any' },
                    { url:'http://placeimg.com/640/480/any' },
                    { url:'http://placeimg.com/640/480/any' }
                ]}/> */}

                <View style={style.midleContainer}>
                    <Middle
                        changeView={(e) => this.setState({ startView: e })}
                        cardData={{ ...shop, prices: !!shop && !!Object.keys(shop).length ? shop.services : [] }}
                        type={type}
                        startView={startView}
                    />
                </View>
                <View style={style.bottomContainer}>
                    <BottomList
                        changeType={id => this.chooseBarber(id)}
                        cardData={shop}
                        barbers={(!!shop.barbers ? shop.barbers : [])}
                        type={type}
                        //view={view}
                        newAppointment={() => this.newAppointment()}
                    />
                </View>

                {
                    createAppointment &&
                    <WizardAppointment
                        family={this.props.user ? this.props.user.family : []}
                        barber={this.props.shop.barbers.length ? this.props.shop.barbers[0].barber._id : null}
                        shop={this.props.shop}
                        services={this.props.shop.services}
                        shop_id={this.props.navigation.getParam('shop_id', null)}
                        stepAppointment={stepAppointment}
                        changeStepAppointment={() => this.changeStepAppointment()}
                        backStepAppointment={() => this.backStepAppointment()}
                        closeCreateAppoint={() => this.closeCreateAppoint()}
                    />
                }
            </View>
        )
    }
}

const mapStateToProps = store => {

    let shop = {
        ...store.shopReducer.shop,
    }
    const barbers = !!shop.barbers ? shop.barbers.filter(barb => barb.status == "accepted") : []

    return ({
        shop: { ...shop, barbers },
        services: store.shopReducer.services,
        user: store.userReducer.user,
        loading: store.shopReducer.loading,
        cuts: store.cutsReducer.list,
        isBarber: store.userReducer.interface,
    })
}

const mapDispatchToProps = dispatch => {
    return bindActionCreators({
        findShop,
        findBarber,
        favorite,
        unFavorite,
        favAction,
        makeAppointment,
    }, dispatch)
}


export default connect(mapStateToProps, mapDispatchToProps)(withTheme(ShopViewer))