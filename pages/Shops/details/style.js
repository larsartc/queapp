import { StyleSheet, PixelRatio } from 'react-native';
import { colors } from 'que/components/base'

export default StyleSheet.create({
   
    imageBackground: {
        flex: 4,
        width: '100%',
        height: "100%",
        resizeMode: "cover"
    },
    midleContainer: {
        flex: 3,
        width: '100%',
        marginTop:-110, // TODO validate the pixelratio
    },
    closeBtnContainer: {
        top: 25,
        right: 10,
        position: 'absolute',
        padding:20,
    },
    btnClose: {
        height: 13,
        width: 13
    },
    favoriteContainer: {
        alignSelf: 'center',
        padding:8,
    },
    btnFavorite: {
        height: 24,
        width: 24
    },
    barberTypeName: {
        fontSize: 10,
        color: colors.white,
        textAlign: 'center',
        marginBottom: 5,
        opacity: 0.6
    },
    barberNameContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
    },
    barberName: {
        fontSize: 22 * PixelRatio.getFontScale(),
        width: 250,
        color: colors.white,
        flexDirection: 'row',
        textAlign: 'center'
    },
    sliderContainer: {
        flex: 1,
    },
    iconsBottomBackgroundContainer: {
        flex: 2,
        flexDirection: 'row',
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'space-between',
        width: '70%'
    },
    lineSeparator: {
        height: 0.5,
        backgroundColor: colors.white,
        width: '100%'
    },
    bottomContainer: {
        backgroundColor: colors.secondary,
        display: 'flex',
        flexDirection: 'column',
        flex: 1.4,
        borderTopColor: colors.white,
        borderTopWidth: 1,
    },
    barbersAvaliableContainer: {
        flex: 1
    },
    distanceText: {
        textAlign: 'center',
        fontSize: 13,
        color: colors.white,
        width: '90%',
        marginBottom: 5
    },
    hourList: {
        alignSelf: 'center',
        height: 20
    },
    hour: {
        color: colors.white,
        alignSelf: 'center',
        textAlign: "center"
    },
    // bgOverlay: {
    //     backgroundColor: 'rgba(0,0,0,.6)',
    //     width: '100%',
    //     height: '100%',
    //     position: 'absolute',
    //     left: 0,
    //     top: 0
    // }
});
