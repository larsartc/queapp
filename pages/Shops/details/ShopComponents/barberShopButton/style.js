import { StyleSheet, Text, View } from 'react-native';

export default StyleSheet.create({
    iconsBottomBackgroundContainer: {
        flex: 2,
        flexDirection: 'row',
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'space-between',
        width: '70%'
    },
    iconContainer: {
        alignSelf: 'center',
        marginBottom: 5
    },
    icon: {
        height: 20,
        width: 20
    }
})