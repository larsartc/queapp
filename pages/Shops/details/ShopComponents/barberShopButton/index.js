import React from 'react';
import { ImageBackground, TouchableOpacity, View } from 'react-native'
import style from './style'

const BarberShopButton = (props) => {
    return (
        <View style={style.iconsBottomBackgroundContainer}>
            <TouchableOpacity style={style.iconContainer}>
                <ImageBackground source={require('que/assets/images/price_empty_barber_shop_icn.png')} style={style.icon} />
            </TouchableOpacity>
            <TouchableOpacity style={style.iconContainer}>
                <ImageBackground source={require('que/assets/images/information_barber_shop_icn.png')} style={style.icon} />
            </TouchableOpacity>
            <TouchableOpacity style={style.iconContainer}>
                <ImageBackground source={require('que/assets/images/raiting_full_card_home_costumer_icn.png')} style={style.icon} />
            </TouchableOpacity>
        </View>
    )
}

export default BarberShopButton