import React, { Fragment } from 'react';

import { View, ScrollView, Image, Text, TouchableOpacity } from 'react-native'
import style from './style.js'

const barbersList = props => {

    const arrayBarbers = [1, 2, 3, 4, 5, 6, 7, 8, 8, 1, 102, 1, 123]

    return (
        <View style={style.barberListContainer}>
            <View style={style.appointmentIconContainer}>
                <Image source={require('que/assets/images/appointment_barber_shop_icn.png')} style={style.appointmentIcon} />
            </View>
            <View style={style.listContainer}>
                {arrayBarbers.map((i, k) => {
                    return (
                        <View style={{width: 50,  marginRight: 10, marginLeft: k == 0 ? 5 : 0 }}>
                            <TouchableOpacity style={{ ...style.barberIconsContainer}}>
                                <Image source={require('que/assets/images/appointment_barber_shop_icn.png')} style={style.appointmentIcon} />
                            </TouchableOpacity>
                            <Text style={style.barberName}>
                                Frank
                            </Text>
                        </View>

                    )
                })}
            </View>
        </View>
    )
}

export default barbersList