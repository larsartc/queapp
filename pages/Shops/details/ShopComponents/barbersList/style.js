import { StyleSheet, Text, View } from 'react-native';
import { colors } from 'que/components/base'

export default StyleSheet.create({
    appointmentIconContainer: {
        height: 40,
        width: 40,
        alignSelf: 'center',
        backgroundColor: colors.white,
        justifyContent: 'center',
        borderRadius: 40 / 2,
        position: 'absolute',
        marginTop: -20,
    },
    appointmentIcon: {
        height: 20,
        width: 20,
        alignSelf: 'center'
    },
    barberListContainer: {
        flex: 0.5,
        backgroundColor: colors.secondary
    },
    barberIconsContainer: {
        height: 40,
        width: 40,
        backgroundColor: colors.white,
        justifyContent: 'center',
        borderRadius: 40 / 2,
    },
    listContainer:{
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center'
    },
    barberName:{
        width: 40,
        fontSize: 12,
        color: colors.white,
        alignSelf: 'center'
    }
})