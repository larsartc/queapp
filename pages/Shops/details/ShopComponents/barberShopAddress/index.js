import React from 'react';

import { View, ScrollView, Image, Text } from 'react-native'
import style from './style.js'

const barberShopAddress = props => {

    const hoursAvaliable = ['Monday: 05:00am - 10:00pm', 'Monday: 05:00am - 10:00pm', 'Monday: 05:00am - 10:00pm', 'Monday: 05:00am - 10:00pm', 'Monday: 05:00am - 10:00pm', 'Monday: 05:00am - 10:00pm']

    return (
        <View style={style.addressContainer}>
            <View style={style.margin}>
                <Image source={require('que/assets/images/pin_information_barber_shop_icn.png')} style={style.icon} />
                <View style={style.boxContainer}>
                    <Text style={style.addressText}>
                        1550 Kingston Rd, Pickering, ON, L1V 1C3 Canada
                    </Text>
                    <Text style={style.distance}>
                        2km
                    </Text>
                </View>


                <View style={{ ...style.separator, marginTop: 30, marginBottom: 15 }} />

                <View style={{ ...style.boxContainer, marginBottom: '10%' }}>
                    <Image source={require('que/assets/images/clock_information_barber_shop_icn.png')} style={{ ...style.icon, width: 22 }} />

                    <View style={style.hoursContainer}>
                        <ScrollView
                            style={{ alignSelf: 'center' }}
                            showsVerticalScrollIndicator={false}
                        >

                            {hoursAvaliable.map((hour, key) => {

                                return (
                                    <Text style={style.hourAvaliable} key={key}>
                                        {hour}
                                    </Text>
                                )

                            }
                            )}
                        </ScrollView>
                    </View>
                </View>
            </View>
        </View>
    )
}

export default barberShopAddress