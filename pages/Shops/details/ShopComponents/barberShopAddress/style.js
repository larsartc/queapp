import { StyleSheet} from 'react-native';
import { colors } from 'que/components/base'

export default StyleSheet.create({
    addressContainer:{
        flex: 1,
        backgroundColor: colors.primary
    },
    boxContainer:{
        flex: 1,
        width: '90%',
        alignSelf: 'center',
    },
    separator:{
        height: 0.4,
        width: '70%',
        backgroundColor: colors.white,
        alignSelf: 'center'
    },
    margin: {
        flex: 1,
        marginTop: '10%',
    },
    icon:{
        height: 22, 
        width: 15,
        alignSelf: 'center'
    },
    addressText: {
        marginTop: '10%',
        textAlign: 'center',
        width: '100%',
        fontSize: 16,
        color: colors.white,
        fontWeight: '200',
        marginBottom: 5,
    },
    distance: {
        textAlign: 'center',
        width: '100%',
        fontSize: 13,
        color: colors.white,
        fontWeight: '100',
        marginBottom: 5,
    },
    hoursContainer:{
        marginTop: '10%',
        height: '20%',
        width: '70%',
        alignSelf: 'center',
    },
    hourAvaliable:{
        textAlign: 'center',
        color: colors.white,
        letterSpacing: 1,
        fontSize: 11,
        marginBottom: 2
    }
})