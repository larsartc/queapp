import { StyleSheet } from 'react-native';
import { colors } from 'que/components/base'

export default StyleSheet.create({
    MiddleContainer: {
        width: '100%',
        height: '100%',
        marginTop: 10,
        marginBottom: 50, 
        zIndex:0,
    },
    styleScroll: {
        alignSelf: 'center',
        width: "100%",
        zIndex: 99,
        height: '100%',
    },
    boxContainer: {
        flex: 1,
        width: '100%',
        alignSelf: 'center',
        marginTop:20,
    },
    separator: {
        height: 0.4,
        width: '70%',
        backgroundColor: colors.white,
        alignSelf: 'center'
    },
    margin: {
        flex: 1,
        marginTop: 10,
    },
    icon: {
        height: 16,
        width: 16,
        alignSelf: 'center'
    },
    MiddleText: {
        marginTop: 15,
        textAlign: 'center',
        width: '100%',
        fontSize: 14,
        color: colors.white,
        fontWeight: '200',
        marginBottom: 5,
    },
    distance: {
        textAlign: 'center',
        width: '100%',
        fontSize: 13,
        color: colors.white,
        fontWeight: '100',
    },
    hoursContainer: {
        marginTop: 15,
        height: '20%',
        width: '70%',
        alignSelf: 'center',
    },
    hourAvaliable: {
        textAlign: 'center',
        color: colors.white,
        fontSize: 12,
        fontWeight: 'normal',
        marginBottom: 2
    },
    divInfoTitle: {
        display: 'flex',
        justifyContent: 'center',
        textAlign: 'center',
        color: colors.white,
        fontSize: 15
    },
    divIndoData: {
        color: colors.white,
        display: 'flex',
        textAlign: 'right',
    },
    divInfoView: {
        flex: 1
    }
})