import React, { Fragment } from 'react';

import { View, ScrollView, Text } from 'react-native'
import style from './style.js'

import { colors } from 'que/components/base'

export default PriceMiddle = (props) => {

    let prices = {}

    if (props.prices) {
        prices = props.prices.reduce((initial, value) => {
            if (!initial[value.category]) {
                initial = { ...initial, [value.category]: [{ ...value }] }
            }else{
                initial[value.category] = [...initial[value.category], {...value}]
            }
            return initial;
        }, {})
    }
    return (
        <ScrollView style={[style.styleScroll, { zIndex: 2 }]} showsVerticalScrollIndicator={false}>
            <View style={{ flex: 1, marginTop: 20 }}>
                {Object.keys(prices).map((i) =>
                    <View style={{ flexDirection: 'column', justifyContent: 'center', alignItens: 'center', marginBottom: 6, marginTop: 12, paddingHorizontal: 50 }}>
                        <View style={{ borderBottomWidth: 1, borderColor: colors.white, paddingBottom: 2 }}><Text style={style.divInfoTitle}>{i}</Text></View>
                        <View style={{ marginTop: 5 }}>
                            {prices[i].map((info) =>
                                <View style={{ display: 'flex', flexDirection: 'row', marginTop: 2, marginBottom: 2 }}>
                                    <View style={{ ...style.divInfoView, flex: 2 }}><Text style={{ color: 'rgba(255, 255, 255, .6)' }}>{info.name}</Text></View>
                                    <View style={style.divInfoView}><Text style={style.divIndoData}>{info.price}</Text></View>
                                </View>
                            )}
                        </View>
                    </View>
                )}
            </View>
            <View style={{ height: 40 }} />
        </ScrollView>
    )
}