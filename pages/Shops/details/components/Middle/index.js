import React from 'react';

import { View, Text, PixelRatio, ViewPropTypes, StyleSheet, Animated, TouchableOpacity } from 'react-native'
import style from './style.js'
import CutsList from '../CutsList'
import LinearGradient from "react-native-linear-gradient"
import { withTheme } from '../../../../../utils/themeProvider'
import ScrollableTabView from 'react-native-scrollable-tab-view'
import PropTypes from 'prop-types'
import LottieView from 'lottie-react-native'
import AddressMiddle from './addressMiddle'
import PriceMiddle from './priceMiddle'
import RatingsMiddle from './ratingsMiddle'

class Middle extends React.PureComponent {
  render() {
    const { startView = 1, type } = this.props
    return (
      <View style={style.MiddleContainer}>
        <LinearGradient colors={[this.props.theme.primary_opacity, this.props.theme.primary]} style={{
          height: 80,
          width: '100%',
          position: 'absolute',
          zIndex: 3,
          bottom: 0,
        }} />
        <ScrollableTabView
          style={{ marginTop: 20 }}
          initialPage={startView}
          renderTabBar={() => (
            <DefaultTabBar
              barber={this.props.cardData}
              shop={this.props.cardData}
              type={this.props.type}
              theme={this.props.theme}
            />
          )}
        >
          <View>
            {type != "barber" ? <PriceMiddle {...this.props.cardData} /> : <CutsList {...this.props.cardData} />}
          </View>
          <View>
            <AddressMiddle {...this.props} />
          </View>
          <View>
            <RatingsMiddle {...this.props.cardData} />
          </View>
        </ScrollableTabView>

      </View>
    )
  }
}

export default withTheme(Middle)


const raitingIcons = {
  0: () => require('que/assets/animations/raitings/zero.json'),
  0.5: () => require('que/assets/animations/raitings/one.json'),
  1: () => require('que/assets/animations/raitings/two.json'),
  1.5: () => require('que/assets/animations/raitings/tree.json'),
  2: () => require('que/assets/animations/raitings/four.json'),
  2.5: () => require('que/assets/animations/raitings/five.json'),
  3: () => require('que/assets/animations/raitings/six.json'),
  3.5: () => require('que/assets/animations/raitings/seven.json'),
  4: () => require('que/assets/animations/raitings/eight.json'),
  4.5: () => require('que/assets/animations/raitings/nine.json'),
  5: () => require('que/assets/animations/raitings/ten.json'),
}

const priceIcons = {
  0: () => require('que/assets/animations/price/zero.json'),
  1: () => require('que/assets/animations/price/one.json'),
  2: () => require('que/assets/animations/price/two.json'),
  3: () => require('que/assets/animations/price/tree.json'),
  4: () => require('que/assets/animations/price/four.json'),
  5: () => require('que/assets/animations/price/five.json'),
}

const infoIcon = require('que/assets/animations/info/info.json')

const Button = (props) => {
  return (
    <TouchableOpacity {...props} activeOpacity={1} >
      {props.children}
    </TouchableOpacity>
  )
}

class DefaultTabBar extends React.Component {
  propTypes = {
    goToPage: PropTypes.func,
    activeTab: PropTypes.number,
    tabs: PropTypes.array,
    backgroundColor: PropTypes.string,
    activeTextColor: PropTypes.string,
    inactiveTextColor: PropTypes.string,
    textStyle: Text.propTypes.style,
    tabStyle: ViewPropTypes.style,
    renderTab: PropTypes.func,
    underlineStyle: ViewPropTypes.style,
  }

  getDefaultProps = () => {
    return {
      activeTextColor: 'navy',
      inactiveTextColor: 'black',
      backgroundColor: null,
    }
  }

  // renderTabOption = (name, page) => {
  // }

  renderTab = (name, page, isTabActive, onPressHandler, animeHere, shop) => {

    let mid_rate = !this.props[this.props.type] ? 0 : this.props[this.props.type].mid_rate || 0
    mid_rate = Math.round(mid_rate);
    let mid_price = !this.props[this.props.type] ? 0 : this.props[this.props.type].mid_price || 0

    let anime1 = animeHere.interpolate({
      inputRange: [0, 1, 2],
      outputRange: [.5, 0, 0],
    })
    let anime2 = animeHere.interpolate({
      inputRange: [0, 1, 2],
      outputRange: [0, .5, 0],
    })
    let anime3 = animeHere.interpolate({
      inputRange: [0, 1, 2],
      outputRange: [0, 0, .5],
    })

    let anime = [anime1, anime2, anime3]

    let icon

    if (this.props.type == "barber") {
      switch (page) {
        case 0:
          icon = require('que/assets/animations/cuts/cuts.json')
          break
        case 1:
          icon = require('que/assets/animations/info/info.json')
          break
        case 2:
          icon = raitingIcons[mid_rate]()
          break
      }
    } else {
      switch (page) {
        case 0:
          icon = priceIcons[mid_price]()
          break
        case 1:
          icon = infoIcon
          break
        case 2:
          icon = raitingIcons[mid_rate]()
          break
      }
    }

    return <Button
      style={{ flex: 1, }}
      key={name}
      accessible={true}
      accessibilityLabel={name}
      accessibilityTraits='button'
      onPress={() => onPressHandler(page)} >
      <View style={[styles.tab, this.props.tabStyle]}>
        <LottieView
          style={{ height: PixelRatio.get() > 1.5 ? 30 : 20 }}
          source={icon}
          autoPlay={false}
          loop={false}
          ref={anim => this.anim1 = anim}
          progress={anime[page]}
          onAnimationFinish={() => { }}
        />
      </View>
    </Button>;
  }

  render() {
    const containerWidth = this.props.containerWidth;
    const numberOfTabs = this.props.tabs.length;

    const translateX = this.props.scrollValue.interpolate({
      inputRange: [0, 1],
      outputRange: [(containerWidth / numberOfTabs), -(containerWidth / (numberOfTabs * containerWidth))],
    });

    return (
      <LinearGradient colors={[this.props.theme.secondary_opacity, this.props.theme.secondary]} style={{ paddingTop: 40 }}>
        <View style={{
          borderWidth: 2,
          borderTopWidth: 0,
          borderLeftWidth: 0,
          borderRightWidth: 0,
          borderColor: 'white'
        }}>
          <Animated.View style={[
            styles.tabs,
            {
              transform: [
                { translateX }
              ]
            }
          ]}>
            {this.props.tabs.map((name, page) => {

              const isTabActive = this.props.activeTab === page;
              const renderTab = this.props.renderTab || this.renderTab;

              return renderTab(name, page, isTabActive, this.props.goToPage, this.props.scrollValue, this.props.shop);
            })}
          </Animated.View>
        </View>
      </LinearGradient>
    );
  }

}

const styles = StyleSheet.create({
  tab: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingBottom: 10,
  },
  tabs: {
    height: 40,
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
});