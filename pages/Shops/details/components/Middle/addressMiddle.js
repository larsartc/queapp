import React, { Fragment } from 'react';

import { View, ScrollView, Image, Text } from 'react-native'
import style from './style.js'

export default AddressMiddle = (props) => {

  const daysEnum = {
      0: "Monday",
      1: "Tuesday",
      2: "Wednesday",
      3: "Thursday",
      4: "Friday",
      5: "Saturday",
      6: "Sunday"
  }

  const LocationIcon = props.type == "shop" ?
      <Image source={require('que/assets/images/barbers/shop_pin_location_icn.png')}
          style={{ ...style.icon, width: 22, height: 22 }} /> :
      <Image source={require('que/assets/images/shop_owner_icn_white.png')}
          style={{ ...style.icon, width: 18, height: 14 }} />

  return (
      <ScrollView style={[style.styleScroll, {zIndex:2}]} showsVerticalScrollIndicator={false}>
          
          <View style={style.boxContainer}>
          {props.startView == 1 && LocationIcon}

              {props.type == 'barber' &&
                  <Text style={style.MiddleText}>
                      {props.cardData.shop_name}
                  </Text>
              }
              {!!props.cardData.address &&
                  <Text style={style.MiddleText}>
                      {props.cardData.address}
                  </Text>
              }

              {!!props.cardData.distance &&
                  <Text style={style.distance}>
                      {Math.ceil(props.cardData.distance)} km
                  </Text>
              }
          </View>

          <View style={{ ...style.separator, marginTop: 15, marginBottom: 15 }} />

          <View style={{ ...style.boxContainer }}>
              <Image source={require('que/assets/images/barbers/shop_clock_hour_icn.png')}
                  style={{ ...style.icon, width: 22, height: 22 }} />
              <View style={style.hoursContainer}>
                  {!!props.cardData.open_time && props.cardData.open_time.map((item, key) =>
                      <Text style={style.hourAvaliable} key={key}>
                          {daysEnum[item.week_day]} {item.start_time} - {item.end_time}
                      </Text>
                  )}
              </View>
          </View>
          <View style={{height:40}} />
      </ScrollView>
  )
}