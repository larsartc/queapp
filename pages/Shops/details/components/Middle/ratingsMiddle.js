import React from 'react';

import { View, ScrollView, Image, Text } from 'react-native'
import style from './style.js'
import { colors } from 'que/components/base'
import serializeIconsRatings from "que/utils/serializeIconsRatings"

export default RatingsMiddle = (props) => {
    
    let stars = !!props.ratings ? props.ratings.map((rating => {
        const mid_rate = serializeIconsRatings(rating.mid_rate);
        return {
        ...rating,
        ...mid_rate
        }

    })) : null

    return (
        <ScrollView style={[style.styleScroll, { zIndex: 2 }]} showsVerticalScrollIndicator={false}>
            <View style={{ flex: 1, marginTop: 20 }}>
                {stars != null && stars.map((i) =>
                    <View style={{ flexDirection: 'row', marginBottom: 6, marginTop: 6, paddingHorizontal: 50 }}>
                        <View style={{ flex: 1 }}>
                            <Text style={{ color: colors.ratingsColor }}>
                                {i.category}
                            </Text>
                        </View>
                        <View style={{ flexDirection: 'row', flex: 1, alignItens: 'center', justifyContent: 'flex-end', textAlign: 'right' }}>

                            <>
                                {i.number != null && !!i.partial &&
                                    [...Array(i.number).keys()].map((p, k) => {
                                        if (k + 1 != i.number) {
                                            return <Image source={require('que/assets/images/shopdetails/shop_raiting_filled_icn.png')}
                                                style={{ ...style.icon, marginRight: 5 }} />
                                        } else {
                                            return <Image key={k} source={require('que/assets/images/shopdetails/shop_raiting_semi_icn.png')} style={{ ...style.icon, marginRight: 5 }} />
                                        }
                                    })
                                }
                            </>
                            <>
                                {i.number != null && !i.partial &&
                                    [...Array(i.number).keys()].map((p, k) => {
                                        return <Image key={k} source={require('que/assets/images/shopdetails/shop_raiting_filled_icn.png')} style={{ ...style.icon, marginRight: 5 }} />
                                    })
                                }
                            </>
                            <>
                                {i.number == null || i.number < 5 &&
                                    [...Array(5 - i.number).keys()].map((p, k) => {
                                        return <Image source={require('que/assets/images/shopdetails/shop_raiting_empty_icn.png')}
                                            style={{ ...style.icon, marginRight: 5 }} />
                                    })
                                }
                            </>
                        </View>
                    </View>
                )}
            </View>

            <View style={{ height: 40 }} />
        </ScrollView>
    )
}