import React from 'react';

import { View, ScrollView, Image, Text, TouchableOpacity, Alert, Linking } from 'react-native'
import style from './style'
import { colors } from 'que/components/base'
import { withTheme } from 'que/utils/themeProvider';
import serializeIconsRatings from "que/utils/serializeIconsRatings"

const BottomList = props => {

    goTo = () => {
        if (props.barbers.length == 0) return false;
        props.newAppointment()
    }

    return (
        <View style={{ ...style.barberListContainer, 'backgroundColor': props.theme.secondary }}>

            <TouchableOpacity activeOpacity={1} style={style.appointmentIconContainer} onPress={() => goTo()}>
                <Image source={require('que/assets/images/appointment_barber_shop_icn.png')} style={style.appointmentIconCalendar} />
            </TouchableOpacity>

            <View style={style.listContainer}>
                {props.type == "shop" ? (
                    props.barbers.length > 0 ?
                        <ScrollView horizontal={true}>
                            {props.barbers.map((i, k) => BarberList(props, i, k))}
                        </ScrollView>
                        : <Text style={{ color: colors.ratingsColor }}>No barbers available</Text>
                ) : <SocialList barber={props.cardData.barber} action={props.action ? props.action : false} />}
            </View>
            {props.children}
        </View>
    )
}

const BarberList = (props, i, k) => {

    if (!i.barber.barber) {
        return null;
    }

    ratings = i.barber.barber.mid_rate

    let stars = serializeIconsRatings(ratings);

    return (
        <View style={style.baseIconBarber}>
            {i.appointment &&
                <View style={{ ...style.icon_barber_appt_container }}>
                    {props.theme.key == "BARBER" &&
                        <Image source={require("que/assets/images/barbers/appointment_blue_icn.png")} style={{ ...style.icon_barber_has_appt }} />
                    }
                    {props.theme.key == "CUSTOMER" &&
                        <Image source={require("que/assets/images/customer/appointment_purple_icn.png")} style={{ ...style.icon_barber_has_appt }} />
                    }
                    {props.theme.key != "BARBER" && props.theme.key != "CUSTOMER" &&
                        <Image source={require("que/assets/images/shops/appointment_yellow_icn.png")} style={{ ...style.icon_barber_has_appt }} />
                    }
                </View>
            }
            <TouchableOpacity activeOpacity={1} style={{ textAlign: 'center', alignSelf: 'center' }} onPress={() => props.changeType(i.barber._id)}>
                <Image source={{ uri: i.barber.barber.photos[0] }} style={{ ...style.barberIconsContainerProfile }} />
            </TouchableOpacity>
            <Text style={style.barberName}>
                {!!i.barber.name && i.barber.name.split(" ")[0]}
            </Text>
            <View style={{ display: 'flex', flexDirection: 'row' }}>
                {
                    <>
                        {stars.number != null && !!stars.partial &&
                            [...Array(stars.number).keys()].map((p, k) => {
                                if (k + 1 != stars.number) {
                                    return <Image key={k} source={require('que/assets/images/shopdetails/shop_raiting_filled_icn.png')} style={{ ...style.icon, marginRight: 2 }} />
                                } else {
                                    return <Image key={k} source={require('que/assets/images/shopdetails/shop_raiting_semi_icn.png')} style={{ ...style.icon, marginRight: 2 }} />
                                }
                            })
                        }

                        {stars.number != null && !stars.partial &&
                            [...Array(stars.number).keys()].map((p, k) => {
                                return <Image key={k} source={require('que/assets/images/shopdetails/shop_raiting_filled_icn.png')} style={{ ...style.icon, marginRight: 2 }} />
                            })
                        }


                        {stars.number == null || stars.number < 5 &&
                            [...Array(5 - stars.number).keys()].map((p, k) => {
                                return <Image key={k} source={require('que/assets/images/shopdetails/shop_raiting_empty_icn.png')} style={{ ...style.icon, marginRight: 2 }} />
                            })
                        }
                    </>
                }
            </View>
        </View>
    )
}

const SocialList = (props) => {
    const icons = [
        {
            tag: 'facebook',
            img: require('que/assets/images/facebook_barber_profile_icn.png'),
            link: props.barber != undefined ? props.barber.barber.facebook : "",
        },
        {
            tag: 'instagram',
            img: require('que/assets/images/instagram_barber_profile_icn.png'),
            link: props.barber != undefined ? props.barber.barber.instagram : "",
        },
        {
            tag: 'pinterest',
            img: require('que/assets/images/pinterest_barber_profile_icn.png'),
            link: props.barber != undefined ? props.barber.barber.pinterest  : "",
        },
        {
            tag: 'whatsapp',
            img: require('que/assets/images/whatsapp_barber_profile_icn.png'),
            link: props.barber != undefined ? props.barber.barber.whatsapp : "",
        },
        {
            tag: 'twitter',
            img: require('que/assets/images/twitter_barber_profile_icn.png'),
            link: props.barber != undefined ? props.barber.barber.twitter: "",
        },
    ]

    return (
        <View style={style.socialListContainer}>
            {icons.map((i =>
                <TouchableOpacity
                    style={{ ...style.barberIconsContainer }}
                    onPress={() => props.action ? props.action(i.tag) : (i.link !== "" ? Linking.openURL(i.link) : null)} >
                    <Image source={i.img} style={style.appointmentIcon} />
                </TouchableOpacity>
            ))}
        </View>
    )
}

export default withTheme(BottomList)