import { StyleSheet, Text, View, PixelRatio, Platform } from 'react-native';
import {colors} from 'que/components/base'

export default StyleSheet.create({
    icon_barber_appt_container: {
        width: 30,
        height: 30,
        borderRadius: 15,
        position: "absolute",
        zIndex: 2,
        right: -10,
        top: -10,
    },  
    icon_barber_has_appt:{
        width: "100%",
        height: "100%"
    },
    appointmentIconContainer: {
        height: 60,
        width: 60,
        alignSelf: 'center',
        backgroundColor: colors.white,
        justifyContent: 'center',
        borderRadius: 30,
        position: 'absolute',
        marginTop: -30,
        borderWidth: 4,
        borderColor: colors.primary,
    },
    appointmentIcon: {
        width: PixelRatio.get()>1.5 ? 30 : 20,
        height: PixelRatio.get()>1.5 ? 30 : 20
    },
    appointmentIconCalendar:{
        width: 16,  
        height: 16,
        alignSelf: 'center'
    },
    barberListContainer: {
        paddingTop: 15,
        paddingBottom: 2,
        flex: 1,
        alignItems: 'center',
    },
    socialListContainer: {
        flex: 1, 
        display: 'flex', 
        justifyContent: 'space-between', 
        alignItems: 'center', 
        flexDirection: 'row', 
        textAlign: 'center',
        marginHorizontal:16
    },
    barberIconsContainer: {
        height: 50,
        width: 50,
        justifyContent: 'center',
        borderRadius: 70 / 2,
        textAlign: 'center',
        display: 'flex',
        alignItems: 'center'
    },
    baseIconBarber:{
        width: PixelRatio.get()>1.5 ? 60 : 50,
        marginHorizontal: PixelRatio.get()>1.5 ? 12 : 10,
        textAlign: 'center',
        position: 'relative',
        marginVertical: 20
    },
    barberIconsContainerProfile:{
        height: PixelRatio.get()>1.5 ? 50 : 40,
        width: PixelRatio.get()>1.5 ? 50 : 40,
        borderRadius: 25,
        justifyContent: 'center',
        textAlign: 'center',
        display: 'flex',
        alignItems: 'center',
        borderWidth: 1, 
        borderColor: colors.white
    },
    listContainer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    barberName: {
        fontSize: 11,
        color: colors.white,
        textAlign: 'center'
    },
    icon:{
        width: 10, 
        height: 10
    }
})