import { StyleSheet, Text, View } from 'react-native';
import {colors} from 'que/components/base'

export default StyleSheet.create({
    cutName:{
        color: colors.white,
        textAlign: 'center',
        marginBottom: 20
    },
    IconsContainer: {
        height: 55,
        width: 55,
        backgroundColor: colors.white,
        justifyContent: 'center',
        borderRadius: 70 / 2,
        borderWidth: 1,
        borderColor: colors.white
    },
    listContainer:{
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
    },
    cutTime:{
        width: 55,
        fontSize: 12,
        marginTop: 5,
        color: colors.white,
        textAlign: 'center'
    }
})