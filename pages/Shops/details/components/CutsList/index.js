import React, { Fragment } from 'react'

import { ScrollView, View, Text, TouchableOpacity, Image } from 'react-native'
import style from './style'

class CutsList extends React.Component {
    render() {

        const cuts = this.props.past_cuts || []

        return (
            <Fragment>
                <Text style={{...style.cutName, marginVertical: 16}}>PAST CUTS</Text>
                <View style={style.listContainer}>
                    <ScrollView
                    >
                        <View style={{ flex: 1, flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'center' }}>
                            {cuts.map((cut, k) => Cut(this.props, cut, k))}
                        </View>
                    </ScrollView>
                </View>
            </Fragment>
        )
    }
}

const Cut = (props, i, k) => {
    return (
        <View style={{ width: 50, marginRight: 15, marginLeft: 15, marginBottom: 20 }}>
            <TouchableOpacity onPress={() => props.changeType(i)}>
                <Image source={i.image} style={{ ...style.IconsContainer }} />
            </TouchableOpacity>
            <Text style={style.cutTime}>
                {i.hour}
            </Text>
        </View>
    )
}
export default CutsList