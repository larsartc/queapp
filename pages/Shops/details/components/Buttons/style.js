import { StyleSheet, Text, View, PixelRatio } from 'react-native';

export default StyleSheet.create({
    iconsBottomBackgroundContainer: {
        flexDirection: 'row',
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'space-around',
        width:'100%',
    },
    iconContainer: {
        alignSelf: 'center',
        marginBottom: 5
    },
    icon: {
        height: 20,
        width: 20
    }, 
    iconAnim: {
        height: PixelRatio.get()>1.5 ? 30 : 20,
    }, 
    gradientBox: {
        display: 'flex',
        height: 200,
        position: 'absolute',
        bottom: 0,
        width: '100%',
        justifyContent:'flex-end', 
        paddingBottom: 16,
    }
})