import React from 'react';
import { TouchableOpacity, View, ImageBackground, Image, Dimensions, PixelRatio } from 'react-native'
import style from './style'
import LinearGradient from "react-native-linear-gradient"
import { colors } from 'que/components/base'
import LottieView from 'lottie-react-native'
import { withTheme } from '../../../../../utils/themeProvider';
import Carousel from 'react-native-snap-carousel'

const raitingIcons = {
    0:      () => require('que/assets/animations/raitings/zero.json'), 
    0.5:    () => require('que/assets/animations/raitings/one.json'), 
    1:      () => require('que/assets/animations/raitings/two.json'), 
    1.5:    () => require('que/assets/animations/raitings/tree.json'), 
    2:      () => require('que/assets/animations/raitings/four.json'), 
    2.5:    () => require('que/assets/animations/raitings/five.json'), 
    3:      () => require('que/assets/animations/raitings/six.json'), 
    3.5:    () => require('que/assets/animations/raitings/seven.json'), 
    4:      () => require('que/assets/animations/raitings/eight.json'), 
    4.5:    () => require('que/assets/animations/raitings/nine.json'), 
    5:      () => require('que/assets/animations/raitings/ten.json'), 
}

const priceIcons = {
    0: () => require('que/assets/animations/price/zero.json'),
    1: () => require('que/assets/animations/price/one.json'),
    2: () => require('que/assets/animations/price/two.json'),
    3: () => require('que/assets/animations/price/tree.json'),
    4: () => require('que/assets/animations/price/four.json'),
    5: () => require('que/assets/animations/price/five.json'),
}

class BarberShopButton extends React.PureComponent {
    constructor(props){
        super(props)
        
        this.state = {
            actualView: 1
        }

        this.anim1 = React.createRef()
        this.anim2 = React.createRef()
        this.anim3 = React.createRef()
    }
    
    renderItem = (val) => {
        let viewsCarousel

        if(this.props.type==="barber"){

            let mid_rate = 0
            var iconRate = raitingIcons[mid_rate]()
            
            viewsCarousel = [
                <LottieView 
                style={style.iconAnim}
                source={require('que/assets/animations/cuts/cuts.json')} 
                autoPlay={false}
                loop={false}
                ref={anim=>this.anim1 = anim} 
                progress={0}
                onAnimationFinish={()=>{}}
                />,
                
                <LottieView 
                style={style.iconAnim}
                source={require('que/assets/animations/info/info.json')} 
                autoPlay={false}
                loop={false}
                ref={anim=>this.anim2 = anim} 
                progress={0}
                onAnimationFinish={()=>{}}
                />,
                
                <LottieView 
                style={style.iconAnim}
                source={iconRate} 
                autoPlay={false}
                loop={false}
                ref={anim=>this.anim3 = anim} 
                progress={0}
                onAnimationFinish={()=>{}}
                />,
            ]
        }else{

            let mid_rate = this.props.shop.mid_rate || 0
            var iconRate = raitingIcons[mid_rate]()

            let mid_price = this.props.shop.mid_price || 0 
            var iconPrice = priceIcons[mid_price]()

            viewsCarousel = [
                <LottieView 
                style={style.iconAnim}
                source={iconPrice} 
                autoPlay={false}
                loop={false}
                ref={anim=>this.anim1 = anim} 
                progress={0}
                onAnimationFinish={()=>{}}
                />,
                
                <LottieView 
                style={style.iconAnim}
                source={require('que/assets/animations/info/info.json')} 
                autoPlay={false}
                loop={false}
                ref={anim=>this.anim2 = anim} 
                progress={0}
                onAnimationFinish={()=>{}}
                />,
                
                <LottieView 
                style={style.iconAnim}
                source={iconRate} 
                autoPlay={false}
                loop={false}
                ref={anim=>this.anim3 = anim} 
                progress={0}
                onAnimationFinish={()=>{}}
                />,
            ]
        }

        return <View >
            <TouchableOpacity style={style.iconContainer} onPress={() => this.changeView(val.index)}>

            {viewsCarousel[val.index]}
            </TouchableOpacity>
            </View>
    }

    changeView = (e, changeView=true) => {
        this._carousel.snapToItem(e,true, ()=>{})

        if(changeView){
            this.props.changeView(e)
        }

        if(this.anim1.play!=undefined){
            switch(e){
                case 0:
                    this.anim1.play(0,60)
                    this.anim2.play(60,0)
                return
                case 1:
                    this.anim1.play(60,0)
                    this.anim2.play(0,60)
                    this.anim3.play(60,0)
                    return 
                case 2:
                    this.anim2.play(60,0)
                    this.anim3.play(0,60)
                return 
            }
        }
    }
    
    componentWillReceiveProps = nextProps => {
        if(this.state.actualView!=nextProps.startView){

            this.setState({actualView:nextProps.startView}, ()=>{
                if(this._carousel!=undefined){
                    this.changeView(this.state.actualView, false)
                }
            })
        }
    }

    componentDidMount = () => {
        if(this._carousel!=undefined){
            this.changeView(1, false)
        } 
    }

    // shouldComponentUpdate = (nextProps, nextState) => {
    //     if(this.state.actualView!=nextProps.startView){
    //         return true; 
    //     }
    //     return false;
    // }

    render(){
        const {theme, startView = 1} = this.props 
        const {actualView} = this.state 

        return (
            <LinearGradient colors={[theme.secondary_opacity, theme.secondary]} style={style.gradientBox}>
                <View style={style.iconsBottomBackgroundContainer}>
                    <Carousel
                        ref={(c) => this._carousel = c}
                        data={[0,1,3]}
                        renderItem={this.renderItem}
                        sliderWidth={Dimensions.get('window').width}
                        itemWidth={(Dimensions.get('window').width / 3)}
                        onSnapToItem={e=>console.log(e)}
                        onBeforeSnapToItem={e=>console.log(e)}
                        inactiveSlideOpacity={.8}
                        inactiveSlideScale={1}
                        firstItem={1}
                        scrollEnabled={false}
                    />
                </View>
            </LinearGradient>
        )
    }
            
}

export default withTheme(BarberShopButton)