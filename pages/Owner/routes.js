import OwnerSelectShop from './ownerSelectShop'
import AddNewShop from './addNewShop'
import ShopMapAdd from './shopMapAdd'
import AddNewBarber from './addNewBarber'

export default {
  OwnerSelectShop, 
  AddNewShop,
  ShopMapAdd,
  AddNewBarber,
}