import React from 'react'
import { View, StatusBar } from 'react-native'
import SideBar from '../../components/Sidebar'
import HeaderBar from "../../components/HeaderBar"
import Geolocation from 'react-native-geolocation-service'
import MapView from "../../components/MapView"
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import AddressFillPop from './components/addressFillPop'

class ShopMapAdd extends React.Component {
  constructor(props) {
    super(props)
  }

  state = {
    shop: {},
    view: 'map',
    search: '',
    position: {},
    sidebar: "menu",
    showMenu: false,
  }

  componentDidMount = () => {
    // watchPosition
    // getCurrentPosition
    Geolocation.watchPosition(pos => {
      console.log("gps", pos)
      // pos.coords.latitude / longitude
    }, e => {
      console.log(e.code, e.message);
    }, { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
    );
  }

  render() {
    const { position, filterBar, showMenu } = this.state

    return (
      <>

        <StatusBar barStyle="dark-content" />

        <SideBar
          {...this.props}
          navigation={this.props.navigation}
          close={() => this.setState({ showMenu: false })}
          showMenu={showMenu} />

  
        <View style={{ flex: 1 }}>
          <HeaderBar
            title={'SHOP ADDRESS'}
            rightIcon={null}
            onPressRightIcon={null}
            onPressLeftIcon={() => this.props.navigation.pop()}
          />

          <View style={{ flex: 1 }}>
            <MapView
              position={position}
              shops={[]}
              pressAction={(id, shop) => {}}
            />
          </View>

          <AddressFillPop />
          
        </View>
      </>
    )
  }
}

const mapStateToProps = store => ({
  filters: store.shopReducer.filters,
  shops: store.shopReducer.list_shop,
  loading: store.shopReducer.loading,
  user: store.userReducer.user,
  categories: store.categoryReducer.list
})

const mapDispatchToProps = dispatch =>
  bindActionCreators({
    //
  }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(ShopMapAdd)