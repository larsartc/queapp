import React from "react";
import {StyleSheet} from "react-native";

import styled from 'styled-components';

import Cut from "que/components/Cut";
import SearchBar from "que/components/SearchBar"
import { View, Content } from "native-base";
import { Image, TouchableOpacity, Platform, Text, StatusBar } from "react-native";
import SideBar from 'que/components/Sidebar'
import HeaderBar from "que/components/HeaderBar"
import { Favorite } from "que/utils/enums"
import PopBottom from "que/components/PopBottom"
// import AddPop from "./components/addPop"
import LinearGradient from "react-native-linear-gradient"
// import TutorialHint from './components/tutorialHint'
import AsyncStorage from "@react-native-community/async-storage"
import * as ASYNCTAG from "../../utils/storeNames"
import RightSideBar from "que/components/rightSideBar"
import FilterSideBar from 'que/components/FilterSideBar'

//Redux
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import {listCut, favoriteCut, unfavoriteCut} from '../../store/cut'

//BASE COMPONENTS
import {colors, ShopContainer} from 'que/components/base'

class AddNewBarber extends React.Component {
  state = {
    saveCut: false,
    filter: 'shaving',
    name_cut: '',
    cutDetails: [], 
    search: '', 
    showTutorial: false,
    showMenu: false,
  };

  componentDidMount = () => {
   
  }

 
  render() {
    const { cuts, loading } = this.props;
    const {filter, saveCut, showTutorial, showMenu} = this.state 
    const cutsFiltered = cuts.filter(c => c.type == filter);

    return (

        <ShopContainer>

        <StatusBar barStyle="dark-content" />

        <SideBar 
            {...this.props} 
            navigation={this.props.navigation} 
            close={() => this.setState({showMenu: false})}
            showMenu={showMenu} />

          {/* {cutsFiltered.length>0 && showTutorial && <TutorialHint close={()=>this.closeTutorial()} />} */}

          <HeaderBar
            title={'CUTS'}
            rightIcon={filter == 'shaving' ? require('que/assets/images/beard_cuts_icn.png') : require('que/assets/images/hair_cuts_icn.png')}
            onPressRightIcon={() => filter=='shaving' ? this.pressFilter('haircut') : this.pressFilter('shaving')}
            onPressLeftIcon={() => this.setState({showMenu: true})}
          /> 

          <View style={{ flex: 1 }}>
          <SearchBar
            value={this.state.search}
            onChangeText={value => this.setState({ search: value })}
            placeholder={'Search for style...'}
          />

          <Styled.CardsContainer style={{marginTop:70}}>
            {/* {Platform.OS==="ios" && (
              <LinearGradient 
                colors={["#ffffff", "rgba(255,255,255,0)"]} 
                style={{position:"relative", height:30, marginBottom:-30, zIndex: 2 }} />
            )} */}
            <Content padder>
              <Styled.ViewCuts>
                {/* { loading && <LoadingDefault color={colors.primary} /> } */}
                {
                  cutsFiltered.map((c, i) => 
                    <Cut
                      saves={c.num_favorites}
                      title={c.name}
                      favorite={c.favorite}
                      unfavoriteCut={() => this.removeCutFinal(c)}
                      type={this.state.filter}
                      saveCut={() => this.saveCut(c)}
                      imageBg={require('que/assets/images/cuts/cut1.jpg')}
                      key={i}
                    ></Cut>
                  )
                }
              </Styled.ViewCuts>
            </Content>
          </Styled.CardsContainer>
          </View>

          {this.state.saveCut && (
            <PopBottom
              height={200}
              isFinish={true}
              size={0}
              activePosition={0}
              showNext={false} >
              {/* <AddPop 
                photo={require('que/assets/images/cuts/cut1.jpg')}
                actionContinue={(e)=>this.saveCutFinal(e)} 
                actionCancel={()=>this.setState({saveCut: !this.state.saveCut})} /> */}
            </PopBottom>
          )}

        </ShopContainer>
    )
  }
}

const mapStateToProps = store => ({
  cuts: store.cutsReducer.list, 
  loading: store.cutsReducer.loading, 
  user: store.userReducer.user,
  filters: store.shopReducer.filters,
})

const mapDispatchToProps = dispatch => 
  bindActionCreators({
    //
  }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(AddNewBarber)


const Styled = {
    ShopContainer: styled.View`
        height: 100%;
        width: 100%;
        position: relative
    `,
    ViewCuts: styled.View`
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
    `,
    SaveContainer: styled.View`
        height: 140;
        width: 100%;
        background-color: ${colors.white};
        position: absolute;
        bottom: 0;
        align-items: center; 
    `,
    HeaderIcon: styled.Image`
        height: 15;
        width: 15;
    `,
    HeaderSearchBarContainer: styled.View`
        height: 40;
        width: 90%; 
        margin-top: 10;
        margin-bottom: 10;
        align-self: center;
    `,
    HeaderSearchBar: styled.View`
        height: 100%;
        width: 100%;
        background-color: ${colors.white};
        display: flex;
        flex-direction: row;
        align-items: center;
    `,
    SearchBarInput: styled.TextInput`
        margin-left: 10;
        flex: 2;
    `,
    SearchBarIconContainer: styled.View`
        margin-right: 10;
        flex: 1;
        display: flex;
        flex-direction: row;
        align-items: center;
        justify-content: space-between;
    `,
    SearchSeparator: styled.Text`
        color: ${colors.gray};
    `,
    CardsContainer: styled.View`
        flex: 1;
    `,
    CardsMargin: styled.FlatList`
        align-self: center;
    `,
    CardContainer: styled.View`
        margin-bottom: 5;
        margin-right: 5;
        margin-left: 5;
        margin-top: 5;
    `
}