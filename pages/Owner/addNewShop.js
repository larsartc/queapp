import React from 'react'
import { ImageBackground, TouchableOpacity, View, Image, Text, ScrollView, TextInput, PixelRatio } from 'react-native'
import style from '../Shops/details/style'
import BottomList from '../Shops/details/components/BottomList'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { findShop, findBarber, favAction, favorite, unFavorite } from '../../store/shop'
import { createProfileBarber } from '../../store/barber'
import { withTheme } from '../../utils/themeProvider'
import TabButtons from 'que/components/TabButtons'
import HeaderBar from "que/components/HeaderBar"
import CustomButton from 'que/components/CustomButton'
//import WorkingHours from './components/workingHours'
// import InfoBarber from './components/infoBarber'
// import CutsBarber from './components/cutsBarber'
// import CareerBarber from './components/careerBarber'
import CustomInput from 'que/components/CustomInput'
import PopBottom from 'que/components/PopBottom'

const closeIcon = require('que/assets/images/close_barber_shop_icn.png')

class AddNewShop extends React.Component {

    state = {
      imgBackground: null,
      workingHours: false,
      tempPhoto: false,
      activeTab: 1,

      payload: {
        email: null,
        photos: [],
        facebook: null,
        instagram: null,
        pinterest: null,
        whatsapp: null,
        twitter: null,
        mid_rate: 0,
      }, 

      socialLinkConfig: false,
      socialLinkActive: null,

      listWorkingHours: [],

    }

    close = () => {
        this.props.navigation.goBack()
    }

    changePhoto = () => {
      this.setState({tempPhoto: true})
    }

    removePhoto = () => {
      this.setState({tempPhoto: false})
    }

    saveProfileBarber = () => { 
      const {createProfileBarber} = this.props 

      let payload = {
        ...this.state.payload,
        work_time: this.state.listWorkingHours
      }

      createProfileBarber(payload)
    }

    setHoursWork = (field, value, key) => {
      const {shopWorking} = this.props 

      let timeFromShop = shopWorking[0].shop.open_time
      let timeFromBarber = []

      timeFromShop.forEach((val, i)=>{
        if(i==key){
          timeFromBarber.push({
            ...val,
            start_time: field=="start"?value:val.start_time,
            end_time: field=="end"?value:val.end_time,
          })
        }else{
          timeFromBarber.push(val)
        }
      })

      this.setState({...this.state, listWorkingHours: timeFromBarber})

    }

    setSocialLink = () => {
      const {socialLinkText} = this.state 

      this.setState({
        ...this.state,
        socialLinkConfig: false,
        socialLinkActive: null,
      })
    }

    openSocialLink = (tag) => {
      this.setState({
        ...this.state,
        socialLinkConfig: !this.state.socialLinkConfig,
        socialLinkActive: tag,
      })
    }
    
    render() {
        const { user, barber, shop, cuts, theme, listCarrer } = this.props
        const { 
            type, 
            activeTab, 
            createAppointment, 
            imgBackground, 
            workingHours, 
            tempPhoto, 
            socialLinkConfig, 
            socialLinkActive, 
            listWorkingHours } = this.state
        
        let shopWorking = user!=undefined ? user.shopsWorking : null

        const tabs = [
          // <CutsBarber />,
          // <InfoBarber 
          // shop={shopWorking[0]}
          // openStep={()=>this.setState({workingHours:true})}
          // workingHours={listWorkingHours}
          //  />,
          // <CareerBarber 
          //   {...this.props}
          //   carrers={[]} />,
        ]

        return (
            <View style={{ display: 'flex', flex: 1, flexDirection: 'column', backgroundColor: theme.primary }} contentContainerStyle={{ flex: 1 }}>

              <HeaderBar
                title={'ADD NEW SHOP'}
                rightIcon={closeIcon}
                onPressRightIcon={()=>this.close()}
                lightMode
              /> 

              <ImageBackground source={imgBackground} style={style.imageBackground}>

                <View style={{flex:1, justifyContent:'center', alignItems:'center'}}>

                  {tempPhoto ? (
                    <View style={{width:'100%', flexDirection:'row', justifyContent:'space-between', paddingHorizontal:32}}>
                      <TouchableOpacity onPress={()=>this.removePhoto()} style={{
                        width:36, 
                        height:36, 
                        borderRadius:18, 
                        borderWidth:2, 
                        borderColor:'white',
                        justifyContent:'center', 
                        alignItems:'center',
                      }}>
                        <Image source={require('que/assets/images/barbers/delete_photo_icn.png')} style={style.btnFavorite} />
                      </TouchableOpacity>

                      <View style={{width:80}}>
                      <CustomButton 
                        onPress={() => this.saveProfileBarber()} 
                        text={'SAVE'} />
                      </View>

                      <TouchableOpacity onPress={()=>this.addPhoto()} style={{
                        width:36, 
                        height:36, 
                        borderRadius:18, 
                        borderWidth:2, 
                        borderColor:'white',
                        justifyContent:'center', 
                        alignItems:'center',
                      }}>
                        <Image source={require('que/assets/images/add_member_account_costumer_icn.png')} style={style.btnFavorite} />
                      </TouchableOpacity>
                    </View>
                  ) : (
                    <TouchableOpacity onPress={()=>this.changePhoto()} style={{alignItems:'center'}}>
                      <View style={{width:28, height:28, backgroundColor:'white', borderRadius:14, padding:8, justifyContent:'center', alignItems:'center'}}>
                        <Image source={require('que/assets/images/barbers/add_photo_barber_profile_icn.png')} style={{height:20, width:20}} />
                      </View>
                      <Text style={{color:'rgba(255,255,255,1)', fontSize:18}}>Add a Photo</Text>
                    </TouchableOpacity>
                  )
                  }

                  </View>

                  <View style={{flex:1}}>


                  <TabButtons
                    type={'barber'}
                    active={activeTab}
                    changeView={v => this.setState({ activeTab:v })}
                  />

                  <View style={{
                            zIndex:2, 
                            marginTop:70, 
                            marginHorizontal:24, 
                            borderWidth:1 * PixelRatio.get(), 
                            borderColor:theme.primary, 
                            justifyContent:'center', 
                            alignItems:'center', }}>

                    <View style={{
                        marginTop:-8, 
                        width:120, 
                        backgroundColor:'white', 
                        paddingHorizontal:6,
                        borderRadius:10}}>
                      <Text style={{textAlign:'center', fontSize:11, color: theme.primary}}>
                          BARBER SHOP
                      </Text>
                    </View>
                    
                    <TextInput style={{
                                fontSize:22, 
                                color: '#fff', 
                                textAlign:'center', 
                                paddingVertical:16,
                                }}
                                placeholder={"Shop Name..."}
                                multiline={true}
                                numberOfLines={2}
                                textAlignVertical={'center'}
                                placeholderTextColor={'rgba(255,255,255,.4)'} />

                    
                   

                    <View style={style.sliderContainer} />
                  </View>
                    
                </View>

              </ImageBackground>

              <View style={style.midleContainer}>
                  {tabs[activeTab]}
              </View>

              <View style={style.bottomContainer}>
                  <BottomList
                    changeType={(type, id) => this.setState({ type }, () => this.fetchData(type, id))}
                    cardData={barber}
                    barbers={shop.barbers}
                    type={'barber'}
                    view={activeTab}
                    newAppointment={this.newAppointment}
                    action={(tag)=>this.openSocialLink(tag)}
                  >
                  <Text style={{color:'rgba(255,255,255,.6)', textAlign:'center',marginBottom:20}}>Tap an icon to connect a social media account</Text>
                  </BottomList>
                  
              </View>

              {workingHours &&
                {/* <WorkingHours 
                  open_time={listWorkingHours.length==0 ? shopWorking[0].shop.open_time : listWorkingHours}
                  onClose={()=>this.setState({workingHours: false})}
                  setTimeChanged={(field, val, key)=>this.setHoursWork(field, val, key)}
                /> */}
              }

              {socialLinkConfig && (
                <PopBottom
                  height={200}
                  imageIcon={require("que/assets/images/shopdetails/checkbox_services_checked_icn.png")}
                  actionClose={() => this.setSocialLink()}
                  isFinish={true}
                  size={0} >
                  <View style={{alignItems:'center', paddingHorizontal:16, paddingTop:8}}>
                    <Text>SET {socialLinkActive.toUpperCase()} LINK</Text>
                    <CustomInput
                      placeholder={"Input the link here..."}
                      onChange={val => this.setState({...this.state, payload:{...this.state.payload, [socialLinkActive]: val}})} 
                      value={null}
                      textColor={"#333333"}
                      placeholderTextColor={"#cccccc"}
                      lineColor={"#333333"} />
                  </View>
                </PopBottom>
              )}

            </View>
        )
    }
}

const mapStateToProps = store => ({
    shop: store.shopReducer.shop,
    user: store.userReducer.user,
    loading: store.shopReducer.loading,
    barber: store.shopReducer.barber,
    cuts: store.cutsReducer.list, 
    listCarrer: store.barberReducer.listCarrer,
})

const mapDispatchToProps = dispatch => {
    return bindActionCreators({
        //
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(withTheme(AddNewShop))