import React, {useState} from 'react'
import { View, Text, Image, TouchableOpacity, Animated, KeyboardAvoidingView, TextInput, PixelRatio } from 'react-native'
import CloseImageAnimated from "que/components/CloseImageAnimated"
import DoneImageAnimated from "que/components/DoneImageAnimated"
import {useAnimation} from 'que/hooks'
import { StyleSheet } from 'react-native'
import {colors} from 'que/components/base'
import CustomInput from "que/components/CustomInput"
// import DarkMask from 'que/components/DarkMask'
import BasePop from "que/components/BasePop"
// import CloseImageAnimated from "que/components/CloseImageAnimated"
// import DoneImageAnimated from "que/components/DoneImageAnimated"


const checkImg = require("que/assets/images/shopdetails/checkbox_services_checked_icn.png")
const closeImg = require("que/assets/images/close_pop_up_gray_account_costumer_icn.png")


export default (props) => {
  const animation = useAnimation({doAnimation:true, duration: 600})

  return (
    <>
    <BasePop>
      <TouchableOpacity activeOpacity={1} onPress={() => {}} style={styles.floatButton}>
        <CloseImageAnimated source={closeImg} />
      </TouchableOpacity>

      <View>
    
      
      <KeyboardAvoidingView behavior="position" >
            <TextInput
              placeholder={"Address..."}
              style={{
                borderColor:'gray',
                borderWidth: 1,
                paddingHorizontal: 8,
                paddingVertical: 8,
                marginHorizontal: 16,
                marginVertical: 16,
                height: "50%"
              }}
              multiline={true}
              numberOfLines={11}
              textAlignVertical={'top'}
              onChangeText={e => {}}
            />
          </KeyboardAvoidingView>

      </View>

    
    </BasePop>
    </>
  )
}


const styles = StyleSheet.create({
  viewCenter:{
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center", 
    marginTop:20,
  },
  clickCloseSave:{
    height: 66,
    width: 66,
    borderRadius: 33,
    borderWidth: 4,
    borderColor: colors.gray,
    alignItems: "center",
    justifyContent: "center"
  },
  clickSaveSave:{
    height: 66,
    width: 66,
    borderRadius: 33,
    borderWidth: 4,
    borderColor: colors.green,
    alignItems: "center",
    justifyContent: "center"
  },
  floatButton: {
    width: PixelRatio.get()>1.5 ? 60 : 50,
    height: PixelRatio.get()>1.5 ? 60 : 50,
    borderRadius: 30,
    borderWidth: 4,
    borderColor: colors.primary,
    marginTop: PixelRatio.get()>1.5 ? -30 : -25,
    backgroundColor: "white",
    justifyContent: "center",
    alignContent: "center",
    alignItems: "center",
  },
});
