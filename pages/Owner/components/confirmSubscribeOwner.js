import React, {useState} from 'react'
import { View, Text, Image, TouchableOpacity, Animated, PixelRatio } from 'react-native'
import CloseImageAnimated from "que/components/CloseImageAnimated"
import DoneImageAnimated from "que/components/DoneImageAnimated"
import {useAnimation} from 'que/hooks'
import { StyleSheet } from 'react-native'
import {colors} from 'que/components/base'
import CustomInput from "que/components/CustomInput"
import DarkMask from 'que/components/DarkMask'
import BasePop from "que/components/BasePop"


const checkImg = require("que/assets/images/shopdetails/checkbox_services_checked_icn.png")
const closeImg = require("que/assets/images/close_pop_up_gray_account_costumer_icn.png")


export default (props) => {
  const animation = useAnimation({doAnimation:true, duration: 600})

  return (
    <>
    <DarkMask />
    <BasePop height={300}>
      <TouchableOpacity activeOpacity={1} onPress={() => props.actionCancel()} style={styles.floatButton}>
        <CloseImageAnimated source={closeImg} />
      </TouchableOpacity>

      <View style={{marginHorizontal:28, marginTop:8}}>
        <Text style={{textAlign:'center'}}>BEFORE YOU BEGIN PLEASE MAKE SURE YOU HAVE THE FOLLOWING DOCUMENTS READY IF YOU ARE ADDING A NEW SHOP</Text>
        <View style={{height:1, backgroundColor:'gray', marginVertical:16}} />
        <Text style={{textAlign:'center', fontSize:12}}>{`BUSINESS REGISTRATION NO BUSINESS REGISTRANT NO.\nBUSINESS REGISTRANT ID\nBUSINESS PHONE NUMBER`}</Text>
      </View>

      <View style={styles.viewCenter}>
        <TouchableOpacity onPress={() => props.actionCancel()} style={styles.clickCloseSave}>
          <CloseImageAnimated 
            source={require("../../../assets/images/close_pop_up_gray_account_costumer_icn.png")}
            style={{ width: 25, height: 30, borderRadius: 23 }} />
        </TouchableOpacity>

        <Text style={{marginHorizontal:16}}>ARE YOU READY?</Text>
       
        <TouchableOpacity onPress={() => props.actionContinue()} style={styles.clickSaveSave}>
          <DoneImageAnimated 
            source={require("../../../assets/images/confirm_pop_up_account_costumer_icn.png")}
            style={{ width: 25, height: 30, borderRadius: 23 }} />
        </TouchableOpacity>
      </View>
    </BasePop>
    </>
  )
}


const styles = StyleSheet.create({
  viewCenter:{
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center", 
    marginTop:20,
  },
  clickCloseSave:{
    height: 66,
    width: 66,
    borderRadius: 33,
    borderWidth: 4,
    borderColor: colors.gray,
    alignItems: "center",
    justifyContent: "center"
  },
  clickSaveSave:{
    height: 66,
    width: 66,
    borderRadius: 33,
    borderWidth: 4,
    borderColor: colors.green,
    alignItems: "center",
    justifyContent: "center"
  },
  floatButton: {
    width: PixelRatio.get()>1.5 ? 60 : 50,
    height: PixelRatio.get()>1.5 ? 60 : 50,
    borderRadius: 30,
    borderWidth: 4,
    borderColor: colors.primary,
    marginTop: PixelRatio.get()>1.5 ? -30 : -25,
    backgroundColor: "white",
    justifyContent: "center",
    alignContent: "center",
    alignItems: "center",
  },
});
