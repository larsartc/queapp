import React from 'react'
import {View, Text, Image, TouchableOpacity, PixelRatio} from 'react-native'
import {useAnimation} from 'que/hooks'
import {Animated} from 'react-native'
import CustomButton from 'que/components/CustomButton'

export default (props) => {
  const animation = useAnimation({doAnimation: true, duration: 800})
  const animation2 = useAnimation({doAnimation: true, duration: 1200})

  return (
    <TouchableOpacity 
      activeOpacity={1} 
      onPress={()=>props.close()} 
      style={{
        flex:1, 
        width:'100%', 
        height:'50%', 
        zIndex:9, 
        position: 'absolute', 
        top:140/PixelRatio.get(), 
        left:0,
        }}>
      <Animated.View style={{
        transform: [
            // {
            //     translateX: animation.interpolate({
            //       inputRange: [0,1],
            //       outputRange: [120,0]
            //     })
            // },
            // {
            //     translateY: animation.interpolate({
            //     inputRange: [0,1],
            //     outputRange: [50,0]
            //   })
            // },
            {
                scaleX: animation.interpolate({
                  inputRange: [0,1],
                  outputRange: [.1,1]
                })
            },
            {
                scaleY: animation.interpolate({
                  inputRange: [0,1],
                  outputRange: [.1,1]
                })
            }
        ],
        position: 'absolute', 
        top:-360, 
        left:-100,
        width: 1000, 
        height: 1000, 
        borderRadius: 500,
        justifyContent:'center', 
        alignItems:'center', 
        backgroundColor:'rgba(40,10,100,.8)',
      }} />
      <View style={{justifyContent:'center', alignItems:'center', flex:1}}>
        <View style={{width:'100%', justifyContent:'flex-end', flexDirection:'row', marginTop:80}}>
          <Animated.View 
              style={{
                marginRight:18, 
                width:48, 
                height:48,
                alignItems:'center', 
                justifyContent:'center',
                display:'flex',
                backgroundColor:'white', 
                borderRadius:24, 
                transform: [
                    {
                      scaleX: animation2.interpolate({
                        inputRange: [0,1],
                        outputRange: [.1,1]
                      })
                    },
                    {
                      scaleY: animation2.interpolate({
                        inputRange: [0,1],
                        outputRange: [.1,1]
                      })
                    }
                ],
              }}
          >
            <Image source={require('que/assets/images/heart_onboarding_icn.png')} style={{width:24, height:24}} />
          </Animated.View>
          
        </View>
        
        <Image source={require('que/assets/images/arrow_curve.png')} style={{width:100, height:100}} />
        <Text style={{color:'white', textAlign:'center'}}>Use this to favorite cuts and beards</Text>
        <Text style={{color:'white', textAlign:'center', marginTop:20, paddingHorizontal:16, marginBottom:60}}>You can select a favorited hair style when booking appointments!</Text>
      
        <CustomButton 
          onPress={() => props.close()} 
          text={'DISMISS'} />
          
      </View>
    </TouchableOpacity>
  )
}