import React, {useState} from 'react'
import { View, Text, Image, TouchableOpacity, Animated } from 'react-native'
import CloseImageAnimated from "que/components/CloseImageAnimated"
import DoneImageAnimated from "que/components/DoneImageAnimated"
import {useAnimation} from 'que/hooks'
import { StyleSheet } from 'react-native'
import {colors} from 'que/components/base'
import CustomInput from "que/components/CustomInput"

export default AddPop = (props) => {
  const animation = useAnimation({doAnimation:true, duration: 600})
  const [nameCut, setNameCut] = useState()

  return (
    <View style={{marginHorizontal:30}}>
      <View style={{marginVertical:30, alignItems:"center"}}>
        <Animated.Image source={props.photo} 
        style={{
          width: animation.interpolate({
                    inputRange: [0,1], 
                    outputRange: [100, 120],
                  }), 
          height: animation.interpolate({
                    inputRange: [0,1], 
                    outputRange: [100, 120],
                  }),
          backgroundColor:"white", 
          borderRadius: 60,
          marginTop: animation.interpolate({
                    inputRange: [0,1], 
                    outputRange: [-70, -90],
                  }),
          borderColor: "#CCCCCC", 
          borderWidth:3,
        }} />
        <Text style={styles.saveCut}>SAVE CUT</Text>
      </View>

      <View style={styles.viewFormSaveCut}>
        <TouchableOpacity onPress={() => props.actionCancel()} style={styles.clickCloseSave}>
          <CloseImageAnimated 
            source={require("../../../assets/images/close_pop_up_gray_account_costumer_icn.png")}
            style={{ width: 25, height: 30, borderRadius: 23 }} />
        </TouchableOpacity>
        <View style={styles.viewForm}>
          <CustomInput
            lineColor={colors.gray}
            textColor={colors.black}
            placeholder={"Name your cut"}
            placeholderColor={"#999999"}
            onChange={(value) => setNameCut(value)}
          />
        </View>
        <TouchableOpacity onPress={() => props.actionContinue(nameCut)} style={styles.clickSaveSave}>
          <DoneImageAnimated 
            source={require("../../../assets/images/confirm_pop_up_account_costumer_icn.png")}
            style={{ width: 25, height: 30, borderRadius: 23 }} />
        </TouchableOpacity>
      </View>
    </View>
  )
}


const styles = StyleSheet.create({
  saveCut:{
    textAlign: "center", 
    color: colors.gray, 
    fontSize: 14, 
  },
  saveCut:{
    textAlign: "center", 
    color: colors.gray, 
    fontSize: 14
  },
  viewFormSaveCut:{
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center", 
    marginTop:20,
  },
  clickCloseSave:{
    height: 66,
    width: 66,
    borderRadius: 33,
    borderWidth: 4,
    borderColor: colors.gray,
    alignItems: "center",
    justifyContent: "center"
  },
  clickSaveSave:{
    height: 66,
    width: 66,
    borderRadius: 33,
    borderWidth: 4,
    borderColor: colors.green,
    alignItems: "center",
    justifyContent: "center"
  },
  viewForm:{
    width: 180,
    height: 40,
    marginRight: 10,
    marginLeft: 10
  },
});
