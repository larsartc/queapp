import React from "react";
import { StyleSheet } from "react-native";
import Styled from "./styled";
import Cut from "que/components/Cut";
import SearchBar from "que/components/SearchBar";
import { View, Content } from "native-base";
import {
  Image,
  TouchableOpacity,
  Platform,
  Text,
  StatusBar
} from "react-native";
import SideBar from "que/components/Sidebar";
import HeaderBar from "que/components/HeaderBar";
import { Favorite } from "que/utils/enums";
import PopBottom from "que/components/PopBottom";
import AddPop from "./components/addPop";
import TutorialHint from "./components/tutorialHint";
import AsyncStorage from "@react-native-community/async-storage";
import * as ASYNCTAG from "../../utils/storeNames";
import FullScreen from 'que/components/FullScreen';

//Redux
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import {
  listCut,
  favoriteCut,
  unfavoriteCut,
  cutSearch,
  setSearch
} from "../../store/cut";

//BASE COMPONENTS
import { colors, ShopContainer } from "que/components/base";
class Cuts extends React.Component {
  state = {
    saveCut: false,
    filter: "shaving",
    name_cut: "",
    cutDetails: {
      photo: ''
    },
    showTutorial: false,
    showMenu: false,
    scroll: 0,
    fullscreen: false
  };

  componentDidMount = () => {
    const { listCut, user } = this.props;
    listCut();

    favCuts = user.favorites.filter(fav => fav.type === 1);
    // to hiden the tutorial animation
    AsyncStorage.getItem(ASYNCTAG.TAG_CUT_TUTORIAL)
      .then(val => {
        if (val == null && favCuts.length === 0) {
          this.setState({ ...this.state, showTutorial: true });
        }
      })
      .catch(e => {});
  };

  closeTutorial = async () => {
    // save viewed tutorial
    await AsyncStorage.setItem(ASYNCTAG.TAG_CUT_TUTORIAL, "true");
    this.setState({ showTutorial: false });
  };

  saveCut = c => {
    this.setState({ cutDetails: c, saveCut: true });
  };

  saveCutFinal = nameCut => {
    const { favoriteCut, listCut } = this.props;
    const { cutDetails } = this.state;

    const type = "cut";
    favoriteCut({
      type: Favorite[type],
      entity_id: cutDetails._id,
      name: nameCut
    });
    this.setState({ saveCut: false });
    listCut();
  };

  removeCutFinal = c => {
    const type = c.type === 'shaving' ? 1 : 2
    const { unfavoriteCut } = this.props;
    unfavoriteCut({ type, entity_id: c._id });
  };

  pressFilter = pos => {
    this.setState({ ...this.state, filter: pos });
  };

  handleVisibleFullScreen = (c, visible) => {
    this.setState({ fullscreen: visible, cutDetails: c })
  }

  render() {
    const { cuts, loading, setSearch, search } = this.props;
    const {
      filter,
      saveCut,
      showTutorial,
      showMenu,
      cutDetails,
      scroll,
      fullscreen
    } = this.state;
    const cutsFiltered = cuts.filter(c => c.type == filter);

    return (
      <ShopContainer>
        <StatusBar barStyle="dark-content" />
        <FullScreen visible={fullscreen} setVisible={this.handleVisibleFullScreen} photo={cutDetails.photo}/>
        <SideBar
          {...this.props}
          navigation={this.props.navigation}
          close={() => this.setState({ showMenu: false })}
          showMenu={showMenu}
        />

        {cutsFiltered.length > 0 && showTutorial && (
          <TutorialHint close={() => this.closeTutorial()} />
        )}

        <HeaderBar
          title={"CUTS"}
          rightIcon={
            filter == "shaving"
              ? require("que/assets/images/hair_cuts_icn.png")
              : require("que/assets/images/beard_cuts_icn.png")
          }
          onPressRightIcon={() =>
            filter == "shaving"
              ? this.pressFilter("haircut")
              : this.pressFilter("shaving")
          }
          onPressLeftIcon={() => this.setState({ showMenu: true })}
        />

        <View style={{ flex: 1 }}>
          <SearchBar
            value={search}
            onChangeText={value => setSearch(value)}
            placeholder={"Search for style..."}
          />
          <View style={{ flex: 1, position: "relative", marginTop: 70 }}>
            {/* <LinearGradient
              style={{
                position: "absolute",
                top: 0,
                width: "100%",
                height: 40,
                zIndex: scroll ? 10 : 0
              }}
              colors={["rgba(255, 255, 255, .8)", "rgba(255, 255, 255, 0)"]}
              pointerEvents={"none"}
            /> */}
            <Styled.CardsContainer
              onScroll={() => this.setState({ scroll: !scroll })}
            >
              <Styled.ViewCuts>
                {cutsFiltered.map((c, i) => (
                  <Cut
                    onPress={() => this.handleVisibleFullScreen(c, true)}
                    showMask={!showMenu || Platform.OS === "ios"}
                    saves={c.num_favorites}
                    title={c.name}
                    favorite={c.favorite}
                    unfavoriteCut={() => this.removeCutFinal(c)}
                    type={this.state.filter}
                    saveCut={() => this.saveCut(c)}
                    imageBg={{ uri: c.photo }}
                    key={i}
                  />
                ))}
              </Styled.ViewCuts>
            </Styled.CardsContainer>
          </View>
        </View>

        {this.state.saveCut && (
          <PopBottom
            height={200}
            isFinish={true}
            size={0}
            activePosition={0}
            showNext={false}
          >
            <AddPop
              photo={{ uri: cutDetails.photo }}
              actionContinue={e => this.saveCutFinal(e)}
              actionCancel={() =>
                this.setState({ saveCut: !this.state.saveCut })
              }
            />
          </PopBottom>
        )}
      </ShopContainer>
    );
  }
}

const mapStateToProps = store => ({
  cuts: cutSearch(store),
  loading: store.cutsReducer.loading,
  user: store.userReducer.user,
  filters: store.shopReducer.filters,
  search: store.cutsReducer.search
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      listCut,
      favoriteCut,
      unfavoriteCut,
      cutSearch,
      setSearch
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(Cuts);
