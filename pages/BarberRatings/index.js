import React from 'react'
import { ImageBackground, TouchableOpacity, View, Image, Text, ScrollView, StyleSheet, StatusBar, PixelRatio } from 'react-native'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import {getRatingsBarberProfile} from '../../store/barber'
import { colors, BasePop, FavoriteContainer, FavoriteIcon } from 'que/components/base'
import { withTheme } from '../../utils/themeProvider'
import HeaderBar from "que/components/HeaderBar"
import styleShop from '../Shops/details/style'
import {ImageUser} from 'que/components/base'

const closeIcon = require('que/assets/images/close_barber_shop_icn.png')

const SKILL_MOCK = [
  'Fades', 'Line Ups', 'Professionalism', 'Cleanlines', 'Service', 'Family Friendly', 'Custom designs', 'Beards', 'Cuts', 'Tapers'
]

class BarberRatings extends React.PureComponent {

    state = {
      
    }

    componentDidMount = () => {
      this.props.getRatingsBarberProfile()
    }
    
    render() {
        const { user, barber, shop, cuts, theme, barberRatings } = this.props
        return (
            <View style={{ 
              display: 'flex', 
              flex: 1, 
              flexDirection: 'column', 
              backgroundColor: theme.primary 
              }} contentContainerStyle={{ flex: 1 }} >

              <StatusBar translucent backgroundColor={'rgba(0,0,0,.2)'} barStyle="light-content" />

              <HeaderBar
                title={'CAREER'}
                leftIcon={closeIcon}
                onPressLeftIcon={()=>this.props.navigation.pop()}
                lightMode
              /> 

              <View style={{alignItems:'center'}}>
                <ImageUser size={40} source={require("que/assets/images/profile_sign_in_img.jpg")} />
                <Text style={style.barberTypeName}> BARBER </Text>
                <View style={style.barberNameContainer}>
                  <Text style={style.barberName}> {user.name} </Text>
                </View>
                <View style={{flexDirection:'row', width:200, marginTop:12, marginBottom:12,justifyContent:'center'}}>
                  {Array(Math.ceil(user.barber.mid_rate)).fill(null).map((r) => {
                    return <Image source={require('que/assets/images/shopdetails/shop_raiting_filled_icn.png')} style={{ width:24, height:24, marginRight: 5 }} />
                  })}
                  {Array(5 - Math.ceil(user.barber.mid_rate)).fill(null).map((r) => {
                    return <Image source={require('que/assets/images/shopdetails/shop_raiting_empty_icn.png')} style={{ width:24, height:24, marginRight: 5 }} />
                  })}
                  <Text style={{color:'white', fontSize:20}}>({Math.ceil(user.barber.mid_rate)})</Text>
                </View>
              </View>

              <View style={{backgroundColor:'white', height:1, marginVertical:16}} />

              <ScrollView style={{width:'100%', height:'100%'}} showsVerticalScrollIndicator={false}>
              {/*barberRatings*/ }
                {user.barber instanceof Object && user.barber.ratings.map((i) =>
                  <View style={{ 
                    flexDirection: 'row', 
                    marginVertical: 8,
                    paddingHorizontal: 50 }}>
                    
                    <View style={{flex:1}}>
                      <Text style={{color: 'white'}}>{i.category}</Text>
                    </View>
                    <View style={{
                      flexDirection:'row', 
                      flex:1, 
                      alignItens:'center', 
                      justifyContent:'flex-end', 
                      textAlign:'right'}}>
                      {Array(Math.ceil(i.mid_rate)).fill(null).map((r) => {
                        return <Image 
                                  source={require('que/assets/images/shopdetails/shop_raiting_filled_icn.png')}
                                  style={{ 
                                  width:16, 
                                  height:16, 
                                  marginRight: 5 }} />
                      })}
                      {Array(5 - Math.ceil(i.mid_rate)).fill(null).map((r) => {
                        return <Image 
                                  source={require('que/assets/images/shopdetails/shop_raiting_empty_icn.png')} 
                                  style={{ 
                                  width:16, 
                                  height:16, 
                                  marginRight: 5 }} />
                      })}
                      <Text style={{color:'white'}}>({Math.ceil(i.mid_rate)})</Text>
                    </View>
                  </View>
                )}
              </ScrollView>

            </View>
        )
    }
}

const mapStateToProps = store => ({
    shop: store.shopReducer.shop,
    user: store.userReducer.user,
    loading: store.shopReducer.loading,
    barber: store.shopReducer.barber,
    cuts: store.cutsReducer.list, 
    listCarrer: store.barberReducer.listCarrer,
    barberRatings: store.barberReducer.barberRatings,
})

const mapDispatchToProps = dispatch => {
    return bindActionCreators({
      getRatingsBarberProfile,
    }, dispatch)
}

const style = StyleSheet.create({
  barberTypeName: {
    ...styleShop.barberTypeName,
    marginTop:10
  }, 
  barberNameContainer: styleShop.barberNameContainer, 
  barberName: styleShop.barberName,
})

export default connect(mapStateToProps, mapDispatchToProps)(withTheme(BarberRatings))