import styled from 'styled-components/native';
import { colors } from 'que/components/base';

export const Icons = styled.ImageBackground`
    height: 10;
    width: 10;
    margin-right: 3;
`