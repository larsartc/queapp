import React from 'react';
import BlockHelp from 'que/components/BlockHelp';
import { View, Content, Spinner, Drawer, Fab, Icon } from 'native-base'
import LoadingDefault from '../../components/LoadingDefault'
import SideBar from 'que/components/Sidebar'
import {StatusBar, Text, Image} from "react-native"
import HeaderBar from "que/components/HeaderBar"
import { ShopContainer } from '../../components/base'
import {withTheme} from '../../utils/themeProvider'

//Redux
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import {listHelps} from '../../store/help'

class Help extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showMenu: false
        }
    }

    componentDidMount = () => {
        const {listHelps} = this.props 
        listHelps()
    }

    render() {
        const {showMenu} = this.state
        const { helps, loading, theme } = this.props

        return (
            
                <ShopContainer background={theme.primary}>

                    <StatusBar barStyle="light-content" />

                    <SideBar 
                    {...this.props} 
                    navigation={this.props.navigation} 
                    close={() => this.setState({showMenu: false})}
                    showMenu={showMenu} />
                    <HeaderBar
                        title={'HELP'}
                        onPressLeftIcon={() => this.setState({showMenu: true})}
                        leftIcon={require('que/assets/images/menu_open_home_costumer_icn.png')}
                        lightMode
                    /> 
                    <View style={{flex:1}}>
                        <Content padder>
                            <View style={{display: 'flex', flexDirection: 'column', flexWrap: 'wrap'}}>
                                {/* {loading && <LoadingDefault color={colors.white} />} */}
                                {!loading && helps.length==0 && <Text style={{color:"white", textAlign:"center", width:"100%"}}>No items to show</Text> }
                                { helps.map((h, i) => <BlockHelp question={h.question} answer={h.answer} key={i} />) }
                            </View>
                        </Content>

                        {!showMenu && <Fab
                            active={true}
                            direction="up"
                            containerStyle={{ }}
                            style={{ backgroundColor: '#FFFFFF' }}
                            position="bottomRight"
                            onPress={() => this.props.navigation.push("ChatScreen")}>
                            <Image source={require('que/assets/images/mesage_notification.png')} style={{width:24, height:24}} />
                        </Fab>}
                    </View>
                </ShopContainer>
        )
    }
}

const mapStateToProps = store => ({
    helps: store.helpReducer.list, 
    loading: store.helpReducer.loading, 
    user: store.userReducer.user,
})

const mapDispatchToProps = dispatch => 
  bindActionCreators({
    listHelps,
  }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(withTheme(Help))