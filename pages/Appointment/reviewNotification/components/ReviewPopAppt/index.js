import React, { useState } from 'react'
import { ScrollView, View, Text, Image, TouchableOpacity } from 'react-native'
import styled from "styled-components/native"
import DailyLog from "que/components/DailyLog"
import { colors, TitlePop } from 'que/components/base'
import RateStarsAppt from '../RateStarsAppt'
import moment from 'moment'
import { TextInput } from 'react-native-gesture-handler';

export default ({ data, clickNext = () => { }, addRating, feedback, moreFeedback }) => {
    const [customDesign, setCustomDesign] = useState("")
    const { appointment } = data;
    const { barber, user, shop } = appointment;
    const time = moment.unix(Number.parseFloat(appointment.date)).format('hh:mm')

    const beard_style = appointment.beard_style.name != "" ? appointment.beard_style.name : "";
    const hair_style = appointment.hair_style.name != "" ? appointment.hair_style.name : "";

    let rates = ["PROFESSIONALISM", "CLEANLINESS", beard_style, hair_style].filter(item => !!item);
    return (
        <>
            <TitlePop>HOW WAS YOUR EXPERIENCE?</TitlePop>
            <ScrollView>
                <View style={{ paddingHorizontal: 24 }}>
                    <DailyLog
                        barber_image={{ uri: barber.barber.photos[0] }}
                        user_image={{ uri: user.photo }}
                        hair_style={appointment.hair_style}
                        beard_style={appointment.beard_style}
                        time={time}
                        barber_name={barber.name}
                        style_service={'Waves4'}
                        shop_name={shop.name}
                        background={colors.white}
                        line={false} />
                </View>

                {
                    rates.map(item => <RateStarsAppt
                        onClick={addRating}
                        title={item}
                    />)
                }

                {
                    !feedback &&
                    <View style={{ justifyContent: "center", alignContent: "center", alignItems: "center", marginBottom: 80 }}>
                        <TouchableOpacity onPress={() => moreFeedback()}>
                            <LinkHaveMore>Have more feedback?</LinkHaveMore>
                        </TouchableOpacity>
                    </View>
                }
                {
                    feedback &&
                    <View style={{width: "100%", justifyContent: "center"}}>
                        <View style={{height: 1, width: "100%", marginHorizontal: 20}}></View>
                        <Text style={{textAlign: "center"}}>
                            CUSTOM DESIGN
                        </Text>
                        <View style={{padding: 10, justifyContent:"center"}}>
                            <TextInput style={{textAlign: "center"}} onChangeText={(e) => setCustomDesign(e)} value={customDesign}/>
                        </View>
                        <RateStarsAppt
                            onClick={(rating) => addRating({  category: customDesign, value: rating.value } )}
                        />
                    </View>
                }

            </ScrollView>
        </>
    )
}

const LinkHaveMore = styled.Text`
  color: ${colors.lightBlue};
  font-size: 14;
`