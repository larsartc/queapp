import React, {useState, useEffect} from 'react'
import {ScrollView, FlatList, Text, TextInput} from 'react-native'
import styled from "styled-components/native"

import {colors} from 'que/components/base';

export default (props) => {
  return (
    <>
    <ScrollView>
        <TextInput 
        placeholder={`Let ${props.barber_name} know your thoughts on the experience!`}
        style={{
            borderColor: colors.primary,
            borderWidth: 1,
            paddingHorizontal:8, 
            paddingVertical:8, 
            marginHorizontal:16, 
            marginVertical:16,
        }}
        multiline={true}
        numberOfLines={11}
        textAlignVertical={'top'}
        onChangeText={e => props.onChange(e)}
        />
    </ScrollView>
    </>
  )
}