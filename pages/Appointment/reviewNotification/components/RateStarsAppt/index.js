import React, {useState} from "react" 
import { View, Text, TouchableOpacity} from 'react-native'
import styled from "styled-components/native"

export default (props) => {
  const [active, setActive] = useState(null)
  const title = !!props.title ? props.title : ""
  return ( 
    <View style={{justifyContent:"center", alignContent:"center", alignItems:"center", marginBottom: 15}}>
      {!!title && <Text>{title}</Text>}
      <View style={{flexDirection: "row", marginVertical:2}}>

        {

          Array(5).fill(null).map((v,i)=>{
            let imgsel = require('que/assets/images/shopdetails/apointment_purple_star_empty_icn.png')
            
            if(active !== null && active>=i){
              imgsel = require('que/assets/images/shopdetails/apointment_purple_star_complete_icn.png')
            }
            
            return (
              <TouchableOpacity onPress={()=> { props.onClick({ category: title, value: i }); setActive(active === null && i === 0 ? 0 : active === 0 && i === 0 ? null: i)} }>
                <Icons source={imgsel} />
              </TouchableOpacity>
            )
          
          })

        }
        
      </View>
    </View>
  )
}

export const Icons = styled.ImageBackground`
    height: 32;
    width: 32;
`