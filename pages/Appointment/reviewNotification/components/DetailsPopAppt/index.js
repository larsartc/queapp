import React, { useState } from 'react'
import { ScrollView, View, Text, Image, TouchableOpacity } from 'react-native'
import styled from "styled-components/native"
import DailyLog from "que/components/DailyLog"
import { colors, TitlePop } from 'que/components/base';
import moment from 'moment';

export default ({ data, clickNext}) => {
  const { appointment } = data;
  const { barber, user, shop } = appointment;
  const time = moment.unix(Number.parseFloat(appointment.date)).format('hh:mm')
  return (
    <>
      <TitlePop>APPOINTMENT IN LAST 24 HOURS</TitlePop>
      <ScrollView>
        <View style={{ paddingHorizontal: 24 }}>
          <DailyLog
            barber_image={{ uri: barber.barber.photos[0] }}
            user_image={{ uri: user.photo }}
            hair_style={appointment.hair_style}
            beard_style={appointment.beard_style}
            time={time}
            barber_name={barber.name}
            style_service={'Waves4'}
            shop_name={shop.name}
            background={colors.white}
            line={false} />
          <ButtonCustom onPress={() => clickNext(1)}>
            <Text style={{ color: colors.secondary, textAlign: "center" }}> HOW WAS YOUR APPOINTMENT? </Text>
          </ButtonCustom>
        </View>
      </ScrollView>
    </>
  )
}

export const ButtonCustom = styled.TouchableOpacity`
  width: 300;
  border-width: 1;
  border-color: ${colors.primary};
  border-radius: 60;
  color: ${colors.primary};
  padding: 10px;
  margin-left: 24;
  margin-right:24;
`