import React from 'react'
import { Dimensions, View } from 'react-native'
import PopBottom from "../../../components/PopBottom"
import CommentsAppt from "./components/CommentsAppt"
import DetailsPopAppt from "./components/DetailsPopAppt"
import ReviewPopAppt from "./components/ReviewPopAppt"
import AsyncStorage from "@react-native-community/async-storage"
import * as ASYNCTAG from "../../../utils/storeNames"
import Carousel from 'react-native-snap-carousel'

class ReviewNotification extends React.Component {

  state = {
    apptDetailsActive: 0,
    ratings: [],
    feedback: "",
    moreFeedback: false
  }

  closePopBottom = () => {
    this.setState({ showReviewAppt: false, notificationPayload: false })
  }

  clickNext = val => {
    this.setState({ apptDetailsActive: val })
  }

  makeRating = () => {
    const { ratings } = this.state;
    this.clickNext(2)
    this.props.makeRate(ratings)
  }

  makeFeedback = () => {
    const { feedback } = this.state;
    const { notificationPayload } = this.props;
    this.closePop()
    this.props.makeFeedback({ text: feedback, appointment: notificationPayload.appointment._id })
  }

  moreDetails = () => {
    this.setState({ moreFeedback: true })
  }

  addRating = rating => {
    rating.value++;
    let ratings = this.state.ratings;
    const { appointment } = this.props.notificationPayload;
    const { barber, shop, user } = appointment;

    ratings = ratings.filter(item => item.category !== rating.category);
    const temp_rating = { ...rating, user: user._id }
    ratings.push({ ...temp_rating, entity_id: barber._id, entity_type: "barber" });
    ratings.push({ ...temp_rating, entity_id: shop._id, entity_type: "shop" });

    this.setState({ ratings });
  }

  feedback = (value) => {
    this.setState({ feedback: value });
  }

  closePop = () => {
    this.setState({ apptDetailsActive: 0, feedback: "", ratings: [] });
    this.props.close();
  }

  renderItem = (val) => {
    const { apptDetailsActive, moreFeedback } = this.state
    const { notificationPayload } = this.props
    const detailsAppt = [
      <DetailsPopAppt data={notificationPayload} clickNext={() => this.clickNext(1)} />,
      <ReviewPopAppt addRating={(rating) => this.addRating(rating)} data={notificationPayload} clickNext={() => this.clickNext(2)} feedback={moreFeedback} moreFeedback={() => this.moreDetails()}  />,
      <CommentsAppt barber_name={!!Object.keys(notificationPayload).length ? notificationPayload.appointment.barber.name : ""} onChange={this.feedback} />
    ]
    return <View>{detailsAppt[val.index]}</View>
  }

  render() {
    const { apptDetailsActive, ratings, feedback, moreFeedback } = this.state
    const { showReviewAppt, notificationPayload } = this.props
    console.log('APPOINTMENT REVIEW')
    console.log(showReviewAppt)
    console.log(notificationPayload)

    const detailsAppt = [
      <DetailsPopAppt data={notificationPayload} clickNext={() => this.clickNext(1)} />,
      <ReviewPopAppt addRating={(rating) => this.addRating(rating)} data={notificationPayload} clickNext={() => this.clickNext(2)} feedback={moreFeedback} moreFeedback={() => this.moreDetails()}/>,
      <CommentsAppt barber_name={!!Object.keys(notificationPayload).length ? notificationPayload.appointment.barber.name : ""} onChange={this.feedback} />
    ]

    if (!showReviewAppt) {
      return null
    }

    let image = require("que/assets/images/close_pop_up_gray_account_costumer_icn.png")
    if (apptDetailsActive === 0) {
      image = require("que/assets/images/alert_icn.png")
    }
    if (ratings.length > 0 || feedback !== "") {
      image = require("que/assets/images/shopdetails/checkbox_services_checked_icn.png")
    }

    return (
      <PopBottom
        height={apptDetailsActive === 1  ? 400: 315}
        imageIcon={image}
        actionClose={() =>
          apptDetailsActive !== 0 && (ratings.length > 0 || feedback !== "")
            ? (apptDetailsActive === 1 ? this.makeRating() : this.makeFeedback())
            : this.closePop()
        }
        isFinish={true}
        size={detailsAppt.length}
        activePosition={apptDetailsActive}
        showNext={false} 
        hide={true} >
        {detailsAppt[apptDetailsActive]}

      </PopBottom>
    )

  }
}

export default ReviewNotification