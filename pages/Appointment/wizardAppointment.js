import React from 'react'
import { StyleSheet, TouchableOpacity, View, Image, Text, Alert, PixelRatio } from 'react-native'

import DarkMask from '../../components/DarkMask'
import DefaultConfigAptt from '../../components/DefaultConfigAptt'
import ServicesConfigAptt from '../../components/ServicesConfigAptt'
import StyleSelect from '../../components/StyleSelect'
import PinCounters from '../../components/PinCounters'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { findShop, findBarber, favAction, favorite, unFavorite, } from '../../store/shop'
import { listCut } from '../../store/cut'
import { makeAppointment, updateAppointment } from '../../store/appointment'
import { useAnimation } from 'que/hooks'
import { Animated } from 'react-native'
import { ifIphoneX } from 'react-native-iphone-x-helper'
import styled from "styled-components/native"
import BasePop from "que/components/BasePop"
import CloseImageAnimated from "que/components/CloseImageAnimated"
import DoneImageAnimated from "que/components/DoneImageAnimated"
import _ from "lodash"
//Colors
import { colors } from 'que/components/base'
import moment from 'moment'

const checkImg = require("que/assets/images/shopdetails/checkbox_services_checked_icn.png")
const closeImg = require("que/assets/images/close_pop_up_gray_account_costumer_icn.png")

const titles = {
  DefaultConfigAptt: {
    CUSTOMER: "SETUP APPOINTMENT",
    BARBER: "VIEW APPOINTMENT"
  },
  ServicesConfigAptt: {
    CUSTOMER: "SELECT SERVICES",
    BARBER: "VIEW SERVICES"
  },
  HairStyle: {
    CUSTOMER: "SELECT HAIR STYLE",
    BARBER: "VIEW HAIR STYLE"
  },
  BeardStyle: {
    CUSTOMER: "SELECT BEARD STYLE",
    BARBER: "VIEW BEARD STYLE"
  },
}

class WizardAppointment extends React.Component {
  state = {
    payloadAppointment: {
      day: null,
      time: "",
      barber: "",
      date: "",
      user: "",
      services: [],
      hair_style: "",
      beard_style: ""
    },
  }

  componentDidMount() {
    this.props.listCut()
    if (this.props.barber) return this.setState({ payloadAppointment: { ...this.state.payloadAppointment, barber: this.props.barber } });

    if (!this.props.appointment) return false;

    this.setState({
      payloadAppointment: this.getDefaultData()
    })

  }

  setAppointCheck = (key, val) => {
    this.setState({
      ...this.state,
      [key]: val
    })
  }

  setAppointment = (e, val) => {
    const { payloadAppointment } = this.state

    if (this.props.userType != "CUSTOMER") return false;

    let { barber, date, user, services, hair_style, beard_style, day, time, } = payloadAppointment;

    switch (e) {
      case "appointmentBarber":
        barber = val
        break
      case "appointmentDay":
        day = val.day;
        date = val.date;
        break;
      case "appointmentTime":
        time = val.time;
        date = val.date;
        break;
      case "appointmentUser":
        user = val
        break
      case "serviceSelect":
        services = val
        break
      case "selectHairStyle":
        hair_style = val
        break
      case "selectBeardStyle":
        beard_style = val
        break
      default:
        break
    }
    this.setState({
      payloadAppointment: {
        ...this.state.payloadAppointment,
        user,
        barber,
        shop: this.props.shop,
        hair_style,
        beard_style,
        date,
        day,
        time,
        services
      }
    })
  }

  formatData = () => {
    let total_price = 0;

    this.state.payloadAppointment.services.forEach((item) => {
      total_price = total_price + item.price
    });

    return {
      ...this.state.payloadAppointment,
      shop: this.state.payloadAppointment.shop._id,
      total_price,
    }
  }

  sendAppointment = () => {
    if (this.props.appointment) {
      this.props.updateAppointment({ ...this.state.payloadAppointment, shop: this.props.shop._id }, this.props.appointment.day)
    } else {
      this.props.makeAppointment(this.formatData())
    }
    this.props.closeCreateAppoint()
  }

  verifyAppointment = () => {
    const { stepAppointment } = this.props
    if (!this.props.edit && stepAppointment == 0) {
      this.props.closeCreateAppoint()
    } else {
      this.sendAppointment()
    }
  }

  getDefaultData = () => {
    const { appointment } = this.props;

    const beard_style = this.props.appointment.beard_style;
    const hair_style = this.props.appointment.hair_style;

    return {
      date: appointment.date,
      barber: appointment.barber ? appointment.barber : "",
      day: appointment.date ? moment.unix(appointment.date).format("YYYY/MM/DD") : "",
      time: appointment.date ? moment.unix(appointment.date).format("HH:mm") : "",
      user: appointment.user ? appointment.user._id : "",
      services: appointment.selected_services ? appointment.selected_services : [],
      hair_style: !!hair_style ? hair_style : null,
      beard_style: !!beard_style ? beard_style : null,
      _id: appointment._id ? appointment._id : null
    }
  }

  checkChanges = () => {
    let changed = false;

    //attrs to check if any change.
    const attrs = ["user", "barber", "shop", "total_price", "hair_style", "beard_style", "date", "services"];

    const currentValues = this.formatData();
    attrs.forEach((attr) => {
      if (!_.isEqual(currentValues[attr], this.props.appointment[attr])) {
        console.log("changed", attr)
        changed = true;
      }
    });

    return changed;

  }

  render() {
    const { shop, cuts, stepAppointment, userType, family } = this.props;

    const stepsAppointment = [
      <DefaultConfigAptt
        title={titles["DefaultConfigAptt"][userType]}
        barbers={!!shop && !!shop.barbers ? shop.barbers : []}
        family={family}
        selected={{ ...this.state.payloadAppointment }}
        click={(e, val) => this.setAppointment(e, val)}
      />,
      <ServicesConfigAptt
        title={titles["ServicesConfigAptt"][userType]}
        list={this.props.services}
        selected={this.state.payloadAppointment.services}
        click={(val) => this.setAppointment("serviceSelect", val)}
      />,
      <StyleSelect
        title={titles["HairStyle"][userType]}
        list={cuts.filter((item) => item.type != "shaving")}
        selected={this.state.payloadAppointment.hair_style}
        click={(val) => this.setAppointment("selectHairStyle", val)}
      />,
      <StyleSelect
        title={titles["BeardStyle"][userType]}
        list={cuts.filter((item) => item.type == "shaving")}
        selected={this.state.payloadAppointment.beard_style}
        click={(val) => this.setAppointment("selectBeardStyle", val)}
      />,
    ]

    return (
      <>
        <DarkMask />
        <BasePop>
          <TouchableOpacity activeOpacity={1} onPress={() => this.verifyAppointment()} style={styles.floatButton}>
            {(stepAppointment < 1 && !this.props.appointment || userType != "CUSTOMER") ? <CloseImageAnimated source={closeImg} /> : <DoneImageAnimated source={checkImg} />}
          </TouchableOpacity>
          <View style={{ width: "100%", flex: 1 }}>
            {stepsAppointment[stepAppointment]}
            <View style={styles.content}>
              <PinCounters size={4} active={stepAppointment} />
              {
                stepAppointment !== 0 && !this.props.edit && (
                  <TouchableOpacity onPress={() => this.props.backStepAppointment()} style={styles.backButton}>
                    <Text style={{ color: 'red' }}>BACK</Text>
                  </TouchableOpacity>
                )
              }

              {
                stepAppointment !== 3 && (
                  <TouchableOpacity onPress={() => this.props.changeStepAppointment()} style={styles.nextButton}>
                    <Text>NEXT</Text>
                  </TouchableOpacity>
                )
              }

              {
                this.props.edit && (
                  <TouchableOpacity onPress={() => this.props.cancelAppointment()} style={{ position: 'absolute', left: 10, bottom: 5 }}>
                    <Text style={{ fontSize: 12, color: colors.red }}>CANCEL</Text>
                  </TouchableOpacity>
                )
              }
            </View>
          </View>
        </BasePop>
      </>
    )
  }
}

const styles = StyleSheet.create({
  floatButton: {
    width: PixelRatio.get() > 1.5 ? 60 : 50,
    height: PixelRatio.get() > 1.5 ? 60 : 50,
    borderRadius: 30,
    borderWidth: 4,
    borderColor: colors.primary,
    marginTop: PixelRatio.get() > 1.5 ? -30 : -25,
    backgroundColor: "white",
    justifyContent: "center",
    alignContent: "center",
    alignItems: "center",
  },
  icon: {
    width: PixelRatio.get() > 1.5 ? 34 : 28,
    height: PixelRatio.get() > 1.5 ? 34 : 28
  },
  content: { flexDirection: "row", position: 'relative', justifyContent: 'center', alignItems: 'center', textAlign: 'center', display: 'flex' },
  nextButton: { position: 'absolute', right: 10, bottom: 5 },
  backButton: { position: 'absolute', left: 10, bottom: 5 },
})

const mapStateToProps = store => ({
  userType: store.userReducer.interface,
  loading: store.shopReducer.loading,
  cuts: store.cutsReducer.list,
})

const mapDispatchToProps = dispatch => {
  return bindActionCreators({
    findShop,
    findBarber,
    favorite,
    unFavorite,
    favAction,
    makeAppointment,
    updateAppointment,
    listCut
  }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(WizardAppointment)