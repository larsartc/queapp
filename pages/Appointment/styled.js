import styled from 'styled-components';
import { colors } from 'que/components/base';

export default styledContainer = {
    DailyLog: styled.View`
        ${'' /* width: 100%;
        margin-top: 20; */}
        background-color: ${colors.white};
        ${'' /* flex: 2; */}
    `,
    TextDaily: styled.Text`
        color: ${colors.primary};
        text-transform: uppercase;
        text-align: center;
        padding-top: 10px;
        padding-bottom: 10px;
    `,
    DescriptionText: styled.Text`
        color: ${colors.primary};
        text-align: center;
    `,
    Header: styled.View`
        margin-top: 10;
        margin-bottom: 10;
        height: 10%;
    `,
    VerticalLine: styled.View`
        position: absolute;
        left: 15.8;
        width: 1;
        backgroundColor: ${colors.third};
        height: 100%;
        z-index: 2;
        margin-left: 5;
        margin-right: 5;
    `,
    VerticalLineRight: styled.View`
        position: absolute;
        right: 15.8;
        width: 1;
        backgroundColor: ${colors.third};
        height: 100%;
        z-index: 2;
        margin-left: 5;
        margin-right: 5;
    `,
    HeaderTitleBar: styled.View`
        display: flex;
        height: 20;
        flex-direction: row;
        justify-content: space-between;
        margin-left: 10%;
        margin-right: 10%;
    `,
    HeaderTitle: styled.Text`
        color: ${colors.white};
    `,
    HeaderIconContainer: styled.TouchableOpacity`
    `,
    HeaderIcon: styled.Image`
        height: 15;
        width: 15;
    `,
    DailyLeft: styled.Image`
        margin-left: 1;
        top: 3;
        position: absolute;
        height: 30;
        width: 30;
        background-color: white;
        z-index: 3;
    `,
    DailyRight: styled.Image`
        margin-right: 1;
        top: 3;
        right: 0;
        position: absolute;
        height: 30;
        width: 30;
        background-color: white;
        z-index: 3;
    `,
    CardsContainer: styled.View`
        flex: 1;
    `,
    DayComponentContainer: styled.TouchableOpacity`
        border-width: 0.5;
        width: 42;
        height: ${props => props.height};
        border-color: white;
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        position: relative;
        border-bottom-width: ${props => props.borderBottomWidth}; 
        background-color: ${props => props.bgColor}
    `,

    // border-top-left-radius: ${data => data.itemKey == 1  ? 8 : 0};
    // border-top-right-radius: ${(data) => data.itemKey == 3 ? 8 : 0 };
    // border-bottom-left-radius: ${data => data.itemKey == 29  ? 8 : 0};
    // border-bottom-right-radius: ${data => data.itemKey == 35  ? 8 : 0};
    // background-color: ${props => !!props.date ? "red" : "white"}

    DayLabel: styled.Text`
        color: white;
        font-size: 18;
    `,

    BallToday: styled.View`
        width: 4px;
        height: 4px;
        border-radius: 2;
        background-color: white;
        position: absolute;
        bottom: 0;
        margin-bottom: 3;
    `,
    LineScrollViewAppt: styled.View`
    width: 100%;
    height: 3;
    background-color: #5E4086;
    border-top-width: 0.5;
    border-top-color: #FFFFFF;
    `
}