import React from "react"
import {useAnimation} from 'que/hooks'
import {Animated} from 'react-native'


// TODO REMOVE this ... but before confirme it
export default ({doAnimation = false, source}) => {
  const animation = useAnimation({doAnimation, duration: 100})
  return (
    <Animated.Image style ={{
      height: animation.interpolate({
        inputRange: [0,1],
        outputRange: [50,70]
      }),
      width: animation.interpolate({
        inputRange: [0,1],
        outputRange: [50,70]
      }),
      borderWidth: 2,
      borderColor: 'white',
      marginLeft: 10, 
      marginRight: 10,
      borderRadius: 50,
    }} 
    source={source}
    />
  )
}