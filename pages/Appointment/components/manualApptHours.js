import React from 'react'
import { View, Text, ScrollView } from 'react-native'
import CustomInput from 'que/components/CustomInput'
import TimeRoundPicker from 'que/components/TimeRoundPicker'
import { getHoursByDay } from "que/utils/getHoursByDay"
import moment from 'moment'

export default (props) => {

  let hours = []

  getHoursByDay().map(val => {
    hours.push({ time: moment(val).format("hh:mm a") })
  })

  return (
    <View style={{ alignItems: 'center', paddingHorizontal: 16, paddingTop: 8 }}>
      <Text>SETUP APPOINTMENT</Text>
        <CustomInput
          image={require("que/assets/images/password_sign_in_icn.png")}
          placeholder={"Customer name..."}
          onChange={val => props.changeName(val)}
          value={props.name}
          textColor={"#333333"}
          placeholderTextColor={"#cccccc"}
          lineColor={"#333333"}
        />

      <ScrollView horizontal={true} style={{ marginTop: 10 }} showsHorizontalScrollIndicator={false}>

        {hours.map(val => {
          return <TimeRoundPicker
            data={val}
            active={props.selectedTime == val.time}
            click={() => props.changeDate(val)}
          />
        })}

      </ScrollView>
    </View>
  )
}