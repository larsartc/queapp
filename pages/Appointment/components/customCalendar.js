import React from "react" 
import moment from 'moment'

import {View, Text, TouchableOpacity} from 'react-native'
import Icon from "react-native-vector-icons/AntDesign"

export default CustomCalendar = () => {
  
  let month = moment().format("MMMM").toUpperCase()
  let dayWeek = moment().isoWeekday(0)
  let daysMonth = moment().daysInMonth()

  var arrDays = [];

  while(daysMonth) {
    var current = moment().date(daysMonth);
    arrDays.push(current);
    daysMonth--;
  }

  const daysWeek = ["SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"]
  
  return ( 
    <View>
      
      <View style={{flexDirection:'row',justifyContent:"center", alignItems:'center', marginVertical:8}}>
        <TouchableOpacity style={{padding:8}}><Icon name={'left'} style={{ color: "white" }} /></TouchableOpacity>
        <Text style={{color:'white', marginHorizontal:24}}>{month}</Text>
        <TouchableOpacity style={{padding:8}}><Icon name={'right'} style={{ color: "white" }} /></TouchableOpacity>
      </View>

      <View>

        <View style={{flexDirection:'row',justifyContent:"space-between"}}>{daysWeek.map(val=><Text style={{color:'white', paddingHorizontal:8}}>{val}</Text>)}</View>

        {arrDays.map(val=><Text style={{color:'white', paddingHorizontal:8}}>{val.format("DD")}</Text>)}

      </View>


    </View>
  )

}



/*
<Calendar
  dayComponent={({ date, state }) => {
    return (
      <Styled.DayComponentContainer
        key={date.dateString}
        ref={cmp => !date.dateString ? '' : this.accordionContent[date.dateString] = cmp}
        onPress={() => this.setDay(date.dateString)}
        bgColor={this.state.date == date.dateString ? "#5E4086" : "#201233"}
        height={this.state.date == date.dateString && !!appointments.length ? 50 : 42}
        borderBottomWidth={this.state.date == date.dateString && !!appointments.length ? 0 : 0.5} >
        {(!state || currentDay == date.dateString) &&
          <>
            <Styled.DayLabel>{date.day}</Styled.DayLabel>
            {datesWithAppointments.includes(date.dateString) &&
              <Styled.BallToday></Styled.BallToday>
            }
          </>
        }
      </Styled.DayComponentContainer>
    )
  }}
  current={this.state.date}
  currentMonth={moment(this.state.date, "YYYY-MM-DD").format("MM")}
  minDate={'2012-05-10'}
  onDayLongPress={(day) => { console.log('selected day', day) }}
  monthFormat={'MMMM'}
  // onMonthChange={({ month, year }) => {
  //     this.changeMonth(month, year)
  // }}
  onPressArrowLeft={() => {
      this.setState({ date: moment(this.state.date, "YYYY-MM-DD").subtract(1, "months").format("YYYY-MM-DD") })
  }}
  onPressArrowRight={() => {
      this.setState({ date: moment(this.state.date, "YYYY-MM-DD").add(1, "months").format("YYYY-MM-DD") })
  }}
  markedDates={this.getDays()}
  refreshing={true}
  hideArrows={false}
  renderArrow={(direction) => <Icon name={direction} style={{ color: "white" }} />}
  hideExtraDays={false}
  disableMonthChange={false}
  firstDay={1}
  hideDayNames={false}
  showWeekNumbers={false}
  theme={{
    calendarBackground: theme.secondary,
    textSectionTitleColor: '#ffffff',
    selectedDayBackgroundColor: "red",
    todayTextColor: '#ffffff',
    dayTextColor: '#ffffff',
    textDisabledColor: '#d9e1e8',
    dotColor: '#00adf5',
    selectedDotColor: '#ffffff',
    arrowColor: 'orange',
    monthTextColor: '#ffffff',
    indicatorColor: '#ffffff',
    textDayFontWeight: '300',
    textMonthFontWeight: 'bold',
    textDayHeaderFontWeight: '300',
    textDayFontSize: 16,
    textMonthFontSize: 16,
    textDayHeaderFontSize: 16
  }}
/>
*/