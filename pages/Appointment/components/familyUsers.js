import React, {useState, useEffect} from "react" 
import {TouchableOpacity, Image, Dimensions, View} from 'react-native'
import Carousel from 'react-native-snap-carousel'

export default ({user}) => {

  return (
    <View>
      
      {/* <Carousel
        ref={(c) => this._carousel = c}
        data={user.family}
        renderItem={()=>{
          return (
            <TouchableOpacity onPress={()=>{}} >
              <Image style ={{
                      height: 70,
                      width: 70,
                      borderWidth: 2,
                      borderColor: 'white',
                      marginLeft: 10, 
                      marginRight: 10,
                      borderRadius: 50,
                    }} source={require("que/assets/images/profile_sign_in_img.jpg")} />
            </TouchableOpacity>
          )
        }}
        sliderWidth={Dimensions.get('window').width}
        itemWidth={80}
        onSnapToItem={e=>console.log(e)}
        inactiveSlideOpacity={1}
        inactiveSlideScale={.6}
        /> */}

        <View style={{flexDirection:"row", justifyContent:"center"}}>
        {
          user.family.map(val => {
            return (
              <TouchableOpacity onPress={()=>{}} >
                <Image 
                  style ={{
                    height: 30,
                    width: 30,
                    borderWidth: 2,
                    borderColor: 'white',
                    marginLeft: 8, 
                    marginRight: 8,
                    borderRadius: 25,
                  }} 
                  source={!!val.photo && !!val.photo.length > 0 ? { uri: val.photo } : require("que/assets/images/profile_sign_in_img.jpg")} />
              </TouchableOpacity>
            )
          })
        }
        </View>

    </View>
  )
}