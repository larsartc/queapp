import React from 'react';
import Styled from './styled';
import DailyLog from 'que/components/DailyLog';
import DailyLogBarber from 'que/components/DailyLogBarber';
import { View, Content, Fab } from 'native-base'
import { TouchableOpacity, Image, Text, ScrollView, Alert, StatusBar, Dimensions, KeyboardAvoidingView, UIManager, findNodeHandle } from 'react-native'
import { ShopContainer } from 'que/components/base'
import { Calendar } from 'react-native-calendars'
import SideBar from 'que/components/Sidebar'
import Icon from "react-native-vector-icons/AntDesign"
import HeaderBar from "que/components/HeaderBar"
import { withTheme } from '../../utils/themeProvider'
import moment from "moment"
import WizardAppointment from './wizardAppointment'
import FamilyUsers from './components/familyUsers'
import PopBottom from "que/components/PopBottom"
import CancelAppt from './components/cancelAppt'
import _ from "lodash"
import CustomCalendary from "./components/customCalendar"
import ManualApptHours from './components/manualApptHours'
import ServicesConfigAptt from 'que/components/ServicesConfigAptt'
import Carousel from 'react-native-snap-carousel'

//Redux
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { listAppoitments, removeAppointment, getApptByDay, appointmentsClear } from '../../store/appointment'
import { setShopOpened, findShop } from '../../store/shop'
import { listCut } from '../../store/cut'
import { makeAppointment } from '../../store/barber'

const containerDayStyle = {
  borderRadius: 0,
  borderWidth: 0.5,
  width: 42,
  height: 42,
  borderColor: "white",
  justifyContent: "center"
}

const textDayStyle = {
  color: 'white',
  fontSize: 18,
  alignSelf: "center",
}

const manualAppt = {
  manualAppointment: false,
  manualAppointmentStep: 0,
  manualApptPayload: {
    user_name: null,
    total_price: 0,
    date: null,
    services: [],
  }
}

class Appointments extends React.Component {
  constructor(props) {
    super(props)
    this.timerCalendar = {}
    this.accordionContent = {};
    this.state = {
      appointment: {
        _id: null,
        day: null,
        time: null,
        barber: null,
        barber_name: null,
        date: null,
        user: null,
        shop: null,
        services: null,
        hair_style: null,
        beard_style: null,
      },
      datesWithAppointments: [],
      appointments: [],
      editAppointment: false,
      stepAppointment: 0,
      cancelAppointment: 0,
      date: null,
      marginTopScroll: 0,
      showMenu: false,
      ...manualAppt
    }
  }

  componentDidMount = () => {
    const { listAppoitments } = this.props
    listAppoitments(moment().format('MM'))
    listCut();
    const day = moment().format("YYYY-MM-DD");

    this.timerCalendar = setTimeout(() => this.setDay(day), 1000);
  }


  componentDidUpdate(prevProps, prevState) {
    if (!_.isEqual(this.props.shop, prevProps.shop)) {
      this.setState({
        appointment: { ...this.state.appointment, shop: this.props.shop },
        editAppointment: true
      })
    }
  }

  close = () => {
    this.props.close()
  }

  openAppointment = val => {

    if (moment(this.state.date, 'YYYY-MM-DD').isBefore(moment().format('YYYY-MM-DD'))) {
      Alert.alert("Ops", "This appointment was closed.")
      return false
    }

    // if (!val.user) {
    if (val.user_name != "") {
      Alert.alert("Ops", "You can't edit this appointment!")
      return false;
    }
    // }

    let shop = {};

    if(val.shop._doc){
      shop = val.shop._doc;
    }else{
      shop = val.shop
    }
    this.setState({
      appointment: {
        _id: val._id,
        day: moment.unix(val.date).format('YYYY-MM-DD'),
        time: moment.unix(val.date).format('YYYY-MM-DD'),
        barber: val.barber._id,
        barber_name: val.barber.name,
        date: val.date,
        user: val.user,
        shop: shop,
        selected_services: val.services,
        services: shop.services,
        hair_style: val.hair_style ? val.hair_style._id : null,
        beard_style: val.beard_style ? val.beard_style._id : null,
      },
      editAppointment: true
    })
  }

  closeEditAppointment = () => {
    this.setState({
      ...this.state,
      editAppointment: false,
      stepAppointment: 0
    })
  }

  cancelAppointment = () => {
    this.setState({
      ...this.state,
      editAppointment: false,
      stepAppointment: 0,
      cancelAppointment: 1
    })
  }

  changeStepAppointment = () => {
    this.setState({
      ...this.state,
      stepAppointment: this.state.stepAppointment + 1
    })
  }

  backStepAppointment = () => {
    this.setState({
      ...this.state,
      stepAppointment: this.state.stepAppointment - 1
    })
  }

  setAppointCheck = (key, val) => {
    this.setState({
      ...this.state,
      [key]: val
    })
  }

  getHours = val => {
    return moment.unix(val).format("HH:ss")
  }

  getDays = (month = moment(this.state.date, 'YYYY-MM-DD').format('MM')) => {
    var count = moment().month(month).daysInMonth();
    var dates = {}
    for (var i = 1; i < count + 1; i++) {
      const dayMoment = moment(i, "D").format("DD");
      const date = moment(this.state.date, 'YYYY-MM-DD').format("YYYY-MM") + "-" + dayMoment;
      let containerStyle = { ...containerDayStyle }
      dates[date] = {
        customStyles: {
          container: this.state.date == date ? { ...containerStyle, backgroundColor: "#5E4086" } : containerStyle,
          text: textDayStyle,
        },
      }
    }
    return dates;
  }

  setDay = (date) => {
    if (!this.accordionContent.hasOwnProperty(date)) return false;
    return this.accordionContent[date].measure((fx, fy, width, height, px, py) => {
      const month = moment(date, "YYYY-MM-DD").format("MM");
      const day = moment(date, "YYYY-MM-DD").format("DD");
      this.props.getApptByDay(month, day)
      if (py != 0) {
        this.setState({ date: date, marginTopScroll: py + 50 }, () => {
          clearTimeout(this.timerCalendar);
          return py
        });
      }
    })
  }

  createManualAppt = async () => {
    let payload = {
      shop: this.props.user.shopsWorking[0].shop._id,
      user_name: this.state.manualApptPayload.user_name,
      total_price: this.state.manualApptPayload.total_price,
      hair_style: "hair_style",
      beard_style: "beard_style",
      date: moment(this.state.date + ' ' + this.formatHour(this.state.manualApptPayload.date.time)).unix(),//this.state.date . this.state.manualApptPayload.date,
      services: this.state.manualApptPayload.services,
    }
    await this.props.makeAppointment(payload)
    const month = moment(this.state.date, "YYYY-MM-DD").format("MM");
    const day = moment(this.state.date, "YYYY-MM-DD").format("DD");
    await this.props.listAppoitments(month)
    await this.props.getApptByDay(month, day)
    this.setState({ manualAppointmentStep: this.state.manualAppointmentStep + 1 })
  }

  formatHour = (oldHour) => {
    var itemHour = oldHour;
    var splithere = itemHour.split(" ");
    return splithere[0];
  }

  renderManualAppointment = () => {
    const { manualAppointmentStep } = this.state

    const steps = [
      <ManualApptHours
        name={this.state.manualApptPayload.user_name}
        changeName={(v) => this.setState({ ...this.state, manualApptPayload: { ...this.state.manualApptPayload, user_name: v } })}
        date={this.state.manualApptPayload.date}
        selectedTime={this.state.manualApptPayload.date instanceof Object && this.state.manualApptPayload.date.time}
        changeDate={(v) => this.setState({ ...this.state, manualApptPayload: { ...this.state.manualApptPayload, date: v } })}
      />,
      <ServicesConfigAptt
        list={this.props.user.shopsWorking[0].shop.services}
        selected={this.state.manualApptPayload.services}
        click={(val) => this.setState({ ...this.state, manualApptPayload: { ...this.state.manualApptPayload, services: val } })} //this.setAppointment("serviceSelect", val)}
      />,
      <View style={{ marginTop: 8 }} ><Text style={{ textAlign: "center" }}>YOU ADDED A NEW APPOINTMENT</Text></View>
    ]

    return steps[manualAppointmentStep]
  }

  nextManualAppointment = () => {
    const { manualAppointmentStep } = this.state

    if (manualAppointmentStep == 0) {
      if (!this.state.manualApptPayload.user_name) {
        return Alert.alert("Fill the client name")
      }

      if (!this.state.manualApptPayload.date) {
        return Alert.alert("Fill the date")
      }
    }

    let total = 1
    if (manualAppointmentStep + 1 == total) {
      this.setState({ manualAppointmentStep: manualAppointmentStep + 1, manualAppointmentIsFinish: true })
    } else {
      this.setState({ manualAppointmentStep: manualAppointmentStep + 1 })
    }
  }

  changeMonth = async (month, year) => {

    const newDay = "01";
    this.props.appointmentsClear()
    await this.props.listAppoitments(month)
    this.props.getApptByDay(month, newDay)
    this.setState({ date: `${year}-${month}-${newDay}` })
  }

  render() {

    const { showMenu, manualAppointment, manualAppointmentStep } = this.state
    const { theme, user, userType } = this.props;

    const currentDay = moment().format("YYYY-MM-DD")

    const appointments = this.props.appointments;

    let scrollApointmentsStyle = {}

    if (appointments.length > 0 && this.accordionContent.hasOwnProperty(this.state.date)) {
      scrollApointmentsStyle.position = "absolute";
      scrollApointmentsStyle.marginTop = this.state.marginTopScroll;
      scrollApointmentsStyle.width = "100%";
      scrollApointmentsStyle.height = "100%";
    }

    let imgNoAppointments = require('que/assets/images/no_appointments_onboarding_img.jpg')

    return (
      <ShopContainer background={theme.secondary}>

        <StatusBar translucent backgroundColor="transparent" barStyle="light-content" />

        <SideBar
          {...this.props}
          navigation={this.props.navigation}
          close={() => this.setState({ showMenu: false })}
          showMenu={showMenu} />

        <HeaderBar
          title={'APPOINTMENTS'}
          onPressLeftIcon={() => this.setState({ showMenu: true })}
          leftIcon={require('que/assets/images/menu_open_home_costumer_icn.png')}
          lightMode
        />

        <FamilyUsers {...this.props} />

        <View style={{ margin: 16 }}>
          <Calendar
            dayComponent={({ date, state }) => {
              return (
                <Styled.DayComponentContainer
                  key={date.dateString}
                  ref={cmp => !date.dateString ? '' : this.accordionContent[date.dateString] = cmp}
                  onPress={() => this.setDay(date.dateString)}
                  bgColor={this.state.date == date.dateString ? theme.primary : theme.secondary}
                  height={this.state.date == date.dateString && appointments.length ? 50 : 42}
                  borderBottomWidth={this.state.date == date.dateString && appointments.length ? 0 : 0.5} >
                  {(!state || currentDay == date.dateString) &&
                    <>
                      <Styled.DayLabel>{date.day}</Styled.DayLabel>
                      {this.props.daysWithAppt.includes(moment(date.dateString, "YYYY-MM-DD").format("DD")) &&
                        <Styled.BallToday></Styled.BallToday>
                      }
                    </>
                  }
                </Styled.DayComponentContainer>
              )
            }}
            current={this.state.date}
            currentMonth={moment(this.state.date, "YYYY-MM-DD").format("MM")}
            minDate={'2012-05-10'}
            onDayLongPress={(day) => { console.log('selected day', day) }}
            monthFormat={'MMMM'}
            onMonthChange={(data) => this.changeMonth(moment(data.month, "M").format("MM"), data.year)}
            // onPressArrowLeft={() => this.changeMonth("subtract")}
            // onPressArrowLeft={() => this.changeMonth("add")}
            markedDates={this.getDays()}
            refreshing={true}
            hideArrows={false}
            renderArrow={(direction) => <Icon name={direction} style={{ color: "white" }} />}
            hideExtraDays={false}
            disableMonthChange={false}
            firstDay={1}
            hideDayNames={false}
            showWeekNumbers={false}
            theme={{
              calendarBackground: theme.secondary,
              textSectionTitleColor: '#ffffff',
              selectedDayBackgroundColor: "red",
              todayTextColor: '#ffffff',
              dayTextColor: '#ffffff',
              textDisabledColor: '#d9e1e8',
              dotColor: '#00adf5',
              selectedDotColor: '#ffffff',
              arrowColor: 'orange',
              monthTextColor: '#ffffff',
              indicatorColor: '#ffffff',
              textDayFontWeight: '300',
              textMonthFontWeight: 'bold',
              textDayHeaderFontWeight: '300',
              textDayFontSize: 16,
              textMonthFontSize: 16,
              textDayHeaderFontSize: 16
            }}
          />
        </View>
        <ScrollView style={{scrollApointmentsStyle, backgroundColor: 'white'}}>
          {appointments.length > 0 && <Styled.LineScrollViewAppt />}

          <View style={{ backgroundColor: 'white' }}>
            <View style={{ marginLeft: 5, marginRight: 5 }}>
              {appointments > 0 && <Styled.DailyLeft source={require('que/assets/images/are_you_a_barber_icn.png')} />}
              <Styled.TextDaily>DailyLog</Styled.TextDaily>
              {(appointments > 0 && userType == "BARBER") && <Styled.DailyRight source={require('que/assets/images/are_you_a_barber_icn.png')} />}
            </View>
          </View>

          <View style={{ backgroundColor: 'white' }}>
            <View>
              {appointments.length > 0 && (
                <>
                  <Styled.VerticalLine />
                  <Styled.CardsContainer>
                    <Content>
                      <View style={{ display: 'flex', flexDirection: 'column', flexWrap: 'wrap' }}>
                        {
                          appointments.map((a, i) => {

                            let shop_name = "Shop Name";
                            if (a.shop && a.shop.name) {
                              shop_name = a.shop.name
                            } else if (a.shop && a.shop._doc) {
                              shop_name = a.shop._doc.name
                            }

                            return (
                              //My Cut like a customer
                              a.user == user._id ?
                                <TouchableOpacity onPress={() => this.openAppointment(a)} key={i}>
                                  <DailyLog
                                    time={this.getHours(a.date)}
                                    barber_image={require('que/assets/images/barbers/barber1.jpg')}
                                    user_image={require('que/assets/images/profile_sign_in_img.jpg')}
                                    barber_name={(a.barber instanceof Object ? a.barber.name : "Barber Name")}
                                    hair_style={a.hair_style}
                                    beard_style={a.beard_style}
                                    shop_name={shop_name}
                                  />
                                </TouchableOpacity>
                                :
                                //My Cut like a barber
                                <TouchableOpacity onPress={() => this.openAppointment(a)} key={i}>
                                  <DailyLogBarber
                                    time={this.getHours(a.date)}
                                    barber_image={require('que/assets/images/barbers/barber1.jpg')}
                                    user_image={require('que/assets/images/profile_sign_in_img.jpg')}
                                    barber_name={(a.barber instanceof Object ? a.barber.name : "Barber Name")}
                                    shop_name={shop_name}
                                  />
                                </TouchableOpacity>
                              // a.hair_style
                            )
                          }
                          )
                        }
                      </View>
                    </Content>
                  </Styled.CardsContainer>
                  {userType == "BARBER" && <Styled.VerticalLineRight />}
                </>
              )}

              {appointments <= 0 && (
                <View style={{ paddingTop: 10, paddingHorizontal: 16, alignContent: "center", alignItems: "center", flex: 1, flexDirection: "column" }}>
                  <Styled.DescriptionText>No appointments</Styled.DescriptionText>

                  {userType == "BARBER" ? (
                    <Carousel
                      data={[0, 1]}
                      renderItem={({ index }) => {
                        if (index == 0) {
                          return (
                            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                              <Styled.DescriptionText style={{ marginVertical: 10 }}>{`You and your family members appointments\n will show up here when you book!`}</Styled.DescriptionText>
                              <Image source={imgNoAppointments} style={{ width: 300, height: 300 }} resizeMode={"contain"} />
                            </View>
                          )
                        } else {
                          return (
                            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                              <Styled.DescriptionText style={{ marginVertical: 10 }}>{`You can use this button to\n manually add an appointment!`}</Styled.DescriptionText>
                              <Image style={{ width: 120, height: 120 }} resizeMode={'contain'} source={require('que/assets/images/barbers/arrow_onboarding_career_icn.png')} />
                            </View>
                          )
                        }
                      }}
                      sliderWidth={Dimensions.get('window').width}
                      itemWidth={Dimensions.get('window').width}
                      inactiveSlideOpacity={0}
                      inactiveSlideScale={1}
                      firstItem={0}
                    />
                  ) : (
                      <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <Styled.DescriptionText style={{ marginVertical: 10 }}>{`You and your family members appointments\n will show up here when you book!`}</Styled.DescriptionText>
                        <Image source={imgNoAppointments} style={{ width: 300, height: 300 }} resizeMode={"contain"} />
                      </View>
                    )}

                </View>
              )}

            </View>
          </View>
        </ScrollView>
        {this.state.editAppointment && (
          <WizardAppointment
            shop={this.state.appointment.shop}
            stepAppointment={this.state.stepAppointment}
            changeStepAppointment={() => this.changeStepAppointment()}
            backStepAppointment={() => this.backStepAppointment()}
            closeCreateAppoint={() => this.closeEditAppointment()}
            cancelAppointment={() => this.setState({ cancelAppt: true, editAppointment: false })}
            appointment={this.state.appointment}
            services={this.state.appointment.services}
            family={this.props.userType != "CUSTOMER" ? [this.state.appointment.user] : this.props.user.family}
            edit
          />
        )}
        {this.state.cancelAppt &&
          <PopBottom
            height={200}
            actionClose={() => this.setState({ ...this.state, removeUser: false, userSelected: {} })}
            isFinish={true}
            size={0}
            activePosition={0}
            showNext={false} >
            <CancelAppt
              text={"CANCEL APPOINTMENT WITH " + this.state.appointment.barber_name}
              user={{}}
              barber_image={require('que/assets/images/barbers/barber1.jpg')}
              actionContinue={() => {
                this.props.removeAppointment(this.state.appointment._id, this.state.date)
                this.setState({ cancelAppt: false, appointment: false })
              }}
              actionCancel={() => this.setState({ cancelAppt: false, appointment: false })}
            />
          </PopBottom>
        }

        {(userType == "BARBER" && !manualAppointment) && (
          <Fab
            active={true}
            direction="up"
            containerStyle={{}}
            style={{ backgroundColor: '#FFFFFF' }}
            position="bottomRight"
            onPress={() => this.setState({ manualAppointment: true })}>
            <Image source={require('que/assets/images/close_forgot_password_sign_in_icn.png')} style={{ width: 18, height: 18, transform: [{ rotate: '45deg' }] }} />
          </Fab>
        )}

        {manualAppointment && (
          <PopBottom
            height={manualAppointmentStep == 1 ? 400 : 200}
            imageIcon={manualAppointmentStep == 1 ? require("que/assets/images/shopdetails/checkbox_services_checked_icn.png") : require("que/assets/images/close_pop_up_gray_account_costumer_icn.png")}
            actionClose={() => {
              if (this.state.manualAppointmentStep == 0) {
                this.setState({ ...manualAppt })
              } else if (this.state.manualAppointmentStep == 1) {
                this.createManualAppt()
              } else {
                this.setState({ ...manualAppt })
              }
            }}
            isFinish={manualAppointmentStep > 0}
            size={manualAppointmentStep > 1 ? 0 : 2}
            activePosition={manualAppointmentStep}
            actionNext={() => this.nextManualAppointment()}
            showNext={manualAppointmentStep < 1} >

            {this.renderManualAppointment()}

          </PopBottom>
        )}

      </ShopContainer>
    )
  }
}

const mapStateToProps = store => ({
  appointments: store.appointmentReducer.list,
  daysWithAppt: store.appointmentReducer.listDays,
  loading: store.appointmentReducer.loading,
  user: store.userReducer.user,
  shop: store.shopReducer.shop,
  userType: store.userReducer.interface,
  services: store.shopReducer.services,
  cuts: store.cutsReducer.list,
})

const mapDispatchToProps = dispatch =>
  bindActionCreators({
    listAppoitments,
    listCut,
    setShopOpened,
    findShop,
    removeAppointment,
    makeAppointment,
    getApptByDay,
    appointmentsClear
  }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(withTheme(Appointments))