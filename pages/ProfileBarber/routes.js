import ProfileBarber from './index'
import CareerBarber from './Career'
import CareerShopSelect from './Career/shopSelect'

export default {
  ProfileBarber,
  CareerBarber,
  CareerShopSelect,
}