import React from 'react'
import { ImageBackground, View, Image, Text, ScrollView } from 'react-native'

export default () => {
  return (
    <ScrollView>
      <View style={{padding:16, margin:16, justifyContent:'center', alignItems:'center'}}>
        <Text style={{color:'#fff', textAlign:'center', marginBottom:16}}>PAST CUTS</Text>
        <Text style={{color:'rgba(255,255,255,.6)', textAlign:'center', width:230}}>Past cuts will show up here once an appointment is completed!</Text>
        <ImageBackground source={require('que/assets/images/barbers/past_cuts_onboarding_img.jpg')} style={{flex:1, height:200, width:"100%"}}>
        
        </ImageBackground>
      </View>

    </ScrollView>
  )
}