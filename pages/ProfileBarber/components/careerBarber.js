import React from 'react'
import { TouchableOpacity, View, Image, Text, ScrollView } from 'react-native'
import CustomButton from 'que/components/CustomButton'
import styled from 'styled-components/native'
import moment from 'moment'

export default (props) => {
  return (
    <ScrollView>
      <View style={{padding:16, margin:16, justifyContent:'center', alignItems:'center'}}>
        
        {props.listCarrer.length==0 && (
          <>
          <Text style={{color:'#fff', textAlign:'center', marginBottom:16, width:200}}>
            Only shop owners will be able to see your career.
          </Text>

          <View style={{width:140}}>
            <CustomButton 
              onPress={() => props.navigation.push('CareerBarber')} 
              text={'Add Carrer'} />
          </View>
          </>
        )}

        {props.listCarrer.length>0 && (
          <View style={{
              borderColor:'rgba(255,255,255,.8)', 
              borderWidth:1, 
              borderRadius:16,
              padding:8,
              width:'90%'
              }}>

              {
                props.listCarrer.map(val=>{
                  return (
                    <ShopCarrerHolderDark 
                      shop={val}
                      press={()=>props.navigation.push('CareerBarber', {carrer: val})} />
                  )
                })
              }

          </View>
        )}
        
      </View>

    </ScrollView>
  )
}


const ShopCarrerHolderDark = (props) => {
  let image = props.shop.shop.photos[0]
  return (
    <CardContainer activeOpacity={1} onPress={()=>props.press()}>
      
      <FlexOne>
        <ImageBarber source={image} />
      </FlexOne>

      <FlexTwo>
        <Text style={{color:'white'}}>{moment(props.shop.started_at).format('YYYY MMM')} - {moment(props.shop.ended_at).format('YYYY MMM')}</Text>
        <View>
          <TextInformation>
            <ImageIcon source={require("que/assets/images/shop_owner_icn_white.png")}/> 
            <Text style={{color:'white'}}>{props.shop.shop.name}</Text>
          </TextInformation>
        </View>
      </FlexTwo>

    </CardContainer>
  )
}

const CardContainer = styled.TouchableOpacity`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  background-color: rgba(0,0,0,.2);
  border-radius:8;
  padding:8px;
  padding-top:24px;
  padding-bottom:24px;
  margin-bottom:8px;
`
const ImageBarber = styled.Image`
  height: 50;
  width: 50;
  border-radius: 60;
  border-width: 2;
  border-color: white; 
`
const FlexOne = styled.View`
  flex: 1;
  justify-content: center;
  flex-direction: row;
  align-items: center;
`

const FlexTwo = styled.View`
  flex: 3;
  justify-content: center;
`
const TextInformation = styled.View`
  margin-top: 5;
  align-items: center; 
  display: flex;
  justify-content: flex-start;
  flex-direction: row;
`
const ImageIcon = styled.Image`
  width: 14;
  height: 14;
  margin-right: 5;
`