import React from 'react'
import { TouchableOpacity, View, Image, Text, ScrollView } from 'react-native'

const week = [ "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" ] // TODO make it a enum 

export default (props) => {
  return (
    <ScrollView>
      <View style={{borderRadius:24, borderWidth:1, borderColor:'rgba(255,255,255,.6)', padding:16, margin:16, justifyContent:'center', alignItems:'center'}}>
        <Image source={require('que/assets/images/shops_favorites_icn.png')} style={{height:20, width:20, marginBottom:16}} />
        <Text style={{color:'#fff', textAlign:'center', marginBottom:16}}>{props.shop.address}</Text>
        <Text style={{color:'rgba(255,255,255,.6)'}}>{props.shop.distance || 0} km</Text>
      </View>
      

      {props.workingHours.length<=0 ? (
        <View style={{justifyContent:'center', alignItems:'center'}}>
          <View style={{width:28, height:28, backgroundColor:'white', borderRadius:14, padding:8, justifyContent:'center', alignItems:'center'}}>
            <Image source={require('que/assets/images/barbers/appointment_icn.png')} style={{height:20, width:20}} />
          </View>
          <TouchableOpacity onPress={()=>props.openStep()} style={{borderRadius:24, borderWidth:1, borderColor:'rgba(255,255,255,.6)', padding:16, margin:16, justifyContent:'center', alignItems:'center'}}>
            <Text style={{color:'rgba(255,255,255,.6)'}}>Set Working Hours</Text>
          </TouchableOpacity>
        </View>
      ):(
        <View style={{borderRadius:24, borderWidth:1, borderColor:'rgba(255,255,255,.6)', padding:16, margin:16, justifyContent:'center', alignItems:'center'}}>
          <Image source={require('que/assets/images/shops_favorites_icn.png')} style={{height:20, width:20, marginBottom:16}} />
          {props.workingHours.map(val=><Text style={{color:'#fff', textAlign:'center', marginBottom:16}}>{week[val.week_day]}: {val.start_time} - {val.end_time}</Text>)}
          <TouchableOpacity onPress={()=>props.openStep()} style={{borderRadius:24, borderWidth:1, borderColor:'rgba(255,255,255,.6)', padding:16, margin:16, justifyContent:'center', alignItems:'center'}}>
            <Text style={{color:'rgba(255,255,255,.6)'}}>Set Working Hours</Text>
          </TouchableOpacity>
        </View>
      )}
        


      
    </ScrollView>
  )
}