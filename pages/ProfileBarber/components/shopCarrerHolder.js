import React from "react"
import { View, Text } from "native-base"
import styled from 'styled-components/native'
import {colors} from 'que/components/base'
import moment from 'moment'

export default (props) => {
  let image = props.shop.shop.photos[0]

  return (
    <CardContainer activeOpacity={1} background={props.background} onPress={()=>props.press()}>
      
      <View style={{marginLeft:10}}>
        <Line/>
      </View>

      <FlexOne>
        <ImageBarber source={image} />
      </FlexOne>

      <FlexTwo>
        <Text>{moment(props.shop.started_at).format('YYYY MMM')} - {moment(props.shop.ended_at).format('YYYY MMM')}</Text>
        <View>
            <TextInformation>
                <ImageIcon source={require("que/assets/images/shop_timeline_icn.png")}/> 
                <Text>{props.shop.shop.name}</Text>
            </TextInformation>
        </View>
      </FlexTwo>

    </CardContainer>
  );
};

const CardContainer = styled.TouchableOpacity`
  width: 95%;
  padding-left: 10;
  padding-right: 10;
  padding-top: 20;
  padding-bottom: 20;
  margin-bottom: 12;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  background-color: ${props=>props.background?props.background:colors.lightGray};
`


const ImageBarber = styled.Image`
  height: 50;
  width: 50;
  border-radius: 60;
  border-width: 2;
  border-color: #3D2E52; 
`


const ImageUser = styled.Image`
  height: 40;
  width: 40;
  border-radius: 60;
  borderWidth: 2;
  borderColor: ${colors.white};
`

const Line = styled.View`
  width: 6;
  height: 6;
  background-color: #5E4086;
  border-radius: 100;
  margin-right: 5;
`


const FlexOne = styled.View`
  flex: 1;
  justify-content: center;
  flex-direction: row;
  align-items: center;
`

const FlexTwo = styled.View`
  flex: 3;
  justify-content: center;
`


const FlexImageRight = styled.View`
  width: 50;
  display: flex;
  align-items: center;
  justify-content: flex-start;
`


const TextInformation = styled.View`
  font-size: 14;
  margin-top: 5;
  align-items: center; 
  display: flex;
  justify-content: flex-start;
  flex-direction: row;
`


const Hour = styled.Text`
  font-size: 12;
  background-color: ${colors.primary};
  color: #fff;
  width: 50;
  text-align: center;
  border-radius: 10;
  padding-top: 2px;
  padding-bottom: 2px;
`


const ImageIcon = styled.Image`
  width: 14;
  height: 14;
  margin-right: 5;
`