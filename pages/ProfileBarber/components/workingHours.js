import React from 'react'
import { 
  StyleSheet, 
  TouchableOpacity, 
  View, 
  Image, 
  Text, 
  Alert, 
  TextInput } from 'react-native'

import DarkMask from 'que/components/DarkMask'

import BasePop from "que/components/BasePop"
import CloseImageAnimated from "que/components/CloseImageAnimated"
import DoneImageAnimated from "que/components/DoneImageAnimated"

//Colors
import { colors } from 'que/components/base'

const checkImg = require("que/assets/images/shopdetails/checkbox_services_checked_icn.png")
const closeImg = require("que/assets/images/close_pop_up_gray_account_costumer_icn.png")

const HourLine = (prop) => {
  return (
    <View style={{paddingVertical:8, flexDirection:'row', marginHorizontal:48, justifyContent:"center", alignItems:"center"}}>
      <View style={{flex:1}}>
        <Text>{prop.day}</Text>
      </View>

      <View style={{flex:2, flexDirection:'row'}}>
        <View>
          <TextInput 
          style={{width:100, textAlign:"center"}}
          placeholder={prop.start}
          value={prop.start}
          onChangeText={text=>prop.changeValue("start",text)} />
        </View>
        <Text> - </Text>
        <View>
          <TextInput 
          style={{width:100, textAlign:"center"}}
          placeholder={prop.end}
          value={prop.end}
          onChangeText={text=>prop.changeValue("end",text)} />
        </View>
      </View>
    </View>
  )
}

const week = [ "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" ]  // TODO make it a enum 

export default (props)=> {

  setHours = (day, val, key) => {
    props.setTimeChanged(day, val, key)
  }

    return (
      <>
        <DarkMask />
        <BasePop>
          <TouchableOpacity activeOpacity={1} onPress={() => props.onClose()} style={styles.floatButton}>
            {/* {stepAppointment < 1 ? <CloseImageAnimated source={closeImg} /> : <DoneImageAnimated source={checkImg} />} */}
            <DoneImageAnimated source={checkImg} />
          </TouchableOpacity>

          <View style={{ width: "100%", flex: 1, alignItems:'center'}}>

            <Text style={{marginVertical:16}}>WORKING HOURS</Text>
           
            { props.open_time.map((val,key)=><HourLine key={key} day={week[val.week_day]} start={val.start_time} end={val.end_time} changeValue={(day, e)=>this.setHours(day, e, key)} />) }
           
          </View>
        </BasePop>
      </>
    )
}

const styles = StyleSheet.create({
  floatButton: {
    width: 60,
    height: 60,
    borderRadius: 30,
    borderWidth: 4,
    borderColor: colors.primary,
    marginTop: -30,
    backgroundColor: "white",
    justifyContent: "center",
    alignContent: "center",
    alignItems: "center",
  }, 
  icon: { width: 34, height: 34 }, 
  content: { flexDirection: "row", position: 'relative', justifyContent: 'center', alignItems: 'center', textAlign: 'center', display: 'flex' },
  nextButton: { position: 'absolute', right: 10, bottom: 5 },
})
