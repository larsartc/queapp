import React from 'react'
import { Dimensions, View, Text } from 'react-native'
import PopBottom from "que/components/PopBottom"
import PickerDate from 'que/components/PickerDate'
import PickerSwitch from 'que/components/PickerSwitch'

class PeriodShop extends React.Component {

  state = {
    pickerSwitch: 0,
    
    started_at: 0,
    ended_at: 0,
  }

  componentDidMount = () => {
    
  }

  closePopBottom = () => {

    const payload = {
      started_at: this.state.started_at,
      ended_at: this.state.ended_at,
    }
  
    this.props.onDone(payload)
  }

  render(){
  
    if(!this.props.active){
      return null
    }

    return (
      <PopBottom
        height={400}
        imageIcon={require("que/assets/images/shopdetails/checkbox_services_checked_icn.png")}
        actionClose={() => this.closePopBottom()}
        isFinish={true}
        size={0}
        activePosition={0}
        showNext={false} >

        <View style={{flex:1, alignItems:'center', marginTop:16}}>

          <Text>PERIOD WORKING ON THIS SHOP</Text>
          
          <PickerDate
            label={"Start Date"} />

          <PickerSwitch 
            labels={["Current", "End Date"]}
            position={this.state.pickerSwitch} 
            onChange={(val)=>this.setState({pickerSwitch:val})}
          />

          {this.state.pickerSwitch==1 && <PickerDate />}

        </View>

      </PopBottom>
    )

  }
}

export default PeriodShop