import React from 'react'
import { ImageBackground, TouchableOpacity, View, Image, Text, ScrollView, StyleSheet, StatusBar, PixelRatio } from 'react-native'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { colors, BasePop, FavoriteContainer, FavoriteIcon } from 'que/components/base'
import {Fab} from 'native-base'
import { withTheme } from '../../../utils/themeProvider'
import HeaderBar from "que/components/HeaderBar"
import styleShop from '../../Shops/details/style'
import {ImageUser} from 'que/components/base'
import styled from 'styled-components/native'
import ShopCarrerHolder from '../components/shopCarrerHolder'

const closeIcon = require('que/assets/images/close_barber_shop_icn.png')

class CareerBarber extends React.Component {

    state = {
      imgBackground: null,
      workingHours: false,
      tempPhoto: false,
      activeTab: 0,
      openDetails: false,
    }

    close = () => {
        this.props.navigation.pop()
    }

    changePhoto = () => {
      this.setState({tempPhoto: true})
    }

    removePhoto = () => {
      this.setState({tempPhoto: false})
    }

    openDetails = (val=0) => {
      this.setState({...this.state, openDetails: true})
    }

    componentDidMount = () => {
    
      if(!!this.props.navigation.state.params){
        const {carrer} = this.props.navigation.state.params

        if(!!carrer){
          this.openDetails(carrer)
        }
      }

    }

    componentWillReceiveProps = nextProps => {
      console.log("teste1", nextProps.listCarrer)
    }
    
    render() {
        const { user, barber, shop, cuts, theme, listCarrer } = this.props
  
        return (
            <View style={{ 
                display: 'flex', 
                flex: 1, 
                flexDirection: 'column', 
                backgroundColor: theme.primary 
                }} contentContainerStyle={{ flex: 1 }}>

              <StatusBar translucent backgroundColor={'rgba(0,0,0,.2)'} barStyle="light-content" />

              <HeaderBar
                title={'CAREER'}
                leftIcon={closeIcon}
                onPressLeftIcon={()=>this.close()}
                lightMode
              /> 

              <View style={{alignItems:'center'}}>
      
                <ImageUser size={40} source={require("que/assets/images/profile_sign_in_img.jpg")} />

                <Text style={style.barberTypeName}>
                    BARBER
                </Text>
                <View style={style.barberNameContainer}>
                    <Text style={style.barberName}>
                        {user.name}
                    </Text>
                </View>

                <View style={{flexDirection:'row', width:200, marginTop:12, marginBottom:12,justifyContent:'center'}}>
                {Array(5).fill(null).map((r) => {
                  return <Image source={require('que/assets/images/shopdetails/shop_raiting_empty_icn.png')} style={{ width:24, height:24, marginRight: 5 }} />
                })}
                </View>
                
              </View>

              {!this.state.openDetails && listCarrer.length == 0 && (
                  <View style={{backgroundColor:'white', flex: 1, justifyContent: 'center', alignItems:'center'}}>
                    <Text>No past shops have been added.</Text>
                    <View style={{marginTop:100}}>
                      <Text style={{textAlign:'center'}}>You can use this button to add shops to</Text>
                      <Text style={{textAlign:'center'}}>your work history!</Text>
                      <View style={{justifyContent:'center', alignItems:'center'}}>
                      <Image style={{width:120, height:120}} resizeMode={'contain'} source={require('que/assets/images/barbers/arrow_onboarding_career_icn.png')} />
                      </View>
                    </View>
                  </View>
                )}
                
              {!this.state.openDetails && listCarrer.length > 0 && (
                  <View style={{backgroundColor:'white', flex: 1}}>
                  
                    <View style={{ marginLeft: 5, marginRight: 5 }}>
                      <DailyLeft source={require('que/assets/images/are_you_a_barber_icn.png')} />
                      <TextDaily>TIMELINE</TextDaily>
                    </View>

                    {listCarrer.map(val=>{
                      return (
                        <ShopCarrerHolder 
                        shop={val}
                        press={()=>this.openDetails()} />
                      )
                    })}

                  </View>
                )}


              {this.state.openDetails && (
                  <View style={{backgroundColor:'#f2f2f2', flex: 1}}>
                    
                    <ScrollView style={{flex:1, width:'100%'}} contentContainerStyle={{justifyContent: 'center', alignItems:'center'}}>

                      <TouchableOpacity 
                        activeOpacity={1} 
                        onPress={()=>this.setState({openDetails:false})}
                        style={{position:'absolute', right:16, top:16}}>
                        <Image 
                          source={require('que/assets/images/close_forgot_password_sign_in_icn.png')} 
                          style={{width:16, height:16}}
                          />
                      </TouchableOpacity>

                      <ImageUser 
                        size={70} 
                        source={require("que/assets/images/profile_sign_in_img.jpg")}
                        style={{marginVertical:16}}
                         />
                      <Text style={{marginBottom:10}}>NAME_SHOP</Text>
                      <Text>Apr - Jan 2018</Text>
                      
                      <TextDaily>TESTIMONIALS</TextDaily>

                      <View style={{width:'80%', padding:8, marginBottom:16, marginTop:30, backgroundColor:'white', justifyContent:'center', alignItems:'center'}}>
                        <ImageUser 
                          size={30} 
                          source={require("que/assets/images/profile_sign_in_img.jpg")} 
                          style={{marginTop:-20}}
                          />
                        <Text style={{textAlign:'center', marginTop:10,marginBottom:10, fontSize:12}}>ASOHASASAS ASDAS DASDASDASD ASDASD ASDASDA SDASD</Text>
                        <View style={{height:2, backgroundColor:'#999', marginVertical:16}} />
                        <View style={{flexDirection:'row'}}>
                          {Array(5).fill(null).map((r) => {
                            return <Image source={require('que/assets/images/shopdetails/apointment_purple_star_complete_icn.png')} style={{ width:24, height:24, marginRight: 5 }} />
                          })}
                        </View>
                      </View>

                    </ScrollView>

                  </View>
                )}

              

              {!this.state.openDetails && (<Fab
                  active={true}
                  direction="up"
                  containerStyle={{ }}
                  style={{ backgroundColor: '#FFFFFF' }}
                  position="bottomRight"
                  onPress={() => this.props.navigation.push("CareerShopSelect")}>
                  <Image source={require('que/assets/images/close_forgot_password_sign_in_icn.png')} style={{width:18, height:18, transform: [{ rotate: '45deg'}]}} />
              </Fab>)}


            </View>
        )
    }
}

const mapStateToProps = store => ({
    //shop: store.shopReducer.shop,
    user: store.userReducer.user,
    //loading: store.shopReducer.loading,
    barber: store.shopReducer.barber,
    //cuts: store.cutsReducer.list, 
    listCarrer: store.barberReducer.listCarrer,
})

const mapDispatchToProps = dispatch => {
    return bindActionCreators({
       //
    }, dispatch)
}



const style = StyleSheet.create({
  barberTypeName: {
    ...styleShop.barberTypeName,
    marginTop:10
  }, 
  barberNameContainer: styleShop.barberNameContainer, 
  barberName: styleShop.barberName,
  
})


const LineScrollViewAppt = styled.View`
  width: 100%;
  height: 3;
  background-color: #5E4086;
  border-top-width: 0.5;
  border-top-color: #FFFFFF;
`

const DailyLeft = styled.Image`
  margin-left: 1;
  top: 3;
  position: absolute;
  height: 30;
  width: 30;
  background-color: white;
  z-index: 3;
`

const TextDaily = styled.Text`
  color: ${colors.primary};
  text-transform: uppercase;
  text-align: center;
  padding-top: 16px;
  padding-bottom: 16px;
`


export default connect(mapStateToProps, mapDispatchToProps)(withTheme(CareerBarber))