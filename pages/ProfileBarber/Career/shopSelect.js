import React from 'react'
import { Platform, PermissionsAndroid, View, StyleSheet, Text, Image, TouchableOpacity, TouchableWithoutFeedback } from 'react-native'
import Geolocation from 'react-native-geolocation-service'

import FilterSideBar from 'que/components/FilterSideBar'
import SearchBar from "que/components/SearchBar"
import HeaderBar from "que/components/HeaderBar"
import PopBottom from "que/components/PopBottom"
import MapView from "que/components/MapView"
import ListView from "que/components/ListView"
import {ShopContainer} from 'que/components/base'
import RightSideBar from "que/components/rightSideBar"
import PeriodShop from '../components/periodShop'

//Redux
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import {withTheme} from '../../../utils/themeProvider'
import { listShops, favorite, unFavorite, updateListShop } from '../../../store/shop'
import { saveTempCarrerSelected } from '../../../store/barber'
import {changeEnvToBarber} from '../../../store/user'

class CareerShopSelect extends React.Component {

    state = {
        shop: {},
        view: 'map',
        search: '',

        position: {},
        stepChange: 0, 
        filterBar: false,
        
        periodShopShow: false,
        shopSelected: null,
    }

    closeDrawer = () => {
      this.drawer._root.close()
    }

    selectShop = (id, item) => {
      this.setState({ periodShopShow: true, shopSelected: item })
    }
    
    componentDidMount = () => {
        const { listShops } = this.props

        if(Platform == "IOS"){
            Geolocation.requestAuthorization();
        }else{
            PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,{
                'title': 'Access your location',
                'message': 'This app need access your location.'
                }
            )
        }

        // watchPosition
        // getCurrentPosition
        Geolocation.watchPosition(pos => {
                console.log(pos);

                listShops({
                    latitude: pos.coords.latitude,
                    longitude: pos.coords.longitude,
                })

                this.setState({...this.state, position: pos.coords})
                // pos.coords.latitude / longitude
            }, e => {
                console.log(e.code, e.message);
            }, { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
        );
    }

    changeView = () => {
        this.setState({ view: this.state.view == 'map' ? 'cards' : 'map' })
    }

    openFilter = () => {
      this.setState({
        ...this.state, 
        filterBar: !this.state.filterBar
      })
    }

    changeToBarber = () => {
      //this.props.changeEnvToBarber()
      this.setState({stepChange:2})
    }

    changePeriodWorkShop = (val) => {
      this.setState({
        periodShopShow: !this.state.periodShopShow
      })

      let newList = this.props.listCarrer 

      let newItem = {
        shop: this.state.shopSelected, 
        started_at: val.started_at, 
        ended_at: val.ended_at,
      }

      newList.push(newItem)

      this.props.saveTempCarrerSelected(newList, ()=>this.props.navigation.pop())

    }

    render() {
        const { shopSelected, position, stepChange, filterBar } = this.state
        const { navigation, shops } = this.props

        return (
          <ShopContainer style={{flex: 1}}>

          {filterBar && (
              <RightSideBar close={()=>this.openFilter()}>
                  <FilterSideBar 
                      {...this.props} 
                      {...this.props.filters}
                      close={() => this.openFilter()} 
                      action={(field, value) => this.filterSideBar(field, value)} 
                  />
              </RightSideBar>
          )}

          <HeaderBar
            title={'SELECT SHOP'}
            rightIcon={this.state.view === "map" ?
            require('que/assets/images/cards_view_home_costumer_icn.png') :
            require('que/assets/images/pin_map_view_home_icn.png')}
            onPressRightIcon={() => this.changeView()}
            onPressLeftIcon={() => this.props.navigation.pop()}
            leftIcon={require('que/assets/images/arrow_back_icn.png')}
          /> 

          <View style={{flex: 1}}>

          <SearchBar
            value={this.state.search}
            onChangeText={value => this.setState({ search: value })}
            placeholder={'Where do you work...'}
            openDrawer={() => this.openDrawer()}
            openFilter={()=>this.openFilter()}
          />

          {
            this.state.view == 'map' ?
            <MapView
              position={position}
              shops={shops}
              pressAction={(id, item) => this.selectShop(id, item)}
              // favorite={(obj) => this.props.favorite(obj)}
              // unFavorite={(obj) => this.props.unFavorite(obj)}
              updateListShop={list => this.props.updateListShop(list)}
            />
            :
            <ListView
              pressAction={(id, item) => this.selectShop(id, item)}
              // favorite={(obj) => this.props.favorite(obj)}
              // unFavorite={(obj) => this.props.unFavorite(obj)}
              updateListShop={list => this.props.updateListShop(list)}
              shops={shops}
            />
          }
          </View>

          <PeriodShop active={this.state.periodShopShow} onDone={(val)=>this.changePeriodWorkShop(val)} />

          </ShopContainer>
        )
    }
}

const mapStateToProps = store => ({
    filters: store.shopReducer.filters,
    shops: store.shopReducer.list_shop,
    loading: store.shopReducer.loading,
    user: store.userReducer.user,
    listCarrer: store.barberReducer.listCarrer,
})

const mapDispatchToProps = dispatch =>
  bindActionCreators({
    listShops, 
    favorite, 
    unFavorite, 
    updateListShop, 
    changeEnvToBarber,

    saveTempCarrerSelected,
  }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(withTheme(CareerShopSelect))