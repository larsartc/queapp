import React from "react";
import Styled from "./styled";
import Cut from "que/components/Cut";
import Card from "que/components/Card";
import Barbers from "que/components/Barbers";
import SideBar from 'que/components/Sidebar'
import NoFavorite from 'que/components/NoFavorite'
import { View, Content } from "native-base";
import { Text, TouchableOpacity, StatusBar } from "react-native";

//Redux
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import {
    listFavoriteCut,
    unfavoriteCut,
    listFavoriteBarber,
    unfavoriteBarber,
    listFavoriteShop,
    unfavoriteShops
} from "que/store/favorite";
import {findBarber, setShopOpened} from 'que/store/shop'

import HeaderBar from "que/components/HeaderBar"
import CutOnboard from "que/assets/images/cuts/select_hair_onboarding_img.jpg"
import BarberOnboard from "que/assets/images/barbers/onboarding_favorites_barber_img.jpg"
import ShopOnboard from "que/assets/images/shops/onboarding_shops_favorites_img.jpg"
import { colors, ShopContainer } from "que/components/base"
import LottieView from 'lottie-react-native'

const categoryString = ["", "CUTS", "BARBERS", "SHOPS"]

class Favorites extends React.Component {
    state = {
        category: 1, 
        posview: 1,
        showMenu: false,
        buttons: {
            first: 1,
            second: 2,
            third: 3,
        }, 
        animationPos: 0,
    };

    removeCutFinal = m => {
        const { unfavoriteCut } = this.props;
        unfavoriteCut({ type: 1, entity_id: m._id });
    };

    removeBarberFinal = c => {
        const { unfavoriteBarber } = this.props;
        unfavoriteBarber({ type: 2, entity_id: c._id });
    };

    removeShopFinal = s => {
        const { unfavoriteShops } = this.props;
        unfavoriteShops({ type: 3, entity_id: s._id });
    }

    componentDidMount = () => {
        const { listFavoriteCut, listFavoriteBarber, listFavoriteShop, user } = this.props;
        let position = {}
        if (user) {
            position = user.position;
        }
        listFavoriteCut(position);
        listFavoriteBarber(position);
        listFavoriteShop(position);
    }

    openShop = (id, obj) => {
        this.props.setShopOpened(obj)
        this.props.navigation.navigate('ShopViewer', { shop_id: id })
    }

    clickBarber = (id) => {
        this.props.findBarber(id)
        this.props.navigation.navigate('BarberScreen')
    }

    changeTypeView = type => {
        const {animationPos, buttons} = this.state 
        let newAnimationPos = 0
        let newButtons = {}
        /* {
        90 - CUTS 1 
        60 - BARBERS 2
        30 - SHOPS 3 
        }*/

        switch(type){
            case 1:
                newAnimationPos = animationPos == 30 ? 90 : 0
                newButtons = {
                    first:1,
                    second:2,
                    third:3,
                }
                this.animCateg.play(animationPos,90)
                break
            case 2:
                newAnimationPos = 60
                newButtons = {
                    first:2,
                    second:3,
                    third:1,
                }
                this.animCateg.play(animationPos, 60)
                break
            case 3:
                newAnimationPos = 30
                newButtons = {
                    first:3,
                    second:1,
                    third:2,
                }
                this.animCateg.play(animationPos, 30)
                break
            default:
                break
        }
        
        this.setState({ 
            category: type, //val.item.id, 
            //posview: val.index, 
            animationPos: newAnimationPos,
            buttons: newButtons,
        })
    }

    renderTypes = () => {

        return (
            <View style={{justifyContent:'center', alignItems:'center', overflow:'hidden'}}>

                <View style={{padding:4}}>
                <LottieView
                    ref={anim => this.animCateg = anim} 
                    style={{
                    width: 200,
                    }}
                    source={require('que/assets/animations/favorites/favorites.json')} 
                    autoPlay={false}
                    loop={false}
                    onAnimationFinish={()=>{}}
                />
                    <View style={{flexDirection:'row', justifyContent:'space-between', marginTop:-60}}>
                        <TouchableOpacity onPress={()=>this.changeTypeView(this.state.buttons.second)} activeOpacity={1} style={{width:60, height:60}} />
                        <TouchableOpacity onPress={()=>this.changeTypeView(this.state.buttons.first)} activeOpacity={1} style={{width:60, height:60}} />
                        <TouchableOpacity onPress={()=>this.changeTypeView(this.state.buttons.third)} activeOpacity={1} style={{width:60, height:60}} />
                    </View>
                </View>
            </View>
        )
    }

    render() {
        const { cuts, barbers, shops, loading } = this.props;
        const { category, posview, showMenu } = this.state


        if(this._carousel!=undefined){
            this._carousel.snapToItem(posview,true, ()=>{})
        }

        return (
            <ShopContainer>

                <StatusBar barStyle="dark-content" />

                <SideBar 
                    {...this.props} 
                    navigation={this.props.navigation} 
                    close={() => this.setState({showMenu: false})}
                    showMenu={showMenu} />

                    <HeaderBar
                        title={'FAVORITES'}
                        onPressLeftIcon={() => this.setState({showMenu: true})}
                    />

                    <Styled.CardsContainer>
                            <View
                                style={{
                                    flexDirection: "row",
                                    display: "flex",
                                    justifyContent: "center",
                                    marginBottom: 10,
                                    alignItens: "center",
                                    textAlign: "center"
                                }} >

                                {this.renderTypes()}

                            </View>
                        <Content padder>
                            

                            <View>
                                <Text style={{ textAlign: "center", color: colors.primary }}>{categoryString[category]}</Text>
                                <View style={{ marginHorizontal: 30, height: 2, backgroundColor: "#ccc", marginBottom: 10 }} />
                            </View>

                            {category == 3 && shops.length > 0 && (
                                <View style={{
                                    display: "flex",
                                    flexDirection: "row",
                                    flexWrap: "wrap",
                                }} >
                                    {shops.map((s, i) => (
                                        <Card
                                            onPress={() => this.openShop(s._id, s)}
                                            title={s.name}
                                            stars={s.mid_rate}
                                            price={s.mid_price}
                                            distance={s.distance}
                                            imageBg={require("que/assets/images/shops/shop4.jpg")}
                                            favorite={true}
                                            unfavoriteShop={() => this.removeShopFinal(s)}
                                            key={i}
                                        />
                                    ))}
                                </View>
                            )}

                            {category == 1 && cuts.length > 0 && (
                                <View style={{
                                    display: "flex",
                                    flexDirection: "row",
                                    flexWrap: "wrap",
                                }}>
                                    {cuts.map((m, i) => (
                                        <Cut
                                            saves={m.num_favorites}
                                            title={m.name}
                                            favorite={true}
                                            unfavoriteCut={() => this.removeCutFinal(m)}
                                            type={this.state.filter}
                                            imageBg={require("que/assets/images/cuts/cut1.jpg")}
                                            key={i}
                                        ></Cut>
                                    ))}
                                </View>
                            )}

                            {category == 2 && barbers.length > 0 && (
                                <View style={{
                                    display: "flex",
                                    flexDirection: "row",
                                    flexWrap: "wrap",
                                }} >
                                    {barbers.map((b, i) => (
                                        <Barbers
                                            rate={b.rate}
                                            unfavoriteBarber={() => this.removeBarberFinal(b)}
                                            imageBg={require("que/assets/images/barbers/barber1.jpg")}
                                            title={b.name}
                                            favorite={true}
                                            key={i}
                                            action={(id)=>this.clickBarber(id)}
                                        />
                                    ))}
                                </View>
                            )}

                            {category == 3 && !loading && shops.length <= 0 && (
                                <NoFavorite
                                    headerText={"No favorite barber shops"}
                                    bodyText={"You can favorite shops by pressing\n the heart icon on shop cards!"}
                                    sourceImg={ShopOnboard}
                                />
                            )}
                            {category == 2 && !loading && barbers.length <= 0 && (
                                <NoFavorite
                                    headerText={"No favorite barbers"}
                                    bodyText={"You can favorite barbers by pressing\n the heart icon on barber profile!"}
                                    sourceImg={BarberOnboard}
                                />
                            )}
                            {category == 1 && !loading && cuts.length <= 0 && (
                                <NoFavorite
                                    headerText={"No favorite cuts"}
                                    bodyText={"You can favorite cuts by pressing\n the heart icon on cut cards! Favorited\n cuts can be selected when booking\n appointments."}
                                    sourceImg={CutOnboard}
                                />
                            )}


                        </Content>
                    </Styled.CardsContainer>
                </ShopContainer>
        )
    }
}

const mapStateToProps = store => ({
    cuts: store.favoriteReducer.listCuts,
    barbers: store.favoriteReducer.listBarbers,
    shops: store.favoriteReducer.listShops,
    loading: store.favoriteReducer.loading,
    user: store.userReducer.user,
});

const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            listFavoriteCut,
            unfavoriteCut,
            listFavoriteBarber,
            unfavoriteBarber,
            listFavoriteShop,
            unfavoriteShops,
            findBarber,
            setShopOpened,
        },
        dispatch
    );

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Favorites);
