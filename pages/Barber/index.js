import React from "react";
import {
  ImageBackground,
  TouchableOpacity,
  View,
  Image,
  Text
} from "react-native";
import style from "../Shops/details/style";
import Buttons from "../Shops/details/components/Buttons";
import Middle from "../Shops/details/components/Middle";
import BottomList from "../Shops/details/components/BottomList";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import {
  findShop,
  findBarber,
  favAction,
  favorite,
  unFavorite
} from "../../store/shop";
import {
  colors,
  BasePop,
  FavoriteContainer,
  FavoriteIcon
} from "que/components/base";

import DefaultConfigAptt from "../../components/DefaultConfigAptt";
import ServicesConfigAptt from "../../components/ServicesConfigAptt";
import PinCounters from "../../components/PinCounters";
import { Favorite } from "que/utils/enums";
import StyleSelect from "../../components/StyleSelect";
import WizardAppointment from "../Appointment/wizardAppointment";
import { withTheme } from "../../utils/themeProvider";
import LinearGradient from "react-native-linear-gradient";
import LottieView from "lottie-react-native";

class BarberView extends React.PureComponent {
  state = {
    type: "barber",
    //view: 'info',
    createAppointment: false,
    stepAppointment: 0,
    startView: 1,
    posFavorite: 0
  };

  close = () => {
    this.props.navigation.goBack();
  };

  newAppointment = () => {
    if (this.props.user != undefined) {
      this.setState({
        ...this.state,
        createAppointment: true
      });
    } else {
      Alert.alert(
        "SignIn",
        "Please, make your signin to create a new appointment."
      );
    }
  };

  closeCreateAppoint = () => {
    this.setState({
      ...this.state,
      createAppointment: false,
      stepAppointment: 0
    });
  };

  changeStepAppointment = () => {
    this.setState({
      ...this.state,
      stepAppointment: this.state.stepAppointment + 1
    });
  };

  backStepAppointment = () => {
    this.setState({
      ...this.state,
      stepAppointment: this.state.stepAppointment - 1
    });
  };

  // TODO refactor all favorite functions
  favActions = () => {
    const { favAction, unFavorite, barber, favorite } = this.props;
    const type = Favorite.barber;

    if (barber.barber.favorite) {
      unFavorite({ type, entity_id: barber.barber._id }).then(() =>
        favAction(type)
      );
    } else {
      favorite({ type, entity_id: barber.barber._id }).then(() =>
        favAction(type)
      );
    }
  };

  playHeart = () => {
    if (this.props.barber.favorite) {
      this.heartAnim.play(0, 30);
    } else {
      this.heartAnim.play(30, 0);
    }
  };

  render() {
    const { shop, theme } = this.props;
    const { type, startView, createAppointment, stepAppointment } = this.state;

    console.log("PROPS AQUIII");
    console.log(this.props.barber);
    let photo = require("que/assets/images/barbers/barberBanner.jpg");
    //const photos = !!Object.keys(this.props.barber).length ? this.props.barber.barber.photos[0] : require("que/assets/images/barbers/barberBanner.jpg")
    let barber = {
      ...this.props.barber,
      mid_rate:
        !!this.props.barber && !!Object.keys(this.props.barber).length
          ? this.props.barber.barber.barber.mid_rate
          : 0,
      ratings:
        !!this.props.barber && !!Object.keys(this.props.barber).length
          ? this.props.barber.barber.barber.ratings
          : [],
      past_cuts:
        !!this.props.barber && !!Object.keys(this.props.barber).length
          ? this.props.barber.barber.barber.past_cuts
          : []
    };

    const posFavorite = barber.favorite ? 0 : 1;

    console.log("barber details, barber obj", barber);

    // const iconFavorite =
    //   !!barber.barber && barber.barber.favorite
    //     ? require("que/assets/images/heart_full_card_favorites_icn.png")
    //     : require("que/assets/images/heart_empty_card_favorites_icn.png");

    return (
      <View
        style={{
          display: "flex",
          flex: 1,
          flexDirection: "column",
          backgroundColor: theme.primary
        }}
        contentContainerStyle={{ flex: 1 }}
      >
        <ImageBackground
          source={
            !!this.props.barber.barber
              ? { uri: this.props.barber.barber.barber.photos[0] }
              : ""
          }
          style={style.imageBackground}
        >
          <LinearGradient
            colors={["rgba(0,0,0,.8)", "rgba(0,0,0,0)"]}
            style={{ flex: 1, height: 100 }}
          >
            <TouchableOpacity
              style={[style.closeBtnContainer, { paddingTop: 20 }]}
              onPress={() => this.close()}
            >
              <Image
                source={require("que/assets/images/close_barber_shop_icn.png")}
                style={style.btnClose}
              />
            </TouchableOpacity>
          </LinearGradient>

          <View style={{ flex: 1 }}>
            <TouchableOpacity
              style={style.favoriteContainer}
              onPress={() => this.playHeart()}
            >
              <LottieView
                style={style.btnFavorite}
                source={require("que/assets/animations/heart.json")}
                autoPlay={false}
                loop={false}
                ref={anim => (this.heartAnim = anim)}
                progress={posFavorite}
                onAnimationFinish={() => this.favActions()}
              />
              {/* <Image source={iconFavorite} style={style.btnFavorite} /> */}
            </TouchableOpacity>

            {/* <Buttons
                            startView={this.state.startView}
                            type={type}
                            //active={view}
                            barber={barber}
                            changeView={(view) => this.setState({ startView: view })}
                        /> */}

            <View style={{ zIndex: 2 }}>
              <Text style={style.barberTypeName}>BARBER</Text>
              <View style={style.barberNameContainer}>
                <Text style={style.barberName}>{barber.name}</Text>
              </View>
              <View style={style.sliderContainer} />
            </View>
          </View>
        </ImageBackground>

        <View style={style.midleContainer}>
          <Middle
            changeView={e => this.setState({ startView: e })}
            cardData={barber}
            type={type}
            barber={barber}
            startView={startView}
          />
        </View>

        <View style={style.bottomContainer}>
          <BottomList
            changeType={(type, id) =>
              this.setState({ type }, () => this.fetchData(type, id))
            }
            cardData={{ ...barber }}
            barbers={shop.barbers}
            type={type}
            //view={view}
            newAppointment={this.newAppointment}
          />
        </View>

        {createAppointment && (
          <WizardAppointment
            family={this.props.user ? this.props.user.family : []}
            shop={{
              ...this.props.shop,
              barbers: shop.barbers
                ? shop.barbers.filter(barb => barb.status == "accepted")
                : []
            }}
            stepAppointment={stepAppointment}
            barber={barber.barber._id}
            services={this.props.shop.services}
            changeStepAppointment={() => this.changeStepAppointment()}
            backStepAppointment={() => this.backStepAppointment()}
            closeCreateAppoint={() => this.closeCreateAppoint()}
          />
        )}
      </View>
    );
  }
}

const mapStateToProps = store => {
  let shop = {
    ...store.shopReducer.shop
  };
  const barbers = !!shop.barbers
    ? shop.barbers.filter(barb => barb.status == "accepted")
    : [];

  return {
    shop: { ...shop, barbers },
    user: store.userReducer.user,
    loading: store.shopReducer.loading,
    barber: store.shopReducer.barber,
    cuts: store.cutsReducer.list
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      findShop,
      findBarber,
      favorite,
      unFavorite,
      favAction
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTheme(BarberView));
