import React from 'react'
import { View, Text, TouchableWithoutFeedback } from 'react-native'
import styles from '../style'

export default Step3 = props => {
  return (
    <TouchableWithoutFeedback style={{marginHorizontal:30}} onPress={()=>props.actionCancel()}>
      <View style={{marginVertical:30}}><Text style={styles.saveCut}>
      YOUR REQUEST HAS BEEN SENT AND IS PENDING OWNER APPROVAL</Text></View>
    </TouchableWithoutFeedback>
  )
}