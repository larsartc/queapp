import React from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'
import styles from '../style'

export default Step2 = props => {
  return (
    <View style={{marginHorizontal:30}}>
      <View style={{marginVertical:30}}><Text style={styles.saveCut}>REQUEST WILL BE SENT TO THE SHOP OWNER</Text></View>

      <View style={styles.viewFormSaveCut}>
        <TouchableOpacity onPress={() => props.actionCancel()} style={styles.clickCloseSave}>
          <Image source={require("../../../assets/images/close_pop_up_gray_account_costumer_icn.png")} style={{ width: 25, height: 30, borderRadius: 23 }} />
        </TouchableOpacity>
        <View style={styles.viewForm}>
          <Text style={styles.saveCut}>SEND THE REQUEST?</Text>
        </View>
        <TouchableOpacity onPress={() => props.actionContinue()} style={styles.clickSaveSave}>
          <Image source={require("../../../assets/images/confirm_pop_up_account_costumer_icn.png")} style={{ width: 25, height: 30, borderRadius: 23 }} />
        </TouchableOpacity>
      </View>
    </View>
  )
}
