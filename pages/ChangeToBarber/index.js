import React from 'react'
import { Platform, PermissionsAndroid, View, StyleSheet, Text, Image, TouchableOpacity, TouchableWithoutFeedback } from 'react-native'
import { Drawer } from 'native-base'
import Geolocation from 'react-native-geolocation-service'

import FilterSideBar from '../../components/FilterSideBar'
import SearchBar from "../../components/SearchBar"
import HeaderBar from "../../components/HeaderBar"
import PopBottom from "../../components/PopBottom"
import MapView from "../../components/MapView"
import ListView from "../../components/ListView"
import { ShopContainer } from 'que/components/base'
import Step1 from './components/step1'
import Step2 from './components/step2'
import Step3 from './components/step3'

//Redux
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { listShops, favorite, unFavorite, updateListShop } from '../../store/shop'
import { listCut } from '../../store/cut'
import { listHelps } from '../../store/help'
import { changeEnvToBarber } from '../../store/barber'
import RightSideBar from "que/components/rightSideBar"
import { withTheme } from '../../utils/themeProvider'
import styles from './style'

class ChangeToBarber extends React.Component {

  state = {
    shop: {},
    view: 'cards',
    search: '',
    showReviewAppt: false,
    shopSelected: null,
    position: {},
    stepChange: 0,
    filterBar: false,
  }

  closeDrawer = () => {
    this.drawer._root.close()
  }

  closePopBottom = () => {
    this.setState({ showReviewAppt: false, shopSelected: null })
  }

  selectShop = (id, item) => {
    this.setState({ showReviewAppt: true, shopSelected: item })
  }


  componentDidMount = () => {
    const { listShops, listCut, listHelps } = this.props

    listCut()
    listHelps()

    if (Platform == "IOS") {
      Geolocation.requestAuthorization();
    } else {
      PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION, {
        'title': 'Access your location',
        'message': 'This app need access your location.'
      }
      )
    }

    // watchPosition
    // getCurrentPosition
    Geolocation.watchPosition(pos => {
      console.log(pos);

      listShops({
        latitude: pos.coords.latitude,
        longitude: pos.coords.longitude,
      })

      this.setState({ ...this.state, position: pos.coords })
      // pos.coords.latitude / longitude
    }, e => {
      console.log(e.code, e.message);
    }, { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
    );
  }

    changeToBarber = () => {
      const {shopSelected} = this.state
      // send request to shop
      this.props.changeEnvToBarber(shopSelected._id)
      this.setState({stepChange:2})
    }

    successChange = () => {
      this.props.navigation.pop()
    }

    openFilter = () => {
      this.setState({
        ...this.state,
        filterBar: !this.state.filterBar
      })
    }

    changeView = () => {
      this.setState({ view: this.state.view == 'map' ? 'cards' : 'map' })
    }

    render() {
        const { showReviewAppt, shopSelected, position, stepChange, filterBar } = this.state
        const { navigation, shops } = this.props

        const stepChangeView = [
          <Step1 actionContinue={()=>this.setState({stepChange:1})} actionCancel={()=>this.setState({showReviewAppt: false, stepChange:0})} />, 
          <Step2 actionContinue={()=>this.changeToBarber()} actionCancel={()=>this.setState({showReviewAppt: false, stepChange:0})} />, 
          <Step3 actionCancel={()=>this.successChange()} />,
        ]

        const imageShopSelected = !!this.state.shopSelected && this.state.shopSelected.photos.length ? { uri: this.state.shopSelected.photos[0] } : "";
        console.log(imageShopSelected);

        return (
      
          <ShopContainer style={{flex: 1}}>
          {filterBar && (
              <RightSideBar close={()=>this.openFilter()}>
                  <FilterSideBar 
                      {...this.props} 
                      {...this.props.filters}
                      close={() => this.openFilter()} 
                      action={(field, value) => this.filterSideBar(field, value)} 
                  />
              </RightSideBar>
          )}

          <HeaderBar
            title={''}
            rightIcon={this.state.view === "map" ?
            require('que/assets/images/cards_view_home_costumer_icn.png') :
            require('que/assets/images/pin_map_view_home_icn.png')}
          onPressRightIcon={() => this.changeView()}
          onPressLeftIcon={() => this.props.navigation.pop()}
          leftIcon={require('que/assets/images/arrow_back_icn.png')}
        />

        <View style={{ flex: 1 }}>

          <SearchBar
            value={this.state.search}
            onChangeText={value => this.setState({ search: value })}
            placeholder={'Where do you work...'}
            openDrawer={() => this.openDrawer()}
            openFilter={() => this.openFilter()}
          />

          {
            this.state.view == 'map' ?
              <MapView
                position={position}
                shops={shops}
                pressAction={(id, item) => this.selectShop(id, item)}
                favorite={(obj) => this.props.favorite(obj)}
                unFavorite={(obj) => this.props.unFavorite(obj)}
                updateListShop={list => this.props.updateListShop(list)}
              />
              :
              <ListView
                pressAction={(id, item) => this.selectShop(id, item)}
                favorite={(obj) => this.props.favorite(obj)}
                unFavorite={(obj) => this.props.unFavorite(obj)}
                updateListShop={list => this.props.updateListShop(list)}
                shops={shops}
              />
          }
        </View>

        {showReviewAppt &&
          <PopBottom
            height={200}
            imageAvatar={imageShopSelected}
            actionClose={() => this.closePopBottom()}
            isFinish={true}
            size={0}
            activePosition={0}
            showNext={false}
          >
            {stepChangeView[stepChange]}
          </PopBottom>
        }

      </ShopContainer>
    )
  }
}

const mapStateToProps = store => ({
  filters: store.shopReducer.filters,
  shops: store.shopReducer.list_shop,
  loading: store.shopReducer.loading,
  user: store.userReducer.user,
})

const mapDispatchToProps = dispatch =>
  bindActionCreators({
    listShops,
    favorite,
    unFavorite,
    updateListShop,
    listCut,
    listHelps,
    changeEnvToBarber,
  }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(withTheme(ChangeToBarber))