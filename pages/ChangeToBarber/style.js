import { StyleSheet } from 'react-native'
import {colors} from 'que/components/base'

export default styles = StyleSheet.create({
  imageModel: {
    width: 90,
    height: 90,
    borderRadius: 45,
    borderWidth: 4,
    borderColor: colors.white,
    marginTop: -45
  },
  bgOverlay:{
    position: "absolute",
    width: "100%",
    height: "100%",
    backgroundColor: "rgba(0,0,0,0.7)",
    zIndex: 999
  },
  saveCut:{
    textAlign: "center", 
    color: colors.gray, 
    fontSize: 14
  },
  viewFormSaveCut:{
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center", 
    marginTop:20,
  },
  clickCloseSave:{
    height: 66,
    width: 66,
    borderRadius: 33,
    borderWidth: 4,
    borderColor: colors.gray,
    alignItems: "center",
    justifyContent: "center"
  },
  clickSaveSave:{
    height: 66,
    width: 66,
    borderRadius: 33,
    borderWidth: 4,
    borderColor: colors.green,
    alignItems: "center",
    justifyContent: "center"
  },
  viewForm:{
    width: 180,
    height: 40,
    marginRight: 10,
    marginLeft: 10
  },
});