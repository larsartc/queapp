import React from "react";
import CustomInput from "que/components/CustomInput";
import { Content, Drawer, Badge } from "native-base";
import { View, Text, InputText, Image, SafeAreaView, ScrollView, Alert } from "react-native"
import ButtonChangeProfile from "que/components/ButtonChangeProfile"
import HeaderBar from "que/components/HeaderBar"
import { withTheme } from '../../utils/themeProvider'
import SideBar from 'que/components/Sidebar'
import UserList from './components/userList'
import StepRemove from './components/stepRemove'
import PopBottom from "que/components/PopBottom"
import ChangePass from './components/changePass'
import BtCreateProfile from './components/btCreateProfile'
import BtProfileBarberDone from './components/btProfileBarberDone'
import ConfirmSubscribeOwner from "../Owner/components/confirmSubscribeOwner"

//Redux
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { removeUser, updateUser } from '../../store/user'

// style 
import {
  DivAccount,
  DivInformation,
  DivButtons,
  ShopContainerProfile,
  InformationsAccount,
  MarginInput
} from "./styled"

class Profile extends React.Component {

  state = {
    name: this.props.user.name,
    email: this.props.user.email,
    //
    userSelected: {},
    removeUser: false,
    //
    password: '',
    passwordNew: '',
    //
    changePassword: false,
    showMenu: false,
    // 
    confirmOwner: false,
  }

  componentDidMount = () => {

  }

  handleField = (field, val) => {
    this.setState({
      ...this.state,
      [field]: val
    })
  }

  tapUser = e => {
    if (e._id == this.props.user._id) return false;
    console.log("USUARIO", e)
    this.setState({
      ...this.state,
      userSelected: e,
      removeUser: true,
    })
  }

  confirmRemoveUser = () => {
    this.props.removeUser(this.state.userSelected._id)
    this.setState({ ...this.state, removeUser: false, userSelected: {} })
  }

  updatePassword = async (password, confirm_password) => {
    const result = await this.props.updateUser({ name: this.props.user.name, email: this.props.user.email, password, confirm_password })
    if (!!result) {
      Alert.alert("Success", "Password updated!")
      this.setState({ changePassword: false })
    } else {
      Alert.alert("Error!", "Check your password!")
    }
  }

  confirmOwner = () => {
    this.setState({ ...this.state, confirmOwner: !this.state.confirmOwner })
  }

  render() {
    const { name, email, password, removeUser, userSelected, changePassword, showMenu, confirmOwner } = this.state
    const { theme, user, isBarber } = this.props

    let shopWorking = user != undefined ? user.shopsWorking : null

    let renderBarber = <BtCreateProfile onPress={() => this.props.navigation.navigate('ProfileBarber')} theme={theme} />

    if (isBarber === "BARBER") {
      let { barber } = user
      if (barber.work_time.length > 0) {
        renderBarber = <BtProfileBarberDone
          shop={shopWorking[0].shop}
          rating={barber.mid_rate}
          hours={barber.work_time}
          onPress={() => this.props.navigation.navigate('ProfileBarber')}
          theme={theme} />
      }
    }

    return (
      <>

        <SideBar
          {...this.props}
          navigation={this.props.navigation}
          close={() => this.setState({ showMenu: false })}
          showMenu={showMenu} />

        <DivAccount>
          <DivInformation flex={isBarber === "BARBER" ? 5 : 6}>
            <ShopContainerProfile background={theme.primary}>
              <View style={{ paddingHorizontal: 16 }}>
                <HeaderBar
                  title={'ACCOUNT'}
                  leftIcon={require('que/assets/images/menu_open_home_costumer_icn.png')}
                  onPressLeftIcon={() => this.setState({ showMenu: true })}
                  lightMode
                />
              </View>

              <UserList {...this.props} tapUser={e => this.tapUser(e)} />

              <InformationsAccount>
                <CustomInput
                  image={require("que/assets/images/name_sign_in_icn.png")}
                  placeholder={"Type your name…"}
                  onChange={val => this.handleField("name", val)}
                  value={name}
                />
                <MarginInput />
                <CustomInput
                  image={require("que/assets/images/email_placeholder_icn.png")}
                  placeholder={"Type your email…"}
                  onChange={val => this.handleField("email", val)}
                  value={email}
                />
                <MarginInput />

                <CustomInput
                  image={require("que/assets/images/password_sign_in_icn.png")}
                  placeholder={"Type your password..."}
                  onChange={val => this.setState({ changePassword: true })}
                  value={password}
                  password={true}
                />
              </InformationsAccount>
            </ShopContainerProfile>
          </DivInformation>

          <DivButtons flex={isBarber === "BARBER" ? 4 : 3} background={theme.secondary}>

            {
              (isBarber === "BARBER") ? (
                renderBarber
              ) : (
                  <ButtonChangeProfile
                    image={require('que/assets/images/are_you_a_barber_icn.png')}
                    title={"ARE YOU A BARBER?"}
                    subtitle={"TELL US WHERE YOU WORK!"}
                    action={() => this.props.navigation.push("ChangeToBarber")}
                  />
                )
            }

            <ButtonChangeProfile
              image={require('que/assets/images/are_you_an_owner_icn.png')}
              title={"ARE YOU AN OWNER?"}
              subtitle={"ADD YOUR SHOP!"}
              action={() => this.confirmOwner()}
            />
          </DivButtons>

          {removeUser &&
            <PopBottom
              height={200}
              actionClose={() => this.setState({ ...this.state, removeUser: false, userSelected: {} })}
              isFinish={true}
              size={0}
              activePosition={0}
              showNext={false} >
              <StepRemove
                user={userSelected}
                actionContinue={() => this.confirmRemoveUser()}
                actionCancel={() => this.setState({ ...this.state, removeUser: false, userSelected: {} })} />
            </PopBottom>
          }

        </DivAccount>

        {changePassword && <ChangePass
          closePop={() => this.setState({ ...this.state, changePassword: false })}
          password={'1234'}
          changeNewPass={(password, confirm_password) => this.updatePassword(password, confirm_password)}
        />
        }

        {confirmOwner && <ConfirmSubscribeOwner
          actionCancel={() => this.confirmOwner()}
          actionContinue={() => this.props.navigation.navigate("OwnerSelectShop")} />}

      </>
    )
  }
}

const mapStateToProps = store => ({
  user: store.userReducer.user,
  isBarber: store.userReducer.interface,
  //shopWorking: store.userReducer.user.shopsWorking,
  //barber: store.userReducer.user.barber,
})

const mapDispatchToProps = dispatch =>
  bindActionCreators({
    removeUser, updateUser
  }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(withTheme(Profile))