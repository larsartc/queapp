import React, { useState } from "react"

import CustomInput from "que/components/CustomInput"
import { View, Text, Alert } from "react-native"
import PopBottom from "que/components/PopBottom"

const iconClose = require("que/assets/images/close_pop_up_gray_account_costumer_icn.png")
const iconAccept = require("que/assets/images/shopdetails/checkbox_services_checked_icn.png")
const iconInput = require("que/assets/images/password_sign_in_icn.png")

export default ({
  closePop,
  password,
  changeNewPass,
}) => {

  const [passwordLocal, setPassword] = useState('')
  const [passwordConfirm, setPassConfirm] = useState('')
  const [passwordNew, setPassNew] = useState('')
  const [passwordStep1, setPasswordStep1] = useState(false)
  const [passwordStep2, setPasswordStep2] = useState(true)

  confirmPass = val => {
    setPassword(val)
  }

  verifyAction = () => {
    console.log("passwordLocal", passwordLocal)
    console.log("passwordConfirm", passwordConfirm)
    console.log("passwordNew", passwordNew)
    console.log("passwordConfirm", passwordConfirm)

    if (passwordConfirm.length > 2 && passwordConfirm == passwordNew && !!passwordStep2) {
      changeNewPass(passwordConfirm, passwordNew)
    }
    // if(passwordLocal === ''){
    //   closePop()
    // }else{
    //   if(passwordLocal===password && passwordStep1){
    //     setPasswordStep1(false)
    //     setPasswordStep2(true)
    //   }else if(passwordLocal===password && !passwordStep1){
    //     if(passwordConfirm == passwordNew){
    //       changeNewPass(passwordNew)
    //       closePop()
    //     }else{
    //       Alert.alert("Erro", "Please, confirm the password.")
    //     }
    //   }
    // }

  }

  return (
    <PopBottom
      height={220}
      imageIcon={passwordLocal != '' ? iconAccept : iconClose}
      actionClose={() => verifyAction()}
      isFinish={true}
      size={0}
      activePosition={0}
      showNext={false}
    >

      <View style={{ flex: 1, paddingHorizontal: 24, alignItems: 'center', paddingTop: 10 }}>
        {passwordStep1 && (
          <>
            <Text>ENTER YOUR CURRENT PASSWORD</Text>
            <CustomInput
              image={iconInput}
              placeholder={"* * * *"}
              onChange={val => this.confirmPass(val)}
              value={null}
              password={true}
              textColor={"#333333"}
              placeholderTextColor={"#cccccc"}
              lineColor={"#333333"}
            />
          </>
        )}

        {passwordStep2 && (
          <>
            <Text>SET NEW PASSWORD</Text>
            <CustomInput
              image={iconInput}
              placeholder={"* * * *"}
              onChange={val => setPassNew(val)}
              value={null}
              password={true}
              textColor={"#333333"}
              placeholderTextColor={"#cccccc"}
              lineColor={"#333333"}
            />
            <CustomInput
              image={iconInput}
              placeholder={"* * * *"}
              onChange={val => setPassConfirm(val)}
              value={null}
              password={true}
              textColor={"#333333"}
              placeholderTextColor={"#cccccc"}
              lineColor={"#333333"}
            />
          </>
        )}


      </View>
    </PopBottom>
  )
}