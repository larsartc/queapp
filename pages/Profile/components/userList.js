import React from "react";

import { TouchableOpacity, View } from "react-native"
import { DivPlus, ImagePlus } from "../styled"
import { DivImages, ImageUser } from 'que/components/base'

export default ({ navigation, user, tapUser }) => {
  console.log("user object", user)
  const renderFamily = () => {
    if (user.family.length == 1) {
      return (
        <>
          <View style={{ width: 60, height: 60 }} />
          <TouchableOpacity onPress={() => tapUser(user.family[0])}>
            <ImageUser size={60} source={!!user.family[0].photo && !!user.family[0].photo.length > 0 ? { uri: user.family[0].photo } : require("que/assets/images/profile_sign_in_img.jpg")} />
          </TouchableOpacity>
          <DivPlus onPress={() => navigation.push("FamilyMember")}>
            <ImagePlus
              source={require("que/assets/images/add_member_account_costumer_icn.png")}
            />
          </DivPlus>
        </>
      )
    }
    if (user.family.length > 1) {
      return (
        <>
          <DivPlus onPress={() => navigation.push("FamilyMember")}>
            <ImagePlus
              source={require("que/assets/images/add_member_account_costumer_icn.png")}
            />
          </DivPlus>
          {user.family.map((val, i) => {
            if (i > 1) return null
            return (
              <>
                <TouchableOpacity onPress={() => tapUser(val)}>
                  <ImageUser size={i > 0 ? 40 : 60} source={!!val.photo && !!val.photo.length > 0 ? { uri: val.photo } : require("que/assets/images/profile_sign_in_img.jpg")} />
                </TouchableOpacity>
              </>
            )
          })
          }
        </>
      )
    }
  }

  return (
    <DivImages>
      {!!user && !!user.family && (user.family instanceof Array) ? renderFamily() : null}
    </DivImages>
  )

}