import React from 'react'
import { View, Text, Image, TouchableOpacity, Animated } from 'react-native'
import styles from './style'
import CloseImageAnimated from "que/components/CloseImageAnimated"
import DoneImageAnimated from "que/components/DoneImageAnimated"
import {useAnimation} from 'que/hooks'

export default (props) => {
  const animation = useAnimation({doAnimation:true, duration: 600})

  return (
    <View style={{marginHorizontal:30}}>
      <View style={{marginVertical:30, alignItems:"center"}}>
        <Animated.Image source={props.user.photo || require("que/assets/images/profile_sign_in_img.jpg")}
        style={{
          width: animation.interpolate({
                    inputRange: [0,1], 
                    outputRange: [100, 120],
                  }), 
          height: animation.interpolate({
                    inputRange: [0,1], 
                    outputRange: [100, 120],
                  }),
          backgroundColor:"white", 
          borderRadius: 60,
          marginTop: animation.interpolate({
                    inputRange: [0,1], 
                    outputRange: [-70, -90],
                  }),
          borderColor: "#CCCCCC", 
          borderWidth:3,
        }} />
      </View>

      <View style={styles.viewFormSaveCut}>
        <TouchableOpacity onPress={() => props.actionCancel()} style={styles.clickCloseSave}>
          <CloseImageAnimated 
            source={require("../../../assets/images/close_pop_up_gray_account_costumer_icn.png")}
            style={{ width: 25, height: 30, borderRadius: 23 }} />
        </TouchableOpacity>
        <View style={styles.viewForm}>
          <Text style={styles.saveCut}>DELETE USER FROM FAMILY MEMBERS?</Text>
        </View>
        <TouchableOpacity onPress={() => props.actionContinue()} style={styles.clickSaveSave}>
          <DoneImageAnimated 
            source={require("../../../assets/images/confirm_pop_up_account_costumer_icn.png")}
            style={{ width: 25, height: 30, borderRadius: 23 }} />
        </TouchableOpacity>
      </View>
    </View>
  )
}