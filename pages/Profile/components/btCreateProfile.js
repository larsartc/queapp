import React from 'react'
import {View, Image, Text, TouchableOpacity, ImageBackground, StyleSheet} from 'react-native'
import {colors} from 'que/components/base'

export default ({onPress, theme}) => {
  return (
   
    <TouchableOpacity onPress={()=>onPress()} 
    activeOpacity={1} 
    style={Object.assign({}, styles.baseBtCreateProfile, {backgroundColor:theme.primary})}>
    
      <View style={styles.badgetContent}>
        <View style={styles.badget}><Text style={{fontSize:10, color:'#fff'}}>!</Text></View>
        <Image style={{width:20,height:20, marginTop:-4}} source={require('que/assets/images/barbers/scisor_account_barber_profile_icn.png')} />
      </View>
      
    <ImageBackground source={require('que/assets/images/barbers/no_profile_onboarding_img.jpg')} 
      resizeMode={'cover'} 
      style={{ 
          flex:1, 
          alignItems:'center', 
          justifyContent:'center', 
          width:'100%', 
          height:'100%'
        }}>
      <Text style={styles.textTitle}>No profile</Text>
      <Text style={styles.textDesc}>You can press here to setup your barber profile!</Text>
    </ImageBackground>
  </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  badgetContent: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    marginTop: -20,
    backgroundColor:'#fff', 
    borderRadius:19,
    width:38, 
    height:38,
  }, 
  badget: {
    justifyContent:'center', 
    alignItems:'center',
    marginLeft:-34,
    marginTop:-8,
    backgroundColor:'red', 
    borderColor:'white', 
    borderWidth:1,
    borderRadius:8, 
    height:16,
    width:16,
  },
  baseBtCreateProfile: {
    height:300, 
    alignItems:'center', 
    justifyContent:'center', 
    borderTopColor:'#fff', 
    borderTopWidth:1,
  }, 
  textTitle:{
    color:'white', paddingHorizontal:32, fontSize:18, marginBottom:20, marginTop:30
  }, 
  textDesc: {
    color:'white', 
    width:200, 
    fontSize:14,
    textAlign:'center'
  }
})