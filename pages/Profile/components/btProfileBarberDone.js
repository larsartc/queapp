import React from "react";
import {
  View,
  Image,
  Text,
  TouchableOpacity,
  ImageBackground,
  StyleSheet
} from "react-native";
import { colors } from "que/components/base";

const week = [
  "Monday",
  "Tuesday",
  "Wednesday",
  "Thursday",
  "Friday",
  "Saturday",
  "Sunday"
]; // TODO make it a enum

export default ({ onPress, theme, shop, rating, hours }) => {
  let imgBarber = require("que/assets/images/barbers/no_profile_onboarding_img.jpg");

  return (
    <TouchableOpacity
      onPress={() => onPress()}
      activeOpacity={1}
      style={Object.assign({}, styles.baseBtCreateProfile, {
        backgroundColor: theme.primary
      })}
    >
      <View style={styles.badgetContent}>
        <Image
          style={{ width: 20, height: 20, marginTop: -4 }}
          source={require("que/assets/images/barbers/scisor_account_barber_profile_icn.png")}
        />
      </View>

      <ImageBackground
        source={imgBarber}
        resizeMode={"cover"}
        style={{
          flex: 1,
          alignItems: "center",
          justifyContent: "center",
          width: "100%",
          height: "100%"
        }}
      >
        <Text style={{...styles.textDesc, marginTop: 32}}>{shop.address}</Text>
        <View
          style={{
            backgroundColor: "white",
            height: 0.5,
            width: "80%",
            marginVertical: 16
          }}
        />
        {hours.map(val => (
          <Text style={styles.textDesc}>
            {week[val.week_day]}: {val.start_time} - {val.end_time}
          </Text>
        ))}

        {/* Ratings */}
        <View
          style={{
            flexDirection: "row",
            flex: 1,
            alignItens: "center",
            justifyContent: "flex-end",
            textAlign: "right",
            marginTop: 32
          }}
        >
          {Array(Math.ceil(rating))
            .fill(null)
            .map(r => {
              return (
                <Image
                  source={require("que/assets/images/shopdetails/shop_raiting_filled_icn.png")}
                  style={{
                    width: 20,
                    height: 20,
                    marginRight: 10,
                    marginLeft: 10
                  }}
                />
              );
            })}
          {Array(5 - Math.ceil(rating))
            .fill(null)
            .map(r => {
              return (
                <Image
                  source={require("que/assets/images/shopdetails/shop_raiting_empty_icn.png")}
                  style={{
                    width: 20,
                    height: 20,
                    marginRight: 10,
                    marginLeft: 10
                  }}
                />
              );
            })}
        </View>
      </ImageBackground>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  badgetContent: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
    marginTop: -20,
    backgroundColor: "#fff",
    borderRadius: 19,
    width: 38,
    height: 38
  },
  badget: {
    justifyContent: "center",
    alignItems: "center",
    marginLeft: -34,
    marginTop: -8,
    backgroundColor: "red",
    borderColor: "white",
    borderWidth: 1,
    borderRadius: 8,
    height: 16,
    width: 16
  },
  baseBtCreateProfile: {
    height: 300,
    alignItems: "center",
    justifyContent: "center",
    borderTopColor: "#fff",
    borderTopWidth: 1
  },
  textTitle: {
    color: "white",
    paddingHorizontal: 32,
    fontSize: 18,
    marginBottom: 20,
    marginTop: 30
  },
  textDesc: {
    color: "white",
    width: 200,
    fontSize: 14,
    textAlign: "center"
  }
});
