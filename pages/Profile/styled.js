import styled from 'styled-components';
import { colors } from 'que/components/base'

export const DivAccount = styled.View`
    flex: 1;
`
export const DivInformation = styled.View`
    flex: ${props=>props.flex||6};
` 
export const DivButtons = styled.View`
    flex: ${props=>props.flex||3};
    background-color: ${props=> props.background ? props.background : colors.secondary};
`

export const ShopContainerProfile = styled.View`
    background-color: ${props=> props.background ? props.background : colors.primary};
    display: flex;
    flex-direction: column;
    align-items: center;
`
export const InformationsAccount = styled.View`
    margin-top: 40;
    padding-left: 40;
    padding-right: 40;
    height: 100%;
`
export const Header = styled.View`
    margin-bottom: 10;
`
export const MarginInput = styled.View`
    margin-top: 15;
`
export const HeaderDiv = styled.View`
    margin-top: 25;
    display: flex;
    justify-content: center;
    align-items: center;
`
export const HeaderTitleBar = styled.View`
    display: flex;
    height: 20;
    flex-direction: row;
    margin-left: 10%;
    margin-right: 10%;
`
export const HeaderTitle = styled.Text`
    color: ${colors.white};
    font-size: 16
`
export const HeaderIconContainer = styled.TouchableOpacity`
    display: none;
`
export const HeaderIcon = styled.Image`
    height: 15;
    width: 15;
`
export const ImageUserKids = styled.Image`
    height: 52;
    width: 52;
    border-width: 2;
    border-color: ${colors.white};
    margin-left: 10px; 
    margin-right: 10px;
    border-radius: 50;
`
export const DivPlus = styled.TouchableOpacity`
    height: 35;
    width: 35;
    border-width: 1;
    border-color: ${colors.white};
    margin-left: 10px; 
    margin-right: 10px;
    border-radius: 50;
    display: flex;
    justify-content: center;
    align-items: center;
`
export const ImagePlus = styled.Image`
    height: 15;
    width: 15;
`