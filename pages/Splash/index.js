import React from 'react'
import { View, Animated, StatusBar } from 'react-native'
import LottieView from 'lottie-react-native'
import useAnimation from 'que/hooks/useAnimation'
import { connect } from 'react-redux'
import { refreshToken } from "que/store/user";
import { NavigationActions, StackActions } from 'react-navigation';
import axios from "que/services";
import { bindActionCreators } from 'redux'

const Splash = (props) => {
  const animation = useAnimation({ doAnimation: true, duration: 300 })

  goHead = async () => {
    console.log("splash", props.user)
    if (!props.user || !props.user.token || !props.user.refresh_token) {
      return props.navigation.dispatch(StackActions.reset({
        index: 0,
        actions: [
          NavigationActions.navigate({ routeName: "SignIn" })
        ]
      }))
    } else {
      try {
        await props.refreshToken(props.user.refresh_token)
        axios.defaults.headers.common['Authorization'] = props.user.token
        return props.navigation.dispatch(StackActions.reset({
          index: 0,
          actions: [
            NavigationActions.navigate({ routeName: "ShopsScreen" })
          ]
        }))
      } catch (e) {
        return props.navigation.dispatch(StackActions.reset({
          index: 0,
          actions: [
            NavigationActions.navigate({ routeName: "SignIn" })
          ]
        }))
      }
    }
  }

  return (
    <View style={{
      flex: 1,
      backgroundColor: '#3D2E52',
      justifyContent: 'center',
      alignItems: 'center'
    }}>

      <StatusBar translucent backgroundColor="transparent" barStyle="light-content" />

      <Animated.View style={{
        opacity: animation.interpolate({
          inputRange: [0, 1],
          outputRange: [0, 1]
        }),
      }}>
        <LottieView
          ref={anim => this.logo = anim}
          style={{ width: 125, height: 160 }}
          source={require('que/assets/animations/logo.json')}
          autoPlay={true}
          loop={false}
          onAnimationFinish={() => goHead()}
        />
      </Animated.View>

    </View>
  )
}


const mapStateToProps = state => ({
  user: state.userReducer.user
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({ refreshToken }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Splash)