import React from "react";
import Styled from "./styled";

import { View, Text } from "native-base";
import { TextInput, KeyboardAvoidingView, Keyboard } from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import SideBar from "que/components/Sidebar";
import HeaderBar from "que/components/HeaderBar";

import LineChat from "../../components/LineChat";
import { colors, ShopContainer } from "../../components/base";
import styles from "./styles";

//Redux
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withTheme } from "../../utils/themeProvider";
import { getMessages, createMessage } from "../../store/chat";

class Chat extends React.Component {
  state = {
    showMenu: false,
    message: ""
  };

  componentWillMount() {
    this.props.getMessages();
  }

  changeMessage = text => {
    this.setState({ message: text });
  };

  send = () => {
    // Hide that keyboard!
    Keyboard.dismiss();
    if (
      !this.state.message.length ||
      this.state.message.replace(/\s/g, "").length == 0
    )
      return false;

    const to = this.props.chats.find(chat => chat.user == this.props.user._id);

    if (!!to && !!to.to.length) {
      this.props.createMessage(to.to, { message: this.state.message });
    } else {
      this.props.createMessage(null, { message: this.state.message });
    }
    this.setState({ message: "" });
  };

  render() {
    const { showMenu } = this.state;
    const { theme } = this.props;
    return (
      <ShopContainer>
        <SideBar
          {...this.props}
          navigation={this.props.navigation}
          close={() => this.setState({ showMenu: false })}
          showMenu={showMenu}
        />

        <View style={{ backgroundColor: theme.primary }}>
          <HeaderBar
            title={"CHAT"}
            onPressLeftIcon={() => this.setState({ showMenu: true })}
            leftIcon={require("que/assets/images/menu_open_home_costumer_icn.png")}
            rightIcon={require("que/assets/images/close_button_chat_icn.png")}
            onPressRightIcon={() => this.props.navigation.pop()}
            lightMode
          />
        </View>
        <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding">
          <View style={{ flex: 1 }}>
            <View style={{ ...styles.topInformation, height: 50 }} padder>
              <View style={styles.clipsLeft}>
                <Styled.IconSubHeader
                  source={require("que/assets/images/clips_chat_icn.png")}
                />
                <View style={{position: 'absolute', left: 30, top: 0, height: 1000000000, width: 1, backgroundColor: '#e0e0e0'}} />
                <Text style={styles.colorGrayLight}>CLIPS</Text>
              </View>
              <View style={styles.clipsRight}>
                <Text style={styles.colorGrayLightRight}>ME</Text>
                <View style={{position: 'absolute', right: 30, top: 0, height: 1000000000, width: 1, backgroundColor: '#e0e0e0'}} />
                <Styled.IconSubHeaderRight
                  source={
                    !!this.props.user && this.props.user.photo
                      ? { uri: this.props.user.photo }
                      : require("que/assets/images/profile_sign_in_img.jpg")
                  }
                />
              </View>
            </View>
            <View style={{ flex: 1 }}>
              <ScrollView
                ref="scrollView"
                onContentSizeChange={(width, height) =>
                  this.refs.scrollView.scrollTo({ y: height })
                }
              >
                {this.props.chats.map(chat => {
                  return (
                    <LineChat
                      image={require("que/assets/images/clips_chat_icn.png")}
                      text={chat.message}
                      right={chat.user == this.props.user._id}
                    />
                  );
                })}
              </ScrollView>
            </View>
            <View style={styles.baseInput}>
              <TextInput
                value={this.state.message}
                style={styles.inputText}
                onChangeText={text => this.changeMessage(text)}
                onSubmitEditing={() => this.send()}
                placeholder={'Thanks...'}
              />
              <Styled.TouchableContainer onPress={() => this.send()}>
                <Styled.ImageSend
                  source={require("que/assets/images/send_button_chat_icon.png")}
                />
              </Styled.TouchableContainer>
            </View>
          </View>
        </KeyboardAvoidingView>
      </ShopContainer>
    );
  }
}

const mapStateToProps = store => ({
  user: store.userReducer.user,
  chats: store.chatReducer.chats
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getMessages,
      createMessage
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(withTheme(Chat));
