import {StyleSheet} from 'react-native'
import { colors } from '../../components/base'

export default StyleSheet.create({
  topInformation:{
      display: 'flex',
      alignItems: 'center',
      flexDirection: 'row'
  },
  clipsLeft:{
      flex: 1,
      display: 'flex',
      alignItems: 'center',
      flexDirection: 'row',
      paddingLeft: 15
  },
  clipsRight:{
      flex: 1,
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'flex-end',
      paddingRight: 15
  },
  colorGrayLight:{
      color: colors.gray,
      marginLeft: 7
  },
  colorGrayLightRight:{
      color: colors.gray,
      textAlign: 'right',
      marginRight: 7
  },
  circleIcon: {
      backgroundColor: colors.gray,
      borderRadius: 60,
      height: 42, 
      width: 42,
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      marginLeft: 10, 
      marginRight: 10
  },
  circleIconActive:{
      backgroundColor: colors.gray,
      borderRadius: 60,
      height: 55, 
      width: 55,
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      marginLeft: 10, 
      marginRight: 10
  }, 

  // bar input 
  baseInput: {
    backgroundColor: colors.lightGray, 
    flexDirection: 'row', 
    alignItems: 'center', 
    justifyContent: 'center', 
    height: 76, 
    paddingBottom:24,
    paddingHorizontal: 48,
  },
  inputText: {
    width:'100%',
    backgroundColor:'white', 
    borderRadius:30,
    paddingHorizontal:24,
    paddingVertical: 12,
    marginRight:12
  },

})