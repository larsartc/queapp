import styled from 'styled-components';
import { colors } from 'que/components/base';

export default styledContainer = {
    HeaderTitleBar: styled.View`
        display: flex;
        height: 20;
        flex-direction: row;
        justify-content: space-between;
        margin-left: 10%;
        margin-right: 10%;
    `,
    HeaderTitle: styled.Text`
        color: ${colors.white}
    `,
    HeaderIcon: styled.Image`
        height: 15;
        width: 15;
    `,
    ImageSend: styled.Image`
        height: 40;
        width: 40;
    `,
    TouchableContainer: styled.TouchableOpacity`
        margin-left: 5px;
    `,
    IconSubHeader: styled.Image`
        height: 30;
        width: 30;
        border-color: transparent;
        border-width: 2;
    `,
    IconSubHeaderRight: styled.Image`
        height: 30;
        width: 30;
        border-color: ${colors.primary};
        border-width: 2;
        border-radius: 50;
    `,
}