import React, { useState, Component } from 'react';
import {
    PageContainer,
    PersonImage,
    PageTitle,
    ImagePersonContainer,
    LogoContainer,
    PageMargin,
    InputMargin,
    ButtonContainer
} from './styled';
import CustomInput from 'que/components/CustomInput';
import CustomButton from 'que/components/CustomButton';
import CustomImageCircle from 'que/components/CustomImageCircle';
import CustomBackgroundImage from 'que/components/CustomBackgroundImage'
import LoadingDefault from 'que/components/LoadingDefault'
import Logo from 'que/components/Logo'
import { KeyboardAvoidingView, ScrollView, TouchableOpacity, Alert, TouchableWithoutFeedback, View } from 'react-native';
import AsyncStorage from "@react-native-community/async-storage"
import * as ASYNCTAG from "../../utils/storeNames"
import ImagePicker from 'react-native-image-picker'
import { Thumbnail } from "native-base"

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { create } from '../../store/user'
import { colors } from 'que/components/base'
import CheckTerms from "que/components/checkTerms"

import { changeEnvToDev } from "../../services"
import { uploadImage } from '../../utils/upload'

const configImagePicker = {
    title: 'Choose a photo',
    storageOptions: {
        skipBackup: true,
        path: 'images'
    }
}

class SignUp extends Component {
    state = {
        terms: false,
        name: '',
        email: '',
        password: '',
        confirm_password: '',
        temp_photo: null,
        tapEnv: 0,
        uploadImage: ''
    }

    changeEnv = () => {
        if (this.state.tapEnv >= 8) {
            newTapEnv = 0
            changeEnvToDev()
            Alert.alert("Change env", "ENV: development")
        } else {
            newTapEnv = this.state.tapEnv + 1
        }

        this.setState({
            tapEnv: newTapEnv
        })
    }

    makeRegister = () => {
        const { terms, name, email, password, confirm_password, uploadImage } = this.state;
        if (terms == true && name != '' && email != '' && password != '' && confirm_password != '' && (password === confirm_password)) {

            let player_id = ""
            AsyncStorage.getItem(ASYNCTAG.TAG_PUSH_ID).then(val => {
                player_id = JSON.parse(val).userId
                this.props.create({ name, email, password, confirm_password, player_id, photo: uploadImage })
            })
            //this.props.create({name, email, password, confirm_password})
        } else {
            alert('Oops, some fields are wrong!')
        }
    }

    activeButton = () => {
        const { terms, name, email, password, confirm_password } = this.state
        if (terms == true &&
            name != '' &&
            email != '' &&
            password != '' &&
            confirm_password != '') {
            return true
        } else {
            return false
        }
    }

    pickImage = () => {
        ImagePicker.showImagePicker(configImagePicker, response => {
            if (response.error) {
                alert('Error. ', response.error)
            } else {
                const source = { uri: response.uri }
                if (response.uri != null) {
                    this.setState({
                        temp_photo: source,
                        temp_photo_path: response.path
                    })

                    uploadImage(response)
                        .then(resp => resp.json())
                        .then(resp => {
                            console.log(resp);
                            this.setState({
                                uploadImage: resp.data
                            });
                        });
                }
            }
        })
    }

    componentWillReceiveProps = nextProps => {
        if (this.props !== nextProps) {
            if (nextProps.create_success) {
                alert('Account successfully created')
                this.props.navigation.navigate("SignIn")
                return
            }
        }

        if (nextProps.error != null) {
            Alert.alert("Error", nextProps.error)
        }
    }

    render() {
        const { terms, name, email, password, confirm_password, temp_photo } = this.state;
        const { loading } = this.props;

        return (
            <CustomBackgroundImage>

                <KeyboardAvoidingView behavior="null" keyboardVerticalOffset={0} enabled>

                    <ScrollView>

                        <LogoContainer>
                            <Logo />
                        </LogoContainer>

                        <TouchableWithoutFeedback onPress={() => this.changeEnv()}>
                            <PageTitle>SIGN UP</PageTitle>
                        </TouchableWithoutFeedback>

                        <TouchableOpacity onPress={() => this.pickImage()}>
                            <ImagePersonContainer>
                                <CustomImageCircle width={80} height={80}>
                                    {(temp_photo != null || temp_photo != undefined) && (
                                        <Thumbnail
                                            style={{
                                                width: 80,
                                                height: 80,
                                                borderRadius: 40,
                                                borderColor: 'white',
                                                borderWidth: 2
                                            }}
                                            large
                                            source={temp_photo} />
                                    )}
                                    {(temp_photo == null || temp_photo == undefined) &&
                                        <PersonImage
                                            source={require('que/assets/images/perfil_placeholder_sign_in_icn.png')}
                                            style={{ marginTop: 0 }}
                                        />}
                                </CustomImageCircle>
                            </ImagePersonContainer>
                        </TouchableOpacity>


                        <PageMargin>
                            <CustomInput
                                image={require('que/assets/images/name_sign_in_icn.png')}
                                value={name}
                                placeholder={'Type in your fullname…'}
                                onChange={(name) => this.setState({ name: name })}
                                autoCorrect={false}
                            />
                            <InputMargin />

                            <CustomInput
                                image={require('que/assets/images/email_placeholder_icn.png')}
                                value={email} placeholder={'Your email…'}
                                onChange={(email) => this.setState({ email: email })}
                                autoCorrect={false}
                            />
                            <InputMargin />

                            <CustomInput
                                image={require('que/assets/images/password_sign_in_icn.png')}
                                value={password}
                                placeholder={'Create a password…'}
                                eyeImage={true}
                                onChange={(password) => this.setState({ password: password })}
                                password={true}
                            />
                            <InputMargin />

                            <CustomInput
                                image={require('que/assets/images/password_sign_in_icn.png')}
                                value={confirm_password}
                                placeholder={'Confirm your password…'}
                                eyeImage={true}
                                onChange={(confirm_password) => this.setState({ confirm_password: confirm_password })}
                                password={true} />

                            <CheckTerms checkAction={() => this.setState({ terms: !terms })} checked={terms} />
                        </PageMargin>

                        <ButtonContainer>
                            {loading ? (
                                <View style={{flex: 1}}>
                                    <LoadingDefault color={colors.white} />
                                </View>
                            ) : (
                                    this.activeButton() ?
                                        <CustomButton
                                            opacity={1}
                                            onPress={() => this.makeRegister()}
                                            text={'REGISTER'} /> :
                                        <CustomButton
                                            opacity={1}
                                            onPress={() => this.makeRegister()}
                                            text={'REGISTER'} />
                                )
                            }
                        </ButtonContainer>

                    </ScrollView>

                </KeyboardAvoidingView>

            </CustomBackgroundImage>

        )
    }
}

const mapStateToProps = store => ({
    loading: store.userReducer.loading,
    create_success: store.userReducer.create_success,
    error: store.userReducer.error,
})

const mapDispatchToProps = dispatch =>
    bindActionCreators({
        create,
    }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(SignUp)