import styled from 'styled-components';
import { ScreenTitle, MarginDefaultUnAuthenticatedFlow, MarginInputsUnAuthenticatedFlow } from 'que/assets/defaultStyle'
import { ifIphoneX } from 'react-native-iphone-x-helper'

export const PageContainer = styled.View`
    height: 100%;
    width: 100%;
`

export const LogoContainer = styled.View`
    margin-bottom: 5%;
    display: flex;
    align-self: center;
    ${ifIphoneX({marginTop: 60},{marginTop: 20})}
`

export const ButtonContainer = styled.View`
    margin: 0 auto;
    height: 40;
    width: 100;
    margin-top: 20;
`

export const ImagePersonContainer = styled.View`
   margin-top: 15;
   margin-bottom: 15;
`
export const PersonImage = styled.Image`
    margin: 0 auto;
`

export const PageTitle = styled(ScreenTitle)`
    text-align: center;
`

export const PageMargin = styled(MarginDefaultUnAuthenticatedFlow)``

export const InputMargin = styled(MarginInputsUnAuthenticatedFlow)``
