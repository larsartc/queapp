import styled from 'styled-components';

export const ScreenTitle = styled.Text`
    font-size: 14;
    color: white;
    
`
export const MarginDefaultUnAuthenticatedFlow = styled.View`
    margin-left: 10%;
    margin-right: 10%;
`

export const MarginInputsUnAuthenticatedFlow = styled.View`
    margin-bottom: 10;
`

export const ButtonSignInDefaultStyle = styled.View`
    height: 40;
    width: 100;
`