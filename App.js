import React from 'react'
console.disableYellowBox = true
import Router from 'que/routes'
import { Provider } from 'react-redux'
import { store, persistor } from './store'
import { updateAppointmentByNotification } from './store/appointment'
import { PersistGate } from 'redux-persist/integration/react'

import OneSignal from 'react-native-onesignal'
import AsyncStorage from "@react-native-community/async-storage"
import * as ASYNCTAG from "./utils/storeNames"
import { updateAppointmentStatus } from "./services/appointmentService"

import { ThemeContextProvider } from './utils/themeProvider'

export default class App extends React.Component {

  componentDidMount = () => {
    OneSignal.init("cb484cab-0f5f-45fd-a58e-cb0bc7fad7b5");

    console.log('aquii');
    OneSignal.addEventListener('received', this.onReceived);
    OneSignal.addEventListener('opened', this.onOpened);
    OneSignal.addEventListener('ids', this.onIds);
  }

  componentWillUnmount() {
    OneSignal.removeEventListener('received', this.onReceived)
    OneSignal.removeEventListener('opened', this.onOpened)
    OneSignal.removeEventListener('ids', this.onIds)
  }

  onReceived(notification) {
    console.log("Notification received: ", notification)
  }

  onOpened = async openResult => {
    console.log('Message: ', openResult.notification.payload.body)
    console.log('Data: ', openResult.notification.payload.additionalData)
    console.log('isActive: ', openResult.notification.isAppInFocus)
    console.log('openResult: ', openResult)

    // NOTIFICATION TO REMEMBER APPOINTMENT 

    if (!!openResult.notification.payload.additionalData.type) {

      if (openResult.notification.payload.additionalData.type == "review_appointment") {

        const response = openResult.action.actionID.endsWith('-1') ? "yes" : "no";
        const messageId = openResult.action.actionID.split("-")[0];
        const notification = { response, messageId };

        const { data } = await updateAppointmentStatus(notification);

        if (response === 'yes') {
          store.dispatch({ type: "NOTIFICATION_RECEIVED", payload: data });
          this.props.navigation.navigate("ShopsScreen", notification);
        }
      }
      if (openResult.notification.payload.additionalData.type == "remember_appointment") {

        AsyncStorage.setItem(ASYNCTAG.TAG_NOTIFICATION_PAYLOAD, JSON.stringify(openResult.notification.payload.additionalData)).then(_ => {
          console.log("push payload saved")
        }).catch(_ => {
          console.log("erro on save push payload")
        })

      }
    }
  }

  onIds(device) {
    console.log('Device info: ', device)

    AsyncStorage.setItem(ASYNCTAG.TAG_PUSH_ID, JSON.stringify(device)).then(_ => {
      console.log("push saved")
    }).catch(_ => {
      console.log("erro on save push id")
    })
  }

  render() {
    return (
      <ThemeContextProvider>
        <Provider store={store}>
          <PersistGate loading={null} persistor={persistor}>
            <Router />
          </PersistGate>
        </Provider>
      </ThemeContextProvider>
    )
  }
}
