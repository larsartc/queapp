import axios from "./index"

export const create = data => new Promise((resolve, reject) => {
  axios("feedback", {
    method: "post",
    headers: {
      'Content-type': 'application/json'
    },
    data: data
  }
  ).then(response => resolve(response)).catch(e => reject(e))
})

export async function systemFeedback(data) {
  return await axios("feedback/system", {
    method: "post",
    headers: {
      'Content-type': 'application/json'
    },
    data: data
  })
}