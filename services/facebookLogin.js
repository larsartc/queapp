import { LoginManager, AccessToken, GraphRequest, GraphRequestManager, } from 'react-native-fbsdk';

export const AuthFacebook = async () => {
  var myAccessToken = null;
  let result;

  try {
    result = await LoginManager.logInWithPermissions(['public_profile', 'email']);

    if (result.isCancelled) {
      return { error: 'Canceled auth with Facebook.' };
    }

    AccessToken.getCurrentAccessToken().then((data) => {
      myAccessToken = data.accessToken.toString();
    }).catch(error => {
      console.log("1",  error)
      return { error: 'Erro on auth with Facebook.' };
    });

    const info = await getAccountInfo();
    return { user: info, accessToken: myAccessToken };

  } catch (err) {
    console.log("2",  err)
    return { error: 'Erro on auth with Facebook.' };
  }
};

const getAccountInfo = () => {
  return new Promise((resolve, reject) => {
    const infoRequest = new GraphRequest('/me?fields=id,name,email,picture.type(large)', null, ((error, result) => {
      if (error) {
        reject(error)

      } else {
        resolve(result)
      }
    }));

    new GraphRequestManager().addRequest(infoRequest).start();
  });
}