import axios from "./index"

export const create = data => new Promise((resolve, reject) => {
  axios("barber", {
      method: "post",
      headers: {
        'Content-type': 'application/json'
      },
      data
    }
  ).then(response=>resolve(response)).catch(e=>reject(e))
})

export const list = () => new Promise((resolve, reject)=>{
  axios.get(`barber`).then(response=>resolve(response)).catch(e=>reject(e))
})

export const find = id => new Promise((resolve, reject)=>{
  axios.get(`barber/${id}`).then(response=>resolve(response)).catch(e=>reject(e))
})

export const createCarrer = (data, id) => new Promise((resolve, reject) => {
  axios(`barber/carrer`, {
      method: "post",
      headers: {
        'Content-type': 'application/json'
      },
      data: data
    }
  ).then(response=>resolve(response)).catch(e=>reject(e))
})

export const putCarrer = (data, id) => new Promise((resolve, reject) => {
  axios(`barber/${id}`, {
      method: "patch",
      headers: {
        'Content-type': 'application/json'
      },
      data: data
    }
  ).then(response=>resolve(response)).catch(e=>reject(e))
})

export const workRequest = data => new Promise((resolve, reject) => {
  axios("barber/work-request", {
      method: "post",
      headers: {
        'Content-type': 'application/json'
      },
      data
    }
  ).then(response=>resolve(response)).catch(e=>reject(e))
})

export const createAppointment = data => new Promise((resolve, reject) => {
  axios("barber/appointment", {
      method: "post",
      headers: {
        'Content-type': 'application/json'
      },
      data
    }
  ).then(response=>resolve(response)).catch(e=>reject(e))
})

export const listFeedbackBarber = data => new Promise((resolve, reject)=> {
  // TODO CONFIRMAR SE VAI CRIAR UM NOVO ENDPOINT
  
  axios(`feedback/barber/${data.idBarber}/${data.isShop}`, {
      method: "get",
      headers: {
        'Content-type': 'application/json'
      }
    }
  ).then(response=>resolve(response)).catch(e=>reject(e))
})