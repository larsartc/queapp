import axios from "./index"

export const list = () => new Promise((resolve, reject)=>{
  axios.get(`faq`).then(response=>resolve(response)).catch(e=>reject(e))
})