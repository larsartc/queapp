import axios from "./index"

export const create = data => new Promise((resolve, reject) => {
  // let payload = new FormData()

  // Object.keys(data).map(e => {
  //   payload.append(e, data[e])
  // })

  axios("user", {
    method: "post",
    headers: {
      'Content-type': 'application/json'
    },
    data: data
  }
  ).then(response => resolve(response)).catch(e => reject(e))
})

export async function login(data) {
  return await axios("user/login", {
    method: "post",
    headers: {
      'Content-type': 'application/json'
    },
    data: data
  })
}

export const loginSocial = data => new Promise((resolve, reject) => {
  axios("user/social-media/login", {
    method: "post",
    headers: {
      'Content-type': 'application/json'
    },
    data: data
  }
  ).then(function (response) {
    axios.defaults.headers.common['Authorization'] = response.data.token.token
    resolve(response)
  }).catch(e => reject(e))
})

export const favorite = (data, id) => new Promise((resolve, reject) => {
  let payload = new FormData()

  Object.keys(data).map(e => {
    payload.append(e, data[e])
  })

  axios(`user/favorite/${id}`, {
    method: "post",
    headers: {
      'Content-type': 'application/json'
    },
    data: payload
  }
  ).then(response => resolve(response)).catch(e => reject(e))
})

export const rating = async data => {
  return await axios("/user/rating", {
    method: "post",
    headers: {},
    data
  })
}

export const feedback = async data => {
  return await axios("/feedback", {
    method: "post",
    headers: {},
    data
  })
}

export const getProfile = () => new Promise((resolve, reject) => {
  axios.get("/user/details").then(response => resolve(response)).catch(e => reject(e))
})

export const getToken = data => new Promise((resolve, reject) => {
  axios("user/token", {
    method: "post",
    headers: {},
    data: data,
  }).then(res => resolve(res)).catch(e => reject(e))
})

export async function refresh_token(refresh_token) {
  return await axios("user/refresh-token", {
    method: "post",
    headers: {},
    data: { refresh_token },
  })
}

export const forgotPass = data => new Promise((resolve, reject) => {
  axios("user/forgot-password", {
    method: "post",
    headers: {},
    data: data,
  }).then(res => resolve(res)).catch(e => reject(e))
})

export async function userFamily() {
  return await axios.get("user/family")
}

export const createFamily = data => new Promise((resolve, reject) => {

  axios(`user/family`, {
    method: "post",
    headers: {
      'Content-type': 'application/json'
    },
    data: data
  }
  ).then(response => resolve(response)).catch(e => reject(e))

})

export function updateUser(user) {
  return axios(`user`, {
    method: "put",
    headers: {
      'Content-type': 'application/json'
    },
    data: { ...user }
  })
}

export const removeFamily = data => new Promise((resolve, reject) => {
  axios.delete(`user/family/${data}`).then(response => resolve(response)).catch(e => reject(e))
})

export async function forgotPassword(email) {
  return await axios("user/forgot-password", {
    method: "post",
    headers: {
      'Content-type': 'application/json'
    },
    data: email,
  })
}
