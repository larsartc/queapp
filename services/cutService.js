import axios from "./index"

export const list = () => new Promise((res, rej)=>{
  axios.get('styles').then(data=>res(data)).catch(error=>rej(error))
})

export const favorite = data => new Promise((resolve, reject)=>{
  axios("user/favorite", {
    method: "post", 
    headers: {'Content-type': 'application/json'}, 
    data: data,
  }).then(res=>resolve(res)).catch(e=>reject(e))
})

export const unfavorite = data => new Promise((resolve, reject)=>{
  axios("user/unfavorite", {
    method: "post", 
    headers: {'Content-type': 'application/json'}, 
    data: data,
  }).then(res=>resolve(res)).catch(e=>reject(e))
})