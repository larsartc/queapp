import AsyncStorage from "@react-native-community/async-storage";
import * as ASYNCTAG from "../utils/storeNames"
import * as userApi from "./userService"

const TAG = "TOKEN_SERVICE"

export const verify = () => new Promise((resolve, reject) => {

  AsyncStorage.getItem(ASYNCTAG.TAG_STORAGE_LOGIN).then(val=>{
    let dataToken = JSON.parse(val)

    let date1h = new Date().getTime()-(60*60*1000)
    let date24h = new Date().getTime()-((60*60-1000)*24)

    if(dataToken.timeReflesh <= date24h){
      // token reflesh expirated
      // call the userLogin ...
      return
    }

    if(dataToken.time <= date1h){
      userApi.refleshToken({refresh_token: dataToken.refresh_token}).then(val2=>{
        if(val2.data.message!="Invalid refresh token"){
          resolve(val2)
        }else{
          reject(val2.data.message)
        }
      }).catch(e=>{
        reject(e)
      })
    }else{
      resolve(val)
    }

  }).catch(e=>reject(e))

})