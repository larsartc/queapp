import axios from "./index"

export const make = data => new Promise((resolve, reject) => {
  axios("appointment", {
    method: "post",
    headers: {
      'Content-type': 'application/json'
    },
    data: data
  }
  ).then(response => resolve(response)).catch(e => {
    return reject(e)
  })
})

export async function update(data) {
  return await axios(`appointment/${data._id}`, {
    method: "put",
    headers: {
      'Content-type': 'application/json'
    },
    data: { ...data }
  })
}

export async function updateAppointmentStatus(data) {
  return await axios(`appointment/${data.messageId}/${data.response}`, {
    method: "put",
    headers: {
      'Content-type': 'application/json'
    },
    data: {}
  })
}

export async function list(payload) {
  return await axios.get(`appointment/${payload}`);
}

export async function listDay(payload) {
  return await axios.get(`appointment/${payload.month}/${payload.day}`);
}

export const find = id => new Promise((res, rej) => {
  axios.get(`appointment/${id}`).then(data => res(data)).catch(error => rej(error))
})

export async function remove(id) {
  return await axios.delete(`appointment/${id}`)
}