import axios from "./index"

export const list = () => new Promise((res, rej)=>{
  axios.get('category').then(data=>res(data)).catch(error=>rej(error))
})