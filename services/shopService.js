import axios from "./index"

const TAG = "SHOP_SERVICE"

export const create = data => new Promise((resolve, reject) => {
  axios("shop", {
    method: "post",
    headers: { 'Content-type': 'application/json' },
    data
  }).then(res => resolve(res)).catch(e => reject(e))
})

export const includeWorker = data => new Promise((resolve, reject) => {
  axios("shop/barber", {
    method: "post",
    headers: { 'Content-type': 'application/json' },
    data
  }).then(res => resolve(res)).catch(e => reject(e))
})

export const listByUser = () => new Promise((resolve, reject) => {
  axios.get(`shop`).then(response => resolve(response)).catch(e => reject(e))
})

export const list = data => new Promise((resolve, reject) => {
  let params = {
    "latitude": data.latitude,
    "longitude": data.longitude,
    "order": data.order
  }
  if (data.filter) params.filter = data.filter
  axios("shops", {
    method: "post",
    headers: { 'Content-type': 'application/json' },
    data: params,
  }).then(res => resolve(res)).catch(e => reject(e))
})

export const find = id => new Promise((resolve, reject) => {
  axios.get(`shop/${id}`).then(response => resolve(response)).catch(e => reject(e))
})

export const findBarber = id => new Promise((resolve, reject) => {
  axios.get(`barber/${id}`).then(response => resolve(response)).catch(e => reject(e))
})

export const unfavorite = data => new Promise((resolve, reject) => {
  axios("user/unfavorite", {
    method: "post",
    headers: { 'Content-type': 'application/json' },
    data: data,
  }).then(res => resolve(res)).catch(e => reject(e))
})

export const favorite = data => new Promise((resolve, reject) => {
  axios("user/favorite", {
    method: "post",
    headers: { 'Content-type': 'application/json' },
    data: data,
  }).then(res => resolve(res)).catch(e => reject(e))
})