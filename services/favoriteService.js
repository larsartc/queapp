import axios from "./index"

export const cuts = (data) => new Promise((res, rej)=>{
  axios.post('user/favorites/cut', data).then(data=>res(data)).catch(error=>rej(error))
})

export const barbers = (data) => new Promise((res, rej)=>{
  axios.post('user/favorites/barber', data).then(data=>res(data)).catch(error=>rej(error))
})

export const shops = (data) => new Promise((res, rej)=>{
  axios.post('user/favorites/shop', data).then(data=>res(data)).catch(error=>rej(error))
})

export const unfavorite = data => new Promise((resolve, reject)=>{
    axios("user/unfavorite", {
      method: "post", 
      headers: {'Content-type': 'application/json'}, 
      data: data,
    }).then(res=>resolve(res)).catch(e=>reject(e))
})

export const favorite = data => new Promise((resolve, reject)=>{
  axios("user/favorite", {
    method: "post", 
    headers: {'Content-type': 'application/json'}, 
    data: data,
  }).then(res=>resolve(res)).catch(e=>reject(e))
})