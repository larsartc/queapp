import axios from "./index";

export async function getMessages() {
    return await axios("chat", {
        method: "get",
        headers: {
            'Content-type': 'application/json'
        },
    })
}

export async function createMessage(to = null, message) {
    const url = !to ? "chat" : "chat/" + to
    return await axios(url, {
        method: "post",
        headers: {
            'Content-type': 'application/json'
        },
        data: message
    })
}
