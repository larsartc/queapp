import React, { Component } from 'react'
import { TouchableOpacity, Text, View, Image, Platform } from 'react-native'

import styled from "styled-components"
import { ifIphoneX } from 'react-native-iphone-x-helper'

class HeaderBar extends Component {
  static defaultProps = {
    onPressLeftIcon: () => { },
    title: "QUE",
    onPressRightIcon: () => { },
    rightIcon: null,
    leftIcon: require('que/assets/images/menu_close_home_costumer_icn.png'),
    lightMode: false,
  }

  render() {
    return (
      <View style={{
        ...ifIphoneX({
          paddingTop: 30
        }, {
          ...Platform.select({
            ios: {
              paddingTop: 10
            },
            android: {
              paddingTop: 26
            }
          })
        })
      }}>
        <HeaderTitleBarContainer>
          <HeaderTitleBar>

            <TouchableOpacity onPress={() => this.props.onPressLeftIcon()} style={{ padding: 12 }}>
              <Image source={this.props.leftIcon} style={{ height: 16, width: 16 }} />
            </TouchableOpacity>

            {!this.props.hideTitle &&
              <Text style={this.props.lightMode ? { color: "white" } : null}> {this.props.title} </Text>
            }
            <TouchableOpacity onPress={() => this.props.onPressRightIcon()} style={{ padding: 12 }}>
              <Image source={this.props.rightIcon} style={{ height: 18, width: 18 }} />
            </TouchableOpacity>

          </HeaderTitleBar>
        </HeaderTitleBarContainer>
      </View>
    )
  }
}

export default HeaderBar

const HeaderTitleBarContainer = styled.View`
  width: 100%;
  height: 60;
  ${'' /* background-color: ${colors.white}; */}
  z-index: 10;
`
const HeaderTitleBar = styled.View`
  height: 60;
  width: 90%; 
  flex-direction: row;
  margin: 0 auto;
  justify-content: space-between;
  align-items: center;
  z-index: 99;
  paddingVertical: 4px;
  paddingHorizontal: 4px;
  ${'' /* background-color: ${colors.white}; */}
`