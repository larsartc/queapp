import React from 'react';
import { RadioContainer, RadioChecked } from './styled';

const CustomRadio = (props) => {
    return (
        <RadioContainer onPress={props.onPress}>
            {props.checked &&
                <RadioChecked />
            }
        </RadioContainer>
    )
}

export default CustomRadio;