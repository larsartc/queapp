import styled from 'styled-components';
import {colors} from 'que/components/base';


export const RadioContainer = styled.TouchableOpacity`
    height: 19;
    width: 19;
    border-radius: 9.5;
    border-width: 1;
    border-color: ${colors.white};
`

export const RadioChecked = styled.View`
    height: 12;
    width: 12;
    border-radius: 6;
    background-color: ${colors.white};
    margin-top: 2.5;
    margin-left: 2.5;
`