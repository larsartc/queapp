import React from 'react'
import {PixelRatio} from "react-native"
import styled from "styled-components/native"
import {colors} from 'que/components/base';

export default ({data, click, active}) => {
  return (
      <TouchDayRound onPress={()=>click()} active={active} >
          <TextSmall active={active}>{data.day}</TextSmall>
          <TextSmall active={active}>{data.day2.toUpperCase()}</TextSmall>
      </TouchDayRound>
  )
}

const TextSmall = styled.Text`
    text-align: center;
    font-size: ${PixelRatio.get()>1.5?'12px':'11px'};
    color: ${props=>props.active?colors.white:colors.gray};
`

const TouchDayRound = styled.TouchableOpacity`
    width: ${PixelRatio.get()>1.5 ? 60 : 50};
    height: ${PixelRatio.get()>1.5 ? 60 : 50};
    border-radius: 30;
    border-width: 1;
    margin-right: 5;
    margin-left: 5;
    justify-content: center;
    border-color: ${props=>props.active?colors.primary:colors.gray};
    background-color: ${props=>props.active?colors.primary:colors.white};
`