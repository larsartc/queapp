import React from "react" 
import { View, Image, PixelRatio } from "react-native"
import { Thumbnail } from "native-base"

export default (prop) => {
  return (
    <View style={{ paddingLeft: 8 }}>
      <Image
        source={prop.icon}
        style={{ 
          width: PixelRatio.get() > 1.5 ? 80 : 60,
          height: PixelRatio.get() > 1.5 ? 80 : 60
        }}
        resizeMode={"contain"}
      />
      <Thumbnail
        source={{ uri: prop.image }}
        style={{
          borderRadius: 20,
          marginTop: 4,
          borderWidth: 1,
          borderColor: "white",
          marginLeft: PixelRatio.get() > 1.5 ? 20 : 18,
          position: "absolute",
          width: PixelRatio.get() > 1.5 ? 38 : 28,
          height: PixelRatio.get() > 1.5 ? 38 : 28,
        }}
      />
    </View>
  )
}