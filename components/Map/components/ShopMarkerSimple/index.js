import React from "react"
import { View, Image } from "react-native"
import {withTheme} from 'que/utils/themeProvider'

const ShopMarkerSimple = (prop) => {

  //let icon = require("que/assets/images/circle_map_indicator_icn.png") 

  return (
    <View style={{ paddingLeft: 4 }}>
      <View style={{borderRadius:8, width:16, height:16, backgroundColor:prop.theme.primary}} />
      {/* <Image
        source={icon}
        style={{ width: 60, height: 60 }}
        resizeMode={"contain"}
      /> */}
    </View>
  )
}
export default withTheme(ShopMarkerSimple)