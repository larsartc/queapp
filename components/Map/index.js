import React, { Component } from "react";

//Redux
import { connect } from "react-redux"

import { View, StyleSheet } from "react-native"
import MapView from "react-native-maps"
import { Marker } from "react-native-maps"
import { colors } from "que/components/base"
import ShopMarker from "./components/ShopMarkers"
import ShopMarkerSimple from "./components/ShopMarkerSimple"
import _ from "lodash";

const styles = StyleSheet.create({
  container: {
    justifyContent: "flex-end",
    alignItems: "center",
    flex: 1,
  },
  map: {
    width: "100%",
    flex: 1,
  }
});

class Map extends Component {
  state = {
    markers: [
      {
        latitude: this.props.position ? this.props.position.latitude : 0,
        longitude: this.props.position ? this.props.position.longitude : 0,
        title: "You"
      }
    ]
  }

  changeArea = pos => {
    console.log("mapChange", pos);
  }

  filterPinActive = val => {
    const { active } = this.props;

    if (active) {
      return val._id != active._id;
    }

    return true;
  }

  render() {
    const { isBarber, active } = this.props

    let { latitude, longitude } = this.props.position;

    if (!!active) {
      latitude = !!active.latitude && parseFloat(active.latitude)
      longitude = !!active.longitude && parseFloat(active.longitude)
    }

    return (
      <View accessible={true} style={styles.container}>
        <MapView
          onRegionChangeComplete={(region) => !!this.props.onMapMove && this.props.onMapMove(region)}
          region={{
            latitude,
            longitude,
            latitudeDelta: 0.01,
            longitudeDelta: 0.01
          }}
          style={styles.map}
          initialRegion={{
            latitude,
            longitude,
            latitudeDelta: 0.01,
            longitudeDelta: 0.01
          }}
          showsUserLocation={true}
        >
          {this.props.active &&
            <Marker
              key={0}
              onPress={e => console.log(e.nativeEvent)}
              coordinate={{
                latitude: latitude,
                longitude: longitude
              }}
            >
              <ShopMarker
                icon={isBarber == "BARBER" ? require("que/assets/images/barbers/pin_map.png") : require("que/assets/images/customer/pin_map.png")}
                image={
                  this.props.active && this.props.active.photos && this.props.active.photos.length > 0
                    ? this.props.active.photos[0]
                    : null
                }
              />
            </Marker>}

          {this.props.shops.map((val, i) => {

            let lat = !!val.latitude ? parseFloat(val.latitude) : 0
            let lng = !!val.longitude ? parseFloat(val.longitude) : 0

            return (
              <Marker
                key={i}
                onPress={e => this.props.snapTo(i)} // e.nativeEvent
                coordinate={{
                  latitude: lat,
                  longitude: lng
                }}
              >
                <ShopMarkerSimple />
              </Marker>
            );
          })}
        </MapView>
      </View>
    )
  }
}

const mapStateToProps = store => ({
  shops: store.shopReducer.list_shop,
  loading: store.shopReducer.loading,
  isBarber: store.userReducer.interface,
})

export default connect(mapStateToProps, null)(Map)