import React, {useState} from 'react'
import {Text, View, TouchableOpacity, Image} from 'react-native'
import style from './style.js'

export default (props) => {
    const [active, setActive] = useState(!!props.active);
    return (
        <>
            <TouchableOpacity onPress={() => {setActive(!active); props.click(props.data)} }>
                <View style={style.divGeneralLine}>
                    <View style={{...style.divInfoCheck}}>
                        {active == true ? (
                            <Image source={require('que/assets/images/shopdetails/checkbox_services_checked_icn.png')} style={style.divInfoCheckImage}/>                              
                        ) : (
                            <Image source={require('que/assets/images/shopdetails/checkbox_services_empty_icn.png')} style={style.divInfoCheckImage}/>
                        )}
                    </View>
                    <View style={active==true?style.lineRadiusActive:style.lineRadius}>
                        <View style={{...style.divInfoView, flex: 2}}>
                            <Text style={active==true?style.colorWhite:style.colorPurple}>{props.data.name}</Text>
                        </View>
                        <View style={style.divInfoView}>
                            <Text style={active==true?style.colorWhiteRight:style.colorPurpleRight}>{props.data.price}</Text>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        </>
    )
}