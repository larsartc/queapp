import {StyleSheet} from 'react-native';
import {colors} from 'que/components/base';

export default StyleSheet.create({
    divGeneralLine:{
        display: 'flex', 
        flexDirection: 'row', 
        marginTop: 2, 
        marginBottom: 2,
        justifyContent: 'center', 
        alignItems: 'center'
    },
    divInfoTitle: {
       display: 'flex', 
       justifyContent: 'center', 
       textAlign: 'center', 
       color: colors.primary,
       fontSize: 15
    },
    divIndoData:{
       color: colors.primary, 
       display: 'flex',
       textAlign: 'right', 
    },
    divInfoView:{
        flex: 1
    },
    divInfoCheck:{
        width: 22
    }, 
    divInfoCheckImage:{
        width: 20,
        height: 20,
    },
    lineRadius:{
        borderWidth: 1,
        borderColor: colors.primary,
        borderRadius: 100,
        paddingHorizontal: 10,
        display: 'flex',
        flex: 1,
        flexDirection: 'row'
    },
    lineRadiusActive:{
        backgroundColor: colors.primary,
        color: colors.white,
        borderWidth: 1,
        borderColor: colors.primary,
        borderRadius: 100,
        paddingHorizontal: 10,
        display: 'flex',
        flex: 1,
        flexDirection: 'row'
    },
    colorPurple:{
        color: colors.primary
    },
    colorWhite:{
        color: colors.white
    },
    colorWhiteRight:{
        color: colors.white,
        textAlign: 'right'
    },
    colorPurpleRight:{
        color: colors.primary,
        textAlign: 'right'
    },
})