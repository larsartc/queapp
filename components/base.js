import styled from "styled-components/native"
import { ifIphoneX } from 'react-native-iphone-x-helper'

// redux 
import {UI_TYPE} from "../store/user"
import store from '../store'

export const colors = {
  primary: "#3D2E52", 
  secondary: "#201233", 
  third: "#5e4086", 
  white: "#FFFFFF", 
  black: "#151515",
  green: "#00A52C",
  red: "#C70000",
  darkGray: '#585858',
  gray: "#8F8F8F",
  lightGray: "#F2F0F6",
  lightBlue: "#6988D5",
  ratingsColor: "#B1ABB9",
}

export const Font = {
  small: '10px',
  medium: '14px',
  large: '22px',
}

export const Header = styled.View`
  margin-top: 20;
  margin-bottom: 5;
`

export const HeaderTitleBar = styled.View`
  display: flex;
  height: 20;
  flex-direction: row;
  justify-content: space-between;
  margin-left: 10%;
  margin-right: 10%;
`

export const BasePop = styled.View`
    height: 420;
    width: 100%;
    background-color: ${colors.white};
    position: absolute;
    bottom: 0;
    align-items: center;
    z-index: 1002;
    ${ifIphoneX({paddingBottom: 20},{paddingBottom:0})}
`

export const FavoriteContainer = styled.TouchableOpacity`
    margin-right: 5%;
`

export const FavoriteIcon = styled.Image`
    height: 20;
    width: 20;
`

export const TitlePop = styled.Text`
    text-align: center;
    color: ${colors.primary};
    font-size: 14px;
    margin-top: 8px;
    margin-bottom: 8px;
`

export const ShopContainer = styled.View`
  flex: 1;
  position: relative;
  background-color: ${props=>props.background?props.background:colors.white};
`

export const DivImages = styled.View`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: row;
  margin-top: 10;
  margin-bottom: 10;
`

export const ImageUser = styled.Image`
    height: ${props=>props.size?props.size:80};
    width: ${props=>props.size?props.size:80};
    border-width: 2;
    border-color: #FFF;
    margin-left: 10px; 
    margin-right: 10px;
    border-radius: 50;
`