import React, {Component} from 'react';
import Voice from 'react-native-voice';

import Styled from './styled';
import { TouchableOpacity, Text, Platform } from 'react-native'

class SearchBar extends Component {

    constructor(props) {
        super(props);
        this.state = {
            recognized: '',
            started: '',
            results: [],
        };

        Voice.onSpeechStart = this.onSpeechStart.bind(this);
        Voice.onSpeechRecognized = this.onSpeechRecognized.bind(this);
        Voice.onSpeechResults = this.onSpeechResults.bind(this);
    }

    componentWillUnmount() {
        Voice.destroy().then(Voice.removeAllListeners);
    }

    onSpeechStart(e) {
        this.setState({
            started: '√',
        });
    };

    onSpeechRecognized(e) {
        this.setState({
            recognized: '√',
        });
    };

    onSpeechResults(e) {
        this.setState({
            results: e.value[0],
        }, () => this.props.onChangeText(e.value[0]));
    }

    async _startRecognition(e) {
        this.setState({
            recognized: '',
            started: '',
            results: [],
        });
        try {
            await Voice.start('en-US');
        } catch (e) {
            console.error(e);
        }
    }

    render() {
        return (
            <Styled.HeaderSearchBarContainer>
                <Styled.HeaderSearchBar style={{
                    ...Platform.select({
                        ios: {
                            shadowColor: "#000",
                            shadowOffset: {
                                width: 0,
                                height: 6,
                            },
                            shadowOpacity: 0.15,
                            shadowRadius: 6.84,
                        },
                        android: {
                            elevation: 15,
                        }
                    })
                }}>
                    <Styled.SearchBarInput
                        value={this.props.value}
                        onChangeText={value => this.props.onChangeText(value)}
                        placeholder={this.props.placeholder}
                        placeholderTextColor={'#999999'}
                    />
                    <Styled.SearchBarIconContainer>
                        <TouchableOpacity onPress={this._startRecognition.bind(this)} style={{paddingVertical:8}}>
                            <Styled.HeaderIcon
                                source={require('que/assets/images/voice_search_home_costumer_icn.png')}
                                style={{height: 15, resizeMode: 'contain'}}/>
                        </TouchableOpacity>
                        {
                            this.props.openFilter && (
                                <>
                                <Styled.SearchSeparator />

                                <TouchableOpacity onPress={() => this.props.openFilter()} style={{ paddingVertical:8}}>
                                    <Styled.HeaderIcon source={require('que/assets/images/filter_home_costumer_icn.png')}
                                                        style={{height: 15, resizeMode: 'contain', marginRight:5}}/>
                                </TouchableOpacity>
                                </>
                            )
                        }

                    </Styled.SearchBarIconContainer>
                </Styled.HeaderSearchBar>
            </Styled.HeaderSearchBarContainer>
        )
    }
}

export default SearchBar;