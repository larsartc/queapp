import styled from "styled-components";
import {colors} from 'que/components/base';

export default styledContainer = {
    /*HeaderTitleBarContainer: styled.View`
       width: 100%;
       height: 60;
       background-color: ${colors.white};
       z-index: 10;
       margin-bottom: 10;
    `,
    HeaderTitleBar: styled.View`
        height: 60;
        width: 90%; 
        flex-direction: row;
        margin: 0 auto;
        justify-content: space-between;
        align-items: center;
        z-index: 99;
        padding: 20px;
        background-color: ${colors.white};
    `,*/
    HeaderIcon: styled.Image`
        height: 15;
        width: 15;
    `,
    HeaderSearchBarContainer: styled.View`
        height: 40;
        width: 90%; 
        margin-top: 10;
        margin-bottom: 10;
        align-self: center;
        z-index: 3; 
        position: absolute;
    `,
    HeaderSearchBar: styled.View`
        ${Platform.OS==="ios"? 'height: 100%;' : null}
        /*height: 100%;
        width: 100%;*/
        background-color: white;
        display: flex;
        flex-direction: row;
        align-items: center;
    `,
    SearchBarInput: styled.TextInput`
        margin-left: 16;
        flex: 4;
        font-size: 16;
    `,
    SearchBarIconContainer: styled.View`
        margin-right: 16;
        flex: 1;
        display: flex;
        flex-direction: row;
        align-items: center;
        justify-content: flex-end;
    `,
    SearchSeparator: styled.Text`
        background-color: #cccccc;
        width:1;
        margin-left:14;
        margin-right:14;
    `
}