import React from 'react';
import { TouchableOpacity, View, ImageBackground, Image, StyleSheet } from 'react-native'
import LinearGradient from "react-native-linear-gradient"
import { colors } from 'que/components/base'
import { withTheme } from '../../utils/themeProvider'


class TabButtons extends React.Component {
    
    getIconsButtonComponent = (type, active) => {
        if (type == "shop") {
            switch(active){
                case 'price':
                    return [
                        { image: <ImageBackground source={active == 'info' ? require('que/assets/images/shopdetails/shop_info_filled_icn.png') : require('que/assets/images/shopdetails/shop_info_empty_icn.png')} style={style.icon} />, name: 'info' },
                        { image: <ImageBackground source={active == 'price' ? require('que/assets/images/shopdetails/shop_price_filled_icn.png') : require('que/assets/images/shopdetails/shop_price_semi_icn.png')} style={style.icon} />, name: 'price' },
                        { image: <ImageBackground source={active == 'rating' ? require('que/assets/images/shopdetails/shop_raiting_filled_icn.png') : require('que/assets/images/shopdetails/shop_raiting_empty_icn.png')} style={style.icon} />, name: 'rating' },
                    ]
                    
                case 'info':
                    return [
                        { image: <ImageBackground source={active == 'price' ? require('que/assets/images/shopdetails/shop_price_filled_icn.png') : require('que/assets/images/shopdetails/shop_price_semi_icn.png')} style={style.icon} />, name: 'price' },
                        { image: <ImageBackground source={active == 'info' ? require('que/assets/images/shopdetails/shop_info_filled_icn.png') : require('que/assets/images/shopdetails/shop_info_empty_icn.png')} style={style.icon} />, name: 'info' },
                        { image: <ImageBackground source={active == 'rating' ? require('que/assets/images/shopdetails/shop_raiting_filled_icn.png') : require('que/assets/images/shopdetails/shop_raiting_empty_icn.png')} style={style.icon} />, name: 'rating' },
                    ]
                    
                case 'rating':
                    return [
                        { image: <ImageBackground source={active == 'price' ? require('que/assets/images/shopdetails/shop_price_filled_icn.png') : require('que/assets/images/shopdetails/shop_price_semi_icn.png')} style={style.icon} />, name: 'price' },
                        { image: <ImageBackground source={active == 'rating' ? require('que/assets/images/shopdetails/shop_raiting_filled_icn.png') : require('que/assets/images/shopdetails/shop_raiting_empty_icn.png')} style={style.icon} />, name: 'rating' },
                        { image: <ImageBackground source={active == 'info' ? require('que/assets/images/shopdetails/shop_info_filled_icn.png') : require('que/assets/images/shopdetails/shop_info_empty_icn.png')} style={style.icon} />, name: 'info' },
                    ]
                    
                default: 
                    break
            }
        }

        if (type == "barber") return [
            { image: <ImageBackground source={active == 'cuts' ? require('que/assets/images/shopdetails/scissors_selected_icn.png') : require('que/assets/images/shopdetails/scissors_none_selected_icn.png')} style={style.icon} />, name: 'cuts' },
            { image: <ImageBackground source={active == 'info' ? require('que/assets/images/shopdetails/shop_info_filled_icn.png') : require('que/assets/images/shopdetails/shop_info_empty_icn.png')} style={style.icon} />, name: "info" },
            { image: <ImageBackground source={active == 'rating' ? require('que/assets/images/shopdetails/shop_raiting_filled_icn.png') : require('que/assets/images/shopdetails/shop_raiting_empty_icn.png')} style={style.icon} />, name: 'rating' }
        ]
    }

  render(){
    const {theme} = this.props 

    return (
      <LinearGradient colors={[theme.secondary_opacity, theme.secondary]} style={style.gradientBox}>
        <View style={style.iconsBottomBackgroundContainer}>
          {
            this.getIconsButtonComponent(this.props.type, this.props.active).map((item,i) => {
              return (
                <TouchableOpacity 
                  style={style.iconContainer} 
                  onPress={() => this.props.changeView(i)}
                >
                {item.image}
                </TouchableOpacity>
              )
            })
          }
        </View>
      </LinearGradient>
    )
  }
            
}

export default withTheme(TabButtons)



const style = StyleSheet.create({
    iconsBottomBackgroundContainer: {
        flexDirection: 'row',
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'space-around',
        width:'100%',
    },
    iconContainer: {
        alignSelf: 'center',
        marginBottom: 5
    },
    icon: {
        height: 20,
        width: 20
    }, 
    gradientBox: {
        display: 'flex',
        height: 200,
        position: 'absolute',
        bottom: 0,
        width: '100%',
        justifyContent:'flex-end', 
        paddingBottom: 16,
    }
})