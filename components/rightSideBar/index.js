import React from "react"
import {useAnimation} from 'que/hooks'
import {Animated, View, TouchableOpacity} from 'react-native'

export default RightSideBar = (props) => {
  const animation = useAnimation({doAnimation:true, duration: 300})
  return (
    <>
    <TouchableOpacity onPress={()=>props.close()} activeOpacity={1} style={{
      position: "absolute",
      width: "100%",
      height: "100%",
      backgroundColor: "rgba(0,0,0,0.7)",
      zIndex: 999}} />
    <Animated.View style={
      [{
        flex:1,
        height: "100%",
        width: "90%",
        position: "absolute",
        alignItems: "center",
        zIndex: 1002,
        backgroundColor: props.backgroundColor || "white", 
      }, props.left ? {
        left: animation.interpolate({
          inputRange: [0,1],
          outputRange: [-400,0]
        })
      }: {
        right: animation.interpolate({
          inputRange: [0,1],
          outputRange: [-400,0]
        })
      }]
      }>{props.children}</Animated.View>
    </>
  )
}