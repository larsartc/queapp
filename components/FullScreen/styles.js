import styled from 'styled-components/native';

export const Container = styled.Modal`

`;

export const Content = styled.View`
    background: #000;
    position: relative;
    flex: 1;
    display: flex;
    width: 100%;
    height: 100%;
    
`;

export const Close = styled.TouchableHighlight`
    position: absolute;
    z-index: 5;
    top: 20px;
    right: 20px;
`;
