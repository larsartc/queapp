import React, { useLayoutEffect, useState } from 'react';
import { View, Text, Image, TouchableHighlight } from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import * as S from './styles';

export default function FullScreen(props) {
    const handleClose = () => {
        const { setVisible } = props;
        setVisible({ photo: ''}, false);
    }

    const { visible, photo } = props;
    return (
    <S.Container
    animationType="slide"
    transparent={true}
    visible={visible}
    >
    <S.Content>
        <S.Close onPress={handleClose}>
          <Icon name="close" color="#FFF" size={24}/> 
        </S.Close>
        <Image
          style={{flex: 1, resizeMode: 'contain'}}
          source={{uri: photo}}
        />
    </S.Content>
  </S.Container>)
  }
