import React from 'react' 

import { View } from "native-base"
import { Image, TouchableOpacity} from "react-native"
import {withTheme} from '../../utils/themeProvider'

const TypePicker = ({ key, press, image, selectStyle, theme}) => {
  return (
      <View
          style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center"
          }}
      >
          <TouchableOpacity onPress={() => press()}
              style={[selectStyle, {backgroundColor: theme.primary}]} >
              <Image source={image}
                  style={{
                      marginLeft: 10,
                      marginRight: 10,
                      width: 30,
                      height: 30
                  }}
              />
          </TouchableOpacity>
      </View>
  )
}

export default withTheme(TypePicker)