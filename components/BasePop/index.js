import React from "react"
import {useAnimation} from 'que/hooks'
import {Animated, View, Easing} from 'react-native'

export default BasePop = (props) => {
  const animation = useAnimation({doAnimation:true, duration: 200})

  return <Animated.View style={{
        height: props.height ? props.height : 420,
        width: "100%",
        position: "absolute",
        alignItems: "center",
        zIndex: 1002,
        paddingBottom: 20,
        backgroundColor: "white", 
        bottom: animation.interpolate({
          inputRange: [0,1],
          outputRange: [-400,0]
        })
      }}>{props.children}</Animated.View>
}