import React, { useState, Component } from "react";
import Card from "que/components/Card";
import { View, Image, Dimensions, PixelRatio, Platform } from "react-native";
import Map from "que/components/Map";
import LinearGradient from "react-native-linear-gradient";
import Carousel from "react-native-snap-carousel";
import { useAnimation } from "que/hooks";
import { Animated } from "react-native";
import { Fab, Container } from "native-base";
import _ from "lodash";

export default ({
  position,
  pressAction,
  shops,
  navigation,
  favorite,
  unFavorite,
  updateListShop,
  showMask
}) => {
  const { width } = Dimensions.get("window");
  const [active, setItemActive] = useState(null);
  const [mapPosition, setMapPosition] = useState(null);

  function updateShops(entity_id, bool) {
    const shops_updated = shops.map(shop => {
      if (shop._id == entity_id) {
        shop.favorite = bool;
      }
      return shop;
    });
    updateListShop(shops_updated);
  }

  const renderItemMap = (val, i) => {
    return (
      <Card
        showMask={showMask}
        title={val.item.name}
        imageBg={val.item.photos ? { uri: val.item.photos[0] } : null}
        horizontal={true}
        stars={val.item.mid_rate}
        price={val.item.mid_rate}
        distance={val.item.distance}
        bottomHeight={70}
        onPress={() => pressAction(val.item._id, val.item)}
        key={i}
        carousel={true}
        favorite={val.item.favorite}
        unfavoriteShop={() => {
          unFavorite({ type: 3, entity_id: val.item._id });
          updateShops(val.item._id, false);
        }}
        saveShop={() => {
          favorite({ type: 3, entity_id: val.item._id });
          updateShops(val.item._id, true);
        }}
      />
    );
  };

  const snapTo = e => {
    this._carousel.snapToItem(e);
    setMapPosition(null);
  };

  const selectItemActive = item => {
    console.log("select_item", item);

    setItemActive(item);
  };

  return (
    <Container style={{ flex: 1 }}>
      {(showMask || Platform.OS === "ios") && (
        <LinearGradient
          colors={["#ffffff", "#fff0"]}
          style={{
            position: "relative",
            height: 150,
            marginBottom: -150,
            zIndex: 2
          }}
        />
      )}

      <Map
        onMapMove={val =>
          !_.isEqual(mapPosition, val) && active != null && setMapPosition(val)
        }
        position={mapPosition != null ? mapPosition : position}
        active={active != null && mapPosition == null ? shops[active] : null}
        snapTo={e => snapTo(e)}
      />

      {shops && shops.length > 0 && (
        <Fab
          active={true}
          direction="up"
          containerStyle={{}}
          style={{
            marginBottom: PixelRatio.get() > 1.5 ? 240 : 210,
            backgroundColor: "#FFF"
          }}
          position="bottomRight"
          onPress={() => {
            selectItemActive(null);
            setMapPosition(null);
          }}
        >
          <Image
            source={require("que/assets/images/center_map_view_home_costumer_icn.png")}
            style={{ width: 18, height: 18, transform: [{ rotate: "45deg" }] }}
          />
        </Fab>
      )}

      <View
        style={{
          position: "absolute",
          bottom: 30,
          zIndex: 999,
          //
          ...Platform.select({
            ios: {
              shadowColor: "#000",
              shadowOffset: {
                width: 0,
                height: 6
              },
              shadowOpacity: 0.65,
              shadowRadius: 5.84
            },
            android: {
              elevation: 1
            }
          })
        }}
      >
        {(showMask || Platform.OS === "ios") && shops.length > 0 && (
          <>
            <LinearGradient
              colors={["#fff0", "#FFFFFF"]}
              style={{
                position: "absolute",
                left: 0,
                right: 0,
                bottom: -40,
                height: 220
              }}
            />
            <Carousel
              ref={c => (this._carousel = c)}
              data={shops}
              renderItem={renderItemMap}
              sliderWidth={Dimensions.get("window").width}
              itemWidth={width / 1.7}
              onSnapToItem={e => selectItemActive(e)}
              inactiveSlideScale={0.9}
              activeSlideAlignment={"center"}
              inactiveSlideOpacity={0.9}
              slideStyle={{
                ...Platform.select({
                  ios: {
                    marginBottom: 0
                  },
                  android: {
                    marginBottom: 0 // HERE NEED refactor for android shadow
                  }
                })
              }}
              contentContainerCustomStyle={{}}
              containerCustomStyle={{}}
            />
          </>
        )}
      </View>
    </Container>
  );
};
