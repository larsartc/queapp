import React from "react";
import Styled from "./styled";
import Card from "que/components/Card";
import { View, Platform, PixelRatio, ScrollView, Animated } from "react-native";
import LinearGradient from "react-native-linear-gradient";

export default ({
  shops,
  pressAction,
  favorite,
  unFavorite,
  updateListShop,
  showMask
}) => {
  function updateShops(entity_id, bool) {
    const shops_updated = shops.map(shop => {
      if (shop._id == entity_id) {
        shop.favorite = bool;
      }
      return shop;
    });

    updateListShop(shops_updated);
  }

  return (
    <Styled.CardsContainer>
      <View
        style={{ height: Platform.OS === "ios" ? 16 : 30, display: "flex" }}
      />

      {(showMask || Platform.OS === "ios") && (
        <LinearGradient
          colors={["rgba(255, 255, 255, .5)", "rgba(255, 255, 255, 0)"]}
          style={{
            position: "absolute",
            height: 30,
            top: 30,
            left: 0,
            right: 0
          }}
        />
      )}

      <ScrollView>
        <View
          style={{
            flexDirection: "row",
            flexWrap: "wrap",
            justifyContent: "center"
          }}
        >
          {shops.map((i, k) => (
            <Card
              cardHeaderHeight={PixelRatio.get() > 1.5 ? 140 : 120}
              bottomHeight={80}
              title={i.name}
              imageBg={{ uri: i.photos[0] }}
              stars={i.mid_rate}
              price={i.mid_rate}
              distance={i.distance}
              onPress={() => pressAction(i._id, i)}
              key={k}
              favorite={i.favorite}
              unfavoriteShop={() => {
                unFavorite({ type: 3, entity_id: i._id });
                updateShops(i._id, false);
              }}
              saveShop={() => {
                favorite({ type: 3, entity_id: i._id });
                updateShops(i._id, true);
              }}
            />
          ))}
        </View>
      </ScrollView>
      {/* </Content> */}
    </Styled.CardsContainer>
  );
};
