import styled from 'styled-components';
import { Dimensions } from 'react-native';

export default styledContainer = {
    CardsContainer: styled.View`
        flex: 1;
        margin-top: 50;
    `,
}