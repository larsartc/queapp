import React, { useState } from 'react'
import { View, ScrollView, Image, Text, FlatList, TouchableOpacity } from 'react-native'
import LinePrice from "../LinePrice"
import style from './style'
import { colors, TitlePop } from 'que/components/base';

export default class ServicesConfigAptt extends React.Component {
    selectItem = data => {
        let selected = this.props.selected;

        let newValues = []

        if (selected.find((item) => item._id == data._id)) {
            newValues = selected.filter((item) => item._id != data._id);
        } else {
            newValues = [...selected, { ...data }];
        }

        return this.props.click(newValues)
    }

    getServices = (services) => {

        let servicesObj = {}

        let category = null

        services.forEach(val => {
            if (category == null || category != val.category) {
                category = val.category
            }
            if (!(servicesObj[category] instanceof Array)) {
                servicesObj[category] = []
            }
            servicesObj[category].push({ ...val })
        })

        return { ...servicesObj }
    }

    render() {

        const value = this.props.selected.reduce((initial, value) => {
            if (value.price != undefined) {
                initial = initial + value.price
            }
            return initial;
        }, 0)

        const list = this.getServices(this.props.list);

        return (
            <>
                <TitlePop> {this.props.title} </TitlePop>
                <ScrollView>
                    <>
                        <View style={{ marginBottom: 6, marginTop: 12, borderBottomWidth: 1, borderColor: colors.primary, paddingBottom: 2 }}>
                            <View style={{ paddingHorizontal: 50, flexDirection: 'row', justifyContent: 'center', alignItens: 'center' }}>
                                <View style={style.divTitleValue}>
                                    <Text style={style.divTitleTitle}>ESTIMATED COST</Text>
                                </View>
                                <View style={style.divTitleValueRight}>
                                    <Text style={style.divTitleValue}>$ {value}</Text>
                                </View>
                            </View>
                        </View>
                        {Object.keys(list).map(val =>
                            <View style={{ flexDirection: 'column', justifyContent: 'center', alignItens: 'center', marginBottom: 6, marginTop: 12, paddingHorizontal: 50 }}>
                                <View style={{ borderBottomWidth: 1, borderColor: colors.primary, paddingBottom: 2 }}>
                                    <Text style={style.divInfoTitle}>{val}</Text>
                                </View>
                                <View style={{ marginTop: 5 }}>
                                    {list[val].map((info, k) => {
                                        return (
                                            <LinePrice data={info} active={this.props.selected.find((item) => item._id == info._id)} click={(e) => this.selectItem(info)} />

                                        )
                                    }
                                    )}
                                </View>
                            </View>
                        )}
                    </>
                </ScrollView>
            </>
        )
    }
}