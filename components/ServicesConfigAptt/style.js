import {StyleSheet} from 'react-native';
import {colors} from 'que/components/base';

export default StyleSheet.create({
    divInfoTitle: {
       display: 'flex', 
       justifyContent: 'center', 
       textAlign: 'center', 
       color: colors.primary,
       fontSize: 15
    },
    divIndoData:{
       color: colors.primary, 
       display: 'flex',
       textAlign: 'right', 
    },
    divInfoView:{
        flex: 1
    },
    divInfoCheck:{
        width: 22
    }, 
    divInfoCheckImage:{
        width: 20,
        height: 20,
    },
    divTitleTitle:{
        fontSize: 18, 
        flex: 1
    },
    divTitleValue:{
        fontSize: 18, 
        flex: 1
    },
    divTitleValueRight:{
        textAlign: 'right'
    },
    divTextLeft:{
        textAlign: 'left'
    },
    divTextRight:{
        textAlign: 'right'
    },
})