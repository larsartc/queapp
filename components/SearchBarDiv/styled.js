import styled from 'styled-components';
import { colors } from 'que/components/base';

export default styledContainer = {
    ShopContainer: styled.View`
        height: 100%;
        width: 100%;
        position: relative
    `,
    ViewCuts: styled.View`
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
    `,
    SaveContainer: styled.View`
        height: 140;
        width: 100%;
        background-color: ${colors.white};
        position: absolute;
        bottom: 0;
        align-items: center; 
    `,
    Header: styled.View`
        margin-top: 10;
        margin-bottom: 10;
        height: 10%;
    `,
    HeaderTitleBar: styled.View`
        display: flex;
        height: 20;
        flex-direction: row;
        justify-content: space-between;
        margin-left: 10%;
        margin-right: 10%;
    `,
    HeaderTitle: styled.Text`
        
    `,
    HeaderIconContainer: styled.TouchableOpacity`
    `,
    HeaderIcon: styled.Image`
        height: 15;
        width: 15;
    `,
    HeaderSearchBarContainer: styled.View`
        height: 40;
        width: 90%; 
        margin-top: 10;
        margin-bottom: 10;
        align-self: center;
        border-width: 1px;
        border-color: ${colors.lightGray};
    `,
    HeaderSearchBar: styled.View`
        height: 100%;
        width: 100%;
        background-color: ${colors.white};
        display: flex;
        flex-direction: row;
        align-items: center;
    `,
    SearchBarInput: styled.TextInput`
        margin-left: 10;
        flex: 2;
    `,
    SearchBarIconContainer: styled.View`
        margin-right: 10;
        flex: 1;
        display: flex;
        flex-direction: row;
        align-items: center;
        justify-content: space-between;
    `,
    SearchSeparator: styled.Text`
        color: ${colors.gray};
    `,
    CardsContainer: styled.View`
        flex: 1;
    `,
    CardsMargin: styled.FlatList`
        align-self: center;
    `,
    CardContainer: styled.View`
        margin-bottom: 5;
        margin-right: 5;
        margin-left: 5;
        margin-top: 5;
    `
}