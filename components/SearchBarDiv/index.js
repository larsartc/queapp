import React, {Component} from 'react';

import Styled from './styled';

//Colors
import {colors} from 'que/components/base'

class SearchBarDiv extends Component {
    render(){
        return(
            <Styled.HeaderSearchBarContainer>
                <Styled.HeaderSearchBar>
                    <Styled.SearchBarInput
                        placeholder={"Search for style..."}
                        placeholderTextColor={colors.gray}
                    />
                    <Styled.SearchBarIconContainer>
                    <Styled.HeaderIcon
                        source={require("que/assets/images/voice_search_home_costumer_icn.png")}
                        style={{ height: 15, resizeMode: "contain" }}
                    />
                    <Styled.SearchSeparator>|</Styled.SearchSeparator>
                    <Styled.HeaderIcon
                        source={require("que/assets/images/filter_home_costumer_icn.png")}
                        style={{ height: 15, resizeMode: "contain" }}
                    />
                    </Styled.SearchBarIconContainer>
                </Styled.HeaderSearchBar>
            </Styled.HeaderSearchBarContainer>
        )
    }
}

export default SearchBarDiv;
