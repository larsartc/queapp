import React from 'react'
import styled from 'styled-components'
import { colors } from 'que/components/base'

export default ButtonChangeProfile = ({image, title, subtitle, action}) => {
  return (
    <ButtonAccount onPress={()=>action()}>
      <DivImageAbsolute>
        <ImageAbsolute source={image} />
      </DivImageAbsolute>
      <TitleAccount>{title}</TitleAccount>
      <SubAccount>{subtitle}</SubAccount>
    </ButtonAccount>
  )
}

const ButtonAccount = styled.TouchableOpacity`
    display: flex;
    flex: 1;
    text-align: center;
    align-items: center;
    border-top-width: 1;
    border-color: ${colors.white};
`
const TitleAccount = styled.Text`
    color: ${colors.white};
    font-size: 16;
    margin-top: 14px;
`
const SubAccount = styled.Text`
    color: rgba(255, 255, 255, .5);
    font-size: 14;
`
const DivImageAbsolute = styled.View`
    display: flex;
    justify-content: center;
    align-items: center;
    text-align: center;
    margin-top: -18px;
`
const ImageAbsolute = styled.Image`
    width: 38;
    height: 38;
`