import React, { Component } from "react";
import { Text } from "react-native";
import {
  TextInput,
  Line,
  InputContainer,
  ViewImageInput,
  Image,
  TouchableImageContainer
} from "./styled";
import { colors } from "que/components/base";

const CustomInput = props => {
  const [showPassword, setShowPassword] = React.useState(true);
  return (
    <InputContainer>
      <ViewImageInput>
        {props.image && (
          <Image
            source={props.image}
            resizeMode={"contain"}
            style={props.imageStyle}
          />
        )}
        <TextInput
          rightImage={props.rightImage}
          multiline={props.multiline || false}
          placeholder={props.placeholder || ""}
          textColor={props.textColor}
          placeholderTextColor={
            props.placeholderTextColor || "rgba(255, 255, 255, 0.6)"
          }
          onChangeText={text => props.onChange(text)}
          value={props.value}
          secureTextEntry={(props.password && showPassword) || false}
          autoCapitalize={props.autoCapitalize || false}
          autoCompleteType={"off"}
          fontSize={props.fontSize || null}
          style={{ flex: 1 }}
          caretHidden={props.caretHidden}
        />
        {props.eyeImage && (
          <TouchableImageContainer onPress={() => setShowPassword(!showPassword)}>
            {showPassword ? <Image source={require('que/assets/images/eye_slash_password.png')} /> : <Image source={require('que/assets/images/eye_password.png')} />}
          </TouchableImageContainer>
        )}
        {props.rightImage && (
          <TouchableImageContainer onPress={props.rightImageOnPress}>
            <Image source={props.rightImage} />
          </TouchableImageContainer>
        )}
      </ViewImageInput>
      <Line lineColor={props.lineColor} />
    </InputContainer>
  );
};

export default CustomInput;
