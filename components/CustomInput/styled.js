import styled from 'styled-components';
import {colors} from 'que/components/base'

export const InputContainer = styled.View`
    height: 40;
    width: 100%;
`

export const ViewImageInput = styled.View`
    width: 100%;
    height: 100%;
    display: flex;
    flex-direction: row;
    align-items: center;
    flex: 1;
    margin-left: 5;
`

export const Image = styled.Image`
    height: 25;
    width: 25;
    margin-right: 10;
`
export const TouchableImageContainer = styled.TouchableOpacity`
`


export const TextInput = styled.TextInput`
    width: ${props => props.rightImage ? '84%' : '96%'};
    font-size: ${props => props.fontSize ? props.fontSize : 13};
    color: ${(props) => props.textColor ? props.textColor : colors.white};
`
export const Line = styled.View`
    height: 1.0;
    opacity: 0.7;
    background-color: ${(props) => props.lineColor ? props.lineColor : colors.white};
`