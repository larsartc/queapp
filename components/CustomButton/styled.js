import styled from 'styled-components/native';
import {colors} from 'que/components/base';

export const CustomButtonContainer = styled.TouchableOpacity`
    ${'' /* height: 100%;
    width: 100%; */}
    border-width: 1;
    border-color: ${props => props.borderColor};
    border-radius: ${props => props.borderRadius};
    color: ${props => props.color || colors.white};
    padding: 10px;
    opacity: ${props => props.opacity ? props.opacity : 1};
`

export const ButtonText = styled.Text`
    font-size: ${props => props.fontSize || 12};
    color: ${props => props.color || colors.lightGray};
    text-align: center;
`