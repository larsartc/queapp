import React from 'react';
import { CustomButtonContainer, ButtonText } from './styled';

const CustomButton = (props) => {
    return (
        <CustomButtonContainer
            color={props.color || null}
            height={props.height || null}
            borderRadius={props.borderRadius || 20}
            opacity={props.opacity}
            borderColor={props.borderColor || "white"}
            onPress={() => props.onPress()}
        >
            <ButtonText color={props.color || null} fontSize={props.fontSize || null}>{props.text}</ButtonText>
        </CustomButtonContainer>
    )
}

export default CustomButton;