import { StyleSheet, PixelRatio } from "react-native";
import { colors } from 'que/components/base'

const style = StyleSheet.create({
    signInContainer: {
        flexDirection: 'row',
        padding: 10
    },
    baseHeader: {
        paddingLeft: 40,
        paddingRight: 40,
        paddingTop: 30,
    },
    baseMiddle: {
        paddingLeft: 40,
        paddingRight: 40,
        paddingTop: 30,
        flex: 1,
    },
    sidebarTitle: {
        color: colors.white, 
    },
    filterContainer: {
        marginTop: 15,
        marginBottom: 15,
    },
    filterFlex: {
        paddingTop: 10,
        flexDirection: "row",
        alignItems: "center", 
    },
    separator: {
        marginVertical: 16,
        width: '100%',
        borderBottomColor: 'white',
        borderBottomWidth: 0.5 // / PixelRatio.get(),
    },
    logo: {
        maxWidth: '100%',
        maxHeight: 80,
        marginTop: 20,
    },
    buttonItem: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: '10%',
        paddingRight: '10%',
        paddingBottom: 25,
    },
    buttonItemIcon: {
        width: 20,
        height: 15,
        marginRight: 16,
    },
    filterNameContainer: {
        color: colors.white,
        fontSize: 16,
        marginLeft: 10
    },
    filterTitle: {
        color: colors.white,
        fontSize: 14,
    },
    filterItem: {
        marginTop: 10
    },
    arrowIcon: {
        height: 30,
        width: 30
    },
    arrowIconContainer: {
        marginLeft: 20
    },
    flexRow1: {
        flexDirection: "row",
        flex: 1,
    },
});

export default style;