
import React from 'react';
import { StyleSheet, View } from 'react-native';
import {colors} from 'que/components/base'

class CustomMarker extends React.Component {
    render() {
        return (
            <View
                style={styles.customMarker}
            />
        );
    }
}

const styles = StyleSheet.create({
    customMarker: {
        height: 20,
        width: 20,
        backgroundColor: colors.white,
        borderRadius: 10
    }
});

export default CustomMarker;
