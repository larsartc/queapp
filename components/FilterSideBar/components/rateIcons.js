import React, { useState } from "react"
import { View, Text, TouchableOpacity } from 'react-native'
import styled from "styled-components/native"

export default (props) => {
  return (
    <View style={{ justifyContent: "center", alignContent: "center", alignItems: "center"}}>
      <View style={{ flexDirection: "row", marginVertical: 2 }}>
        {
          Array(5).fill(null).map((v, i) => {
            let imgsel = props.imgs[0]
            if (props.active != null && props.active >= i) {
              imgsel = props.imgs[1]
            }
            return (
              <TouchArea onPress={() => { props.press(i) }}>
                <Icons source={imgsel} />
              </TouchArea>
            )
          })
        }
      </View>
    </View>
  )
}

const Icons = styled.ImageBackground`
    height: 24;
    width: 24;
`

const TouchArea = styled.TouchableOpacity`
  padding: 8px;
`