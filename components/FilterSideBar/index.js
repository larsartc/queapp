import React from "react";
import { Text } from "native-base";
import { View, TouchableOpacity, Image, ImageBackground, ScrollView, Dimensions } from "react-native";
import CustomRadio from 'que/components/CustomRadio'
import CustomSelect from 'que/components/CustomSelect'
import CustomCheckBox from 'que/components/CustomCheckBox'
import style from "./style";
import { colors, ShopContainer } from "que/components/base"
import { withTheme } from '../../utils/themeProvider'
import SideBar from "@ptomasroos/react-native-multi-slider";
import CustomMarker from "./components/customMarker";
import RateIcons from "./components/rateIcons";
import { type } from "../../store/user";
import styled from "styled-components/native";

const menuIcon = require('que/assets/images/leftbar/filter_white_right_bar_icn.png')

const CustomRadioFilter = ({ press, label, checked }) => {
    return (
        <View style={style.filterFlex}>
            <CustomRadio checked={checked} onPress={() => press()} />
            <TouchableOpacity onPress={() => press()} style={{ padding: 10 }}>
                <Text style={{ ...style.filterTitle, marginLeft: 5 }}> {label} </Text>
            </TouchableOpacity>
        </View>
    )
}


const TextDisabled = styled.Text` 
    color: #B1ABBA
`

class FilterSideBar extends React.PureComponent {

    tapFilter = (field, value) => {
        if (!this.props.action) return false;
        this.props.action(field, value)
    }

    render() {
        const { theme, availableNow, availableOn } = this.props
        return (
            <View style={{ backgroundColor: theme.primary, flex: 1, paddingTop: 20, width: '100%' }}>
                <ScrollView>

                    <TouchableOpacity onPress={() => this.props.close()} style={style.baseHeader}>
                        <ImageBackground source={menuIcon} style={{ width: 20, height: 20, marginLeft: -10 }} />
                    </TouchableOpacity>

                    <View style={style.baseMiddle}>
                        <Text style={style.sidebarTitle}> FILTER BY... </Text>

                        <View style={style.separator} />

                        <Text style={style.filterTitle}> AVAILABILITY </Text>
                        <CustomRadioFilter checked={availableNow} press={() => this.tapFilter("availableNow", !availableNow)} label={"Available now"} />
                        <CustomRadioFilter checked={availableOn.check} press={() => this.tapFilter("availableOnCheck", !availableOn.check)} label={"Available on"} />

                        {
                            availableOn.check && (
                                <View style={{ marginTop: 10, marginBottom: 30, zIndex: 999 }}>
                                    <CustomSelect
                                        disabled={!availableOn.check}
                                        value={availableOn.value != null ? this.props.availableOn.options[availableOn.value] : null}
                                        options={availableOn.options.map((i, k) => { return { value: i, id: k } })}
                                        selectValue={({ id }) => this.tapFilter("availableOnOption", id)}
                                    />
                                </View>
                            )
                        }


                        <View style={style.separator} />

                        <Text style={style.filterTitle}> DISTANCE </Text>

                        <View style={[style.filterFlex, { marginLeft: 10 }]}>
                            <SideBar
                                min={5}
                                max={100}
                                sliderLength={Dimensions.get('window').width - 150}
                                customMarker={
                                    () => <CustomMarker />
                                }
                                selectedStyle={{
                                    backgroundColor: 'white',
                                }}
                                unselectedStyle={{
                                    backgroundColor: 'rgba(255,255,255,.2)',
                                }}
                                trackStyle={{
                                    height: 2,
                                }}
                                values={this.props.distance}
                                onValuesChangeFinish={val => this.tapFilter("slider", val)}
                            />
                        </View>
                        <View style={{ flexDirection: "row" }}>
                            <View style={style.flexRow1}>
                                <Text style={style.filterTitle}>5 km</Text>
                            </View>
                            <View style={[style.flexRow1, { flexDirection: "row-reverse" }]}>
                                <Text style={style.filterTitle}>{this.props.distance[0]}km</Text>
                            </View>
                        </View>

                        <View style={style.separator} />

                        <Text style={style.filterTitle}> COST </Text>

                        <View style={{ display: "flex", flexDirection: "row", alignItems: "center" }}>

                            <View style={{ marginLeft: 5, marginRight: 10 }}>
                                <CustomCheckBox
                                    checked={this.props.coast.check}
                                    onPress={(val) => this.tapFilter("coastCheckBox", val)}
                                />
                            </View>

                            <RateIcons
                                imgs={[
                                    require('que/assets/images/price_empty_barber_shop_icn.png'),
                                    require('que/assets/images/price_full_barber_shop_icn.png')
                                ]}
                                active={this.props.coast.count}
                                press={(key) => this.tapFilter("coastValue", key)}
                            />


                        </View>

                        <View style={style.separator} />

                        <Text style={style.filterTitle}> RATING </Text>
                        <View style={{ display: "flex", flexDirection: "row", alignItems: "center" }}>

                            <View style={{ marginLeft: 5, marginRight: 10 }}>
                                <CustomCheckBox
                                    checked={this.props.rating.check}
                                    onPress={(val) => this.tapFilter("ratingCheck", val)} />
                            </View>

                            <RateIcons
                                imgs={[
                                    require('que/assets/images/barbers/shop_raiting_empty_icn.png'),
                                    require('que/assets/images/raiting_full_card_home_costumer_icn.png')
                                ]}
                                active={this.props.rating.count}
                                press={(key) => this.tapFilter("ratingValue", key)}
                            />


                            <View>
                                {!!this.props.rating.check && !!this.props.rating.expanded &&
                                    <TouchableOpacity onPress={() => this.tapFilter("ratingExpanded", !this.props.rating.expanded)} style={style.arrowIconContainer}>
                                        <Image source={require('que/assets/images/arrow_custom_select.png')} style={{
                                            transform: [
                                                { rotate: "180deg" },
                                            ], ...style.arrowIcon
                                        }}
                                        />
                                    </TouchableOpacity>
                                }
                                {!!this.props.rating.check && !this.props.rating.expanded &&
                                    <TouchableOpacity onPress={() => this.tapFilter("ratingExpanded", !this.props.rating.expanded)} style={style.arrowIconContainer}>
                                        <Image source={require('que/assets/images/arrow_custom_select.png')}
                                            style={[style.arrowIcon, style.arrowIconContainer]}
                                        />
                                    </TouchableOpacity>
                                }
                                {!this.props.rating.check &&
                                    <Image source={require('que/assets/images/arrow_custom_select.png')}
                                        style={[style.arrowIcon, style.arrowIconContainer]}
                                    />
                                }
                            </View>
                        </View>

                        {!!this.props.categories && this.props.rating.check && !!this.props.rating.expanded && this.props.categories.map((category => (
                            <View style={{ display: "flex", flexDirection: "row", alignItems: "center" }}>
                                <View style={{ marginLeft: 5, marginRight: 10 }}>
                                    <CustomCheckBox checked={this.props.rating.types.find((ctg) => ctg._id == category._id)} onPress={() => this.tapFilter("ratingType", category)} />
                                </View>
                                <View style={{
                                    flexDirection: 'row',
                                    marginLeft: 10,
                                    justifyContent: "space-between",
                                    alignItems: 'center',
                                }}>
                                    {!!this.props.rating.types.find((ctg) => ctg._id == category._id) ?
                                        <Text
                                            style={style.filterTitle}
                                            numberOfLines={1}

                                        >
                                            {category.name}
                                        </Text>
                                        :
                                        <TextDisabled numberOfLines={1}>
                                            {category.name}
                                        </TextDisabled>

                                    }
                                    <RateIcons
                                        imgs={[
                                            require('que/assets/images/barbers/shop_raiting_empty_icn.png'),
                                            require('que/assets/images/raiting_full_card_home_costumer_icn.png')
                                        ]}
                                        active={(() => {
                                            const types = this.props.rating.types.find((ctg) => ctg._id == category._id);
                                            if (!!types) return types.mid_rate;
                                            return null;
                                        })()}
                                        press={(key) => this.tapFilter("ratingStyle", { key, category })}
                                    />
                                </View>
                            </View>
                        )))}


                        <Text style={style.filterTitle}> ORDER BY... </Text>
                        <View style={style.separator} />
                        <CustomRadioFilter checked={this.props.orderBy == "distance"} press={() => this.tapFilter("order", "distance")} label={"Distance"} />
                        <CustomRadioFilter checked={this.props.orderBy == "name"} press={() => this.tapFilter("order", "name")} label={"Name (A - Z)"} />
                        <CustomRadioFilter checked={this.props.orderBy == "cost"} press={() => this.tapFilter("order", "cost")} label={"Cost (Low - High)"} />

                    </View>
                </ScrollView>
            </View>
        );
    }
}

export default withTheme(FilterSideBar)