import React from "react";
import Styled from "./styled";
import { View, Text } from "native-base";

const DailyLogBarber = props => {
  console.log("daily log barber", props);

  const line = props.line!=undefined?props.line:true;
  //console.log(line);
  return (
    <Styled.CardContainer background={props.background}>
      <Styled.FlexImageRight>
        <Styled.ImageUser
          source={props.user_image}
        />
      </Styled.FlexImageRight>
      <Styled.FlexTwo>
        <View>
          <Styled.TextInformation>
            <Text>{props.barber_name}</Text>
            <Styled.ImageIcon source={require("que/assets/images/scisor_account_barber_profile_icn.png")}/> 
          </Styled.TextInformation>
        </View>
        <View>
          <Styled.TextInformation>
            <Text>{props.style_service}</Text>
            <Styled.ImageIcon source={require("que/assets/images/beard_cuts_icn.png")}/> 
          </Styled.TextInformation>
        </View>
        <View>
          <Styled.TextInformation>
            <Text>{props.shop_name.substring(0,18)}...</Text>
            <Styled.ImageIcon source={require("que/assets/images/shop_timeline_icn.png")}/> 
          </Styled.TextInformation>
        </View>
      </Styled.FlexTwo>
      <Styled.FlexImage>
        <Styled.ImageBarber
          source={props.barber_image}
        />
      </Styled.FlexImage>
      <Styled.FlexTime>
        <Styled.Hour>{props.time}</Styled.Hour>
        {line==true&&(<Styled.Line/>)}
      </Styled.FlexTime>
    </Styled.CardContainer>
  );
};

export default DailyLogBarber;
