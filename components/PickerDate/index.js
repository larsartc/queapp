import React from 'react' 
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native' 
import { withTheme } from '../../utils/themeProvider'

const style = StyleSheet.create({
  base: {
    borderWidth:1, 
    padding:5, 
    paddingHorizontal:12,
    borderRadius:16,
    flexDirection:'row',
    minWidth:240, 
    justifyContent:'center', 
    marginBottom:12,
    marginTop:8,
  }
})

const PickerDate = (props) => {
  
  return (
    <>
      {!!props.label && <Text style={{marginTop:16}}>{props.label}</Text>}
      <View style={[style.base, {borderColor:props.theme.primary}]}>
        <Text style={{flex:1}}>December</Text>
        <Text style={{flex:1, textAlign:'center'}}>26</Text>
        <Text style={{flex:1, textAlign:'center'}}>2019</Text>
      </View>
    </>
  )

}

export default withTheme(PickerDate)