import React from "react"
import {View} from "react-native"

export default DarkMask = () => <View
            style={{
            position: "absolute",
            width: "100%",
            height: "100%",
            backgroundColor: "rgba(0,0,0,0.7)",
            zIndex: 999
            }}
          />