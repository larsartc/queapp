import React from 'react'
import {PixelRatio} from 'react-native'
import styled from "styled-components/native"
import {colors} from 'que/components/base';

export default ({data, click, active}) => {
  return (
      <TouchTimeRound onPress={()=>click()} active={active}>
        <TextSmall active={active}>{data.time}</TextSmall>
      </TouchTimeRound>
  )
}


const TouchTimeRound = styled.TouchableOpacity`
    width: ${PixelRatio.get()>1.5?65:55};
    height: 24;
    border-radius: 20;
    border-width: 1;
    margin-right: 5;
    margin-left: 5;
    justify-content: center;
    border-color: ${props=>props.active?colors.primary:colors.gray};
    background-color: ${props=>props.active?colors.primary:colors.white};
`

const TextSmall = styled.Text`
    text-align: center;
    font-size: ${PixelRatio.get()>1.5?'12px':'11px'};
    color: ${props=>props.active?colors.white:colors.gray};
`