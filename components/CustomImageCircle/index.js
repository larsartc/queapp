import React from 'react';
import { CustomCircleContainer, CustomCircleContainerTouchable } from './styled';

const CustomCircle = (props) => {

    if (!!props.touchable) {
        return (
            <CustomCircleContainerTouchable 
                {...props}
                onPress={() => props.onPress()} >
                {props.children}
                
            </CustomCircleContainerTouchable>
        )
    } else {
        return (
            <CustomCircleContainer {...props}>
                {props.children}
            </CustomCircleContainer>
        )
    }

}

export default CustomCircle;