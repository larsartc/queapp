import styled from 'styled-components';
import {colors} from 'que/components/base';

const size = 55

export const CustomCircleContainer = styled.View`
   width: ${props => props.width || size};
   height: ${props => props.height || size};
   border-radius: ${props => props.width*2 || 30};
   border-color: ${colors.white};
   border-width: 1;
   display: ${props => props.display || 'flex'};
   align-self: ${props => props.alignSelf || 'center'};
   justify-content: ${props => props.justifyContent || 'center'};
   margin-left:10px;
   margin-right:10px;
`

export const CustomCircleContainerTouchable = styled.TouchableOpacity`
   width: ${props => props.width || size};
   height: ${props => props.height || size};
   border-radius: ${props => props.width*2 || 30};
   border-color: ${colors.white};
   border-width: 1;
   display: ${props => props.display || 'flex'};
   align-self: ${props => props.alignSelf || 'flex-start'}; 
   justify-content: ${props => props.justifyContent || 'center'};
   margin-left:10px;
   margin-right:10px;
`