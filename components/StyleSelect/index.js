import React, { useState } from 'react'
import { ScrollView, View } from 'react-native'
import ProfileItem from "../ProfileItem"
import { TitlePop } from 'que/components/base';
import FullScreen from 'que/components/FullScreen';

export default props => {
  const [item, setItem] = useState({ photo: ''});

  clickCut = (val) => {
    props.click(val)
  }
 
  handleVisibleFullScreen = (item, _) => {
    setItem(item)
  }
  return (
    <>
    <FullScreen visible={!!item.photo} setVisible={this.handleVisibleFullScreen} photo={item.photo}/>
      <TitlePop> {props.title} </TitlePop>
      <ScrollView>
        <View style={{ display: 'flex', flexDirection: 'row', alignItens: 'center', flexWrap: 'wrap' }}>
          {
            props.list.map((val, i) => {
              return <ProfileItem key={i} active={props.selected == val._id} data={{ ...val, image: { uri: val.photo } }} action={() => clickCut(val._id)} onLongPress={() => this.handleVisibleFullScreen(val, true)} />
            })
          }
        </View>
      </ScrollView>
    </>
  )
}