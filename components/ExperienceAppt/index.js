import React, {useState, useEffect} from 'react'
import {ScrollView, FlatList, Text, Alert} from 'react-native'
import styled from "styled-components/native"
import {colors, TitlePop} from 'que/components/base';

export default ({data, click}) => {

    const [appointmentBarber, setAppBarber] = useState(0)
    const [appointmentDay, setAppDay] = useState(0)
    const [appopintmentTime, setAppTime] = useState(0)
    const [appointmentUser, setAppUser] = useState(0)


  return (
    <>
    <TitlePop>HOW WAS YOUR EXPERIENCE?</TitlePop>
    <ScrollView>
        <Text>ExperienceAppt</Text>
    </ScrollView>
    </>
  )
}

const TimeRoundPicker = ({data, click, active}) => {
  return (
      <TouchTimeRound onPress={()=>click()} active={active}>
          <TextSmall active={active}>{data.time}</TextSmall>
      </TouchTimeRound>
  )
}

const TextSmall = styled.Text`
    text-align: center;
    font-size:12px;
    color: ${props=>props.active?colors.white:colors.gray};
`

const TouchDayRound = styled.TouchableOpacity`
    width: 60;
    height: 60;
    border-radius: 30;
    border-width: 1;
    margin-right: 5;
    margin-left: 5;
    justify-content: center;
    border-color: ${props=>props.active?colors.primary:colors.gray};
    background-color: ${props=>props.active?colors.primary:colors.white};
`

const TouchTimeRound = styled.TouchableOpacity`
    width: 60;
    height: 20;
    border-radius: 20;
    border-width: 1;
    margin-right: 5;
    margin-left: 5;
    justify-content: center;
    border-color: ${props=>props.active?colors.primary:colors.gray};
    background-color: ${props=>props.active?colors.primary:colors.white};
`