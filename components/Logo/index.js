import React from 'react';
import { LogoContainer, LogoImage } from './styled';

const Logo = () => {
    return (
        <LogoContainer>
            <LogoImage source={require('que/assets/images/que_logo.png')} />
        </LogoContainer>
    )
}

export default Logo