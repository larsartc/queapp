import React from "react"
import {useAnimation} from 'que/hooks'
import {Animated, Image, Easing} from 'react-native'

export default CloseImageAnimated = props => {
  const animation = useAnimation({doAnimation:true, duration: 500})

  return <Animated.Image 
            style={{
              height: 34, 
              width: 34,
              transform: [
                { 
                  rotate: animation.interpolate({
                    inputRange: [0,1], 
                    outputRange: ["180deg", "0deg"],
                  })
                }
              ],
              opacity: animation.interpolate({
                  inputRange: [0,1], 
                  outputRange: [0, 1],
                })
            }} source={props.source} />
}