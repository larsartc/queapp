import React from "react"
import CustomImageCircle from "que/components/CustomImageCircle"
import styled from 'styled-components'

export const LogoIcon = styled.Image`
    margin: 0 auto;
`

export default ({press, icon}) => {
  return (
    <CustomImageCircle touchable={true} onPress={press} >
      <LogoIcon source={icon} style={{ width: 25, height: 25 }} />
    </CustomImageCircle>
  )
}