import React from "react"

import { TouchableOpacity, View, Image, Text } from 'react-native'
import styled from "styled-components/native"

import DarkMask from '../DarkMask'
import RoundButton from "../RoundButton"
import PinCounters from "../PinCounters"
import {colors} from 'que/components/base';
import BasePop from "que/components/BasePop"

export default (props) => {
  const hide = !!props.hide ? props.hide : false;
  return (
    <>
    {!props.feedback&&<DarkMask />}
    <BasePop height={props.height}>

      {props.imageIcon && <RoundButton {...props} />}

      {props.imageAvatar && <BaseRoundImage source={props.imageAvatar} />}

      <View style={{width:"100%", flex: 1}}>
      
          {props.children}
          {!hide &&
          <BaseNavigation>
            <PinCounters size={props.size} active={props.activePosition} />
            {!props.isFinish && props.showNext && (<ButtonNext onPress={props.actionNext}><Text>NEXT</Text></ButtonNext>)}
            {props.showCancel && (<ButtonCancel onPress={props.actionNext}><Text>CANCEL</Text></ButtonCancel>)}
          </BaseNavigation>}
      </View>
    </BasePop>
    </>
  )
}

const ButtonNext = styled.TouchableOpacity`
  position: absolute;
  right: 10;
  bottom: 5;
`

const ButtonCancel = styled.TouchableOpacity`
  position: absolute;
  left: 10;
  bottom: 5;
  color: ${colors.red};
`

const BaseNavigation = styled.View`
  flex-direction: row;
  position: relative;
  justify-content: center;
  align-items: center;
  text-align: center;
`

const BaseRoundImage = styled.Image`
  width: 100;
  height: 100;
  border-radius: 50;
  border-width: 4;
  border-color: white;
  margin-top: -60;
  background-color: white;
  justify-content: center; 
  align-content: center;
  align-items: center;
`
