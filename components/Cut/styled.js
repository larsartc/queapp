import styled from 'styled-components/native';
import {colors} from 'que/components/base';

export const Card = styled.TouchableOpacity`
    height: 220;
    width: 50%;
    padding-left: 7;
    padding-right: 7;
    margin-bottom: 12;
`

export const ImageBackground = styled.ImageBackground`
    height: 100%;
    width: 100%;
    resizeMode: cover;
`

export const CardHeaderContainer = styled.View`
    height: 190;
    width: 100%;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
`

export const CardHeader = styled.View`
    height: 25%;
    width: 100%;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: flex-start;
`


export const CardHeaderDistance = styled.Text`
    color: ${colors.white};   
    margin-left: 5%;
    font-size: 12;
`

export const CardFooterCount = styled.Text`
    flex: 1;
    color: ${colors.white};   
    margin-left: 5%;
    font-size: 12;
`

export const TypeContainer = styled.TouchableOpacity`
    margin-right: 5%;
`

export const TypeIcon = styled.Image`
    height: 20;
    width: 20;
`

export const BottomContainer = styled.TouchableOpacity`
    display: flex;
    flex-direction: row;
    align-items: flex-end;
    flex: 1;
`

export const TextBottomContainer = styled.Text`
    width: 70%;
    height: 50%;
    font-size: 10;
    color: ${colors.white};
    margin-top: 2%;
    margin-left: 5%;
`

export const BottomIconsContainer = styled.View`
    width: 100%;
    height: 40%;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
`

export const PricesContainer = styled.View`
    display: flex;
    flex-direction: row;
    align-items: center;
    width: 100%;
    height: 100%;
    justify-content: space-between;
`
export const IconsContainer = styled.View`
    flex: 1;
    margin-left: 5%;
    margin-right: 5%;
`

export const Icons = styled.ImageBackground`
    height: 10;
    width: 10;
    margin-right: 2;
`