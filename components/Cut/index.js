import React, { Fragment, useState } from "react";
import { Dimensions, PixelRatio, TouchableOpacity, Image } from "react-native";

import {
  Card,
  BottomContainer,
  TypeContainer,
  TypeIcon,
  CardFooterCount,
  ImageBackground,
  CardHeader,
  CardHeaderDistance,
  CardHeaderContainer
} from "./styled";
import LinearGradient from "react-native-linear-gradient";

import { FavoriteContainer, FavoriteIcon } from "../base";

const Cut = props => {
  let newImage = !!props.imageBg
    ? props.imageBg
    : require("que/assets/images/model_picture.jpeg");
  const [favorite, setFavorite] = useState(
    props.favorite ? props.favorite : false
  );


  return (
    <CardContainer onPress={() => props.onPress()}>
      <ImageBackground source={newImage}>       
        <CardHeaderContainer>
          <CardHeader>
            <LinearGradient
              colors={["#000000", "rgba(0,0,0,0)"]}
              style={{
                width: "100%",
                display: "flex",
                flexDirection: "row",
                justifyContent: "space-between",
                alignItems: "center",
                height: "100%"
              }}
            >
              <CardHeaderDistance>{props.title}</CardHeaderDistance>
              <FavoriteContainer
                onPress={() =>
                  favorite ? props.unfavoriteCut() : props.saveCut()
                }
              >
                {favorite == true ? (
                  <FavoriteIcon
                    source={require("que/assets/images/heart_full_card_favorites_icn.png")}
                  />
                ) : (
                  <FavoriteIcon
                    source={require("que/assets/images/heart_empty_card_favorites_icn.png")}
                  />
                )}
              </FavoriteContainer>
            </LinearGradient>
          </CardHeader>
        </CardHeaderContainer>
        <BottomContainer
          onPress={() => props.saveCut && !favorite && props.saveCut()}
        >
          <LinearGradient
            colors={["rgba(0,0,0,0)", "rgba(0,0,0,0.8)"]}
            style={{
              width: "100%",
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-between",
              alignItems: "center",
              height: "100%"
            }}
          >
            <CardFooterCount>{props.saves} saves</CardFooterCount>
            <TypeContainer>
              {props.type == "shaving" ? (
                <TypeIcon
                  source={require("que/assets/images/beard_white_cuts_icn.png")}
                />
              ) : (
                <TypeIcon
                  source={require("que/assets/images/hair_white_cuts_icn.png")}
                />
              )}
            </TypeContainer>
          </LinearGradient>
        </BottomContainer>
      </ImageBackground>
    </CardContainer>
  );
};

export default Cut;

const CardContainer = props => {
  const { width } = Dimensions.get("window");
  const baseHeight = PixelRatio.get() > 1.5 ? 220 : 190;
  const baseWidth = width / 2; //props.widthScroll || width/2
  const baseWidthAndroid = baseWidth - 24;
  const styleBase = { paddingHorizontal: 6, marginBottom: 12 };
  const shadowOpt = {
    width: baseWidthAndroid,
    height: 220,
    color: "#000",
    border: 6,
    radius: 3,
    opacity: 0.3,
    x: 0,
    y: 2
  };

  if (Platform.OS === "ios") {
    return (
      <Card
        onPress={() => props.onPress()}
        activeOpacity={1}>
        {props.children}
      </Card>
    );
  } else {
    // return (
    //     <TouchableOpacity onPress={()=>props.onPress()} activeOpacity={1}>
    //         <BoxShadow setting={shadowOpt} >
    //             {props.children}
    //         </BoxShadow>
    //     </TouchableOpacity>
    // )
    return (
      <Card
        onPress={() => props.onPress()}
        activeOpacity={1}
        // style={{
        //   height: baseHeight,
        //   width: baseWidth,
        //   ...styleBase
        // }}
      >
        {props.children}
      </Card>
    );
  }
};
