import styled from 'styled-components/native';
import { colors } from 'que/components/base';
import {PixelRatio, Dimensions} from 'react-native'

export const CardContainer = styled.TouchableOpacity`
    height: ${PixelRatio.get()>1.5 ? 220 : 190};
    width: ${props => props.widthScroll || (Dimensions.get('window').width/2)};
    padding-left: 6;
    padding-right: 6;
    margin-bottom: 6;
`

export const ImageBackground = styled.ImageBackground`
    height: 100%;
    width: 100%;
    justify-content: flex-end;
    background-color: black;
`

export const CardHeader = styled.View`
    height: ${props => props.cardHeaderHeight ? props.cardHeaderHeight : 120};
    width: 100%;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: flex-start;
    padding-right: 10px;
    padding-left: 10px;
`


export const CardHeaderDistance = styled.Text`
    color: ${colors.white};   
    margin-left: 8;
    margin-top: 8;
    font-size: 12;
`

export const FavoriteContainer = styled.TouchableOpacity`
    padding:8px;
`

export const FavoriteIcon = styled.Image`
    height: 20;
    width: 20;
`

export const BottomContainer = styled.View`
    height: ${props => props.bottomHeight ? props.bottomHeight : 100};
    background-color: ${props => props.background ? props.background : colors.primary};
    padding-right: 17px;
    padding-left: 17px;
`

export const TextBottomContainer = styled.Text`
    font-size: 14;
    color: ${colors.white};
    margin-top: 5;
`

export const BottomIconsContainer = styled.View`
    width: 100%;
    height: 40%;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    opacity: .6;
`

export const PricesContainer = styled.View`
    display: flex;
    flex-direction: row;
    align-items: center;
    width: 100%;
    height: 100%;
`
export const IconsContainer = styled.View`
    display: flex;
    flex-direction: row;
    align-items: center;
`

export const Icons = styled.ImageBackground`
    height: 10;
    width: 10;
    margin-right: 3;
`