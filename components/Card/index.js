import React, { Fragment, useState, useRef, useEffect } from "react";
import {
  BottomContainer,
  PricesContainer,
  BottomIconsContainer,
  Icons,
  TextBottomContainer,
  ImageBackground,
  CardHeader,
  CardHeaderDistance,
  FavoriteContainer,
  FavoriteIcon,
  IconsContainer
} from "./styled";
import serializeIconsRatings from "../../utils/serializeIconsRatings";
import LinearGradient from "react-native-linear-gradient";
import {
  View,
  Dimensions,
  PixelRatio,
  Platform,
  TouchableOpacity,
  Image
} from "react-native";
import { withTheme } from "../../utils/themeProvider";
import styled from "styled-components/native";
import { BoxShadow } from "react-native-shadow";
import LottieView from "lottie-react-native";

const Card = props => {
  let stars = serializeIconsRatings(props.stars);
  let price = serializeIconsRatings(props.price);
  let distance = props.distance ? props.distance.toFixed(2) : "-";
  let title = props.title ? props.title : "No name";
  let newImage = props.imageBg
    ? props.imageBg
    : require("que/assets/images/card_brilliants_img.jpg");

  const heartAnim = useRef(null);

  const favoriteCard = () => {
    if (props.favorite) {
      heartAnim.current.play(0, Platform.OS === "ios"?45:30);
      console.log('REMOVE FAVORITE')
      props.unfavoriteShop();
    } else {
      heartAnim.current.play(Platform.OS === "ios"?45:30, 0);
      console.log('FAVORITE')
      props.saveShop();
    }
  };

  const [favorite, setFavorite] = useState(null);
  useEffect(() => {
    setFavorite(props.favorite ? 0 : 1);
  }, []);

  return (
    <CardContainer
      widthScroll={
        props.horizontal ? "220" : Dimensions.get("window").width / 2
      }
      carousel={props.carousel}
      onPress={() => props.onPress()}
    >
      <ImageBackground source={newImage}>
        <LinearGradient
          style={{ flex: 1 }}
          colors={["#000000", "rgba(0,0,0,0)"]}
        >
          <CardHeader cardHeaderHeight={props.cardHeaderHeight || null}>
            <CardHeaderDistance>{distance} km</CardHeaderDistance>
            <FavoriteContainer onPress={() => favoriteCard()}>
              <LottieView
                style={{ height: 20, width: 20 }}
                source={require("que/assets/animations/heart.json")}
                autoPlay={false}
                loop={false}
                progress={favorite}
                ref={heartAnim}
              />
              {/* <Image source={iconFavorite} style={style.btnFavorite} /> */}
            </FavoriteContainer>
          </CardHeader>
        </LinearGradient>

        <BottomContainer
          style={{ ...props.style }}
          background={props.theme.primary}
          bottomHeight={props.bottomHeight || null}
        >
          <View style={{ flex: 1, justifyContent: "center" }}>
            <TextBottomContainer numberOfLines={2}>{title}</TextBottomContainer>
          </View>
          <BottomIconsContainer>
            <IconsContainer>
              <>
                {price.number != null &&
                  [...Array(price.number).keys()].map((p, k) => {
                    return (
                      <Icons
                        key={k}
                        source={require("que/assets/images/shopdetails/shop_price_filled_icn.png")}
                      />
                    );
                  })}
              </>
              <>
                {price.number != null &&
                  price.number < 5 &&
                  [...Array(5 - price.number).keys()].map((p, k) => {
                    return (
                      <Icons
                        key={k}
                        source={require("que/assets/images/shopdetails/shop_price_empty_circle_icn.png")}
                      />
                    );
                  })}
              </>
              <>
                {price.number == null &&
                  [...Array(5).keys()].map((p, k) => {
                    return (
                      <Icons
                        key={k}
                        source={require("que/assets/images/shopdetails/shop_price_empty_circle_icn.png")}
                      />
                    );
                  })}
              </>
            </IconsContainer>
            <IconsContainer>
              <>
                {stars.number != null &&
                  !!stars.partial &&
                  [...Array(stars.number).keys()].map((p, k) => {
                    if (k + 1 != stars.number) {
                      return (
                        <Icons
                          key={k}
                          source={require("que/assets/images/shopdetails/shop_raiting_filled_icn.png")}
                        />
                      );
                    } else {
                      return (
                        <Icons
                          key={k}
                          source={require("que/assets/images/shopdetails/shop_raiting_semi_icn.png")}
                        />
                      );
                    }
                  })}
              </>
              <>
                {stars.number != null &&
                  !stars.partial &&
                  [...Array(stars.number).keys()].map((p, k) => {
                    return (
                      <Icons
                        key={k}
                        source={require("que/assets/images/shopdetails/shop_raiting_filled_icn.png")}
                      />
                    );
                  })}
              </>
              <>
                {stars.number == null ||
                  (stars.number < 5 &&
                    [...Array(5 - stars.number).keys()].map((p, k) => {
                      return (
                        <Icons
                          key={k}
                          source={require("que/assets/images/shopdetails/shop_raiting_empty_icn.png")}
                        />
                      );
                    }))}
              </>
            </IconsContainer>
          </BottomIconsContainer>
        </BottomContainer>
      </ImageBackground>
    </CardContainer>
  );
};

export default withTheme(Card);

const CardContainer = props => {
  const { width } = Dimensions.get("window");
  const baseHeight = PixelRatio.get() > 1.5 ? 220 : 190;
  const baseWidth = width / (props.carousel ? 1.7 : 2); //props.widthScroll || width/2
  const baseWidthAndroid = baseWidth - 12;
  const styleBase = { paddingHorizontal: 6, marginBottom: 12 };

  const shadowOpt = {
    width: baseWidthAndroid - 12,
    height: baseHeight,
    color: "#000",
    border: 6,
    radius: 3,
    opacity: 0.3,
    x: 0,
    y: 2,
    marginHorizontal: 6,
    marginBottom: 12
  };

  if (Platform.OS === "ios") {
    return (
      <TouchableOpacity
        onPress={() => props.onPress()}
        activeOpacity={1}
        style={{
          height: baseHeight,
          width: baseWidth,
          ...styleBase,
          shadowColor: "#000",
          shadowOffset: {
            width: 0,
            height: 5
          },
          shadowOpacity: 0.34,
          shadowRadius: 6.27,
          elevation: 10
        }}
      >
        {props.children}
      </TouchableOpacity>
    );
  } else {
    // return (
    //     <TouchableOpacity onPress={()=>props.onPress()} activeOpacity={1}>
    //         <BoxShadow setting={shadowOpt} >
    //             {props.children}
    //         </BoxShadow>
    //     </TouchableOpacity>
    // )
    return (
      <TouchableOpacity
        onPress={() => props.onPress()}
        activeOpacity={1}
        style={{ ...styleBase }}
        // style={{
        //   height: baseHeight,
        //   width: baseWidth,
        //   ...styleBase
        // }}
      >
        <BoxShadow setting={shadowOpt}>{props.children}</BoxShadow>
      </TouchableOpacity>
    );
  }
};
