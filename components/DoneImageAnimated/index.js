import React from "react"
import {useAnimation} from 'que/hooks'
import {Animated, Image, Easing} from 'react-native'

export default DoneImageAnimated = props => {
  const animation = useAnimation({doAnimation:true, duration: 300})

  return <Animated.Image 
            style={{
              height: animation.interpolate({
                  inputRange: [0,1], 
                  outputRange: [0, 34],
                }), 
              width: animation.interpolate({
                  inputRange: [0,1], 
                  outputRange: [0, 34],
                }),
              opacity: animation.interpolate({
                  inputRange: [0,1], 
                  outputRange: [0, 1],
                })
            }} source={props.source} />
}