import { StyleSheet, PixelRatio } from "react-native";
import {colors} from 'que/components/base';
import { ifIphoneX } from 'react-native-iphone-x-helper'

const style = StyleSheet.create({
  signInContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    textAlign: 'left',
    padding: 10
  },
  baseHeader: {
    ...ifIphoneX({paddingTop:50},{paddingTop:30}),
    paddingBottom: 20,
    paddingLeft: 20,
    paddingRight: 20,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: "space-between",
  },
  separator: {
    width: '80%',
    alignSelf: 'center',
    borderBottomColor: 'white',
    borderBottomWidth: 0.5,
  },
  logo: {
    maxWidth: '100%',
    maxHeight: 80,
    marginTop: 20,
  },
  buttonItem: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: '10%',
    paddingRight: '10%',
    paddingBottom: 25,
  },
  buttonItemIcon: {
    width: 20,
    height: 20,
    marginRight: 10,
  },
  buttonItemText: {
    color: colors.white,
    fontSize: 12,
  }
});

export default style;