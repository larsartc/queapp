import React from "react"
import { 
  View, 
  TouchableOpacity, 
  Image, 
  Text } from "react-native"
import CustomImageCircle from 'que/components/CustomImageCircle'
import style from "./style"

export default (props) => {
  let haveAvatar = false
  let imgProfile = require('que/assets/images/user_name_icn.png')

  // avatar shop
  if(!!props.barber){
    if(props.barber.photos.length>0){
      haveAvatar = true
      imgProfile = {uri:props.barber.photos[0]}
    }
  }

  // avatar customer
  if(!!props.user){
    if(!!props.user.photo){
      haveAvatar = true
      imgProfile = {uri: props.user.photo}
    }
  }

  return (
    <TouchableOpacity onPress={() => props.pressItem()} style={style.signInContainer}>
        
        { !haveAvatar && (
          <CustomImageCircle 
            touchable={true} 
            width={50} 
            height={50} 
            alignSelf={'flex-start'} 
            onPress={() => props.pressItem()}>
              <Image 
              source={imgProfile} 
              style={{ width: 20, height: 20, alignSelf: 'center' }} />
          </CustomImageCircle>
        )}
        
        { haveAvatar && (
          <TouchableOpacity onPress={()=>props.pressItem()}>
            <Image 
              resizeMode={"cover"}
              source={imgProfile} 
              borderRadius={25}
              style={{ 
                width: 50, 
                height: 50, 
                alignSelf: 'center',
                borderWidth:1,
                borderColor:'white',
                marginHorizontal: 10,
              }} />
          </TouchableOpacity>
        )}

        <View style={{marginLeft: 10}}>
          <Text style={{ color: 'white', fontSize: 14 }}>
            {props.user != undefined ? props.user.name : (props.isBarber=="BARBER" ? props.barber.name : "Sign in")}
          </Text>
          {props.isBarber=="BARBER" && (
            <Text style={{color:'rgba(255,255,255,.6)', fontSize:10}}>{props.shop?"BARBER SHOP":"BARBER"}</Text>
          )}
        </View>
    </TouchableOpacity>
  )
}