import React from "react";
import { Container, Text, Icon } from "native-base";
import {
  View, TouchableOpacity, Alert,
  ImageBackground, Image, TextInput, ScrollView
} from "react-native";
import CustomImageCircle from 'que/components/CustomImageCircle'
import ItemMenu from "./itemMenu"
import PopBottom from "que/components/PopBottom"
import style from "./style";
import { withTheme } from '../../utils/themeProvider'
import AsyncStorage from "@react-native-community/async-storage"
import * as ASYNCTAG from "../../utils/storeNames"
import FeedbackBox from "../FeedbackBox"
import RightSideBar from "que/components/rightSideBar"
import { NavigationActions, StackActions } from 'react-navigation';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { logout } from '../../store/user'

import AvatarProfile from './avatarProfile'

class SideBar extends React.Component {

  state = {
    showFeedback: false
  }

  pressItem = item => {
    this.setState({ showFeedback: false })
    if (item === "ProfileScreen") {
      if (this.props.user != undefined) {
        this.props.navigation.navigate(item)
        this.props.close()
      } else {
        this.props.navigation.navigate("SignIn")
        this.props.close()
      }
      return
    } else if (item !== "Logout") {
      this.props.navigation.navigate(item)
      this.props.close()
    } else {
      Alert.alert(
        'Sign out',
        'Do you want sign out?',
        [

          {
            text: 'YES', onPress: () => {

              AsyncStorage.removeItem(ASYNCTAG.TAG_CUT_TUTORIAL)


              return this.props.navigation.dispatch(StackActions.reset({
                index: 0,
                actions: [
                  NavigationActions.navigate({ routeName: "SignIn" })
                ]
              }))

              this.props.close()

            }, style: 'cancel'
          },
          { text: 'NO', onPress: () => { } },
        ],
        { cancelable: false },
      );
    }
  }

  closeFeedback = () => {
    this.setState({
      ...this.state,
      showFeedback: !this.state.showFeedback
    })
  }

  clickFeedBack = () => {
    this.setState({ showFeedback: true })
  }

  render() {
    const { showFeedback } = this.state
    const { theme, showMenu, isBarber, barber, user } = this.props

    if (!showMenu) {
      return null
    }

    let shopWorking = user != undefined ? user.shopsWorking : null

    return (
      <>
        <RightSideBar close={() => this.props.close()} left>
          <View style={{ flex: 1, backgroundColor: theme.primary, width: '100%' }}>
            <ScrollView>

              <View style={style.baseHeader} >
                <AvatarProfile user={user} isBarber={isBarber} pressItem={() => this.pressItem("ProfileScreen")} />
                <TouchableOpacity onPress={() => this.props.close()}>
                  <Image source={require('que/assets/images/menu_open_home_costumer_icn.png')} style={{ width: 15, height: 15, alignSelf: 'center', marginTop: 15 }} />
                </TouchableOpacity>
              </View>

              <View style={{ ...style.separator, marginBottom: 20 }} />
              <View>

                {
                  (shopWorking instanceof Array) && shopWorking.length > 0 && shopWorking[0].status === "accepted" && (
                    <View style={{ paddingBottom: 20, paddingHorizontal: 20 }} >
                      <AvatarProfile shop barber={shopWorking[0].shop} isBarber={"BARBER"} pressItem={() => { }} />
                    </View>
                  )
                }

                <ItemMenu press={() => this.pressItem("ShopsScreen")} icon={require("../../assets/images/leftbar/shops_menu_icn.png")} label={"SHOPS"} />
                <ItemMenu press={() => this.pressItem("CutsScreen")} icon={require("../../assets/images/leftbar/cuts_menu_icn.png")} label={"CUTS"} />

                {this.props.user != undefined && (
                  <>
                    <ItemMenu press={() => this.pressItem("AppointmentsScreen")} icon={require("../../assets/images/leftbar/appointments_menu_icn.png")} label={"APPOINTMENTS"} />

                    {isBarber == "BARBER" && (
                      <ItemMenu press={() => this.pressItem("BarberRatings")} icon={require("../../assets/images/star_empty_card_favorites_icn.png")} label={"RATINGS"} />
                    )}

                    <ItemMenu press={() => this.pressItem("FavoritesScreen")} icon={require("../../assets/images/leftbar/favorites_menu_icn.png")} label={"FAVORITES"} />
                  </>
                )}

                <View style={{ ...style.separator, marginBottom: 20 }} />
                <ItemMenu press={() => this.pressItem("HelpScreen")} icon={require("../../assets/images/leftbar/help_menu_icn.png")} label={"HELP"} />
                <ItemMenu press={() => this.clickFeedBack()} icon={require("../../assets/images/leftbar/feedback_menu_icn.png")} label={"FEEDBACK"} />

                {this.props.user != undefined && (
                  <ItemMenu press={() => this.pressItem("Logout")} icon={require("../../assets/images/leftbar/sign_out_menu_icn.png")} label={"SIGN OUT"} />
                )}

              </View>
            </ScrollView>
          </View>
        </RightSideBar>
        {showFeedback && <FeedbackBox actionClose={() => this.closeFeedback()} />}
      </>
    )
  }
}

const mapStateToProps = store => ({
  isBarber: store.userReducer.interface,
  barber: store.barberReducer.barber,
  user: store.userReducer.user,
})

const mapDispatchToProps = dispatch => bindActionCreators({
  logout,
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(withTheme(SideBar));