import React from "react"
import { TouchableOpacity, Text, Image } from "react-native"
import style from "./style"


export default ({press, icon, label}) => {
  return (
    <TouchableOpacity onPress={press} style={style.buttonItem} >
      <Image source={icon} style={style.buttonItemIcon} />
      <Text style={style.buttonItemText}>{label}</Text>
    </TouchableOpacity>
  )
}