import React, {useState} from 'react';

import {Dimensions, TouchableOpacity} from 'react-native'

import {
    BottomContainer,
    TypeContainer,
    TypeIcon,
    CardFooterCount,
    TextBottomContainer,
    ImageBackground,
    CardHeader,
    CardHeaderDistance,
    FavoriteContainer,
    FavoriteIcon,
    CardHeaderContainer,
    IconsContainer,
    Icons
} from './styled';
import { BoxShadow } from "react-native-shadow";

const Barbers = (props) => {
    let stars    = !!props.stars?props.stars:4;
    let newImage = !!props.imageBg?props.imageBg:require('que/assets/images/model_picture.jpeg');
    const [favorite, setFavorite] = useState(props.favorite? props.favorite : false )

    return (
        <CardContainer>
            <TouchableOpacity onPress={()=>props.action(props.id)}>
                <ImageBackground source={newImage}>
                    <CardHeaderContainer>
                        <CardHeader>
                            <CardHeaderDistance>{props.title}</CardHeaderDistance>
                            <FavoriteContainer onPress={() => favorite?props.unfavoriteBarber():props.saveBarber()}>
                                {favorite == true ? (
                                    <FavoriteIcon
                                        source={require('que/assets/images/heart_full_card_favorites_icn.png')}/>
                                ) : (
                                    <FavoriteIcon
                                        source={require('que/assets/images/heart_empty_card_favorites_icn.png')}/>
                                )}
                            </FavoriteContainer>
                        </CardHeader>
                    </CardHeaderContainer>
                    <BottomContainer>
                        {Array(stars).fill(null).map((r) => {
                            return <Icons source={require('que/assets/images/shopdetails/shop_raiting_filled_icn.png')} />
                        })}
                        {Array(5 - stars).fill(null).map((r) => {
                            return <Icons source={require('que/assets/images/shopdetails/shop_raiting_empty_icn.png')} />
                        })}
                    </BottomContainer>
                </ImageBackground>
            </TouchableOpacity>
        </CardContainer>
    )
}

const CardContainer = props => {
    const { width } = Dimensions.get("window");
    const baseWidth = width / 2; //props.widthScroll || width/2
    const baseWidthAndroid = baseWidth - 24;
  
    const shadowOpt = {
      width: baseWidthAndroid,
      height: 220,
      color: "#000",
      border: 6,
      radius: 3,
      opacity: 0.3,
      x: 0,
      y: 2
    };
  
    if (Platform.OS === "ios") {
      return (
        <TouchableOpacity
          onPress={() => props.onPress()}
          activeOpacity={1}
          style={{
            shadowColor: "#000",
            shadowOffset: {
              width: 0,
              height: 5,
            },
            shadowOpacity: 0.34,
            shadowRadius: 6.27,
            elevation: 10,
          }}
        >
          {props.children}
        </TouchableOpacity>
      );
    } else {
      return (
        <TouchableOpacity
          onPress={() => props.onPress()}
          activeOpacity={1}
        >
          <BoxShadow setting={shadowOpt}>{props.children}</BoxShadow>
        </TouchableOpacity>
      );
    }
  };

export default Barbers;