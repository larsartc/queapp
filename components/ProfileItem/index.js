import React from "react"
import { StyleSheet, Text, View, TouchableOpacity, Image, PixelRatio } from 'react-native';
import { colors } from 'que/components/base';
import serializeIconsRatings from "../../utils/serializeIconsRatings"

export default ProfileItem = ({ data, action = () => { }, active = false, onLongPress = () => {} }) => {
  let stars = serializeIconsRatings(data.starts);

  return (
    <View style={style.baseProfileItem}>
      <TouchableOpacity style={{ textAlign: 'center', alignSelf: 'center' }} onPress={() => action()} onLongPress={ () => onLongPress()}>
        <Image source={data.image} style={active ? style.iconActive : style.barberIconsContainerProfile} />
      </TouchableOpacity>
      <Text style={active ? style.barberNameActive : style.barberName}>
        {data.name}
      </Text>

      <View style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'center', textAlign: 'center' }}>

        {stars.number != null && !!stars.partial &&
          [...Array(stars.number).keys()].map((p, k) => {
            if (k + 1 != stars.number) {
              return <Image key={k} source={require('que/assets/images/shopdetails/apointment_purple_star_complete_icn.png')} style={{ ...style.icon, marginRight: 0, marginLeft: 0 }} />
            } else {
              // return <Image key={k} source={require('que/assets/images/shopdetails/shop_raiting_semi_icn.png')} />
            }
          })
        }


        {stars.number != null && !stars.partial &&
          [...Array(stars.number).keys()].map((p, k) => {
            return <Image key={k} source={require('que/assets/images/shopdetails/apointment_purple_star_complete_icn.png')} style={{ ...style.icon, marginRight: 0, marginLeft: 0 }} />
          })
        }


        {stars.number == null || stars.number < 5 &&
          [...Array(5 - stars.number).keys()].map((p, k) => {
            return <Image key={k} source={require('que/assets/images/shopdetails/apointment_purple_star_empty_icn.png')} style={{ ...style.icon, marginRight: 0, marginLeft: 0 }} />
          })
        }

      </View>
    </View>
  )
}

const style = StyleSheet.create({
  baseProfileItem: {
    marginTop: 10,
    marginBottom: 10,
    width: PixelRatio.get() > 1.5 ? 60 : 50,
    marginRight: 10,
    marginLeft: 10,
    textAlign: 'center',
    maxWidth: 120
  },
  iconActive: {
    height: PixelRatio.get() > 1.5 ? 60 : 50,
    width: PixelRatio.get() > 1.5 ? 60 : 50,
    justifyContent: 'center',
    borderRadius: 30,
    textAlign: 'center',
    display: 'flex',
    alignItems: 'center',
    borderWidth: 2,
    borderColor: colors.primary,
    backgroundColor: "#f2f2f2"
  },

  barberIconsContainerProfile: {
    height: PixelRatio.get() > 1.5 ? 60 : 50,
    width: PixelRatio.get() > 1.5 ? 60 : 50,
    justifyContent: 'center',
    borderRadius: 30,
    textAlign: 'center',
    display: 'flex',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: colors.gray,
    backgroundColor: "#f2f2f2"
  },
  barberName: {
    fontSize: 11,
    color: 'gray',
    textAlign: 'center'
  },
  barberNameActive: {
    fontSize: 11,
    color: colors.primary,
    textAlign: 'center'
  },
  icon: {
    width: 13,
    height: 13,
    color: colors.primary,
  }

})