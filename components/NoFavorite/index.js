import React from "react";
import { View } from "native-base";
import { Image, Text } from "react-native";

export default NoFavorite = ({ headerText, bodyText, sourceImg}) => (
    <View >
        <Text style={{ marginVertical: 50, textAlign: 'center' }}>{headerText}</Text>
        <Text style={{ textAlign: 'center', marginHorizontal: 30, marginBottom: 40 }}>{bodyText}</Text>
        <Image source={sourceImg} />
    </View>
)