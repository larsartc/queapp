import React from 'react';
import { ImageBackground } from './styled'

const CustomBackgroundImage = (props) => {
    return (
        <ImageBackground source={props.image || require('que/assets/images/sign_in_img.jpg')} resizeMode={"cover"} >
            {props.children}
        </ImageBackground>
    )
}

export default CustomBackgroundImage