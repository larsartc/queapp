import styled from 'styled-components';
import { Dimensions } from 'react-native';

export const ImageBackground = styled.ImageBackground`
    height: 100%;
    flex: 1;
`