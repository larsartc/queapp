import React from 'react'
import styled from "styled-components/native"
import {colors} from 'que/components/base'
import {withTheme} from '../../utils/themeProvider'

const RoundButton = (props) => {
  const newImage = props.imageIcon?props.imageIcon:require("que/assets/images/shopdetails/checkbox_services_checked_icn.png");
  const newImageClose = props.imageIcon?props.imageIcon:require("que/assets/images/close_pop_up_gray_account_costumer_icn.png");
  return (
    <BaseRoundButton activeOpacity={1} onPress={props.actionClose} backgroundColor={props.theme.primary}>
      {!props.isFinish ? <IconTop source={newImage} /> :
      <IconTop source={newImageClose} />}
    </BaseRoundButton>
  )
}

export default withTheme(RoundButton)

const BaseRoundButton = styled.TouchableOpacity`
  width: 60;
  height: 60;
  border-radius: 30;
  border-width: 4;
  border-color: ${props=> props.backgroundColor ? props.backgroundColor : colors.primary};
  margin-top: -30;
  background-color: white;
  justify-content: center; 
  align-content: center;
  align-items: center;
`

const IconTop = styled.Image`
  width:34;
  height:34;
`