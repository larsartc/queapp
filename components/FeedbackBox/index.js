import React, { useState } from 'react'
import { View, TextInput, Text, Alert, KeyboardAvoidingView } from "react-native"
import PopBottom from "que/components/PopBottom"
import { withTheme } from '../../utils/themeProvider'
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { systemFeedBack } from "que/store/feedback";


class FeedbackBox extends React.Component {

  state = {
    message: ""
  }

  sendFeedback = async () => {
    if (this.state.message.lenght > 0) {
      const result = await this.props.systemFeedBack({ text: this.state.message })
      if (!!result) {
        Alert.alert("Feedback", "Thanks you for the feedback!")
      } else {
        Alert.alert("Feedback", "Error! Please try again.")
      }
      this.props.actionClose()
    } else {
      this.props.actionClose()
    }
  }

  render() {

    let iconPop = require("que/assets/images/close_pop_up_gray_account_costumer_icn.png")
    if(!!this.state.message){
      iconPop = require("que/assets/images/shopdetails/checkbox_services_checked_icn.png")
    }

    return (
      <PopBottom
        height={300}
        imageIcon={iconPop}
        actionClose={() => this.sendFeedback()}
        isFinish={true}
        size={0}
        activePosition={0}
        showNext={false}
        feedback={true}>
        <View>
          <Text style={{ marginVertical: 15, textAlign: 'center' }}>PLEASE GIVE US YOUR FEEDBACK</Text>
          <KeyboardAvoidingView
            behavior="position"
          >
            <TextInput
              placeholder={"Send your feedback..."}
              style={{
                borderColor: this.props.theme.primary,
                borderWidth: 1,
                paddingHorizontal: 8,
                paddingVertical: 8,
                marginHorizontal: 16,
                marginVertical: 16,
                height: "50%"
              }}
              multiline={true}
              numberOfLines={11}
              textAlignVertical={'top'}
              onChangeText={e => this.setState({ message: e })}
            />
          </KeyboardAvoidingView>
        </View>
      </PopBottom>
    )
  }
}
FeedbackBox = withTheme(FeedbackBox);


const mapDispatchToProps = dispatch =>
  bindActionCreators({
    systemFeedBack
  }, dispatch)

export default connect(null, mapDispatchToProps)(FeedbackBox)