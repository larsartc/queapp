import styled from 'styled-components';
import {colors} from 'que/components/base';

export default styledContainer = {
    DivBorderHelp: styled.TouchableOpacity`
        flex: 1;
        width: 100%;
    `,
    BorderHelp: styled.View`
        width: 100%;
        flex: 1;
        padding-left: 20;
        padding-right: 20;
        padding-top: 15;
        padding-bottom: 15;
        margin-bottom: 20;
        display: flex;
        flex-direction: column;
        justify-content: flex-start;
        text-align: left;
        background-color: transparent;
        border-width: 2;
        border-color: ${colors.white};
        border-radius: 40;
    `,
    TextHelp: styled.Text`
        color: ${colors.white};
        text-align: left;
    `,
    TextHelpAnswer: styled.Text`
        color: ${colors.white};
        text-align: left;
        margin-top: 5;
    `,
    ViewHelp: styled.View`
        text-align: left;
        justify-content: flex-start;
        display: flex;
        flex: 1;
    `,
    IconArrow: styled.Image`
        position: absolute;
        right: -10;
        top: -9;
    `,
}