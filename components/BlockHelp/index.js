import React, {useState} from "react";
import Styled from "./styled";

const BlockHelp = props => {

  const [active, setActive] = useState( false )

  return (
    <Styled.DivBorderHelp onPress={() => setActive(!active)}>
      <Styled.BorderHelp>
        <Styled.ViewHelp>
          <Styled.TextHelp>
            {props.question}
          </Styled.TextHelp>
            {props.active == 1?(
              <Styled.IconArrow source={require("que/assets/images/arrow_up_help_icn.png")} />
            ):(
              <Styled.IconArrow source={require("que/assets/images/arrow_down_help_icn.png")} />
            )}
        </Styled.ViewHelp>
        {active&&(
          <Styled.ViewHelp>
            <Styled.TextHelpAnswer>{props.answer}</Styled.TextHelpAnswer>
          </Styled.ViewHelp>
        )}
      </Styled.BorderHelp>
    </Styled.DivBorderHelp>
  );
};

export default BlockHelp;
