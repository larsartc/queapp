import React from "react";
import Styled from "./styled";
import { Spinner } from "native-base";

export default class LoadingDefault extends React.Component {
    render() {
        const {color} = this.props;
        return (
            <Styled.DivLoading>
                <Spinner size="large" color={color}/>
            </Styled.DivLoading>
        )
    }
}
