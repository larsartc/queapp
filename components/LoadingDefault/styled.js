import styled from 'styled-components';

export default styledContainer = {
    DivLoading: styled.View`
        display: flex;
        justify-content: center;
        align-items: center;
        text-align: center;
        align-self: center;
        width: 100%;
    `,
}