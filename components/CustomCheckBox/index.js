import React, { Component } from 'react';
import { View, TouchableOpacity } from 'react-native'
import styles from "./styles";


class CustomCheckBox extends Component {

    render() {
        return (
            <TouchableOpacity
                style={styles.container}
                onPress={() => !!this.props.onPress && this.props.onPress(!this.props.checked)}>
                <View style={!!this.props.checked ? styles.checked : {}} />
            </TouchableOpacity>
        );
    }
}

export default CustomCheckBox;