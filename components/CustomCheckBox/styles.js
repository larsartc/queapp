import {StyleSheet} from 'react-native'
import {colors} from 'que/components/base';

export default styles = StyleSheet.create({
    container: {
        width: 20,
        height: 20,
        borderWidth: 0.5,
        borderColor: colors.white,
        justifyContent: 'center',
    },
    checked:{
        width: 10,
        height: 10,
        alignSelf: 'center',
        backgroundColor: colors.white
    }
})