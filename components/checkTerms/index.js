import React from "react"
import {Linking} from "react-native"
import CustomRadio from 'que/components/CustomRadio'
import styled from 'styled-components'

export default (props)=>{
  
  return (
    <TermsContainer>
        <CustomRadio checked={props.checked} onPress={() => props.checkAction()}/>
        <TermsText>I agree with the </TermsText>
        <TermsTextLink onPress={()=>Linking.openURL("http://peexell.com.br")}>Terms and Conditions of Use</TermsTextLink>
    </TermsContainer>
  )
}

const TermsContainer = styled.View`
    margin-top: 25;
    display: flex;
    flex-direction: row;
    justify-content: center;
`

const TermsText = styled.Text`
    margin-left: 10;
    font-size: 11;
    color: rgba(255,255,255,.6);
`

const TermsTextLink = styled.Text`
    text-decoration: underline;
    font-size: 11;
    color: white;
`