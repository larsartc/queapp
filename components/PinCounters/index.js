import React from "react"

import { View } from 'react-native'
import styled from "styled-components/native"
import {colors} from 'que/components/base';

export default ({size=0, active=0}) => {
  if(size==0) return null
  return (
      <View style={{height:30, flex:1, marginLeft: 20, marginRight: 20, flexDirection:"row", justifyContent:"center", alignItems:"center"}}>
          {Array(size).fill(null).map((v,i)=>{
              return <Pin key={i} active={i == active}></Pin>
          })}
      </View>
  )
}

const Pin = styled.View`
  height: 6px;
  width: 6px;
  border-radius:6px;
  margin:3px;
  background-color: ${props=>props.active==true?colors.primary:colors.gray};
`