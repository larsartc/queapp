import React, { useState, useEffect } from 'react'
import { ScrollView, FlatList, Text, Alert } from 'react-native'
import ProfileItem from "../ProfileItem"
import DayRoundPicker from '../DayRoundPicker';
import TimeRoundPicker from "../TimeRoundPicker"
import { colors, TitlePop } from 'que/components/base';
import moment from 'moment'
import _ from 'lodash';

export default (props) => {

    const { barbers, click, family, } = props;

    console.log("barbers appt", barbers, "family appt", family)

    function getBarberHoursAvaliable(hours) {
        const start_type = hours.start_time.includes('am') ? 'am' : 'pm';
        const end_type = hours.end_time.includes('am') ? 'am' : 'pm';

        let hoursBetween = moment.duration(moment(hours.end_time, 'HH:mm' + start_type).diff(moment(hours.start_time, 'HH:mm' + end_type))).asHours();
        let hourStart = moment(hours.start_time, 'HH:mm' + start_type).format('HH:mm');

        let arrayHours = [];

        while (hoursBetween >= 0) {
            const hour = moment(hourStart, "HH:mm").add(hoursBetween, 'hours').format('HH:mm');
            arrayHours.push({ time: hour });
            hoursBetween = hoursBetween - 1;
        }

        return _.sortBy(arrayHours, "time");
    };

    function getDaysInMonth() {
        const days = 30;
        let daysObj = {};
        const barber_worktime = barbers.find((barb) => barb.barber._id == props.selected.barber);
        if (!barber_worktime) return {}

        for (day = 0; day < days; day++) {
            const now = moment().format('YYYY/MM/DD')
            const date = moment(now, "YYYY/MM/DD").add(day, "d");
            const dayWeek = (parseInt(moment(date, "YYYY/MM/DD").day())) - 1
            const hoursByDay = barber_worktime.barber.barber.work_time.find((dayWork) => dayWork.week_day == dayWeek);
            if (!!hoursByDay) {
                daysObj[moment(date).format('YYYY/MM/DD')] = {
                    date: moment(date).format('YYYY/MM/DD'),
                    day: moment(date).format('MMM DD'),
                    day2: moment(date).format('ddd'),
                    hoursAvaliable: getBarberHoursAvaliable(hoursByDay)
                }
            }
        }

        if (!props.selected.day) {
            let date = new Date(Object.keys(daysObj)[0]).getTime() / 1000
            click("appointmentDay", { day: Object.keys(daysObj)[0], date })
        }

        return daysObj;
    }
    setAppointCheck = (key, val) => {
        switch (key) {
            case "appointmentBarber":
                click(key, val)
                break
            case "appointmentDay":
                let date = new Date(val).getTime() / 1000
                click("appointmentDay", { date, day: val })
                break
            case "appointmentTime":
                const newDateTime = `${props.selected.day} ${val}:00`;
                let datetime = moment(newDateTime, "YYYY/MM/DD HH:mm").unix();
                click("appointmentTime", { time: val, date: datetime })
                break
            case "appointmentUser":
                click(key, val)
                break
            default:
                break
        }
    }

    const daysInMonth = getDaysInMonth()
    return (
        <>
            <TitlePop> {props.title} </TitlePop>
            <ScrollView>
                <FlatList
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                    data={barbers}
                    keyExtractor={({ item, index }) => index}
                    renderItem={({ item, index }) => (
                        <ProfileItem
                            data={{
                                name: item.barber.name.split(" ")[0],
                                image: item.barber ? { uri: item.barber.barber.photos[0] } : "",
                                starts: item.barber.barber.mid_rate
                            }}
                            active={props.selected.barber == item.barber._id}
                            action={() => { setAppointCheck('appointmentBarber', item.barber._id) }}
                        />
                    )}
                    style={{ marginBottom: 5 }}
                />
                <FlatList
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                    data={Object.values(daysInMonth)}
                    keyExtractor={({ item, index }) => index}
                    renderItem={({ item, index }) => <DayRoundPicker data={item} active={props.selected.day == item.date} click={() => setAppointCheck('appointmentDay', item.date)} />}
                    style={{ marginBottom: 5 }}
                />
                <FlatList
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                    data={props.selected.day && daysInMonth[props.selected.day] && daysInMonth[props.selected.day].hoursAvaliable ? daysInMonth[props.selected.day].hoursAvaliable : []}
                    keyExtractor={({ item, index }) => index}
                    renderItem={({ item, index }) => <TimeRoundPicker data={item} active={props.selected.time == item.time} click={() => setAppointCheck('appointmentTime', item.time)} />}
                    style={{ marginBottom: 5 }}
                />
                <FlatList
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                    data={!!family ? family : []}
                    keyExtractor={({ item, index }) => index}
                    renderItem={({ item, index }) => (
                        <ProfileItem
                            data={{ ...item, image: { uri: item.photo } }}
                            active={props.selected.user == item._id}
                            action={() => setAppointCheck('appointmentUser', item._id)}
                        />)}
                    style={{ marginBottom: 5 }}
                />

            </ScrollView>
        </>
    )
}