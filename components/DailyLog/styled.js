import styled from 'styled-components';
import {colors} from 'que/components/base';

export default styledContainer = {
    CardContainer: styled.View`
        width: 90%;
        padding-left: 10;
        padding-right: 10;
        padding-top: 20;
        padding-bottom: 20;
        margin-bottom: 12;
        display: flex;
        flex-direction: row;
        justify-content: center;
        align-items: center;
        background-color: ${props=>props.background?props.background:colors.lightGray};
    `,

    ImageBackground: styled.ImageBackground`
        height: 100%;
        width: 100%;
    `,

    CardHeaderContainer: styled.View`
        height: 150;
        width: 100%;
        display: flex;
        flex-direction: row;
        justify-content: space-between;
    `,

    CardHeader: styled.View`
        height: 25%;
        width: 100%;
        display: flex;
        flex-direction: row;
        justify-content: space-between;
        align-items: center;
    `,


    CardHeaderDistance: styled.Text`
        color: ${colors.white};   
        margin-left: 5%;
        font-size: 12;
    `,

    CardFooterCount: styled.Text`
        flex: 1;
        color: ${colors.white};   
        margin-left: 5%;
        font-size: 12;
    `,

    FavoriteContainer: styled.TouchableOpacity`
        margin-right: 5%;
    `,

    ImageBarber: styled.Image`
        height: 50;
        width: 50;
        border-radius: 60;
        border-width: 2;
        border-color: #3D2E52; 
    `,

    ImageUser: styled.Image`
        height: 40;
        width: 40;
        border-radius: 60;
        borderWidth: 2;
        borderColor: ${colors.white};
    `,
    Line: styled.View`
        width: 6;
        height: 6;
        background-color: #5E4086;
        border-radius: 100;
        margin-right: 5;
        margin-left: 9;
    `,

    TypeContainer: styled.TouchableOpacity`
        margin-right: 5%;
    `,

    TypeIcon: styled.Image`
        height: 20;
        width: 20;
    `,

    BottomContainer: styled.View`
        display: flex;
        flex-direction: row;
    `,
    
    FlexTime: styled.View`
        justify-content: center;
        flex-direction: row;
        align-items: center;
        margin-right: 5;
    `,

    FlexImage: styled.View`
        justify-content: center;
        flex-direction: row;
        align-items: center;
        margin-right: 10
        margin-left: 10
    `,
    
    FlexOne: styled.View`
        justify-content: center;
        flex-direction: row;
        align-items: center;
        margin-right: 10
    `,

    FlexTwo: styled.View`
        flex: 3;
        justify-content: center;
    `,

    FlexImageRight: styled.View`
        width: 50;
        display: flex;
        align-items: flex-start;
        justify-content: flex-start;
        height: 100%;
    `,

    TextInformation: styled.View`
        font-size: 14;
        margin-top: 5;
        align-items: center; 
        display: flex;
        justify-content: flex-start;
        flex-direction: row;
    `,

    TextBottomContainer: styled.Text`
        width: 70%;
        height: 50%;
        font-size: 10;
        color: ${colors.white};
        margin-top: 2%;
        margin-left: 5%;
    `,

    BottomIconsContainer: styled.View`
        width: 100%;
        height: 40%;
        display: flex;
        flex-direction: row;
        justify-content: space-between;
    `,

    Hour: styled.Text`
        font-size: 12;
        background-color: ${colors.primary};
        color: #fff;
        width: 50;
        text-align: center;
        border-radius: 10;
        padding-top: 2px;
        padding-bottom: 2px;
    `,

    ImageIcon: styled.Image`
        width: 14;
        height: 14;
        margin-right: 5;
    `,

    PricesContainer: styled.View`
        display: flex;
        flex-direction: row;
        align-items: center;
        width: 100%;
        height: 100%;
        justify-content: space-between;
    `,
    IconsContainer: styled.View`
        flex: 1;
        margin-left: 5%;
        margin-right: 5%;
    `,

    Icons: styled.ImageBackground`
        height: 10;
        width: 10;
        margin-right: 2;
    `,
}