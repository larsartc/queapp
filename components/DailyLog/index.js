import React from "react";
import Styled from "./styled";
import { View, Text } from "native-base";

const DailyLog = props => {
  const line = props.line != undefined ? props.line : true;
  //console.log(line);
  return (
    <Styled.CardContainer background={props.background}>
      <Styled.FlexTime>
        {line == true && <Styled.Line />}
        <Styled.Hour>{props.time}</Styled.Hour>
      </Styled.FlexTime>
      <Styled.FlexOne>
        <Styled.ImageBarber source={props.barber_image} />
      </Styled.FlexOne>
      <Styled.FlexTwo>
        <View>
          <Styled.TextInformation>
            <Styled.ImageIcon
              source={require("que/assets/images/scisor_account_barber_profile_icn.png")}
            />
            <Text>{props.barber_name}</Text>
          </Styled.TextInformation>
        </View>
        {props.hair_style != "" && (
          <View>
            <Styled.TextInformation>
              <Styled.ImageIcon
                source={require("que/assets/images/hair_cuts_icn.png")}
              />
              <Text>{props.hair_style.name}</Text>
            </Styled.TextInformation>
          </View>
        )}
        {props.beard_style != "" && (
          <View>
            <Styled.TextInformation>
              <Styled.ImageIcon
                source={require("que/assets/images/beard_cuts_icn.png")}
              />
              <Text>{props.beard_style.name}</Text>
            </Styled.TextInformation>
          </View>
        )}
        <View>
          <Styled.TextInformation>
            <Styled.ImageIcon
              source={require("que/assets/images/shop_timeline_icn.png")}
            />
            <Text>{props.shop_name}</Text>
            {/* <Text>{props.shop_name.substring(0, 18)}...</Text> */}
          </Styled.TextInformation>
        </View>
      </Styled.FlexTwo>
      <Styled.FlexImageRight>
        <Styled.ImageUser source={props.user_image} />
      </Styled.FlexImageRight>
    </Styled.CardContainer>
  );
};

export default DailyLog;
