import { StyleSheet } from "react-native"
import { colors } from 'que/components/base';

export default styles = StyleSheet.create({
    container: {
        width: '100%',
        maxHeight: 200,
        position: "relative",
    },
    listOne: {
        width: "100%",
        height: 30,
        paddingVertical: 5,
        paddingHorizontal: 20,
        borderWidth: 0.5,
        borderColor: colors.lightGray,
        borderRadius: 30,
        flexDirection: "row",
        alignItems: "center",
        position:"absolute"
    },

    listManyContainer: {
        width: "100%",
        paddingVertical: 10,
        paddingHorizontal: 20,
        borderWidth: 0.5,
        borderColor: colors.lightGray,
        borderRadius: 30,
        position:"absolute",
        maxHeight: 200,
        zIndex: 30,
        backgroundColor: colors.primary,
        elevation: 15,
    },
    listItem: {
        paddingVertical:8
    },
    listLabel:{
        flex: 1,
        color: colors.white
    }, 
    arrowContainer: {
        flexDirection: "row",
        alignItems: "center",
        padding:8,
    },
    containerItems: {
        width: "100%",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between"
    },
    text: {
        fontSize: 13,
        color: colors.white,
        width: '90%'
    },
    arrowIcon: {
        height: 25,
        width: 25,
    },
})