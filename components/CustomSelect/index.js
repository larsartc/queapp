import React, { Component, Fragment, PureComponent } from 'react';
import { View, Image, Text, TouchableOpacity, ScrollView, Platform } from "react-native"
import styles from "./styles.js"
import { colors } from 'que/components/base'

class CustomSelect extends PureComponent {

    state = {
        open: false,
    }

    selectValue = (v) => {
        this.setState({ open: false })
        this.props.selectValue(v)
    }

    render() {
        return (
            <View style={styles.container}>
                {!this.state.open ?
                    <TouchableOpacity style={styles.listOne} onPress={() => { !this.props.disabled && this.setState({ open: !this.state.open }) }}>
                        <Text style={styles.listLabel} numberOfLines={1}>{this.props.value != null ? this.props.value : this.props.options[0].value}</Text>
                        <View style={styles.arrowContainer} >
                            <Image source={require('que/assets/images/arrow_custom_select.png')} style={styles.arrowIcon} />
                        </View>
                    </TouchableOpacity>
                    :
                    this.props.options && this.props.options.length > 0 &&
                    <>
                        <View style={[styles.listManyContainer, { borderRadius: 15, elevation: 30 }]}>
                            {this.props.value != null &&
                                <View style={[styles.listOne, { borderWidth: 0, marginTop: 10 }]}>
                                    <Text style={styles.listLabel} numberOfLines={1}>{this.props.value}</Text>
                                    <TouchableOpacity style={styles.arrowContainer} onPress={() => this.setState({ open: !this.state.open })}>
                                        <Image source={require('que/assets/images/arrow_custom_select.png')} style={[styles.arrowIcon, {
                                            transform: [
                                                { rotate: "180deg" },
                                            ]
                                        }]} />
                                    </TouchableOpacity>
                                </View>
                            }
                            <ScrollView style={{ elevation: 30, marginTop: this.props.value ? 30 : 5 }}>
                                {this.props.options.map((v, i) =>
                                    <TouchableOpacity style={[styles.listItem]} onPress={() => this.selectValue(v)}>
                                        <Text style={styles.listLabel} numberOfLines={1}>{v.value}</Text>
                                    </TouchableOpacity>
                                )}
                            </ScrollView>
                        </View>
                    </>
                }
            </View>
        )
    }
}
export default CustomSelect;