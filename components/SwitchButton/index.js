import React from 'react'
import styled from 'styled-components/native'

export default (props) => {
  return (
    <View>
      <Button active={props.active==props.start}><Text>{props.start}</Text></Button>
      <View active={props.active==props.end}><Text>{props.end}</Text></View>
    </View>
  )
}

const Button = styled.View`
  backgroundColor: 'red',
  
`