import React from 'react';
import { Text, View } from 'react-native'
import { DivBgChat, DivInner, ImageUser, TextInner, Pin } from "./styled";

const LineChat = ({ right = false, image, text }) => {
    return (
        <DivBgChat lineDirection={right}>
            <DivInner lineDirection={right}>
                <Pin lineDirection={right} />
                <TextInner><Text>{text}</Text></TextInner>
            </DivInner>
        </DivBgChat>
    )
}

export default LineChat;