import styled from 'styled-components';
import { colors } from 'que/components/base';

export const DivBgChat = styled.View`
        flex-direction: row;
        align-items: center;
        margin-bottom: 10;
        justify-content: ${props => props.lineDirection ? 'flex-end' : 'flex-start'};
        position: relative;
    `

export const DivInner = styled.View`
        background-color: ${colors.lightGray};
        max-width: 100%;
        padding-top: 2;
        padding-bottom: 2;
        flex-direction: ${props => props.lineDirection ? 'row-reverse' : 'row'};
        align-items: center;
        border-top-right-radius: ${props => props.lineDirection ? 0 : 50};
        border-bottom-right-radius: ${props => props.lineDirection ? 0 : 50};
        border-top-left-radius: ${props => props.lineDirection ? 50 : 0};
        border-bottom-left-radius: ${props => props.lineDirection ? 50 : 0};
        
    `

export const TextInner = styled.View`
        background-color: ${colors.white};
        padding-left: 10;
        padding-right: 10;
        padding-top: 7;
        padding-bottom: 7;
        border-radius: 50;
        margin-left: 2;
        margin-right: 2;
    `

export const ImageUser = styled.Image`
        width: 30;
        height: 30;
        margin-right: 7;
        border-radius: 50;
        border-color: transparent;
        border-width: 2;
    `
export const Pin = styled.View`
    width: 6;
    height: 6;
    border-radius: 3;
    background-color: #999;
    margin-left: ${props => props.lineDirection ? 8 : 36};
    margin-right: ${props => props.lineDirection ? 36 : 8};
`

export const ImageUserRight = styled.Image`
        width: 30;
        height: 30;
        margin-left: 7;
        border-radius: 50;
        border-color: ${colors.primary};
        border-width: 2;
    `