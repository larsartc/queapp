import {useState, useEffect} from "react"
import {Animated, Easing} from 'react-native'

export default useAnimation = ({doAnimation, duration, delay=0, easing}) => {
  const [animation, setAnimation] = useState(new Animated.Value(0))

  useEffect(()=> {
    Animated.timing(animation, {
      toValue: doAnimation ? 1 : 0,
      duration, 
      delay, 
      easing,
    }).start()
  }, [doAnimation])

  return animation
}

// EXAMPLE TO USE THIS HOOK
/*
const AnimatedComponent = ({doAnimation}) => {
  const animation = useAnimation({doAnimation, duration: 200})
  return (
    <Animated.View style={{
      width: animation.interpolate({
        inputRange: [0,1],
        outputRange: [50,60]
      }), 
      height: animation.interpolate({
        inputRange: [0,1],
        outputRange: [50,60]
      }),
      backgroundColor: 'red'
    }} />
  )
}*/
